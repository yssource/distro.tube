#+TITLE: Fish Is The Better Shell For More Modern Times
#+DESCRIPTION: DT Articles - Fish Is The Better Shell For More Modern Times
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* Fish Is The Better Shell For More Modern Times
/By Derek Taylor at April 9, 2020/

* Why Use Fish
Fish is a user-friendly command line shell for Linux and MacOS. It has a number of smart features and improvements when compared to other popular shells, namely Bash and Zsh. When you first start typing in Fish, you will immediately notice that it has an autosuggestion feature where, as you type, fish will suggest commands to you based on your shell history.

Also, you will notice the commands that you type will change color. Red means that the command you are typing is not valid. Blue means that it probably will work. So when you type just “c” it is colored red, but as soon as you add “d” for “cd”, the color changes to blue. Oh, by the way, fish supports 24 bit true color.

Want to see a neat trick using colors? There is a function in fish called set_color which is used to control the color of the text in your terminal. For example:

#+begin_example
echo (set_color green) "This text is green."
echo (set_color magenta) "This text is magenta."
#+end_example

Tab completions in Fish are really quite amazing. You don't need to add any plugins or hack Fish in any weird way to get tab completion to work either, unlike with Bash or Zsh. As you type a command, hit TAB and you will be presented a list of possible commands. If you are unsure of a flag for a command, you can hit TAB while typing flags, and you will get a list of possible flags. So if you type “ls -” and then hit TAB, you will get the flags for “ls”. Fish gets this data from the manpages installed on your machine.

* Configuration
Fish is configurable but I think most users will find that Fish works just fine out-of-the-box. But for those that love to hack on things, Fish can be configured easily without ever needing to open up an editor and messing around with text files. There is a builtin web configuration tool that you can launch with “fish_config”. This will open your browser to a page where you select the features and themes that you want. When you're done, save your configuration. Voila!

Another easy way to configure Fish is with the Oh My Fish framework. Oh My Fish provides an infrastructure to allow you to install packages that extend or modify the look of Fish. It's easy to install with just one command in the terminal, and it's easy to use. If you want to install themes with Oh My Fish, you would type “omf theme” to get a listing of all the themes installed and all the themes that are available to be installed. A simple “omf install name-of-theme” will install and activate a theme.

* Scripting
Fish is fully scriptable. Its syntax is simple, clean and consistent but it's not necessarily POSIX compliant. This is both a positive and a negative. Let's start with the postive. Fish has a sane syntax where things like “if” statements and “case” statements no longer have to be closed with “fi” and “esac”. Instead, all such statements now are closed with “end”. Clearly, this makes a lot more sense. But the negative–if you type out those “if” or “case” statements the standard way, Fish will not be able to handle them. This could be frustrating for some users.

While Fish has many of the same commands that your other shells have, it does include some rather unique commands that some users might find helpful. One such command is the “math” command. Doing math calculations can be tricky in some shells but not in Fish. Want to divide 1 by 2? Run:

#+begin_example
math 1/2
#+end_example

* Conclusion
Is Fish the right shell for you? Ultimately, it will depend on the use case and the user. I certainly recommend trying Fish if you haven't already. You will be delighted by all of its advanced features that just work, without the need to spend hours on configuration.

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
