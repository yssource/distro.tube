#+TITLE: Why do most Linux users have an elitist attitude?
#+DESCRIPTION: DT Articles - Why do most Linux users have an elitist attitude?
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* Why do most Linux users have an elitist attitude?
/By Derek Taylor at March 7, 2020/

It is often said that many, if not most, Linux users have an elitist attitude. We love to talk about how much better our OS is compared to the alternatives. Sometimes, we love to talk about how much better our distro is compared to other Linux distros. Linux users often talk about how Linux changed their lives, changed the way they thought about computers and the world in general. And to those outside of the Linux community, we come across as a bunch or elitist, arrogant A-holes. But do we really have an elitist attitude? Yes, we do.

But there is a reason we have an elitist attitude, and a good reason at that…because we are elite. It's not an attitude. We really are elite! Think about it for a minute. Why are we elite? Here are some reasons:

* We are the 2%
Around 2% of the world uses a desktop Linux distribution as their operating system. What could be more elite than that? And it goes deeper than just our OS. The reason most of us end up on Linux, is because we are non-conformists or at the very least not afraid to stray from herd.

* We are far more computer literate.
On average, Linux users are much more computer savvy than the typical computer user. Linux users are aware of things that most Windows and Mac users have never considered–like what an OS is, the kernel, DE, WM, compositor, init system, file systems, user permissions, package management, etc.

* Linux is used in elite fields.
Many Linux users are using Linux on the server, in enterprise, on mainframes, as development platforms, in scientific-endeavors, in really high-end data-intensive environments. Linux dominates in these areas compared to Windows and Mac which are really strictly for home consumer use.

* Linux users are passionate about their OS.
I've never met a Windows user that was passionate about using Windows. Mac users, well some of them are fanboys of MacOS, but not in the way Linux users are. Linux is more than our OS, it is our way of life. Linux is not just software; it also an idea, and it also the community.

And it's not just that we love to go against the grain and run software most people do not. We actually think that Linux is better than Windows and Mac. In fact, we know it is…at least for us. Because…

* Linux users are control freaks.
Answer these questions those of you that have used both Windows and Linux:

+ Which of them can be updated while running?
+ Which of them allows me to set the time for updates to happen?
+ Which of them spends the shortest time booting?
+ Which of them installs quicker?
+ Which of them allow me to switch out the desktop environment, window manager, etc?
+ Which of them respects my privacy?
+ Which of them can be reinstalled in 15 mins or less with all my user data still intact?
 
Linux is definitely an OS that attracts elite computer users in a way than neither Windows nor MacOS does. But, if you are Windows or a Mac user, don't let our elitism be offensive to you. After listing just a few reasons of why we have this attitude, how could we possibly avoid being elitist?

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
