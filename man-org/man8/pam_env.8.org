#+TITLE: Manpages - pam_env.8
#+DESCRIPTION: Linux manpage for pam_env.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_env - PAM module to set/unset environment variables

* SYNOPSIS
*pam_env.so* [debug] [conffile=/conf-file/] [envfile=/env-file/]
[readenv=/0|1/] [user_envfile=/env-file/] [user_readenv=/0|1/]

* DESCRIPTION
The pam_env PAM module allows the (un)setting of environment variables.
Supported is the use of previously set environment variables as well as
/PAM_ITEM/s such as /PAM_RHOST/.

By default rules for (un)setting of variables are taken from the config
file /etc/security/pam_env.conf. An alternate file can be specified with
the /conffile/ option.

Second a file (/etc/environment by default) with simple /KEY=VAL/ pairs
on separate lines will be read. With the /envfile/ option an alternate
file can be specified. And with the /readenv/ option this can be
completely disabled.

Third it will read a user configuration file ($HOME/.pam_environment by
default). The default file can be changed with the /user_envfile/ option
and it can be turned on and off with the /user_readenv/ option.

Since setting of PAM environment variables can have side effects to
other modules, this module should be the last one on the stack.

* OPTIONS
*conffile=*//path/to/pam_env.conf/

#+begin_quote
  Indicate an alternative pam_env.conf style configuration file to
  override the default. This can be useful when different services need
  different environments.
#+end_quote

*debug*

#+begin_quote
  A lot of debug information is printed with *syslog*(3).
#+end_quote

*envfile=*//path/to/environment/

#+begin_quote
  Indicate an alternative environment file to override the default. The
  syntax are simple /KEY=VAL/ pairs on separate lines. The /export/
  instruction can be specified for bash compatibility, but will be
  ignored. This can be useful when different services need different
  environments.
#+end_quote

*readenv=*/0|1/

#+begin_quote
  Turns on or off the reading of the file specified by envfile (0 is
  off, 1 is on). By default this option is on.
#+end_quote

*user_envfile=*/filename/

#+begin_quote
  Indicate an alternative .pam_environment file to override the
  default.The syntax is the same as for //etc/security/pam_env.conf/.
  The filename is relative to the user home directory. This can be
  useful when different services need different environments.
#+end_quote

*user_readenv=*/0|1/

#+begin_quote
  Turns on or off the reading of the user specific environment file. 0
  is off, 1 is on. By default this option is off as user supplied
  environment variables in the PAM environment could affect behavior of
  subsequent modules in the stack without the consent of the system
  administrator.

  Due to problematic security this functionality is deprecated since the
  1.5.0 version and will be removed completely at some point in the
  future.
#+end_quote

* MODULE TYPES PROVIDED
The *auth* and *session* module types are provided.

* RETURN VALUES
PAM_ABORT

#+begin_quote
  Not all relevant data or options could be gotten.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_IGNORE

#+begin_quote
  No pam_env.conf and environment file was found.
#+end_quote

PAM_SUCCESS

#+begin_quote
  Environment variables were set.
#+end_quote

* FILES
/etc/security/pam_env.conf

#+begin_quote
  Default configuration file
#+end_quote

/etc/environment

#+begin_quote
  Default environment file
#+end_quote

$HOME/.pam_environment

#+begin_quote
  User specific environment file
#+end_quote

* SEE ALSO
*pam_env.conf*(5), *pam.d*(5), *pam*(8), *environ*(7).

* AUTHOR
pam_env was written by Dave Kinchlea <kinch@kinch.ark.com>.
