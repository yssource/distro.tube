#+TITLE: Manpages - qemu-pr-helper.8
#+DESCRIPTION: Linux manpage for qemu-pr-helper.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
qemu-pr-helper - QEMU persistent reservation helper

* SYNOPSIS
*qemu-pr-helper* [/OPTION/]

* DESCRIPTION
Implements the persistent reservation helper for QEMU.

SCSI persistent reservations allow restricting access to block devices
to specific initiators in a shared storage setup. When implementing
clustering of virtual machines, it is a common requirement for virtual
machines to send persistent reservation SCSI commands. However, the
operating system restricts sending these commands to unprivileged
programs because incorrect usage can disrupt regular operation of the
storage fabric. QEMU's SCSI passthrough devices *scsi-block* and
*scsi-generic* support passing guest persistent reservation requests to
a privileged external helper program. *qemu-pr-helper* is that external
helper; it creates a socket which QEMU can connect to to communicate
with it.

If you want to run VMs in a setup like this, this helper should be
started as a system service, and you should read the QEMU manual section
on "persistent reservation managers" to find out how to configure QEMU
to connect to the socket created by *qemu-pr-helper*.

After connecting to the socket, *qemu-pr-helper* can optionally drop
root privileges, except for those capabilities that are needed for its
operation.

*qemu-pr-helper* can also use the systemd socket activation protocol. In
this case, the systemd socket unit should specify a Unix stream socket,
like this:

#+begin_quote

  #+begin_quote
    #+begin_example
      [Socket]
      ListenStream=/var/run/qemu-pr-helper.sock
    #+end_example
  #+end_quote
#+end_quote

* OPTIONS

#+begin_quote
  - *-d, --daemon* :: run in the background (and create a PID file)
#+end_quote

#+begin_quote
  - *-q, --quiet* :: decrease verbosity
#+end_quote

#+begin_quote
  - *-v, --verbose* :: increase verbosity
#+end_quote

#+begin_quote
  - *-f, --pidfile=PATH* :: PID file when running as a daemon. By
    default the PID file is created in the system runtime state
    directory, for example */var/run/qemu-pr-helper.pid*.
#+end_quote

#+begin_quote
  - *-k, --socket=PATH* :: path to the socket. By default the socket is
    created in the system runtime state directory, for example
    */var/run/qemu-pr-helper.sock*.
#+end_quote

#+begin_quote
  - *-T, --trace
    [[enable=]PATTERN][,events=FILE][,file=FILE]* :: Specify tracing
    options.

  *[enable=]PATTERN*

  #+begin_quote

    #+begin_quote
      Immediately enable events matching /PATTERN/ (either event name or
      a globbing pattern). This option is only available if QEMU has
      been compiled with the *simple*, *log* or *ftrace* tracing
      backend. To specify multiple events or patterns, specify the
      *-trace* option multiple times.

      Use *-trace help* to print a list of names of trace points.
    #+end_quote
  #+end_quote

  *events=FILE*

  #+begin_quote

    #+begin_quote
      Immediately enable events listed in /FILE/. The file must contain
      one event name (as listed in the *trace-events-all* file) per
      line; globbing patterns are accepted too. This option is only
      available if QEMU has been compiled with the *simple*, *log* or
      *ftrace* tracing backend.
    #+end_quote
  #+end_quote

  *file=FILE*

  #+begin_quote

    #+begin_quote
      Log output traces to /FILE/. This option is only available if QEMU
      has been compiled with the *simple* tracing backend.
    #+end_quote
  #+end_quote
#+end_quote

#+begin_quote
  - *-u, --user=USER* :: user to drop privileges to
#+end_quote

#+begin_quote
  - *-g, --group=GROUP* :: group to drop privileges to
#+end_quote

#+begin_quote
  - *-h, --help* :: Display a help message and exit.
#+end_quote

#+begin_quote
  - *-V, --version* :: Display version information and exit.
#+end_quote

* COPYRIGHT
2021, The QEMU Project Developers
