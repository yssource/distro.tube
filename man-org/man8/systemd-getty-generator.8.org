#+TITLE: Manpages - systemd-getty-generator.8
#+DESCRIPTION: Linux manpage for systemd-getty-generator.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-getty-generator - Generator for enabling getty instances on the
console

* SYNOPSIS
/usr/lib/systemd/system-generators/systemd-getty-generator

* DESCRIPTION
systemd-getty-generator is a generator that automatically instantiates
serial-getty@.service on the kernel console(s), if they can function as
ttys and are not provided by the virtual console subsystem. It will also
instantiate serial-getty@.service instances for virtualizer consoles, if
execution in a virtualized environment is detected. If execution in a
container environment is detected, it will instead enable
console-getty.service for /dev/console, and container-getty@.service
instances for additional container pseudo TTYs as requested by the
container manager (see *Container Interface*[1]). This should ensure
that the user is shown a login prompt at the right place, regardless of
which environment the system is started in. For example, it is
sufficient to redirect the kernel console with a kernel command line
argument such as /console=/ to get both kernel messages and a getty
prompt on a serial TTY. See *kernel-parameters.txt*[2] for more
information on the /console=/ kernel parameter.

systemd-getty-generator implements *systemd.generator*(7).

Further information about configuration of gettys can be found in
*systemd for Administrators, Part XVI: Gettys on Serial Consoles (and
Elsewhere)*[3].

* SEE ALSO
*systemd*(1), *agetty*(8)

* NOTES
-  1. :: Container Interface

  https://systemd.io/CONTAINER_INTERFACE

-  2. :: kernel-parameters.txt

  https://www.kernel.org/doc/Documentation/admin-guide/kernel-parameters.txt

-  3. :: systemd for Administrators, Part XVI: Gettys on Serial Consoles
  (and Elsewhere)

  http://0pointer.de/blog/projects/serial-console.html
