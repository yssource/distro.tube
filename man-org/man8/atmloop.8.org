#+TITLE: Manpages - atmloop.8
#+DESCRIPTION: Linux manpage for atmloop.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
atmloop - get/set loopback mode of ATM interfaces

* SYNOPSIS
*atmloop* *-s* [*-l /level/*] [*-r /level/*] [*/itf/*]\\
*atmloop* [*/itf/*]\\
*atmloop* *-q* [*/itf/*]\\
*atmloop* *-V*

* DESCRIPTION
*atmloop* sets or shows the current loopback settings of an ATM
interface, and allows to query the loopback modes supported by the
interface.

Some ATM cards can be configured to echo data back to the driver (local
loopback) or to the network (remote loopback). The echoing may be done
at several different levels:

- aal :: AAL PDUs

- atm :: ATM cells

- digital :: bits sent to or received from the line interface

- analog :: the analog signal

The levels supported by an interface can be queried with the *-q*
option. If invoked without options, *atmloop* displays the current
loopback settings.

* OPTIONS
- -s :: set loopback mode. Followed by either *-l* or *-r*, both options
  to set local and remote loopback at the same time (if supported by the
  driver), or none of them to turn off any loopback.

- -l level :: enable local loopback.

- -r level :: enable remote loopback.

- -q level :: query loopback levels supported by the interface.

- -V :: print version number of *atmloop on standard output and exit.*

* AUTHOR
Werner Almesberger, EPFL ICA <Werner.Almesberger@epfl.ch>

* SEE ALSO
atmdiag(8), sonetdiag(8)
