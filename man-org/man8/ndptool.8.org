#+TITLE: Manpages - ndptool.8
#+DESCRIPTION: Linux manpage for ndptool.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ndptool --- Neighbor Discovery Protocol tool

* SYNOPSIS
*ndptool* *-h*

- *ndptool [OPTIONS] COMMAND* :: 

* DESCRIPTION
ndptool is a tool which provides wrapper over Neighbor Discovery
Protocol messages.

* OPTIONS
- *-h, --help* :: Print help text to console and exit.

- *-v, --verbose* :: Increase output verbosity.

- *-t type, --msg-type type* :: Specified message type. Following are
  supported:

*rs *- Router Solicitation.

*ra *- Router Advertisement.

*ns *- Neighbor Solicitation.

*na *- Neighbor Advertisement.

- *-i ifname, --ifname ifname* :: Specified interface name.

- *-D dest, --dest dest* :: Specified dest address in IPv6 header for
  NS/NA message.

- *-T target, --target target* :: Specified target address in ICMPv6
  header for NS/NA message.

- *-U, --unsolicited* :: Send Unsolicited NA.

* COMMAND
- *monitor* :: Monitor incoming NDP messages and print them out.

- *send* :: Send NDP message of specified type.

* AUTHOR
Jiri Pirko is the original author and current maintainer of libndp.
