#+TITLE: Manpages - pwhistory_helper.8
#+DESCRIPTION: Linux manpage for pwhistory_helper.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pwhistory_helper - Helper binary that transfers password hashes from
passwd or shadow to opasswd

* SYNOPSIS
*pwhistory_helper* [...]

* DESCRIPTION
/pwhistory_helper/ is a helper program for the /pam_pwhistory/ module
that transfers password hashes from passwd or shadow file to the opasswd
file and checks a password supplied by user against the existing hashes
in the opasswd file.

The purpose of the helper is to enable tighter confinement of login and
password changing services. The helper is thus called only when SELinux
is enabled on the system.

The interface of the helper - command line options, and input/output
data format are internal to the /pam_pwhistory/ module and it should not
be called directly from applications.

* SEE ALSO
*pam_pwhistory*(8)

* AUTHOR
Written by Tomas Mraz based on the code originally in /pam_pwhistory and
pam_unix/ modules.
