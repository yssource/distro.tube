#+TITLE: Manpages - downgrade.8
#+DESCRIPTION: Linux manpage for downgrade.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* USAGE
*downgrade* [option...] <pkg> [pkg...] [*--* pacman_option...]

* DESCRIPTION
Downgrade Arch Linux packages.

* OUTPUT
Just calling *downgrade* on a package will lead to the following output:

/Example:/

#+begin_example
  Available packages (community):

  -  1)  terraform    0.11.11  2  remote
  -  2)  terraform    0.11.12  1  /var/cache/pacman/pkg
  +  3)  terraform    0.11.13  1  remote
  +  4)  terraform    0.11.13  1  /var/cache/pacman/pkg
  -  5)  terraform    0.12.0   1  remote
  -  6)  terraform    0.12.0   1  /var/cache/pacman/pkg
     7)  terraform    0.12.1   1  remote

  select a package by number:
#+end_example

The columns have the following meaning:

/indicator/ Possible values: {-|+}

- indicates that the version was previously installed.

+ indicates the currently installed version.

/enumeration/ An enumeration of the entries for selection.

/package-name/ The name of the package.

/package-epoch/ The epoch of the package in cache or ALA.

/package-version/ The version of the package in cache or ALA.

/package-release/ The release of the package in cache or ALA.

/location/ Possible values: {remote|/path/to/cache/dir}

If you have already downloaded this version, it will show the cache
directory where the package is located. /remote/ indicates that the
version is available on the ALA.

* OPTIONS
*--pacman* /<command>/

#+begin_quote
  Pacman command, default is /pacman/.
#+end_quote

*--pacman-conf* /<path>/

#+begin_quote
  Pacman configuration file, default is //etc/pacman.conf/.
#+end_quote

*--pacman-cache* /<path>/

#+begin_quote
  Pacman cache directory, default value(s) taken from pacman
  configuration file, or otherwise defaults to //var/cache/pacman/pkg/.
  This option can be specified multiple times to indicate multiple cache
  directories.
#+end_quote

*--pacman-log* /<path>/

#+begin_quote
  Pacman log file, default value is extracted from pacman configuration
  file, or otherwise defaults to //var/log/pacman.log/.
#+end_quote

*--maxdepth* /<integer>/

#+begin_quote
  Maximum depth to search for cached packages, defaults to /1/.
#+end_quote

*--ala-url* /<url>/

#+begin_quote
  Location of an ALA server, default is /https://archive.archlinux.org/.
#+end_quote

*--ala-only*

#+begin_quote
  Search ALA only.
#+end_quote

*--cached-only*

#+begin_quote
  Search local cache only.
#+end_quote

*--version*

#+begin_quote
  Show downgrade version.
#+end_quote

*-h, --help*

#+begin_quote
  Show help script.
#+end_quote

** CONFIGURATION FILE
Command-line options can be set persistently in
*/etc/xdg/downgrade/downgrade.conf*. Note that these options are parsed
first by *downgrade*, followed by any other command-line options
provided by the user.

** PACMAN OPTIONS
As per the usage syntax, any options supplied after the *--* character
sequence will be treated as pacman options.

** DEFAULT BEHAVIORS
By default, *downgrade* will search both local caches and the ALA.

If only one package with its corresponding location matches, the package
will be installed without further prompt from the user.

* VERSION FILTERING
*downgrade* allows the use of the following version-filtering operators:
*=*, *==*, *=~*, *<=*, *>=*, *<* and *>*. Note that *=~* represents a
regex match operator and *=*/*==* are aliases.

* EXIT CODES
*downgrade* will stop further processing and exit non-zero if it
encounters any of the following scenarios for any of its arguments:

- No argument value(s) supplied where necessary

- No package(s) found

- Package(s) found, but an invalid selection was made

- =pacman -U= returned non-zero

- Unexpected error when handling =IgnorePkg= additions

* SEE ALSO
*pacman*(8), *vercmp(8)*, *sudo*(8), *pacman.conf*(5), *find*(1),
*su*(1).

* BUGS
Open a GitHub issue on /https://github.com/pbrisbin/downgrade/.

* AUTHORS

- Patrick Brisbin <pbrisbin@gmail.com>

- Atreya Shankar <shankar.atreya@gmail.com>
