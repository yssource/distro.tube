#+TITLE: Manpages - fsck.cramfs.8
#+DESCRIPTION: Linux manpage for fsck.cramfs.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fsck.cramfs - fsck compressed ROM file system

* SYNOPSIS
*fsck.cramfs* [options] /file/

* DESCRIPTION
*fsck.cramfs* is used to check the cramfs file system.

* OPTIONS
*-v*, *--verbose*

#+begin_quote
  Enable verbose messaging.
#+end_quote

*-b*, *--blocksize* /blocksize/

#+begin_quote
  Use this blocksize, defaults to page size. Must be equal to what was
  set at creation time. Only used for *--extract*.
#+end_quote

*--extract*[=/directory/]

#+begin_quote
  Test to uncompress the whole file system. Optionally extract contents
  of the /file/ to /directory/.
#+end_quote

*-a*

#+begin_quote
  This option is silently ignored.
#+end_quote

*-y*

#+begin_quote
  This option is silently ignored.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* EXIT STATUS
*0*

#+begin_quote
  success
#+end_quote

*4*

#+begin_quote
  file system was left uncorrected
#+end_quote

*8*

#+begin_quote
  operation error, such as unable to allocate memory
#+end_quote

*16*

#+begin_quote
  usage information was printed
#+end_quote

* SEE ALSO
*mount*(8), *mkfs.cramfs*(8)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *fsck.cramfs* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
