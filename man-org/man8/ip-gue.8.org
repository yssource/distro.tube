#+TITLE: Manpages - ip-gue.8
#+DESCRIPTION: Linux manpage for ip-gue.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ip-gue.8 is found in manpage for: [[../man8/ip-fou.8][man8/ip-fou.8]]