#+TITLE: Manpages - cupsctl.8
#+DESCRIPTION: Linux manpage for cupsctl.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cupsctl - configure cupsd.conf options

* SYNOPSIS
*cupsctl* [ *-E* ] [ *-U* /username/ ] [ *-h* /server/[*:*/port/] ] [
*--*[*no-*]*debug-logging* ] [ *--*[*no-*]*remote-admin* ] [
*--*[*no-*]*remote-any* ] [ *--*[*no-*]*share-printers* ] [
*--*[*no-*]*user-cancel-any* ] [ /name=value/ ]

* DESCRIPTION
*cupsctl* updates or queries the /cupsd.conf/ file for a server. When no
changes are requested, the current configuration values are written to
the standard output in the format "name=value", one per line.

* OPTIONS
The following options are recognized:

- *-E* :: Enables encryption on the connection to the scheduler.

- *-U */username/ :: Specifies an alternate username to use when
  authenticating with the scheduler.

- *-h */server/[*:*/port/] :: Specifies the server address.

- *--*[*no-*]*debug-logging* :: Enables (disables) debug logging to the
  /error_log/ file.

- *--*[*no-*]*remote-admin* :: Enables (disables) remote administration.

- *--*[*no-*]*remote-any* :: Enables (disables) printing from any
  address, e.g., the Internet.

- *--*[*no-*]*share-printers* :: Enables (disables) sharing of local
  printers with other computers.

- *--*[*no-*]*user-cancel-any* :: Allows (prevents) users to cancel jobs
  owned by others.

* EXAMPLES
Display the current settings:

#+begin_example

      cupsctl
#+end_example

Enable debug logging:

#+begin_example

      cupsctl --debug-logging
#+end_example

Get the current debug logging state:

#+begin_example

      cupsctl | grep '^_debug_logging' | awk -F= '{print $2}'
#+end_example

Disable printer sharing:

#+begin_example

      cupsctl --no-share-printers
#+end_example

* KNOWN ISSUES
You cannot set the Listen or Port directives using *cupsctl*.

* SEE ALSO
*cupsd.conf*(5), *cupsd*(8),\\
CUPS Online Help (http://localhost:631/help)

* COPYRIGHT
Copyright © 2021 by OpenPrinting.
