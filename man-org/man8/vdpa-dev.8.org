#+TITLE: Manpages - vdpa-dev.8
#+DESCRIPTION: Linux manpage for vdpa-dev.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vdpa-dev - vdpa device configuration

* SYNOPSIS
*vdpa* *dev* [ /OPTIONS/ ] { /COMMAND/|/ / *help* }

/OPTIONS/ := { *-V*[/ersion/] }

*vdpa dev show* [ /DEV/ ]

*vdpa dev help*

*vdpa dev add* *name* /NAME/ *mgmtdev* /MGMTDEV/

*vdpa dev del* /DEV/

* DESCRIPTION
** vdpa dev show - display vdpa device attributes
/DEV/ - specifies the vdpa device to show. If this argument is omitted
all devices are listed.

Format is:

VDPA_DEVICE_NAME

** vdpa dev add - add a new vdpa device.
- *name*/ NAME/ :: Name of the new vdpa device to add.

- *mgmtdev*/ MGMTDEV/ :: Name of the management device to use for device
  addition.

** vdpa dev del - Delete the vdpa device.
/DEV/ - specifies the vdpa device to delete.

* EXAMPLES
vdpa dev show

#+begin_quote
  Shows the all vdpa devices on the system.
#+end_quote

vdpa dev show foo

#+begin_quote
  Shows the specified vdpa device.
#+end_quote

vdpa dev add name foo mgmtdev vdpa_sim_net

#+begin_quote
  Add the vdpa device named foo on the management device vdpa_sim_net.
#+end_quote

vdpa dev del foo

#+begin_quote
  Delete the vdpa device named foo which was previously created.
#+end_quote

* SEE ALSO
*vdpa*(8), *vdpa-mgmtdev*(8),\\

* AUTHOR
Parav Pandit <parav@nvidia.com>
