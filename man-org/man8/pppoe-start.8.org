#+TITLE: Manpages - pppoe-start.8
#+DESCRIPTION: Linux manpage for pppoe-start.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pppoe-start - Shell script to bring up a PPPoE link

* SYNOPSIS
*pppoe-start [/config_file/]*

*pppoe-start /interface/ user [/config_file/]*

* DESCRIPTION
*pppoe-start* is a shell script which starts the RP-PPPoE user-space
PPPoE client. If you omit /config_file/, the default file
*/etc/ppp/pppoe.conf* is used. If you supply /interface/ and /user/,
then they override the Ethernet interface and user-name settings in the
configuration file.

* AUTHOR
*pppoe-start* was written by Dianne Skoll <dianne@skoll.ca>.

The *pppoe* home page is /https://dianne.skoll.ca/projects/rp-pppoe//.

* SEE ALSO
pppoe(8), pppoe-stop(8), pppoe-connect(8), pppd(8), pppoe.conf(5),
pppoe-setup(8), pppoe-status(8), pppoe-sniff(8), pppoe-relay(8),
pppoe-server(8)
