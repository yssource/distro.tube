#+TITLE: Manpages - grub-set-default.8
#+DESCRIPTION: Linux manpage for grub-set-default.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grub-set-default - set the saved default boot entry for GRUB

* SYNOPSIS
*grub-set-default* [/OPTION/] /MENU_ENTRY/

* DESCRIPTION
Set the default boot menu entry for GRUB. This requires setting
GRUB_DEFAULT=saved in //etc/default/grub/.

- *-h*, *--help* :: print this message and exit

- *-V*, *--version* :: print the version information and exit

- *--boot-directory*=/DIR/ :: expect GRUB images under the directory
  DIR/grub instead of the //boot/grub/ directory

MENU_ENTRY is a number, a menu item title or a menu item identifier.

* REPORTING BUGS
Report bugs to <bug-grub@gnu.org>.

* SEE ALSO
*grub-reboot*(8), *grub-editenv*(1)

The full documentation for *grub-set-default* is maintained as a Texinfo
manual. If the *info* and *grub-set-default* programs are properly
installed at your site, the command

#+begin_quote
  *info grub-set-default*
#+end_quote

should give you access to the complete manual.
