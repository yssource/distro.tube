#+TITLE: Manpages - btrfs-find-root.8
#+DESCRIPTION: Linux manpage for btrfs-find-root.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
btrfs-find-root - filter to find btrfs root

* SYNOPSIS
*btrfs-find-root* [options] /<device>/

* DESCRIPTION
*btrfs-find-root* is used to find the satisfied root, you can filter by
root tree's objectid, generation, level.

* OPTIONS
-a

#+begin_quote
  Search through all metadata extents, even the root has been already
  found.
#+end_quote

-g /<generation>/

#+begin_quote
  Filter root tree by it's original transaction id, tree root's
  generation in default.
#+end_quote

-o /<objectid>/

#+begin_quote
  Filter root tree by it's objectid,tree root's objectid in default.
#+end_quote

-l /<level>/

#+begin_quote
  Filter root tree by B-+ tree's level, level 0 in default.
#+end_quote

* EXIT STATUS
*btrfs-find-root* will return 0 if no error happened. If any problems
happened, 1 will be returned.

* SEE ALSO
*mkfs.btrfs*(8)
