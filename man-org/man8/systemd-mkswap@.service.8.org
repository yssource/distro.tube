#+TITLE: Manpages - systemd-mkswap@.service.8
#+DESCRIPTION: Linux manpage for systemd-mkswap@.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-mkswap@.service.8 is found in manpage for: [[../systemd-makefs@.service.8][systemd-makefs@.service.8]]