#+TITLE: Manpages - ilmid.8
#+DESCRIPTION: Linux manpage for ilmid.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ilmid - Integrated Local Management Interface Daemon

* SYNOPSIS
*ilmid* [ *-b* ] [ *-d* ] [ *-v* ] [ *-l */logfile/ ] [ *-x* ] [ *-q
*/qos/ ] [ *-i */local_ip/ ] [ *-u */uni/ ] <interface>

* PARAMETERS
- *-b* :: Puts the process in the background.

- *-d* :: Turn on debugging output.

- *-v* :: Verbose.

- *-l */logfile/ :: Set the location of the log file.

- *-x* :: No var bindings?

- *-q */qos/ :: Set the qos parameters to use on the ilmi VC.

- *-i */local_ip/ :: the address that should be used by the switch for
  network management.

- *-u */uni/ :: uni version. This can be 3.0, 3.1 or 4.0.

* DESCRIPTION
ilmid implements SNMP on an ATM PVC. It is used to negotiate operating
parameters with a switch.

* SEE ALSO
*atmsigd*(8)
