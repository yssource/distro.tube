#+TITLE: Manpages - partclone.ntfsfixboot.8
#+DESCRIPTION: Linux manpage for partclone.ntfsfixboot.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
partclone.ntfsfixboot - deals with braindeadness with moving NTFS
filesystems.

* SYNOPSIS
*partclone.ntfsfixboot* [*-w*] [*-h* /n/] [*-t* /n/] [*-s* /n/] [*-b*]
[*-f*] [*-p*] {/DEVICE/}

* DESCRIPTION
*partclone.ntfsfixboot* is NOT a part of *Partclone* project but
included to fix ntfs boot issue. Partclone provide utilities to backup
used blocks and design for higher compatibility of the file system by
using existing library, e.g. e2fslibs is used to read the used block of
ext2 partition.

*Partclone.ntfsfixboot* deals with braindeadness with moving NTFS
filesystems. writted by Orgad Shaneh (2009) and Daniel J. Grace (2006).

* OPTIONS
The program follows the usual GNU command line syntax,a summary of
options is included below.

* -w *

#+begin_quote
  Write new start sector to the partition.
#+end_quote

* -h */num/

#+begin_quote
  Specify number of heads per track. If omitted, determined via ioctl.
#+end_quote

* -t */num/

#+begin_quote
  Specify number of sectors per track. If omitted, determined via ioctl.
#+end_quote

* -s */num/

#+begin_quote
  New start sector to write. If omitted, determined via ioctl.
#+end_quote

* -b *

#+begin_quote
  Proceed even if the specified device is not a partition.
#+end_quote

* -f *

#+begin_quote
  Force the operation to occur even if device does not look like a valid
  NTFS partition or values are equal.
#+end_quote

* -p *

#+begin_quote
  Print debug information (values read, values requested etc.
#+end_quote

/device/

#+begin_quote
  where device points to an NTFS partition
#+end_quote

* DIAGNOSTICS
The following diagnostics may be issued on stderr:

*partclone.ntfsfixboot* provides some return codes, that can be used in
scripts:

| /Code/ | /Diagnostic/                                          |
| *0*    | success (values are correct, or changed successfully) |
| *1*    | a change is needed, but -w was not specified          |
| *2*    | an error occured                                      |

* BUGS
Report bugs to thomas@nchc.org.tw or *http://partclone.org*.

Report bugs to upstrem *http://sourceforge.net/projects/ntfsfixboot/*.

* SEE ALSO
*partclone*(8), *partclone.chkimg*(8), *partclone.restore*(8),
*partclone.dd*(8), *partclone.info*(8)

* AUTHOR
*Yu-Chin Tsai* <thomas@nchc.org.tw>

#+begin_quote
#+end_quote

* COPYRIGHT
\\
Copyright © 2007 Yu-Chin Tsai\\

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU General Public License, Version 2 or (at your
option) any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.
