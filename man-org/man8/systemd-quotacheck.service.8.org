#+TITLE: Manpages - systemd-quotacheck.service.8
#+DESCRIPTION: Linux manpage for systemd-quotacheck.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-quotacheck.service, systemd-quotacheck - File system quota
checker logic

* SYNOPSIS
systemd-quotacheck.service

/usr/lib/systemd/systemd-quotacheck

* DESCRIPTION
systemd-quotacheck.service is a service responsible for file system
quota checks. It is run once at boot after all necessary file systems
are mounted. It is pulled in only if at least one file system has quotas
enabled.

* KERNEL COMMAND LINE
systemd-quotacheck understands one kernel command line parameter:

/quotacheck.mode=/

#+begin_quote
  One of "auto", "force", "skip". Controls the mode of operation. The
  default is "auto", and ensures that file system quota checks are done
  when the file system quota checker deems them necessary. "force"
  unconditionally results in full file system quota checks. "skip" skips
  any file system quota checks.
#+end_quote

* SEE ALSO
*systemd*(1), *quotacheck*(8), *systemd-fsck@.service*(8)
