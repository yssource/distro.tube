#+TITLE: Manpages - systemd-poweroff.service.8
#+DESCRIPTION: Linux manpage for systemd-poweroff.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-poweroff.service.8 is found in manpage for: [[../systemd-halt.service.8][systemd-halt.service.8]]