#+TITLE: Manpages - virtvboxd.8
#+DESCRIPTION: Linux manpage for virtvboxd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
virtvboxd - libvirt VirtualBox management daemon

* SYNOPSIS
*virtvboxd* [/OPTION/]...

* DESCRIPTION
The *virtvboxd* program is a server side daemon component of the libvirt
virtualization management system.

It is one of a collection of modular daemons that replace functionality
previously provided by the monolithic *libvirtd* daemon.

This daemon runs on virtualization hosts to provide management for
VirtualBox virtual machines.

The *virtvboxd* daemon only listens for requests on a local Unix domain
socket. Remote off-host access and backwards compatibility with legacy
clients expecting *libvirtd* is provided by the *virtproxy* daemon.

Restarting *virtvboxd* does not interrupt running guests. Guests
continue to operate and changes in their state will generally be picked
up automatically during startup.

* SYSTEM SOCKET ACTIVATION
The *virtvboxd* daemon is capable of starting in two modes.

In the traditional mode, it will create and listen on UNIX sockets
itself.

In socket activation mode, it will rely on systemd to create and listen
on the UNIX sockets and pass them as pre-opened file descriptors. In
this mode most of the socket related config options in
*/etc/libvirt/virtvboxd.conf* will no longer have any effect.

Socket activation mode is generally the default when running on a host
OS that uses systemd. To revert to the traditional mode, all the socket
unit files must be masked:

#+begin_quote

  #+begin_quote
    #+begin_example
      $ systemctl mask virtvboxd.socket virtvboxd-ro.socket \
         virtvboxd-admin.socket
    #+end_example
  #+end_quote
#+end_quote

* OPTIONS
*-h*, *--help*

Display command line help usage then exit.

*-d*, *--daemon*

Run as a daemon & write PID file.

*-f*, *--config *FILE**

Use this configuration file, overriding the default value.

*-p*, *--pid-file *FILE**

Use this name for the PID file, overriding the default value.

*-t*, *--timeout *SECONDS**

Exit after timeout period (in seconds), provided there are neither any
client connections nor any running domains.

*-v*, *--verbose*

Enable output of verbose messages.

*--version*

Display version information then exit.

* SIGNALS
On receipt of *SIGHUP* *virtvboxd* will reload its configuration.

* FILES
** When run as /root/

#+begin_quote

  - */etc/libvirt/virtvboxd.conf*
#+end_quote

The default configuration file used by *virtvboxd*, unless overridden on
the command line using the *-f* | *--config* option.

#+begin_quote

  - */run/libvirt/virtvboxd-sock*

  - */run/libvirt/virtvboxd-sock-ro*

  - */run/libvirt/virtvboxd-admin-sock*
#+end_quote

The sockets *virtvboxd* will use.

The TLS *Server* private key *virtvboxd* will use.

#+begin_quote

  - */run/virtvboxd.pid*
#+end_quote

The PID file to use, unless overridden by the *-p* | *--pid-file*
option.

** When run as /non-root/

#+begin_quote

  - *$XDG_CONFIG_HOME/libvirt/virtvboxd.conf*
#+end_quote

The default configuration file used by *virtvboxd*, unless overridden on
the command line using the *-f``|*--config`` option.

#+begin_quote

  - *$XDG_RUNTIME_DIR/libvirt/virtvboxd-sock*

  - *$XDG_RUNTIME_DIR/libvirt/virtvboxd-admin-sock*
#+end_quote

The sockets *virtvboxd* will use.

#+begin_quote

  - *$XDG_RUNTIME_DIR/libvirt/virtvboxd.pid*
#+end_quote

The PID file to use, unless overridden by the *-p``|*--pid-file``
option.

If *$XDG_CONFIG_HOME* is not set in your environment, *virtvboxd* will
use *$HOME/.config*

If *$XDG_RUNTIME_DIR* is not set in your environment, *virtvboxd* will
use *$HOME/.cache*

* EXAMPLES
To retrieve the version of *virtvboxd*:

#+begin_quote

  #+begin_quote
    #+begin_example
      # virtvboxd --version
      virtvboxd (libvirt) 7.10.0
    #+end_example
  #+end_quote
#+end_quote

To start *virtvboxd*, instructing it to daemonize and create a PID file:

#+begin_quote

  #+begin_quote
    #+begin_example
      # virtvboxd -d
      # ls -la /run/virtvboxd.pid
      -rw-r--r-- 1 root root 6 Jul  9 02:40 /run/virtvboxd.pid
    #+end_example
  #+end_quote
#+end_quote

* BUGS
Please report all bugs you discover. This should be done via either:

#+begin_quote

  1. the mailing list

  /https://libvirt.org/contact.html/

  2. the bug tracker

  /https://libvirt.org/bugs.html/
#+end_quote

Alternatively, you may report bugs to your software distributor /
vendor.

* AUTHORS
Please refer to the AUTHORS file distributed with libvirt.

* COPYRIGHT
Copyright (C) 2006-2020 Red Hat, Inc., and the authors listed in the
libvirt AUTHORS file.

* LICENSE
*virtvboxd* is distributed under the terms of the GNU LGPL v2.1+. This
is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE

* SEE ALSO
virsh(1), libvirtd(8), /https://www.libvirt.org/daemons.html/,
/https://www.libvirt.org/drvvbox.html/
