#+TITLE: Manpages - vdpa-mgmtdev.8
#+DESCRIPTION: Linux manpage for vdpa-mgmtdev.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vdpa-dev - vdpa management device view

* SYNOPSIS
*vdpa* *mgmtdev* { /COMMAND/|/ / *help* }

/OPTIONS/ := { *-V*[/ersion/] }

*vdpa mgmtdev show* [ /MGMTDEV/ ]

*vdpa mgmtdev help*

* DESCRIPTION
** vdpa mgmtdev show - display vdpa management device attributes
/MGMTDEV/ - specifies the vdpa management device to show. If this
argument is omitted all management devices are listed.

* EXAMPLES
vdpa mgmtdev show

#+begin_quote
  Shows all the vdpa management devices on the system.
#+end_quote

vdpa mgmtdev show bar

#+begin_quote
  Shows the specified vdpa management device.
#+end_quote

* SEE ALSO
*vdpa*(8), *vdpa-dev*(8),\\

* AUTHOR
Parav Pandit <parav@nvidia.com>
