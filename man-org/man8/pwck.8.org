#+TITLE: Manpages - pwck.8
#+DESCRIPTION: Linux manpage for pwck.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pwck - verify integrity of password files

* SYNOPSIS
*pwck* [options] [/passwd/ [ /shadow/ ]]

* DESCRIPTION
The *pwck* command verifies the integrity of the users and
authentication information. It checks that all entries in /etc/passwd
and /etc/shadow have the proper format and contain valid data. The user
is prompted to delete entries that are improperly formatted or which
have other uncorrectable errors.

Checks are made to verify that each entry has:

#+begin_quote
  ·

  the correct number of fields
#+end_quote

#+begin_quote
  ·

  a unique and valid user name
#+end_quote

#+begin_quote
  ·

  a valid user and group identifier
#+end_quote

#+begin_quote
  ·

  a valid primary group
#+end_quote

#+begin_quote
  ·

  a valid home directory
#+end_quote

#+begin_quote
  ·

  a valid login shell
#+end_quote

shadow checks are enabled when a second file parameter is specified or
when /etc/shadow exists on the system.

These checks are the following:

#+begin_quote
  ·

  every passwd entry has a matching shadow entry, and every shadow entry
  has a matching passwd entry
#+end_quote

#+begin_quote
  ·

  passwords are specified in the shadowed file
#+end_quote

#+begin_quote
  ·

  shadow entries have the correct number of fields
#+end_quote

#+begin_quote
  ·

  shadow entries are unique in shadow
#+end_quote

#+begin_quote
  ·

  the last password changes are not in the future
#+end_quote

The checks for correct number of fields and unique user name are fatal.
If the entry has the wrong number of fields, the user will be prompted
to delete the entire line. If the user does not answer affirmatively,
all further checks are bypassed. An entry with a duplicated user name is
prompted for deletion, but the remaining checks will still be made. All
other errors are warning and the user is encouraged to run the *usermod*
command to correct the error.

The commands which operate on the /etc/passwd file are not able to alter
corrupted or duplicated entries. *pwck* should be used in those
circumstances to remove the offending entry.

* OPTIONS
The *-r* and *-s* options cannot be combined.

The options which apply to the *pwck* command are:

*--badname*

#+begin_quote
  Allow names that do not conform to standards.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help message and exit.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  Report errors only. The warnings which do not require any action from
  the user wont be displayed.
#+end_quote

*-r*, *--read-only*

#+begin_quote
  Execute the *pwck* command in read-only mode.
#+end_quote

*-R*, *--root* /CHROOT_DIR/

#+begin_quote
  Apply changes in the /CHROOT_DIR/ directory and use the configuration
  files from the /CHROOT_DIR/ directory.
#+end_quote

*-s*, *--sort*

#+begin_quote
  Sort entries in /etc/passwd and /etc/shadow by UID.
#+end_quote

By default, *pwck* operates on the files /etc/passwd and /etc/shadow.
The user may select alternate files with the /passwd/ and /shadow/
parameters.

* CONFIGURATION
The following configuration variables in /etc/login.defs change the
behavior of this tool:

*PASS_MAX_DAYS* (number)

#+begin_quote
  The maximum number of days a password may be used. If the password is
  older than this, a password change will be forced. If not specified,
  -1 will be assumed (which disables the restriction).
#+end_quote

*PASS_MIN_DAYS* (number)

#+begin_quote
  The minimum number of days allowed between password changes. Any
  password changes attempted sooner than this will be rejected. If not
  specified, -1 will be assumed (which disables the restriction).
#+end_quote

*PASS_WARN_AGE* (number)

#+begin_quote
  The number of days warning given before a password expires. A zero
  means warning is given only upon the day of expiration, a negative
  value means no warning is given. If not specified, no warning will be
  provided.
#+end_quote

* FILES
/etc/group

#+begin_quote
  Group account information.
#+end_quote

/etc/passwd

#+begin_quote
  User account information.
#+end_quote

/etc/shadow

#+begin_quote
  Secure user account information.
#+end_quote

* EXIT VALUES
The *pwck* command exits with the following values:

/0/

#+begin_quote
  success
#+end_quote

/1/

#+begin_quote
  invalid command syntax
#+end_quote

/2/

#+begin_quote
  one or more bad password entries
#+end_quote

/3/

#+begin_quote
  cant open password files
#+end_quote

/4/

#+begin_quote
  cant lock password files
#+end_quote

/5/

#+begin_quote
  cant update password files
#+end_quote

/6/

#+begin_quote
  cant sort password files
#+end_quote

* SEE ALSO
*group*(5), *grpck*(8), *passwd*(5), *shadow*(5), *usermod*(8).
