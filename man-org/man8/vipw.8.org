#+TITLE: Manpages - vipw.8
#+DESCRIPTION: Linux manpage for vipw.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vipw, vigr - edit the password or group file

* SYNOPSIS
*vipw* [options]

*vigr* [options]

* DESCRIPTION
*vipw* edits the password file after setting the appropriate locks, and
does any necessary processing after the password file is unlocked. If
the password file is already locked for editing by another user, *vipw*
will ask you to try again later. The default editor for *vipw* and
*vigr* is *vi*(1). *vigr* edits the group file in the same manner as
*vipw* does the passwd file.

* ENVIRONMENT
If the following environment variable exists, it will be utilized by
*vipw* and *vigr*:

*EDITOR*

#+begin_quote
  The editor specified by the string *EDITOR* will be invoked instead of
  the default editor *vi*(1).
#+end_quote

* HISTORY
The *vipw* command appeared in 4.0BSD. The *vigr* command appeared in
Util-Linux 2.6.

* SEE ALSO
*vi*(1), *passwd*(1), *flock*(2), *passwd*(5)

* AVAILABILITY
The *vigr* and *vipw* commands are part of the util-linux package and
are available from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>
