#+TITLE: Manpages - pscap.8
#+DESCRIPTION: Linux manpage for pscap.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pscap - a program to see capabilities

* SYNOPSIS
*pscap [ -a ]*

* DESCRIPTION
*pscap* is a program that prints out a report of process capabilities.
If the application has any capabilities, it will be in the report with
the exception of init. By giving the -a command line option, init will
be included, too. If a process is not in the report, it has dropped all
capabilities. If the process has partial capabilities, it is further
examined to see if it has an open-ended bounding set. If this is found
to be true, a '+' symbol is added. If the process has ambient
capabilities, a '@' symbols is added.

The command name in the output may be followed by an asterisk mark (*).
This mark denotes processes which run in child user namespaces (relative
to the user namespace of pscap itself).

* SEE ALSO
*netcap*(8), *filecap*(8), *capabilities*(7), *ps*(8).

* AUTHOR
Steve Grubb
