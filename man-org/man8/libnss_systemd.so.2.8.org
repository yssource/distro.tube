#+TITLE: Manpages - libnss_systemd.so.2.8
#+DESCRIPTION: Linux manpage for libnss_systemd.so.2.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about libnss_systemd.so.2.8 is found in manpage for: [[../nss-systemd.8][nss-systemd.8]]