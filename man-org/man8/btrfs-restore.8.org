#+TITLE: Manpages - btrfs-restore.8
#+DESCRIPTION: Linux manpage for btrfs-restore.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
btrfs-restore - try to restore files from a damaged btrfs filesystem
image

* SYNOPSIS
*btrfs restore* [options] /<device>/ /<path>/ | -l /<device>/

* DESCRIPTION
*btrfs restore* is used to try to salvage files from a damaged
filesystem and restore them into /<path>/ or just list the subvolume
tree roots. The filesystem image is not modified.

If the filesystem is damaged and cannot be repaired by the other tools
(*btrfs-check*(8) or *btrfs-rescue*(8)), *btrfs restore* could be used
to retrieve file data, as far as the metadata are readable. The checks
done by restore are less strict and the process is usually able to get
far enough to retrieve data from the whole filesystem. This comes at a
cost that some data might be incomplete or from older versions if
they're available.

There are several options to attempt restoration of various file
metadata type. You can try a dry run first to see how well the process
goes and use further options to extend the set of restored metadata.

For images with damaged tree structures, there are several options to
point the process to some spare copy.

#+begin_quote
  \\

  *Note*

  \\

  It is recommended to read the following btrfs wiki page if your data
  is not salvaged with default option:
  *https://btrfs.wiki.kernel.org/index.php/Restore*
#+end_quote

* OPTIONS
-s|--snapshots

#+begin_quote
  get also snapshots that are skipped by default
#+end_quote

-x|--xattr

#+begin_quote
  get extended attributes
#+end_quote

-m|--metadata

#+begin_quote
  restore owner, mode and times for files and directories
#+end_quote

-S|--symlinks

#+begin_quote
  restore symbolic links as well as normal files
#+end_quote

-i|--ignore-errors

#+begin_quote
  ignore errors during restoration and continue
#+end_quote

-o|--overwrite

#+begin_quote
  overwrite directories/files in /<path>/, eg. for repeated runs
#+end_quote

-t /<bytenr>/

#+begin_quote
  use /<bytenr>/ to read the root tree
#+end_quote

-f /<bytenr>/

#+begin_quote
  only restore files that are under specified subvolume root pointed by
  /<bytenr>/
#+end_quote

-u|--super /<mirror>/

#+begin_quote
  use given superblock mirror identified by /<mirror>/, it can be 0,1 or
  2
#+end_quote

-r|--root /<rootid>/

#+begin_quote
  only restore files that are under a specified subvolume whose objectid
  is /<rootid>/
#+end_quote

-d

#+begin_quote
  find directory
#+end_quote

-l|--list-roots

#+begin_quote
  list subvolume tree roots, can be used as argument for /-r/
#+end_quote

-D|--dry-run

#+begin_quote
  dry run (only list files that would be recovered)
#+end_quote

--path-regex /<regex>/

#+begin_quote
  restore only filenames matching a regular expression (*regex*(7)) with
  a mandatory format

  *^/(|home(|/username(|/Desktop(|/.*))))$*

  The format is not very comfortable and restores all files in the
  directories in the whole path, so this is not useful for restoring
  single file in a deep hierarchy.
#+end_quote

-c

#+begin_quote
  ignore case (--path-regex only)
#+end_quote

-v|--verbose

#+begin_quote
  (deprecated) alias for global /-v/ option
#+end_quote

*Global options*

-v|--verbose

#+begin_quote
  be verbose and print what is being restored
#+end_quote

* EXIT STATUS
*btrfs restore* returns a zero exit status if it succeeds. Non zero is
returned in case of failure.

* AVAILABILITY
*btrfs* is part of btrfs-progs. Please refer to the btrfs wiki
*http://btrfs.wiki.kernel.org* for further details.

* SEE ALSO
*mkfs.btrfs*(8), *btrfs-rescue*(8), *btrfs-check*(8)
