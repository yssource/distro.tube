#+TITLE: Manpages - routef.8
#+DESCRIPTION: Linux manpage for routef.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about routef.8 is found in manpage for: [[../man8/routel.8][man8/routel.8]]