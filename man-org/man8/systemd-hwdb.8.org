#+TITLE: Manpages - systemd-hwdb.8
#+DESCRIPTION: Linux manpage for systemd-hwdb.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-hwdb - hardware database management tool

* SYNOPSIS
*systemd-hwdb [options] update*

*systemd-hwdb [options] query */modalias/

* DESCRIPTION
*systemd-hwdb* expects a command and command specific arguments. It
manages the binary hardware database.

* OPTIONS
*--usr*

#+begin_quote
  Generate in /usr/lib/udev instead of /etc/udev.
#+end_quote

*-r*, *--root=*/PATH/

#+begin_quote
  Alternate root path in the filesystem.
#+end_quote

*-s*, *--strict*

#+begin_quote
  When updating, return non-zero exit value on any parsing error.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

** systemd-hwdb [/options/] update
Update the binary database.

** systemd-hwdb [/options/] query [/MODALIAS/]
Query database and print result.

* SEE ALSO
*hwdb*(7)
