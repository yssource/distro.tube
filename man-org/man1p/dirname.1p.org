#+TITLE: Manpages - dirname.1p
#+DESCRIPTION: Linux manpage for dirname.1p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
dirname --- return the directory portion of a pathname

* SYNOPSIS
#+begin_example
  dirname string
#+end_example

* DESCRIPTION
The /string/ operand shall be treated as a pathname, as defined in the
Base Definitions volume of POSIX.1‐2017, /Section 3.271/, /Pathname/.
The string /string/ shall be converted to the name of the directory
containing the filename corresponding to the last pathname component in
/string/, performing actions equivalent to the following steps in order:

-  1. :: If /string/ is *//*, skip steps 2 to 5.

-  2. :: If /string/ consists entirely of <slash> characters, /string/
  shall be set to a single <slash> character. In this case, skip steps 3
  to 8.

-  3. :: If there are any trailing <slash> characters in /string/, they
  shall be removed.

-  4. :: If there are no <slash> characters remaining in /string/,
  /string/ shall be set to a single <period> character. In this case,
  skip steps 5 to 8.

-  5. :: If there are any trailing non- <slash> characters in /string/,
  they shall be removed.

-  6. :: If the remaining /string/ is *//*, it is implementation-defined
  whether steps 7 and 8 are skipped or processed.

-  7. :: If there are any trailing <slash> characters in /string/, they
  shall be removed.

-  8. :: If the remaining /string/ is empty, /string/ shall be set to a
  single <slash> character.

The resulting string shall be written to standard output.

* OPTIONS
None.

* OPERANDS
The following operand shall be supported:

- string :: A string.

* STDIN
Not used.

* INPUT FILES
None.

* ENVIRONMENT VARIABLES
The following environment variables shall affect the execution of
/dirname/:

- LANG :: Provide a default value for the internationalization variables
  that are unset or null. (See the Base Definitions volume of
  POSIX.1‐2017, /Section 8.2/, /Internationalization Variables/ for the
  precedence of internationalization variables used to determine the
  values of locale categories.)

- LC_ALL :: If set to a non-empty string value, override the values of
  all the other internationalization variables.

- LC_CTYPE :: Determine the locale for the interpretation of sequences
  of bytes of text data as characters (for example, single-byte as
  opposed to multi-byte characters in arguments).

- LC_MESSAGES :: \\
  Determine the locale that should be used to affect the format and
  contents of diagnostic messages written to standard error.

- NLSPATH :: Determine the location of message catalogs for the
  processing of /LC_MESSAGES/.

* ASYNCHRONOUS EVENTS
Default.

* STDOUT
The /dirname/ utility shall write a line to the standard output in the
following format:

#+begin_quote
  #+begin_example

    "%s\n", <resulting string>
  #+end_example
#+end_quote

* STDERR
The standard error shall be used only for diagnostic messages.

* OUTPUT FILES
None.

* EXTENDED DESCRIPTION
None.

* EXIT STATUS
The following exit values shall be returned:

-  0 :: Successful completion.

- >0 :: An error occurred.

* CONSEQUENCES OF ERRORS
Default.

/The following sections are informative./

* APPLICATION USAGE
The definition of /pathname/ specifies implementation-defined behavior
for pathnames starting with two <slash> characters. Therefore,
applications shall not arbitrarily add <slash> characters to the
beginning of a pathname unless they can ensure that there are more or
less than two or are prepared to deal with the implementation-defined
consequences.

* EXAMPLES
The EXAMPLES section of the /basename/() function (see the System
Interfaces volume of POSIX.1‐2017, //basename/ ( )/) includes a table
showing examples of the results of processing several sample pathnames
by the /basename/() and /dirname/() functions and by the /basename/ and
/dirname/ utilities.

See also the examples for the /basename/ utility.

* RATIONALE
The behaviors of /basename/ and /dirname/ in this volume of POSIX.1‐2017
have been coordinated so that when /string/ is a valid pathname:

#+begin_quote
  #+begin_example

    $(basename -- "string")
  #+end_example
#+end_quote

would be a valid filename for the file in the directory:

#+begin_quote
  #+begin_example

    $(dirname -- "string")
  #+end_example
#+end_quote

This would not work for the versions of these utilities in early
proposals due to the way processing of trailing <slash> characters was
specified. Consideration was given to leaving processing unspecified if
there were trailing <slash> characters, but this cannot be done; the
Base Definitions volume of POSIX.1‐2017, /Section 3.271/, /Pathname/
allows trailing <slash> characters. The /basename/ and /dirname/
utilities have to specify consistent handling for all valid pathnames.

* FUTURE DIRECTIONS
None.

* SEE ALSO
/Section 2.5/, /Parameters and Variables/, //basename/ /

The Base Definitions volume of POSIX.1‐2017, /Section 3.271/,
/Pathname/, /Chapter 8/, /Environment Variables/

The System Interfaces volume of POSIX.1‐2017, //basename/ ( )/,
//dirname/ ( )/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
