#+TITLE: Manpages - eval.1p
#+DESCRIPTION: Linux manpage for eval.1p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
eval --- construct command by concatenating arguments

* SYNOPSIS
#+begin_example
  eval [argument...]
#+end_example

* DESCRIPTION
The /eval/ utility shall construct a command by concatenating
/argument/s together, separating each with a <space> character. The
constructed command shall be read and executed by the shell.

* OPTIONS
None.

* OPERANDS
See the DESCRIPTION.

* STDIN
Not used.

* INPUT FILES
None.

* ENVIRONMENT VARIABLES
None.

* ASYNCHRONOUS EVENTS
Default.

* STDOUT
Not used.

* STDERR
The standard error shall be used only for diagnostic messages.

* OUTPUT FILES
None.

* EXTENDED DESCRIPTION
None.

* EXIT STATUS
If there are no /argument/s, or only null /argument/s, /eval/ shall
return a zero exit status; otherwise, it shall return the exit status of
the command defined by the string of concatenated /argument/s separated
by <space> characters, or a non-zero exit status if the concatenation
could not be parsed as a command and the shell is interactive (and
therefore did not abort).

* CONSEQUENCES OF ERRORS
Default.

/The following sections are informative./

* APPLICATION USAGE
Since /eval/ is not required to recognize the *"--"* end of options
delimiter, in cases where the argument(s) to /eval/ might begin with
*'-'* it is recommended that the first argument is prefixed by a string
that will not alter the commands to be executed, such as a <space>
character:

#+begin_quote
  #+begin_example

    eval " $commands"
  #+end_example
#+end_quote

or:

#+begin_quote
  #+begin_example

    eval " $(some_command)"
  #+end_example
#+end_quote

* EXAMPLES
#+begin_example
  foo=10 x=foo
  y='$'$x
  echo $y
  $foo
  eval y='$'$x
  echo $y
  10
#+end_example

* RATIONALE
This standard allows, but does not require, /eval/ to recognize *"--"*.
Although this means applications cannot use *"--"* to protect against
options supported as an extension (or errors reported for unsupported
options), the nature of the /eval/ utility is such that other means can
be used to provide this protection (see APPLICATION USAGE above).

* FUTURE DIRECTIONS
None.

* SEE ALSO
/Section 2.14/, /Special Built-In Utilities/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
