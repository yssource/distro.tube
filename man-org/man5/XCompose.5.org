#+TITLE: Manpages - XCompose.5
#+DESCRIPTION: Linux manpage for XCompose.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XCompose.5 is found in manpage for: [[../man5/Compose.5][man5/Compose.5]]