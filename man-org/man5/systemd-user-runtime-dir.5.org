#+TITLE: Manpages - systemd-user-runtime-dir.5
#+DESCRIPTION: Linux manpage for systemd-user-runtime-dir.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-user-runtime-dir.5 is found in manpage for: [[../user@.service.5][user@.service.5]]