#+TITLE: Manpages - user-dirs.dirs.5
#+DESCRIPTION: Linux manpage for user-dirs.dirs.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
user-dirs.dirs - settings for XDG user dirs

* DESCRIPTION
The $HOME/.config/user-dirs.dirs file is a text file that contains the
user-specific values for the XDG user dirs. It is created and updated by
the xdg-user-dirs-update command.

This file contains lines of the form

#+begin_quote
  #+begin_example
    XDG_NAME_DIR=VALUE
  #+end_example
#+end_quote

The following names are recognised:

#+begin_quote
  DESKTOP
#+end_quote

#+begin_quote
  DOWNLOAD
#+end_quote

#+begin_quote
  TEMPLATES
#+end_quote

#+begin_quote
  PUBLICSHARE
#+end_quote

#+begin_quote
  DOCUMENTS
#+end_quote

#+begin_quote
  MUSIC
#+end_quote

#+begin_quote
  PICTURES
#+end_quote

#+begin_quote
  VIDEOS
#+end_quote

/VALUE/ must be of the form "$HOME/Path" or "/Path".

Lines beginning with a # character are ignored.

The format of user-dirs.dirs is designed to allow direct sourcing of
this file in shell scripts.

* ENVIRONMENT
*XDG_CONFIG_DIRS*

#+begin_quote
  The user-dirs.defaults file is located in this directory. The default
  is /etc/xdg.
#+end_quote

* SEE ALSO
*xdg-user-dirs-update*(1)
