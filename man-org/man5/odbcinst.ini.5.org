#+TITLE: Manpages - odbcinst.ini.5
#+DESCRIPTION: Linux manpage for odbcinst.ini.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
/etc/odbcinst.ini - An unixODBC drivers configuration

* DESCRIPTION
*/etc/odbcinst.ini* is a text configuration file for unixODBC drivers.
It can be edited by hand, but the recommended way to update this file is
to use the *odbcinst*(1) utility.

* FILE FORMAT
The general .ini file format is:

#+begin_quote
  #+begin_example
    [SectionName1]
    key1 = value1
    key2 = value2
    ...

    [SectionName2]
    key1 = value1
    key2 = value2
    ...
  #+end_example
#+end_quote

Each ODBC driver has its own section and can be referred to by the name
of its section. Configuration keys recognised in driver sections by
unixODBC itself are:

- Description :: A text string briefly describing the driver.

- Driver :: A filesystem path to the actual driver library.

- Setup :: A filesystem path to the driver setup library.

- FileUsage :: The section named *[ODBC]* configures global options.
  Keys recognised in the *[ODBC]* section include:

- Trace :: Enable ODBC driver trace output, which is written to the path
  specified by *TraceFile*.

Note that some drivers have their own separate trace control options.
Unlike the *Trace* option these are usually specified at the DSN level.

Values recognised as enabled are any case variation of "1", "y", "yes"
or "on".

- TraceFile :: Path or path-pattern to write the ODBC trace file to. Has
  no effect unless *Trace* is enabled. Default */tmp/sql.log*.

/WARNING/: setting *TraceFile to a path writeable by multiple users* may
not work correctly as only the first user will be able to create and
open the file.

** TEMPLATE FILES
The recommended way to manage the drivers is using the *odbcinst*(1)
utility. You can install the drivers by supplying it with template file,
which has the same format as this file.

* EXAMPLES
An example of the actual PostgreSQL driver:

#+begin_quote
  #+begin_example
    [PostgreSQL]
    Description = PostgreSQL driver for GNU/Linux
    Driver      = /usr/lib/psqlodbcw.so
    Setup       = /usr/lib/libodbcpsqlS.so
    FileUsage   = 1
  #+end_example
#+end_quote

Note that driver paths may vary, and some drivers require *Driver64* and
*Setup64* entries too.

By specifying the driver like that, you can then reference it in the
*odbc.ini*(5) as follows:

#+begin_quote
  #+begin_example
    Driver = PostgreSQL
  #+end_example
#+end_quote

The recommended way to add that driver is by creating a template file
containing:

#+begin_quote
  #+begin_example
    [PostgreSQL]
    Description = PostgreSQL driver for GNU/Linux
    Driver      = /usr/lib/psqlodbcw.so
    Setup       = /usr/lib/libodbcpsqlS.so
  #+end_example
#+end_quote

and call the *odbcinst*(1):

#+begin_quote
  *# odbcinst -i -d -f */template.ini/
#+end_quote

* SEE ALSO
*unixODBC*(7), *odbcinst*(1), *odbc.ini*(5)

®

* AUTHORS
The authors of unixODBC are Peter Harvey </pharvey@codebydesign.com/>
and Nick Gorham </nick@lurcher.org/>. For the full list of contributors
see the AUTHORS file.

* COPYRIGHT
unixODBC is licensed under the GNU Lesser General Public License. For
details about the license, see the COPYING file.
