#+TITLE: Manpages - coredump.conf.d.5
#+DESCRIPTION: Linux manpage for coredump.conf.d.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about coredump.conf.d.5 is found in manpage for: [[../coredump.conf.5][coredump.conf.5]]