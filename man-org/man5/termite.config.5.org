#+TITLE: Manpages - termite.config.5
#+DESCRIPTION: Linux manpage for termite.config.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termite.conf - Termite configuration file

* SYNOPSIS
/$XDG_CONFIG_HOME/termite/config/

* DESCRIPTION
The configuration file format for the *termite*

* OPTIONS
- allow_bold :: Allow the output of bold characters when the bold escape
  sequence appears

- audible_bell :: Have the terminal beep on the terminal bell.

- bold_is_bright :: Display bold text in bright colors.

- browser :: Set the default browser for opening links. If its not set,
  /$BROWSER/ is read. If that's not set, url hints will be disabled.

- word_char_exceptions :: Set characters to exclude as word boundaries
  on selection;

- clickable_url :: Auto-detected URLs can be clicked on to open them in
  your browser. Only enabled if a browser is configured or detected.

- clickable_url_ctrl :: Require the /Ctrl/ key to be pressed while
  clicking URLs to open them.

- hyperlinks :: Enable support for applications to mark text as
  hyperlinks. Requires clickable_url to be set.

- cursor_blink :: Specify the how the terminal's cursor should behave.
  Accepts *system* to respect the gtk global configuration, *on* and
  *off* to explicitly enable or disable them.

- cursor_shape :: Specify how the cursor should look. Accepts *block*,
  *ibeam* and *underline*.

- dynamic_title :: Settings dynamic title allows the terminal and the
  shell to update the terminal's title.

- filter_unmatched_urls :: Whether to hide url hints not matching input
  in url hints mode.

- font :: The font description for the terminal's font.

- fullscreen :: Enables entering fullscreen mode by pressing F11.

- icon_name :: The name of the icon to be used for the terminal process.

- cell_height_scale :: Scale the height of character cells. Valid values
  range from 1.0 to 2.0.

- cell_width_scale :: Scale the width of character cells. Valid values
  range from 1.0 to 2.0.

- modify_other_keys :: Emit escape sequences for extra keys, like the
  *modifyOtherKeys* resource for *xterm*(1).

- mouse_autohide :: Automatically hide the mouse pointer when you start
  typing.

- scrollback_lines :: Set the number of lines to limit the terminal's
  scrollback. Setting the number of lines to 0 disables this feature, a
  negative value makes the scrollback "infinite".

- scrollbar :: Specify scrollbar visibility and position. Accepts *off*,
  *left* and *right*.

- gtk_dark_theme :: Whether to prefer using the GTK dark theme variant.

- scroll_on_keystroke :: Scroll to the bottom automatically when a key
  is pressed.

- scroll_on_output :: Scroll to the bottom when the shell generates
  output.

- search_wrap :: Search from top again when you hit the bottom.

- size_hints :: Enable size hints. Locks the terminal resizing to
  increments of the terminal's cell size. Requires a window manager that
  respects scroll hints.

- urgent_on_bell :: Sets the window as urgent on the terminal bell.

- smart_copy :: Enable smart copy. *^C* will copy if text has been
  selected. *^V* will paste. It is no longer possible to send a *^V* to
  the running program when this option is enabled.
