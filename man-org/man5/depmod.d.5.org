#+TITLE: Manpages - depmod.d.5
#+DESCRIPTION: Linux manpage for depmod.d.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
depmod.d - Configuration directory for depmod

* SYNOPSIS
/usr/lib/depmod.d/*.conf

/usr/local/lib/depmod.d/*.conf

/run/depmod.d/*.conf

/etc/depmod.d/*.conf

* DESCRIPTION
The order in which modules are processed by the *depmod* command can be
altered on a global or per-module basis. This is typically useful in
cases where built-in kernel modules are complemented by custom built
versions of the same and the user wishes to affect the priority of
processing in order to override the module version supplied by the
kernel.

The format of files under depmod.d is simple: one command per line, with
blank lines and lines starting with # ignored (useful for adding
comments). A \ at the end of a line causes it to continue on the next
line, which makes the files a bit neater.

* COMMANDS
search /subdirectory.../

#+begin_quote
  This allows you to specify the order in which /lib/modules (or other
  configured module location) subdirectories will be processed by
  *depmod*. Directories are listed in order, with the highest priority
  given to the first listed directory and the lowest priority given to
  the last directory listed. The special keyword *built-in* refers to
  the standard module directories installed by the kernel. Another
  special keyword *external* refers to the list of external directories,
  defined by the *external* command.

  By default, depmod will give a higher priority to a directory with the
  name *updates* using this built-in search string: "updates built-in"
  but more complex arrangements are possible and are used in several
  popular distributions.
#+end_quote

override /modulename/ /kernelversion/ /modulesubdirectory/

#+begin_quote
  This command allows you to override which version of a specific module
  will be used when more than one module sharing the same name is
  processed by the *depmod* command. It is possible to specify one
  kernel or all kernels using the * wildcard. /modulesubdirectory/ is
  the name of the subdirectory under /lib/modules (or other module
  location) where the target module is installed.

  For example, it is possible to override the priority of an updated
  test module called *kmod* by specifying the following command:
  "override kmod * extra". This will ensure that any matching module
  name installed under the *extra* subdirectory within /lib/modules (or
  other module location) will take priority over any likenamed module
  already provided by the kernel.
#+end_quote

external /kernelversion/ /absolutemodulesdirectory.../

#+begin_quote
  This specifies a list of directories, which will be checked according
  to the priorities in the *search* command. The order matters also, the
  first directory has the higher priority.

  The /kernelversion/ is a POSIX regular expression or * wildcard, like
  in the *override*.
#+end_quote

* COPYRIGHT
This manual page Copyright 2006-2010, Jon Masters, Red Hat, Inc.

* SEE ALSO
*depmod*(8)

* AUTHORS
*Jon Masters* <jcm@jonmasters.org>

#+begin_quote
  Developer
#+end_quote

*Robby Workman* <rworkman@slackware.com>

#+begin_quote
  Developer
#+end_quote

*Lucas De Marchi* <lucas.de.marchi@gmail.com>

#+begin_quote
  Developer
#+end_quote
