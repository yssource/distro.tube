#+TITLE: Manpages - netconfig.5
#+DESCRIPTION: Linux manpage for netconfig.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

file defines a list of

describing their semantics and protocol. In

this file is only used by the RPC library code.

Entries have the following format:

Entries consist of the following fields:

The name of the transport described.

Describes the semantics of the transport. This can be one of:

Connectionless transport.

Connection-oriented transport

Connection-oriented, ordered transport.

A raw connection.

This field is either blank (specified by

or contains a

meaning visible to the

function.

The protocol family of the transport. This is currently one of:

The IPv6

family of protocols.

The IPv4

family of protocols.

The

protocol family.

The name of the protocol used for this transport. Can currently be
either

or empty.

This field is always empty in

This field is always empty in

The order of entries in this file will determine which transport will be
preferred by the RPC library code, given a match on a specified network
type. For example, if a sample network config file would look like this:

udp6 tpi_clts v inet6 udp - - tcp6 tpi_cots_ord v inet6 tcp - - udp
tpi_clts v inet udp - - tcp tpi_cots_ord v inet tcp - - rawip tpi_raw -
inet - - - local tpi_cots_ord - loopback - - -

then using the network type

in calls to the RPC library function (see

will make the code first try

and then

and associated functions will parse this file and return structures of
the following format:

struct netconfig { char *nc_netid; /* Network ID */ unsigned long
nc_semantics; /* Semantics (see below) */ unsigned long nc_flag; /*
Flags (see below) */ char *nc_protofmly; /* Protocol family */ char
*nc_proto; /* Protocol name */ char *nc_device; /* Network device
pathname (unused) */ unsigned long nc_nlookups; /* Number of lookup libs
(unused) */ char **nc_lookups; /* Names of the libraries (unused) */
unsigned long nc_unused[9]; /* reserved */ };
