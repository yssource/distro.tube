#+TITLE: Manpages - putc.3
#+DESCRIPTION: Linux manpage for putc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about putc.3 is found in manpage for: [[../man3/puts.3][man3/puts.3]]