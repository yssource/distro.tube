#+TITLE: Manpages - auparse_get_field_num.3
#+DESCRIPTION: Linux manpage for auparse_get_field_num.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_get_field_num - get current field cursor location

* SYNOPSIS
*#include <auparse.h>*

unsigned int auparse_get_field_num(auparse_state_t *au);

* DESCRIPTION
auparse_get_field_num will retrieve the internal library cursors current
field location in the current record. Fields within the same record are
numbered starting from 0. This is generally not needed but there are
some cases where one may want to know the exact field being looked at.

* RETURN VALUE
Returns the current field cursor location.

* SEE ALSO
*auparse_goto_field_num*(3),*auparse_get_record_num*(3).

* AUTHOR
Steve Grubb
