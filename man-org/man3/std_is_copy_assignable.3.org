#+TITLE: Manpages - std_is_copy_assignable.3
#+DESCRIPTION: Linux manpage for std_is_copy_assignable.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_copy_assignable< _Tp > - is_copy_assignable

* SYNOPSIS
\\

Inherits __is_copy_assignable_impl::type.

* Detailed Description
** "template<typename _Tp>
\\
struct std::is_copy_assignable< _Tp >"is_copy_assignable

Definition at line *1057* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
