#+TITLE: Manpages - xcb_randr_get_providers_unchecked.3
#+DESCRIPTION: Linux manpage for xcb_randr_get_providers_unchecked.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_randr_get_providers_unchecked.3 is found in manpage for: [[../man3/xcb_randr_get_providers.3][man3/xcb_randr_get_providers.3]]