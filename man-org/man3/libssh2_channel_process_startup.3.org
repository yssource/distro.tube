#+TITLE: Manpages - libssh2_channel_process_startup.3
#+DESCRIPTION: Linux manpage for libssh2_channel_process_startup.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_channel_process_startup - request a shell on a channel

* SYNOPSIS
#+begin_example
  #include <libssh2.h>

  int libssh2_channel_process_startup(LIBSSH2_CHANNEL *channel,
                                      const char *request,
                                      unsigned int request_len,
                                      const char *message,
                                      unsigned int message_len);
#+end_example

* DESCRIPTION
/channel/ - Active session channel instance.

/request/ - Type of process to startup. The SSH2 protocol currently
defines shell, exec, and subsystem as standard process services.

/request_len/ - Length of request parameter.

/message/ - Request specific message data to include.

/message_len/ - Length of message parameter.

Initiate a request on a session type channel such as returned by
*libssh2_channel_open_ex(3)*

* RETURN VALUE
Return 0 on success or negative on failure. It returns
LIBSSH2_ERROR_EAGAIN when it would otherwise block. While
LIBSSH2_ERROR_EAGAIN is a negative number, it isn't really a failure per
se.

* ERRORS
/LIBSSH2_ERROR_ALLOC/ - An internal memory allocation call failed.

/LIBSSH2_ERROR_SOCKET_SEND/ - Unable to send data on socket.

/LIBSSH2_ERROR_CHANNEL_REQUEST_DENIED/ -

* SEE ALSO
*libssh2_channel_open_ex(3)*
