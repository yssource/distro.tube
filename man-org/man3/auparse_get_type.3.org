#+TITLE: Manpages - auparse_get_type.3
#+DESCRIPTION: Linux manpage for auparse_get_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_get_type - get record's type

* SYNOPSIS
*#include <auparse.h>*

int auparse_get_type(auparse_state_t *au);

* DESCRIPTION
auparse_get_type will return the integer value for the current record of
the current event.

* RETURN VALUE
auparse_get_type returns 0 if an error occurs; otherwise, the record's
type.

* SEE ALSO
*auparse_get_type_name*(3),*auparse_next_record*(3).

* AUTHOR
Steve Grubb
