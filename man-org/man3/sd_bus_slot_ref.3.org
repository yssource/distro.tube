#+TITLE: Manpages - sd_bus_slot_ref.3
#+DESCRIPTION: Linux manpage for sd_bus_slot_ref.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_slot_ref, sd_bus_slot_unref, sd_bus_slot_unrefp - Create and
destroy references to a bus slot object

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*sd_bus_slot *sd_bus_slot_ref(sd_bus_slot **/slot/*);*

*sd_bus_slot *sd_bus_slot_unref(sd_bus_slot **/slot/*);*

*void sd_bus_slot_unrefp(sd_bus_slot ***/slotp/*);*

* DESCRIPTION
*sd_bus_slot_ref()* increases the reference counter of /slot/ by one.

*sd_bus_slot_unref()* decreases the reference counter of /slot/ by one.
Once the reference count has dropped to zero, slot object is destroyed
and cannot be used anymore, so further calls to *sd_bus_slot_ref()* or
*sd_bus_slot_unref()* are illegal.

*sd_bus_slot_unrefp()* is similar to *sd_bus_slot_unref()* but takes a
pointer to a pointer to an *sd_bus_slot* object. This call is useful in
conjunction with GCCs and LLVMs *Clean-up Variable Attribute*[1]. See
*sd_bus_new*(3) for an example how to use the cleanup attribute.

*sd_bus_slot_ref()* and *sd_bus_slot_unref()* execute no operation if
the passed in bus object address is *NULL*. *sd_bus_slot_unrefp()* will
first dereference its argument, which must not be *NULL*, and will
execute no operation if /that/ is *NULL*.

* RETURN VALUE
*sd_bus_slot_ref()* always returns the argument.

*sd_bus_slot_unref()* always returns *NULL*.

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_new*(3), *sd_bus_message_new*(3),
*sd_bus_call_method_async*(3)

* NOTES
-  1. :: Clean-up Variable Attribute

  https://gcc.gnu.org/onlinedocs/gcc/Common-Variable-Attributes.html
