#+TITLE: Manpages - BIO_printf.3ssl
#+DESCRIPTION: Linux manpage for BIO_printf.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
BIO_printf, BIO_vprintf, BIO_snprintf, BIO_vsnprintf - formatted output
to a BIO

* SYNOPSIS
#include <openssl/bio.h> int BIO_printf(BIO *bio, const char *format,
...) int BIO_vprintf(BIO *bio, const char *format, va_list args) int
BIO_snprintf(char *buf, size_t n, const char *format, ...) int
BIO_vsnprintf(char *buf, size_t n, const char *format, va_list args)

* DESCRIPTION
*BIO_printf()* is similar to the standard C *printf()* function, except
that the output is sent to the specified BIO, *bio*, rather than
standard output. All common format specifiers are supported.

*BIO_vprintf()* is similar to the *vprintf()* function found on many
platforms, the output is sent to the specified BIO, *bio*, rather than
standard output. All common format specifiers are supported. The
argument list *args* is a stdarg argument list.

*BIO_snprintf()* is for platforms that do not have the common
*snprintf()* function. It is like *sprintf()* except that the size
parameter, *n*, specifies the size of the output buffer.

*BIO_vsnprintf()* is to *BIO_snprintf()* as *BIO_vprintf()* is to
*BIO_printf()*.

* RETURN VALUES
All functions return the number of bytes written, or -1 on error. For
*BIO_snprintf()* and *BIO_vsnprintf()* this includes when the output
buffer is too small.

* COPYRIGHT
Copyright 2017 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
