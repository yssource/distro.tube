#+TITLE: Manpages - XtCloseDisplay.3
#+DESCRIPTION: Linux manpage for XtCloseDisplay.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtCloseDisplay.3 is found in manpage for: [[../man3/XtDisplayInitialize.3][man3/XtDisplayInitialize.3]]