#+TITLE: Manpages - gnutls_session_set_ptr.3
#+DESCRIPTION: Linux manpage for gnutls_session_set_ptr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_session_set_ptr - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*void gnutls_session_set_ptr(gnutls_session_t */session/*, void *
*/ptr/*);*

* ARGUMENTS
- gnutls_session_t session :: is a *gnutls_session_t* type.

- void * ptr :: is the user pointer

* DESCRIPTION
This function will set (associate) the user given pointer /ptr/ to the
session structure. This pointer can be accessed with
*gnutls_session_get_ptr()*.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
