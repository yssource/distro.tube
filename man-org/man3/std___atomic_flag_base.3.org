#+TITLE: Manpages - std___atomic_flag_base.3
#+DESCRIPTION: Linux manpage for std___atomic_flag_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__atomic_flag_base - Base type for atomic_flag.

* SYNOPSIS
\\

=#include <atomic_base.h>=

Inherited by *std::atomic_flag*.

** Public Attributes
__atomic_flag_data_type *_M_i*\\

* Detailed Description
Base type for atomic_flag.

Base type is POD with data, allowing atomic_flag to derive from it and
meet the standard layout type requirement. In addition to compatibility
with a C interface, this allows different implementations of atomic_flag
to use the same atomic operation functions, via a standard conversion to
the __atomic_flag_base argument.

Definition at line *179* of file *atomic_base.h*.

* Member Data Documentation
** __atomic_flag_data_type std::__atomic_flag_base::_M_i
Definition at line *181* of file *atomic_base.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
