#+TITLE: Manpages - libarchive.3
#+DESCRIPTION: Linux manpage for libarchive.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

library provides a flexible interface for reading and writing archives
in various formats such as tar and cpio.

also supports reading and writing archives compressed using various
compression filters such as gzip and bzip2. The library is inherently
stream-oriented; readers serially iterate through the archive, writers
serially add things to the archive. In particular, note that there is
currently no built-in support for random access nor for in-place
modification.

When reading an archive, the library automatically detects the format
and the compression. The library currently has read support for:

old-style tar archives,

most variants of the POSIX

format,

the POSIX

format,

GNU-format tar archives,

most common cpio archive formats,

ISO9660 CD images (including RockRidge and Joliet extensions),

Zip archives,

ar archives (including GNU/SysV and BSD extensions),

Microsoft CAB archives,

LHA archives,

mtree file tree descriptions,

RAR archives,

XAR archives.

The library automatically detects archives compressed with

or

and decompresses them transparently. It can similarly detect and decode
archives processed with

or which have an

header.

When writing an archive, you can specify the compression to be used and
the format to use. The library can write

POSIX-standard

archives,

POSIX

archives,

cpio archives,

Zip archive,

two different variants of shar archives,

ISO9660 CD images,

7-Zip archives,

ar archives,

mtree file tree descriptions,

XAR archives.

Pax interchange format is an extension of the tar archive format that
eliminates essentially all of the limitations of historic tar formats in
a standard fashion that is supported by POSIX-compliant

implementations on many systems as well as several newer implementations
of

Note that the default write format will suppress the pax extended
attributes for most entries; explicitly requesting pax format will
enable those attributes for all entries.

The read and write APIs are accessed through the

functions and the

functions, respectively, and either can be used independently of the
other.

The rest of this manual page provides an overview of the library
operation. More detailed information can be found in the individual
manual pages for each API or utility function.

See

See

The

API allows you to write

objects to disk using the same API used by

The

API is used internally by

using it directly can provide greater control over how entries get
written to disk. This API also makes it possible to share code between
archive-to-archive copy and archive-to-disk extraction operations.

The

supports for populating

objects from information in the filesystem. This includes the
information accessible from the

system call as well as ACLs, extended attributes, and other metadata.
The

API also supports iterating over directory trees, which allows
directories of files to be read using an API compatible with the

API.

Detailed descriptions of each function are provided by the corresponding
manual pages.

All of the functions utilize an opaque

datatype that provides access to the archive contents.

The

structure contains a complete description of a single archive entry. It
uses an opaque interface that is fully documented in

Users familiar with historic formats should be aware that the newer
variants have eliminated most restrictions on the length of textual
fields. Clients should not assume that filenames, link names, user
names, or group names are limited in length. In particular, pax
interchange format can easily accommodate pathnames in arbitrary
character sets that exceed

Most functions return

(zero) on success, non-zero on error. The return value indicates the
general severity of the error, ranging from

which indicates a minor problem that should probably be reported to the
user, to

which indicates a serious problem that will prevent any further
operations on this archive. On error, the

function can be used to retrieve a numeric error code (see

The

returns a textual error message suitable for display.

and

return pointers to an allocated and initialized

object.

and

return a count of the number of bytes actually read or written. A value
of zero indicates the end of the data for this entry. A negative value
indicates an error, in which case the

and

functions can be used to obtain more information.

There are character set conversions within the

functions that are impacted by the currently-selected locale.

The

library first appeared in

The

library was originally written by

Some archive formats support information that is not supported by

Such information cannot be fully archived or restored using this
library. This includes, for example, comments, character sets, or the
arbitrary key/value pairs that can appear in pax interchange format
archives.

Conversely, of course, not all of the information that can be stored in
an

is supported by all formats. For example, cpio formats do not support
nanosecond timestamps; old tar formats do not support large device
numbers.

The ISO9660 reader cannot yet read all ISO9660 images; it should learn
how to seek.

The AR writer requires the client program to use two passes, unlike all
other libarchive writers.
