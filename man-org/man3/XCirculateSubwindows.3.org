#+TITLE: Manpages - XCirculateSubwindows.3
#+DESCRIPTION: Linux manpage for XCirculateSubwindows.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XCirculateSubwindows.3 is found in manpage for: [[../man3/XRaiseWindow.3][man3/XRaiseWindow.3]]