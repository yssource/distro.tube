#+TITLE: Manpages - pcap_open_live.3pcap
#+DESCRIPTION: Linux manpage for pcap_open_live.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_open_live - open a device for capturing

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t *pcap_open_live(const char *device, int snaplen,
  int promisc, int to_ms, char *errbuf);
#+end_example

* DESCRIPTION
*pcap_open_live*() is used to obtain a packet capture handle to look at
packets on the network. /device/ is a string that specifies the network
device to open; on Linux systems with 2.2 or later kernels, a /device/
argument of "any" or *NULL* can be used to capture packets from all
interfaces.

/snaplen/ specifies the snapshot length to be set on the handle.

/promisc/ specifies whether the interface is to be put into promiscuous
mode. If /promisc/ is non-zero, promiscuous mode will be set, otherwise
it will not be set.

/to_ms/ specifies the packet buffer timeout, as a non-negative value, in
milliseconds. (See *pcap*(3PCAP) for an explanation of the packet buffer
timeout.)

* RETURN VALUE
*pcap_open_live*() returns a /pcap_t */ on success and *NULL* on
failure. If *NULL* is returned, /errbuf/ is filled in with an
appropriate error message. /errbuf/ may also be set to warning text when
*pcap_open_live*() succeeds; to detect this case the caller should store
a zero-length string in /errbuf/ before calling *pcap_open_live*() and
display the warning to the user if /errbuf/ is no longer a zero-length
string. /errbuf/ is assumed to be able to hold at least
*PCAP_ERRBUF_SIZE* chars.

* SEE ALSO
*pcap_create*(3PCAP), *pcap_activate*(3PCAP)
