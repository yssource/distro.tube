#+TITLE: Manpages - __gnu_cxx_constant_binary_fun.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_constant_binary_fun.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::constant_binary_fun< _Result, _Arg1, _Arg2 > - An *SGI
extension *.

* SYNOPSIS
\\

Inherits __gnu_cxx::_Constant_binary_fun< _Result, _Arg1, _Arg2 >.

** Public Types
typedef _Arg1 *first_argument_type*\\

typedef _Result *result_type*\\

typedef _Arg2 *second_argument_type*\\

** Public Member Functions
*constant_binary_fun* (const _Result &__v)\\

const result_type & *operator()* (const _Arg1 &, const _Arg2 &) const\\

** Public Attributes
_Result *_M_val*\\

* Detailed Description
** "template<class _Result, class _Arg1 = _Result, class _Arg2 = _Arg1>
\\
struct __gnu_cxx::constant_binary_fun< _Result, _Arg1, _Arg2 >"An *SGI
extension *.

Definition at line *312* of file *ext/functional*.

* Member Typedef Documentation
** template<class _Result , class _Arg1 , class _Arg2 > typedef _Arg1
__gnu_cxx::_Constant_binary_fun< _Result, _Arg1, _Arg2
>::first_argument_type= [inherited]=
Definition at line *267* of file *ext/functional*.

** template<class _Result , class _Arg1 , class _Arg2 > typedef _Result
__gnu_cxx::_Constant_binary_fun< _Result, _Arg1, _Arg2
>::result_type= [inherited]=
Definition at line *269* of file *ext/functional*.

** template<class _Result , class _Arg1 , class _Arg2 > typedef _Arg2
__gnu_cxx::_Constant_binary_fun< _Result, _Arg1, _Arg2
>::second_argument_type= [inherited]=
Definition at line *268* of file *ext/functional*.

* Constructor & Destructor Documentation
** template<class _Result , class _Arg1 = _Result, class _Arg2 = _Arg1>
*__gnu_cxx::constant_binary_fun*< _Result, _Arg1, _Arg2
>::*constant_binary_fun* (const _Result & __v)= [inline]=
Definition at line *315* of file *ext/functional*.

* Member Function Documentation
** template<class _Result , class _Arg1 , class _Arg2 > const
result_type & __gnu_cxx::_Constant_binary_fun< _Result, _Arg1, _Arg2
>::operator() (const _Arg1 &, const _Arg2 &) const= [inline]=,
= [inherited]=
Definition at line *275* of file *ext/functional*.

* Member Data Documentation
** template<class _Result , class _Arg1 , class _Arg2 > _Result
__gnu_cxx::_Constant_binary_fun< _Result, _Arg1, _Arg2
>::_M_val= [inherited]=
Definition at line *270* of file *ext/functional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
