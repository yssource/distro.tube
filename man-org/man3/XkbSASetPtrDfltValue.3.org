#+TITLE: Manpages - XkbSASetPtrDfltValue.3
#+DESCRIPTION: Linux manpage for XkbSASetPtrDfltValue.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbSASetPtrDfltValue - Sets the valueXXX field of act from val

* SYNOPSIS
*void XkbSASetPtrDfltValue* *( XkbAction */act/* ,* *int */val/* );*

* ARGUMENTS
- /- act/ :: action in which to set valueXXX

- /- val/ :: value to set in valueXXX

* DESCRIPTION
Actions associated with the XkbPtrDfltAction structure change the
mk_dflt_btn attribute of the MouseKeys control.

If the MouseKeys control is not enabled, KeyPress and KeyRelease events
are treated as though the action is XkbSA_NoAction. Otherwise, this
action changes the /mk_dflt_btn/ attribute of the MouseKeys control.

The /type/ field of the XkbPtrDfltAction structure should always be
XkbSA_SetPtrDflt.

The /flags/ field is composed of the bitwise inclusive OR of the values
shown in Table 1 (currently there is only one value defined).

TABLE

The /affect/ field specifies what changes as a result of this action.
The only valid value for the /affect/ field is XkbSA_AffectDfltBtn.

The /valueXXX/ field is a signed character that represents the new
button value for the /mk_dflt_btn/ attribute of the MouseKeys control.
If XkbSA_DfltBtnAbsolute is set in /flags, valueXXX/ specifies the
button to be used; otherwise, /valueXXX/ specifies the amount to be
added to the current default button. In either case, illegal button
choices are wrapped back around into range. Xkb provides macros, to
convert between the integer and signed character values in
XkbPtrDfltAction structures.

* STRUCTURES
#+begin_example

      typedef struct _XkbPtrDfltAction {
          unsigned char    type;      /* XkbSA_SetPtrDflt */
          unsigned char    flags;     /* controls the pointer button number */
          unsigned char    affect;    /* XkbSA_AffectDfltBtn */
          char             valueXXX;  /* new default button member */
      } XkbPtrDfltAction;
#+end_example
