#+TITLE: Manpages - wcsncat.3
#+DESCRIPTION: Linux manpage for wcsncat.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wcsncat - concatenate two wide-character strings

* SYNOPSIS
#+begin_example
  #include <wchar.h>

  wchar_t *wcsncat(wchar_t *restrict dest",constwchar_t*restrict"src,
   size_t n);
#+end_example

* DESCRIPTION
The *wcsncat*() function is the wide-character equivalent of the
*strncat*(3) function. It copies at most /n/ wide characters from the
wide-character string pointed to by /src/ to the end of the
wide-character string pointed to by /dest/, and adds a terminating null
wide character (L'\0').

The strings may not overlap.

The programmer must ensure that there is room for at least
/wcslen(dest)/+/n/+1 wide characters at /dest/.

* RETURN VALUE
*wcsncat*() returns /dest/.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface   | Attribute     | Value   |
| *wcsncat*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C99.

* SEE ALSO
*strncat*(3), *wcscat*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
