#+TITLE: Manpages - EVP_PKEY_set1_RSA.3ssl
#+DESCRIPTION: Linux manpage for EVP_PKEY_set1_RSA.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_PKEY_set1_RSA, EVP_PKEY_set1_DSA, EVP_PKEY_set1_DH,
EVP_PKEY_set1_EC_KEY, EVP_PKEY_get1_RSA, EVP_PKEY_get1_DSA,
EVP_PKEY_get1_DH, EVP_PKEY_get1_EC_KEY, EVP_PKEY_get0_RSA,
EVP_PKEY_get0_DSA, EVP_PKEY_get0_DH, EVP_PKEY_get0_EC_KEY,
EVP_PKEY_assign_RSA, EVP_PKEY_assign_DSA, EVP_PKEY_assign_DH,
EVP_PKEY_assign_EC_KEY, EVP_PKEY_assign_POLY1305,
EVP_PKEY_assign_SIPHASH, EVP_PKEY_get0_hmac, EVP_PKEY_get0_poly1305,
EVP_PKEY_get0_siphash, EVP_PKEY_type, EVP_PKEY_id, EVP_PKEY_base_id,
EVP_PKEY_set_alias_type, EVP_PKEY_set1_engine, EVP_PKEY_get0_engine -
EVP_PKEY assignment functions

* SYNOPSIS
#include <openssl/evp.h> int EVP_PKEY_set1_RSA(EVP_PKEY *pkey, RSA
*key); int EVP_PKEY_set1_DSA(EVP_PKEY *pkey, DSA *key); int
EVP_PKEY_set1_DH(EVP_PKEY *pkey, DH *key); int
EVP_PKEY_set1_EC_KEY(EVP_PKEY *pkey, EC_KEY *key); RSA
*EVP_PKEY_get1_RSA(EVP_PKEY *pkey); DSA *EVP_PKEY_get1_DSA(EVP_PKEY
*pkey); DH *EVP_PKEY_get1_DH(EVP_PKEY *pkey); EC_KEY
*EVP_PKEY_get1_EC_KEY(EVP_PKEY *pkey); const unsigned char
*EVP_PKEY_get0_hmac(const EVP_PKEY *pkey, size_t *len); const unsigned
char *EVP_PKEY_get0_poly1305(const EVP_PKEY *pkey, size_t *len); const
unsigned char *EVP_PKEY_get0_siphash(const EVP_PKEY *pkey, size_t *len);
RSA *EVP_PKEY_get0_RSA(EVP_PKEY *pkey); DSA *EVP_PKEY_get0_DSA(EVP_PKEY
*pkey); DH *EVP_PKEY_get0_DH(EVP_PKEY *pkey); EC_KEY
*EVP_PKEY_get0_EC_KEY(EVP_PKEY *pkey); int EVP_PKEY_assign_RSA(EVP_PKEY
*pkey, RSA *key); int EVP_PKEY_assign_DSA(EVP_PKEY *pkey, DSA *key); int
EVP_PKEY_assign_DH(EVP_PKEY *pkey, DH *key); int
EVP_PKEY_assign_EC_KEY(EVP_PKEY *pkey, EC_KEY *key); int
EVP_PKEY_assign_POLY1305(EVP_PKEY *pkey, ASN1_OCTET_STRING *key); int
EVP_PKEY_assign_SIPHASH(EVP_PKEY *pkey, ASN1_OCTET_STRING *key); int
EVP_PKEY_id(const EVP_PKEY *pkey); int EVP_PKEY_base_id(const EVP_PKEY
*pkey); int EVP_PKEY_type(int type); int
EVP_PKEY_set_alias_type(EVP_PKEY *pkey, int type); ENGINE
*EVP_PKEY_get0_engine(const EVP_PKEY *pkey); int
EVP_PKEY_set1_engine(EVP_PKEY *pkey, ENGINE *engine);

* DESCRIPTION
*EVP_PKEY_set1_RSA()*, *EVP_PKEY_set1_DSA()*, *EVP_PKEY_set1_DH()* and
*EVP_PKEY_set1_EC_KEY()* set the key referenced by *pkey* to *key*.

*EVP_PKEY_get1_RSA()*, *EVP_PKEY_get1_DSA()*, *EVP_PKEY_get1_DH()* and
*EVP_PKEY_get1_EC_KEY()* return the referenced key in *pkey* or *NULL*
if the key is not of the correct type.

*EVP_PKEY_get0_hmac()*, *EVP_PKEY_get0_poly1305()*,
*EVP_PKEY_get0_siphash()*, *EVP_PKEY_get0_RSA()*, *EVP_PKEY_get0_DSA()*,
*EVP_PKEY_get0_DH()* and *EVP_PKEY_get0_EC_KEY()* also return the
referenced key in *pkey* or *NULL* if the key is not of the correct type
but the reference count of the returned key is *not* incremented and so
must not be freed up after use.

*EVP_PKEY_assign_RSA()*, *EVP_PKEY_assign_DSA()*,
*EVP_PKEY_assign_DH()*, *EVP_PKEY_assign_EC_KEY()*,
*EVP_PKEY_assign_POLY1305()* and *EVP_PKEY_assign_SIPHASH()* also set
the referenced key to *key* however these use the supplied *key*
internally and so *key* will be freed when the parent *pkey* is freed.

*EVP_PKEY_base_id()* returns the type of *pkey*. For example an RSA key
will return *EVP_PKEY_RSA*.

*EVP_PKEY_id()* returns the actual OID associated with *pkey*.
Historically keys using the same algorithm could use different OIDs. For
example an RSA key could use the OIDs corresponding to the NIDs
*NID_rsaEncryption* (equivalent to *EVP_PKEY_RSA*) or *NID_rsa*
(equivalent to *EVP_PKEY_RSA2*). The use of alternative non-standard
OIDs is now rare so *EVP_PKEY_RSA2* et al are not often seen in
practice.

*EVP_PKEY_type()* returns the underlying type of the NID *type*. For
example EVP_PKEY_type(EVP_PKEY_RSA2) will return *EVP_PKEY_RSA*.

*EVP_PKEY_get0_engine()* returns a reference to the ENGINE handling
*pkey*.

*EVP_PKEY_set1_engine()* sets the ENGINE handling *pkey* to *engine*. It
must be called after the key algorithm and components are set up. If
*engine* does not include an *EVP_PKEY_METHOD* for *pkey* an error
occurs.

*EVP_PKEY_set_alias_type()* allows modifying a EVP_PKEY to use a
different set of algorithms than the default. This is currently used to
support SM2 keys, which use an identical encoding to ECDSA.

* NOTES
In accordance with the OpenSSL naming convention the key obtained from
or assigned to the *pkey* using the *1* functions must be freed as well
as *pkey*.

*EVP_PKEY_assign_RSA()*, *EVP_PKEY_assign_DSA()*,
*EVP_PKEY_assign_DH()*, *EVP_PKEY_assign_EC_KEY()*,
*EVP_PKEY_assign_POLY1305()* and *EVP_PKEY_assign_SIPHASH()* are
implemented as macros.

Most applications wishing to know a key type will simply call
*EVP_PKEY_base_id()* and will not care about the actual type: which will
be identical in almost all cases.

Previous versions of this document suggested using
EVP_PKEY_type(pkey->type) to determine the type of a key. Since
*EVP_PKEY* is now opaque this is no longer possible: the equivalent is
EVP_PKEY_base_id(pkey).

*EVP_PKEY_set1_engine()* is typically used by an ENGINE returning an HSM
key as part of its routine to load a private key.

* RETURN VALUES
*EVP_PKEY_set1_RSA()*, *EVP_PKEY_set1_DSA()*, *EVP_PKEY_set1_DH()* and
*EVP_PKEY_set1_EC_KEY()* return 1 for success or 0 for failure.

*EVP_PKEY_get1_RSA()*, *EVP_PKEY_get1_DSA()*, *EVP_PKEY_get1_DH()* and
*EVP_PKEY_get1_EC_KEY()* return the referenced key or *NULL* if an error
occurred.

*EVP_PKEY_assign_RSA()*, *EVP_PKEY_assign_DSA()*,
*EVP_PKEY_assign_DH()*, *EVP_PKEY_assign_EC_KEY()*,
*EVP_PKEY_assign_POLY1305()* and *EVP_PKEY_assign_SIPHASH()* return 1
for success and 0 for failure.

*EVP_PKEY_base_id()*, *EVP_PKEY_id()* and *EVP_PKEY_type()* return a key
type or *NID_undef* (equivalently *EVP_PKEY_NONE*) on error.

*EVP_PKEY_set1_engine()* returns 1 for success and 0 for failure.

*EVP_PKEY_set_alias_type()* returns 1 for success and 0 for error.

* EXAMPLES
After loading an ECC key, it is possible to convert it to using SM2
algorithms with EVP_PKEY_set_alias_type:

EVP_PKEY_set_alias_type(pkey, EVP_PKEY_SM2);

* SEE ALSO
*EVP_PKEY_new* (3)

* COPYRIGHT
Copyright 2002-2019 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
