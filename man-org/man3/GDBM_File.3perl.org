#+TITLE: Manpages - GDBM_File.3perl
#+DESCRIPTION: Linux manpage for GDBM_File.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
GDBM_File - Perl5 access to the gdbm library.

* SYNOPSIS
use GDBM_File; [$db =] tie %hash, GDBM_File, $filename, &GDBM_WRCREAT,
0640; # Use the %hash array. $e = $db->errno; $e = $db->syserrno; $str =
$db->strerror; $bool = $db->needs_recovery; $db->clear_error;
$db->reorganize; $db->sync; $n = $db->count; $n = $db->flags; $str =
$db->dbname; $db->cache_size; $db->cache_size($newsize); $n =
$db->block_size; $bool = $db->sync_mode; $db->sync_mode($bool); $bool =
$db->centfree; $db->centfree($bool); $bool = $db->coalesce;
$db->coalesce($bool); $bool = $db->mmap; $size = $db->mmapsize;
$db->mmapsize($newsize); $db->recover(%args); untie %hash ;

* DESCRIPTION
*GDBM_File* is a module which allows Perl programs to make use of the
facilities provided by the GNU gdbm library. If you intend to use this
module you should really have a copy of the gdbm manualpage at hand.

Most of the libgdbm.a functions are available through the GDBM_File
interface.

Unlike Perl's built-in hashes, it is not safe to =delete= the current
item from a GDBM_File tied hash while iterating over it with =each=.
This is a limitation of the gdbm library.

* STATIC METHODS
** GDBM_version
$str = GDBM_File->GDBM_version; @ar = GDBM_File->GDBM_version;

Returns the version number of the underlying *libgdbm* library. In
scalar context, returns the library version formatted as string:

MINOR.MAJOR[.PATCH][ (GUESS)]

where /MINOR/, /MAJOR/, and /PATCH/ are version numbers, and /GUESS/ is
a guess level (see below).

In list context, returns a list:

( MINOR, MAJOR, PATCH [, GUESS] )

The /GUESS/ component is present only if *libgdbm* version is 1.8.3 or
earlier. This is because earlier releases of *libgdbm* did not include
information about their version and the *GDBM_File* module has to
implement certain guesswork in order to determine it. /GUESS/ is a
textual description in string context, and a positive number indicating
how rough the guess is in list context. Possible values are:

- 1 - exact guess :: The major and minor version numbers are guaranteed
  to be correct. The actual patchlevel is most probably guessed right,
  but can be 1-2 less than indicated.

- 2 - approximate :: The major and minor number are guaranteed to be
  correct. The patchlevel is set to the upper bound.

- 3 - rough guess :: The version is guaranteed to be not newer than
  */MAJOR/./MINOR/*.

* METHODS
** close
$db->close;

Closes the database. You are not advised to use this method directly.
Please, use *untie* instead.

** errno
$db->errno

Returns the last error status associated with this database.

** syserrno
$db->syserrno

Returns the last system error status (C =errno= variable), associated
with this database,

** strerror
$db->strerror

Returns textual description of the last error that occurred in this
database.

** clear_error
$db->clear_error

Clear error status.

** needs_recovery
$db->needs_recovery

Returns true if the database needs recovery.

** reorganize
$db->reorganize;

Reorganizes the database.

** sync
$db->sync;

Synchronizes recent changes to the database with its disk copy.

** count
$n = $db->count;

Returns number of keys in the database.

** flags
$db->flags;

Returns flags passed as 4th argument to *tie*.

** dbname
$db->dbname;

Returns the database name (i.e. 3rd argument to *tie*.

** cache_size
$db->cache_size; $db->cache_size($newsize);

Returns the size of the internal *GDBM* cache for that database.

Called with argument, sets the size to /=$newsize=/.

** block_size
$db->block_size;

Returns the block size of the database.

** sync_mode
$db->sync_mode; $db->sync_mode($bool);

Returns the status of the automatic synchronization mode. Called with
argument, enables or disables the sync mode, depending on whether
=$bool= is *true* or *false*.

When synchronization mode is on (*true*), any changes to the database
are immediately written to the disk. This ensures database consistency
in case of any unforeseen errors (e.g. power failures), at the expense
of considerable slowdown of operation.

Synchronization mode is off by default.

** centfree
$db->centfree; $db->centfree($bool);

Returns status of the central free block pool (*0* - disabled, *1* -
enabled).

With argument, changes its status.

By default, central free block pool is disabled.

** coalesce
$db->coalesce; $db->coalesce($bool);

** mmap
$db->mmap;

Returns true if memory mapping is enabled.

This method will *croak* if the *libgdbm* library is complied without
memory mapping support.

** mmapsize
$db->mmapsize; $db->mmapsize($newsize);

If memory mapping is enabled, returns the size of memory mapping. With
argument, sets the size to *=$newsize=*.

This method will *croak* if the *libgdbm* library is complied without
memory mapping support.

** recover
$db->recover(%args);

Recovers data from a failed database. *=%args=* is optional and can
contain following keys:

- err => sub { ... } :: Reference to code for detailed error reporting.
  Upon encountering an error, *recover* will call this sub with a single
  argument - a description of the error.

- backup => \$str :: Creates a backup copy of the database before
  recovery and returns its filename in *=$str=*.

- max_failed_keys => $n :: Maximum allowed number of failed keys. If the
  actual number becomes equal to /=$n=/, *recover* aborts and returns
  error.

- max_failed_buckets => $n :: Maximum allowed number of failed buckets.
  If the actual number becomes equal to /=$n=/, *recover* aborts and
  returns error.

- max_failures => $n :: Maximum allowed number of failures during
  recovery.

- stat => \%hash :: Return recovery statistics in /=%hash=/. Upon
  return, the following keys will be present:

  - recovered_keys :: Number of successfully recovered keys.

  - recovered_buckets :: Number of successfully recovered buckets.

  - failed_keys :: Number of keys that failed to be retrieved.

  - failed_buckets :: Number of buckets that failed to be retrieved.

* AVAILABILITY
gdbm is available from any GNU archive. The master site is
=ftp.gnu.org=, but you are strongly urged to use one of the many
mirrors. You can obtain a list of mirror sites from
<http://www.gnu.org/order/ftp.html>.

* SECURITY AND PORTABILITY
*Do not accept GDBM files from untrusted sources.*

GDBM files are not portable across platforms.

The GDBM documentation doesn't imply that files from untrusted sources
can be safely used with =libgdbm=.

A maliciously crafted file might cause perl to crash or even expose a
security vulnerability.

* SEE ALSO
*perl* (1), *DB_File* (3), perldbmfilter, *gdbm* (3),
<https://www.gnu.org.ua/software/gdbm/manual.html>.
