#+TITLE: Manpages - zip_error_init_with_code.3
#+DESCRIPTION: Linux manpage for zip_error_init_with_code.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function initializes the zip_error pointed to by

must be allocated before calling

The

function does the same, but additionally sets the zip error code to

and sets the system error code to the current

value, if appropriate.

and

were added in libzip 1.0.

and
