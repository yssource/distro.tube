#+TITLE: Manpages - db.3
#+DESCRIPTION: Linux manpage for db.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about db.3 is found in manpage for: [[../man3/dbopen.3][man3/dbopen.3]]