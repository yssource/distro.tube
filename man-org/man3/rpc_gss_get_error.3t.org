#+TITLE: Manpages - rpc_gss_get_error.3t
#+DESCRIPTION: Linux manpage for rpc_gss_get_error.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Get details of the last RPCSEC_GSS error.

A pointer to a structure where the error details will be returned

The

function is part of libtirpc.

This manual page was written by
