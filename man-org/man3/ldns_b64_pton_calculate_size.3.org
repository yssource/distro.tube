#+TITLE: Manpages - ldns_b64_pton_calculate_size.3
#+DESCRIPTION: Linux manpage for ldns_b64_pton_calculate_size.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_b32_ntop_calculate_size, ldns_b32_pton_calculate_size,
ldns_b64_ntop_calculate_size, ldns_b64_pton_calculate_size - return size
needed for b32 or b64 encoded or decoded data

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_b32_ntop_calculate_size();

ldns_b32_pton_calculate_size();

ldns_b64_ntop_calculate_size();

ldns_b64_pton_calculate_size();

* DESCRIPTION
/ldns_b32_ntop_calculate_size/()

/ldns_b32_pton_calculate_size/()

/ldns_b64_ntop_calculate_size/()

/ldns_b64_pton_calculate_size/()

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
