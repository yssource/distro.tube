#+TITLE: Manpages - OpenMPI.3
#+DESCRIPTION: Linux manpage for OpenMPI.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about OpenMPI.3 is found in manpage for: [[../man3/MPI.3][man3/MPI.3]]