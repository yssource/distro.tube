#+TITLE: Manpages - ne_strparam.3
#+DESCRIPTION: Linux manpage for ne_strparam.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ne_strparam - HTTP extended parameter value encoding

* SYNOPSIS
#+begin_example
  #include <ne_string.h>
#+end_example

*char *ne_strparam(const char **/charset/*, const char **/lang/*, const
char **/value/*);*

* DESCRIPTION
The *ne_strparam* function can be used to encode an extended parameter
value for an HTTP header, as defined in RFC 5987. The function takes as
arguments the /value/ to encode, using a given MIME /charset/ character
set, and /lang/ language tag. The extended parameter encoding is used in
HTTP protocol specifications to allow easily embedding special
characters (such as quote marks, separators or non-ASCII) in header
values.

In accordance with RFC 5987, the /charset/ argument must be either
*"UTF-8"* or *"IS0-8859-1"*, but the /lang/ argument may be NULL.

* RETURN VALUE
The return value is either:

NULL

#+begin_quote
  if the value parameter is a "regular parameter" and does not need
  extended encoding
#+end_quote

non-NULL

#+begin_quote
  the encoding of the input value as an extended parameter as a
  NUL-terminated, malloc-allocated string
#+end_quote

* SEE ALSO
*https://tools.ietf.org/html/rfc5987*

* AUTHOR
*Joe Orton* <neon@lists.manyfish.co.uk>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
