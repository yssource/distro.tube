#+TITLE: Manpages - xcb_xevie_query_version_reply.3
#+DESCRIPTION: Linux manpage for xcb_xevie_query_version_reply.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_xevie_query_version_reply.3 is found in manpage for: [[../man3/xcb_xevie_query_version.3][man3/xcb_xevie_query_version.3]]