#+TITLE: Manpages - __gnu_internal.3
#+DESCRIPTION: Linux manpage for __gnu_internal.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_internal - GNU implemenation details, not for public use or
export. Used only when anonymous namespaces cannot be substituted.

* SYNOPSIS
\\

* Detailed Description
GNU implemenation details, not for public use or export. Used only when
anonymous namespaces cannot be substituted.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
