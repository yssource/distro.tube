#+TITLE: Manpages - SDL_PollEvent.3
#+DESCRIPTION: Linux manpage for SDL_PollEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_PollEvent - Polls for currently pending events.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_PollEvent*(*SDL_Event *event*);

* DESCRIPTION
Polls for currently pending events, and returns *1* if there are any
pending events, or *0* if there are none available.

If *event* is not *NULL*, the next event is removed from the queue and
stored in that area.

* EXAMPLES
#+begin_example
  SDL_Event event; /* Event structure */

  .
  .
  .
  /* Check for events */
  while(SDL_PollEvent(&event)){  /* Loop until there are no events left on the queue */
    switch(event.type){  /* Process the appropiate event type */
      case SDL_KEYDOWN:  /* Handle a KEYDOWN event */         
        printf("Oh! Key press
  ");
        break;
      case SDL_MOUSEMOTION:
        .
        .
        .
      default: /* Report an unhandled event */
        printf("I don't know what this event is!
  ");
    }
  }
#+end_example

* SEE ALSO
*SDL_Event*, *SDL_WaitEvent*, *SDL_PeepEvents*
