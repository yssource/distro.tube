#+TITLE: Manpages - pcap_set_snaplen.3pcap
#+DESCRIPTION: Linux manpage for pcap_set_snaplen.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_set_snaplen - set the snapshot length for a not-yet-activated
capture handle

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_set_snaplen(pcap_t *p, int snaplen);
#+end_example

* DESCRIPTION
*pcap_set_snaplen*() sets the snapshot length to be used on a capture
handle when the handle is activated to /snaplen/.

* RETURN VALUE
*pcap_set_snaplen*() returns *0* on success or *PCAP_ERROR_ACTIVATED* if
called on a capture handle that has been activated.

* SEE ALSO
*pcap*(3PCAP), *pcap_create*(3PCAP), *pcap_activate*(3PCAP)
