#+TITLE: Manpages - XtIsWidget.3
#+DESCRIPTION: Linux manpage for XtIsWidget.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtIsWidget.3 is found in manpage for: [[../man3/XtClass.3][man3/XtClass.3]]