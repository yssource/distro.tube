#+TITLE: Manpages - Test2_Event_Skip.3perl
#+DESCRIPTION: Linux manpage for Test2_Event_Skip.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::Event::Skip - Skip event type

* DESCRIPTION
Skip events bump test counts just like Test2::Event::Ok events, but they
can never fail.

* SYNOPSIS
use Test2::API qw/context/; use Test2::Event::Skip; my $ctx = context();
my $event = $ctx->skip($name, $reason);

or:

my $ctx = context(); my $event = $ctx->send_event( Skip, name => $name,
reason => $reason, );

* ACCESSORS
- $reason = $e->reason :: The original true/false value of whatever was
  passed into the event (but reduced down to 1 or 0).

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://www.perl.com/perl/misc/Artistic.html/
