#+TITLE: Manpages - XSetDeviceButtonMapping.3
#+DESCRIPTION: Linux manpage for XSetDeviceButtonMapping.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XSetDeviceButtonMapping, XGetDeviceButtonMapping - query or change
device button mappings

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  int XSetDeviceButtonMapping( Display *display,
                               XDevice *device,
                               unsigned char map[],
                               int nmap);
#+end_example

#+begin_example
  int XGetDeviceButtonMapping( Display *display,
                               XDevice *device,
                               unsigned char map_return[],
                               int nmap);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  device
         Specifies the device whose button mapping is to be
         queried or changed.
#+end_example

#+begin_example
  map
         Specifies the mapping list.
#+end_example

#+begin_example
  map_return
         Returns the mapping list.
#+end_example

#+begin_example
  nmap
         Specifies the number of items in the mapping list.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    The XSetDeviceButtonMapping request sets the mapping of the
    specified device. If it succeeds, the X server generates a
    DeviceMappingNotify event, and XSetDeviceButtonMapping returns
    MappingSuccess. Element map[i] defines the logical button
    number for the physical button i+1. The length of the list must
    be the same as XGetDeviceButtonMapping would return, or a
    BadValue error results. A zero element disables a button, and
    elements are not restricted in value by the number of physical
    buttons. However, no two elements can have the same nonzero
    value, or a BadValue error results. If any of the buttons to be
    altered are logically in the down state,
    XSetDeviceButtonMapping returns MappingBusy, and the mapping is
    not changed.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XSetDeviceButtonMapping can generate BadDevice, BadMatch, and
    BadValue errors.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XGetDeviceButtonMapping request returns the current mapping
    of the specified device. Buttons are numbered starting from
    one. XGetDeviceButtonMapping returns the number of physical
    buttons actually on the device. The nominal mapping for a
    device is map[i]=i+1. The nmap argument specifies the length of
    the array where the device mapping is returned, and only the
    first nmap elements are returned in map_return.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XGetDeviceButtonMapping can generate BadDevice or BadMatch
    errors.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The specified device
           does not exist or has not been opened by this client via
           XOpenInputDevice. This error may also occur if the
           specified device is the X keyboard or X pointer device.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadMatch
           This error may occur if an XGetDeviceButtonMapping or
           XSetDeviceButtonMapping request was made specifying a
           device that has no buttons.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadValue
           Some numeric value falls outside the range of values
           accepted by the request. Unless a specific range is
           specified for an argument, the full range defined by the
           arguments type is accepted. Any argument defined as a
           set of alternatives can generate this error.
  #+end_example
#+end_quote

* SEE ALSO

#+begin_quote
  #+begin_example
    XChangeDeviceKeyboardControl(3), XChangeDeviceKeyMapping(3),
    XChangeDeviceModifierMapping(3)
  #+end_example
#+end_quote
