#+TITLE: Manpages - XtIsSubclass.3
#+DESCRIPTION: Linux manpage for XtIsSubclass.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtIsSubclass.3 is found in manpage for: [[../man3/XtClass.3][man3/XtClass.3]]