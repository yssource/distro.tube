#+TITLE: Manpages - acl_init.3
#+DESCRIPTION: Linux manpage for acl_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function allocates and initializes the working storage for an ACL of at
least

ACL entries. The ACL created initially contains no ACL entries. A
pointer to the working storage is returned.

This function may cause memory to be allocated. The caller should free
any releasable memory, when the new ACL is no longer required, by
calling

with the

returned by

as an argument.

On success, this function returns a pointer to the working storage. On
error, a value of

is returned, and

is set appropriately.

If any of the following conditions occur, the

function returns a value of

and sets

to the corresponding value:

The value of count is less than zero.

The

to be returned requires more memory than is allowed by the hardware or
system-imposed memory management constraints.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
