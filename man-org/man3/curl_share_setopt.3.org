#+TITLE: Manpages - curl_share_setopt.3
#+DESCRIPTION: Linux manpage for curl_share_setopt.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_share_setopt - Set options for a shared object

* SYNOPSIS
*#include <curl/curl.h>*

CURLSHcode curl_share_setopt(CURLSH *share, CURLSHoption option,
parameter);

* DESCRIPTION
Set the /option/ to /parameter/ for the given /share/.

* OPTIONS
- CURLSHOPT_LOCKFUNC :: The /parameter/ must be a pointer to a function
  matching the following prototype:

void lock_function(CURL *handle, curl_lock_data data, curl_lock_access
access, void *userptr);

The /data/ argument tells what kind of data libcurl wants to lock. Make
sure that the callback uses a different lock for each kind of data.

/access/ defines what access type libcurl wants, shared or single.

/userptr/ is the pointer you set with /CURLSHOPT_USERDATA/.

- CURLSHOPT_UNLOCKFUNC :: The /parameter/ must be a pointer to a
  function matching the following prototype:

void unlock_function(CURL *handle, curl_lock_data data, void *userptr);

/data/ defines what data libcurl wants to unlock, and you must make sure
that only one lock is given at any time for each kind of data.

/userptr/ is the pointer you set with /CURLSHOPT_USERDATA/.

- CURLSHOPT_SHARE :: The /parameter/ specifies a type of data that
  should be shared. This may be set to one of the values described
  below.

  - CURL_LOCK_DATA_COOKIE :: Cookie data will be shared across the easy
    handles using this shared object. Note that this does not activate
    an easy handle's cookie handling. You can do that separately by
    using /CURLOPT_COOKIEFILE(3)/ for example.

  - CURL_LOCK_DATA_DNS :: Cached DNS hosts will be shared across the
    easy handles using this shared object. Note that when you use the
    multi interface, all easy handles added to the same multi handle
    will share DNS cache by default without using this option.

  - CURL_LOCK_DATA_SSL_SESSION :: SSL session IDs will be shared across
    the easy handles using this shared object. This will reduce the time
    spent in the SSL handshake when reconnecting to the same server.
    Note SSL session IDs are reused within the same easy handle by
    default. Note this symbol was added in 7.10.3 but was not
    implemented until 7.23.0.

  - CURL_LOCK_DATA_CONNECT :: Put the connection cache in the share
    object and make all easy handles using this share object share the
    connection cache.

  Note that due to a known bug, it is not safe to share connections this
  way between multiple concurrent threads.

  Connections that are used for HTTP/1.1 Pipelining or HTTP/2
  multiplexing only get additional transfers added to them if the
  existing connection is held by the same multi or easy handle. libcurl
  does not support doing HTTP/2 streams in different threads using a
  shared connection.

  Support for *CURL_LOCK_DATA_CONNECT* was added in 7.57.0, but the
  symbol existed before this.

  Note that when you use the multi interface, all easy handles added to
  the same multi handle will share connection cache by default without
  using this option.

  - CURL_LOCK_DATA_PSL :: The Public Suffix List stored in the share
    object is made available to all easy handle bound to the later.
    Since the Public Suffix List is periodically refreshed, this avoids
    updates in too many different contexts.

  *CURL_LOCK_DATA_PSL* exists since 7.61.0.

  Note that when you use the multi interface, all easy handles added to
  the same multi handle will share PSL cache by default without using
  this option.

- CURLSHOPT_UNSHARE :: This option does the opposite of
  /CURLSHOPT_SHARE/. It specifies that the specified /parameter/ will no
  longer be shared. Valid values are the same as those for
  /CURLSHOPT_SHARE/.

- CURLSHOPT_USERDATA :: The /parameter/ allows you to specify a pointer
  to data that will be passed to the lock_function and unlock_function
  each time it is called.

* EXAMPLE
#+begin_example
    CURLSHcode sh
    share = curl_share_init();
    sh = curl_share_setopt(share, CURLSHOPT_SHARE, CURL_LOCK_DATA_CONNECT);
    if(sh)
      printf("Error: %s\n", curl_share_strerror(sh));
#+end_example

* AVAILABILITY
Added in 7.10

* RETURN VALUE
CURLSHE_OK (zero) means that the option was set properly, non-zero means
an error occurred as /<curl/curl.h>/ defines. See the /libcurl-errors.3/
man page for the full list with descriptions.

* SEE ALSO
*curl_share_cleanup*(3), *curl_share_init*(3)
