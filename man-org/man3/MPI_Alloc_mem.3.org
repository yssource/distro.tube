#+TITLE: Manpages - MPI_Alloc_mem.3
#+DESCRIPTION: Linux manpage for MPI_Alloc_mem.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Alloc_mem * - Allocates a specified memory segment.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Alloc_mem(MPI_Aint size, MPI_Info info, void *baseptr)
#+end_example

* Fortran Syntax (see FORTRAN NOTES)
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_ALLOC_MEM(SIZE, INFO, BASEPTR, IERROR)
  	INTEGER INFO, IERROR
  	INTEGER(KIND=MPI_ADDRESS_KIND) SIZE, BASEPTR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Alloc_mem(size, info, baseptr, ierror)
  	USE, INTRINSIC :: ISO_C_BINDING, ONLY 
  	INTEGER(KIND=MPI_ADDRESS_KIND), INTENT(IN) :: size
  	TYPE(MPI_Info), INTENT(IN) :: info
  	TYPE(C_PTR), INTENT(OUT) :: baseptr
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void* MPI::Alloc_mem(MPI::Aint size, const MPI::Info& info)
#+end_example

* INPUT PARAMETERS
- size :: Size of memory segment in bytes (nonnegative integer).

- info :: Info argument (handle).

* OUTPUT PARAMETERS
- baseptr :: Pointer to beginning of memory segment allocated.

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_Alloc_mem allocates /size/ bytes of memory. The starting address of
this memory is returned in the variable /base/.

* FORTRAN NOTES
There is no portable FORTRAN 77 syntax for using MPI_Alloc_mem. There is
no portable Fortran syntax for using pointers returned from
MPI_Alloc_mem. However, MPI_Alloc_mem can be used with Sun Fortran
compilers.

From FORTRAN 77, you can use the following non-standard declarations for
the SIZE and BASEPTR arguments:

#+begin_example
             INCLUDE "mpif.h"
             INTEGER*MPI_ADDRESS_KIND SIZE, BASEPTR
#+end_example

From either FORTRAN 77 or Fortran 90, you can use "Cray pointers" for
the BASEPTR argument. Cray pointers are described further in the Fortran
User's Guide and are supported by many Fortran compilers. For example,

#+begin_example
             INCLUDE "mpif.h"
             REAL*4 A(100,100)
             POINTER (BASEPTR, A)
             INTEGER*MPI_ADDRESS_KIND SIZE

             SIZE = 4 * 100 * 100
             CALL MPI_ALLOC_MEM(SIZE,MPI_INFO_NULL,BASEPTR,IERR)

             ! use A

             CALL MPI_FREE_MEM(A, IERR)
#+end_example

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

* SEE ALSO
MPI_Free_mem
