#+TITLE: Manpages - SDL_CondSignal.3
#+DESCRIPTION: Linux manpage for SDL_CondSignal.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CondSignal - Restart a thread wait on a condition variable

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*int SDL_CondSignal*(*SDL_cond *cond*);

* DESCRIPTION
Restart one of the threads that are waiting on the condition variable,
*cond*. Returns *0* on success of *-1* on an error.

* SEE ALSO
*SDL_CondWait*, *SDL_CondBroadcast*
