#+TITLE: Manpages - arc4random.3bsd
#+DESCRIPTION: Linux manpage for arc4random.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

This family of functions provides higher quality data than those
described in

and

Use of these functions is encouraged for almost all random number
consumption because the other interfaces are deficient in either
quality, portability, standardization, or availability. These functions
can be called in almost all coding environments, including

and

High quality 32-bit pseudo-random numbers are generated very quickly. On
each call, a cryptographic pseudo-random number generator is used to
generate a new result. One data pool is used for all consumers in a
process, so that consumption under program flow can act as additional
stirring. The subsystem is re-seeded from the kernel random number
subsystem using

on a regular basis, and also upon

The

function returns a single 32-bit value.

The

function fills the region

of length

with random data.

will return a single 32-bit value, uniformly distributed but less than

This is recommended over constructions like

as it avoids "modulo bias" when the upper bound is not a power of two.
In the worst case, this function may consume multiple iterations to
ensure uniformity; see the source code to understand the problem and
solution.

The

function reads data from

and uses it to re-seed the subsystem via

There is no need to call

before using

functions family, since they automatically initialize themselves.

These functions are always successful, and no return value is reserved
to indicate an error.

These functions first appeared in

and

The original version of this random number generator used the RC4 (also
known as ARC4) algorithm. In

it was replaced with the ChaCha20 cipher, and it may be replaced again
in the future as cryptographic techniques advance. A good mnemonic is
