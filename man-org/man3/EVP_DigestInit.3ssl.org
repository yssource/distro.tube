#+TITLE: Manpages - EVP_DigestInit.3ssl
#+DESCRIPTION: Linux manpage for EVP_DigestInit.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_MD_CTX_new, EVP_MD_CTX_reset, EVP_MD_CTX_free, EVP_MD_CTX_copy,
EVP_MD_CTX_copy_ex, EVP_MD_CTX_ctrl, EVP_MD_CTX_set_flags,
EVP_MD_CTX_clear_flags, EVP_MD_CTX_test_flags, EVP_Digest,
EVP_DigestInit_ex, EVP_DigestInit, EVP_DigestUpdate, EVP_DigestFinal_ex,
EVP_DigestFinalXOF, EVP_DigestFinal, EVP_MD_type, EVP_MD_pkey_type,
EVP_MD_size, EVP_MD_block_size, EVP_MD_flags, EVP_MD_CTX_md,
EVP_MD_CTX_type, EVP_MD_CTX_size, EVP_MD_CTX_block_size,
EVP_MD_CTX_md_data, EVP_MD_CTX_update_fn, EVP_MD_CTX_set_update_fn,
EVP_md_null, EVP_get_digestbyname, EVP_get_digestbynid,
EVP_get_digestbyobj, EVP_MD_CTX_pkey_ctx, EVP_MD_CTX_set_pkey_ctx - EVP
digest routines

* SYNOPSIS
#include <openssl/evp.h> EVP_MD_CTX *EVP_MD_CTX_new(void); int
EVP_MD_CTX_reset(EVP_MD_CTX *ctx); void EVP_MD_CTX_free(EVP_MD_CTX
*ctx); void EVP_MD_CTX_ctrl(EVP_MD_CTX *ctx, int cmd, int p1, void* p2);
void EVP_MD_CTX_set_flags(EVP_MD_CTX *ctx, int flags); void
EVP_MD_CTX_clear_flags(EVP_MD_CTX *ctx, int flags); int
EVP_MD_CTX_test_flags(const EVP_MD_CTX *ctx, int flags); int
EVP_Digest(const void *data, size_t count, unsigned char *md, unsigned
int *size, const EVP_MD *type, ENGINE *impl); int
EVP_DigestInit_ex(EVP_MD_CTX *ctx, const EVP_MD *type, ENGINE *impl);
int EVP_DigestUpdate(EVP_MD_CTX *ctx, const void *d, size_t cnt); int
EVP_DigestFinal_ex(EVP_MD_CTX *ctx, unsigned char *md, unsigned int *s);
int EVP_DigestFinalXOF(EVP_MD_CTX *ctx, unsigned char *md, size_t len);
int EVP_MD_CTX_copy_ex(EVP_MD_CTX *out, const EVP_MD_CTX *in); int
EVP_DigestInit(EVP_MD_CTX *ctx, const EVP_MD *type); int
EVP_DigestFinal(EVP_MD_CTX *ctx, unsigned char *md, unsigned int *s);
int EVP_MD_CTX_copy(EVP_MD_CTX *out, EVP_MD_CTX *in); int
EVP_MD_type(const EVP_MD *md); int EVP_MD_pkey_type(const EVP_MD *md);
int EVP_MD_size(const EVP_MD *md); int EVP_MD_block_size(const EVP_MD
*md); unsigned long EVP_MD_flags(const EVP_MD *md); const EVP_MD
*EVP_MD_CTX_md(const EVP_MD_CTX *ctx); int EVP_MD_CTX_size(const
EVP_MD_CTX *ctx); int EVP_MD_CTX_block_size(const EVP_MD_CTX *ctx); int
EVP_MD_CTX_type(const EVP_MD_CTX *ctx); void *EVP_MD_CTX_md_data(const
EVP_MD_CTX *ctx); int (*EVP_MD_CTX_update_fn(EVP_MD_CTX
*ctx))(EVP_MD_CTX *ctx, const void *data, size_t count); void
EVP_MD_CTX_set_update_fn(EVP_MD_CTX *ctx, int (*update)(EVP_MD_CTX *ctx,
const void *data, size_t count)); const EVP_MD *EVP_md_null(void); const
EVP_MD *EVP_get_digestbyname(const char *name); const EVP_MD
*EVP_get_digestbynid(int type); const EVP_MD *EVP_get_digestbyobj(const
ASN1_OBJECT *o); EVP_PKEY_CTX *EVP_MD_CTX_pkey_ctx(const EVP_MD_CTX
*ctx); void EVP_MD_CTX_set_pkey_ctx(EVP_MD_CTX *ctx, EVP_PKEY_CTX
*pctx);

* DESCRIPTION
The EVP digest routines are a high-level interface to message digests,
and should be used instead of the cipher-specific functions.

- EVP_MD_CTX_new() :: Allocates and returns a digest context.

- EVP_MD_CTX_reset() :: Resets the digest context *ctx*. This can be
  used to reuse an already existing context.

- EVP_MD_CTX_free() :: Cleans up digest context *ctx* and frees up the
  space allocated to it.

- EVP_MD_CTX_ctrl() :: Performs digest-specific control actions on
  context *ctx*. The control command is indicated in *cmd* and any
  additional arguments in *p1* and *p2*. *EVP_MD_CTX_ctrl()* must be
  called after *EVP_DigestInit_ex()*. Other restrictions may apply
  depending on the control type and digest implementation. See CONTROLS
  below for more information.

- EVP_MD_CTX_set_flags(), EVP_MD_CTX_clear_flags(),
  EVP_MD_CTX_test_flags() :: Sets, clears and tests *ctx* flags. See
  FLAGS below for more information.

- EVP_Digest() :: A wrapper around the Digest Init_ex, Update and
  Final_ex functions. Hashes *count* bytes of data at *data* using a
  digest *type* from ENGINE *impl*. The digest value is placed in *md*
  and its length is written at *size* if the pointer is not NULL. At
  most *EVP_MAX_MD_SIZE* bytes will be written. If *impl* is NULL the
  default implementation of digest *type* is used.

- EVP_DigestInit_ex() :: Sets up digest context *ctx* to use a digest
  *type* from ENGINE *impl*. *type* will typically be supplied by a
  function such as *EVP_sha1()*. If *impl* is NULL then the default
  implementation of digest *type* is used.

- EVP_DigestUpdate() :: Hashes *cnt* bytes of data at *d* into the
  digest context *ctx*. This function can be called several times on the
  same *ctx* to hash additional data.

- EVP_DigestFinal_ex() :: Retrieves the digest value from *ctx* and
  places it in *md*. If the *s* parameter is not NULL then the number of
  bytes of data written (i.e. the length of the digest) will be written
  to the integer at *s*, at most *EVP_MAX_MD_SIZE* bytes will be
  written. After calling *EVP_DigestFinal_ex()* no additional calls to
  *EVP_DigestUpdate()* can be made, but *EVP_DigestInit_ex()* can be
  called to initialize a new digest operation.

- EVP_DigestFinalXOF() :: Interfaces to extendable-output functions,
  XOFs, such as SHAKE128 and SHAKE256. It retrieves the digest value
  from *ctx* and places it in *len*-sized <B>md. After calling this
  function no additional calls to *EVP_DigestUpdate()* can be made, but
  *EVP_DigestInit_ex()* can be called to initialize a new operation.

- EVP_MD_CTX_copy_ex() :: Can be used to copy the message digest state
  from *in* to *out*. This is useful if large amounts of data are to be
  hashed which only differ in the last few bytes.

- EVP_DigestInit() :: Behaves in the same way as *EVP_DigestInit_ex()*
  except it always uses the default digest implementation and calls
  *EVP_MD_CTX_reset()*.

- EVP_DigestFinal() :: Similar to *EVP_DigestFinal_ex()* except the
  digest context *ctx* is automatically cleaned up.

- EVP_MD_CTX_copy() :: Similar to *EVP_MD_CTX_copy_ex()* except the
  destination *out* does not have to be initialized.

- EVP_MD_size(), EVP_MD_CTX_size() :: Return the size of the message
  digest when passed an *EVP_MD* or an *EVP_MD_CTX* structure, i.e. the
  size of the hash.

- EVP_MD_block_size(), EVP_MD_CTX_block_size() :: Return the block size
  of the message digest when passed an *EVP_MD* or an *EVP_MD_CTX*
  structure.

- EVP_MD_type(), EVP_MD_CTX_type() :: Return the NID of the OBJECT
  IDENTIFIER representing the given message digest when passed an
  *EVP_MD* structure. For example, =EVP_MD_type(EVP_sha1())= returns
  *NID_sha1*. This function is normally used when setting ASN1 OIDs.

- EVP_MD_CTX_md_data() :: Return the digest method private data for the
  passed *EVP_MD_CTX*. The space is allocated by OpenSSL and has the
  size originally set with *EVP_MD_meth_set_app_datasize()*.

- EVP_MD_CTX_md() :: Returns the *EVP_MD* structure corresponding to the
  passed *EVP_MD_CTX*.

- EVP_MD_CTX_set_update_fn() :: Sets the update function for *ctx* to
  *update*. This is the function that is called by EVP_DigestUpdate. If
  not set, the update function from the *EVP_MD* type specified at
  initialization is used.

- EVP_MD_CTX_update_fn() :: Returns the update function for *ctx*.

- EVP_MD_flags() :: Returns the *md* flags. Note that these are
  different from the *EVP_MD_CTX* ones. See *EVP_MD_meth_set_flags* (3)
  for more information.

- EVP_MD_pkey_type() :: Returns the NID of the public key signing
  algorithm associated with this digest. For example *EVP_sha1()* is
  associated with RSA so this will return *NID_sha1WithRSAEncryption*.
  Since digests and signature algorithms are no longer linked this
  function is only retained for compatibility reasons.

- EVP_md_null() :: A null message digest that does nothing: i.e. the
  hash it returns is of zero length.

- EVP_get_digestbyname(), EVP_get_digestbynid(),
  EVP_get_digestbyobj() :: Returns an *EVP_MD* structure when passed a
  digest name, a digest *NID* or an *ASN1_OBJECT* structure
  respectively.

- EVP_MD_CTX_pkey_ctx() :: Returns the *EVP_PKEY_CTX* assigned to *ctx*.
  The returned pointer should not be freed by the caller.

- EVP_MD_CTX_set_pkey_ctx() :: Assigns an *EVP_PKEY_CTX* to
  *EVP_MD_CTX*. This is usually used to provide a customized
  *EVP_PKEY_CTX* to *EVP_DigestSignInit* (3) or
  *EVP_DigestVerifyInit* (3). The *pctx* passed to this function should
  be freed by the caller. A NULL *pctx* pointer is also allowed to clear
  the *EVP_PKEY_CTX* assigned to *ctx*. In such case, freeing the
  cleared *EVP_PKEY_CTX* or not depends on how the *EVP_PKEY_CTX* is
  created.

* CONTROLS
*EVP_MD_CTX_ctrl()* can be used to send the following standard controls:

- EVP_MD_CTRL_MICALG :: Gets the digest Message Integrity Check
  algorithm string. This is used when creating S/MIME multipart/signed
  messages, as specified in RFC 3851. The string value is written to
  *p2*.

- EVP_MD_CTRL_XOF_LEN :: This control sets the digest length for
  extendable output functions to *p1*. Sending this control directly
  should not be necessary, the use of =EVP_DigestFinalXOF()= is
  preferred. Currently used by SHAKE.

* FLAGS
*EVP_MD_CTX_set_flags()*, *EVP_MD_CTX_clear_flags()* and
*EVP_MD_CTX_test_flags()* can be used the manipulate and test these
*EVP_MD_CTX* flags:

- EVP_MD_CTX_FLAG_ONESHOT :: This flag instructs the digest to optimize
  for one update only, if possible.

- EVP_MD_CTX_FLAG_NO_INIT :: This flag instructs *EVP_DigestInit()* and
  similar not to initialise the implementation specific data.

- EVP_MD_CTX_FLAG_FINALISE :: Some functions such as EVP_DigestSign only
  finalise copies of internal contexts so additional data can be
  included after the finalisation call. This is inefficient if this
  functionality is not required, and can be disabled with this flag.

* RETURN VALUES
- EVP_DigestInit_ex(), EVP_DigestUpdate(),
  EVP_DigestFinal_ex() :: Returns 1 for success and 0 for failure.

- EVP_MD_CTX_ctrl() :: Returns 1 if successful or 0 for failure.

- EVP_MD_CTX_copy_ex() :: Returns 1 if successful or 0 for failure.

- EVP_MD_type(), EVP_MD_pkey_type() :: Returns the NID of the
  corresponding OBJECT IDENTIFIER or NID_undef if none exists.

- EVP_MD_size(), EVP_MD_block_size(), EVP_MD_CTX_size(),
  EVP_MD_CTX_block_size() :: Returns the digest or block size in bytes.

- EVP_md_null() :: Returns a pointer to the *EVP_MD* structure of the
  null message digest.

- EVP_get_digestbyname(), EVP_get_digestbynid(),
  EVP_get_digestbyobj() :: Returns either an *EVP_MD* structure or NULL
  if an error occurs.

- EVP_MD_CTX_set_pkey_ctx() :: This function has no return value.

* NOTES
The *EVP* interface to message digests should almost always be used in
preference to the low-level interfaces. This is because the code then
becomes transparent to the digest used and much more flexible.

New applications should use the SHA-2 (such as *EVP_sha256* (3)) or the
SHA-3 digest algorithms (such as *EVP_sha3_512* (3)). The other digest
algorithms are still in common use.

For most applications the *impl* parameter to *EVP_DigestInit_ex()* will
be set to NULL to use the default digest implementation.

The functions *EVP_DigestInit()*, *EVP_DigestFinal()* and
*EVP_MD_CTX_copy()* are obsolete but are retained to maintain
compatibility with existing code. New applications should use
*EVP_DigestInit_ex()*, *EVP_DigestFinal_ex()* and *EVP_MD_CTX_copy_ex()*
because they can efficiently reuse a digest context instead of
initializing and cleaning it up on each call and allow non default
implementations of digests to be specified.

If digest contexts are not cleaned up after use, memory leaks will
occur.

*EVP_MD_CTX_size()*, *EVP_MD_CTX_block_size()*, *EVP_MD_CTX_type()*,
*EVP_get_digestbynid()* and *EVP_get_digestbyobj()* are defined as
macros.

*EVP_MD_CTX_ctrl()* sends commands to message digests for additional
configuration or control.

* EXAMPLES
This example digests the data Test Message\n and Hello World\n, using
the digest name passed on the command line.

#include <stdio.h> #include <string.h> #include <openssl/evp.h> int
main(int argc, char *argv[]) { EVP_MD_CTX *mdctx; const EVP_MD *md; char
mess1[] = "Test Message\n"; char mess2[] = "Hello World\n"; unsigned
char md_value[EVP_MAX_MD_SIZE]; unsigned int md_len, i; if (argv[1] ==
NULL) { printf("Usage: mdtest digestname\n"); exit(1); } md =
EVP_get_digestbyname(argv[1]); if (md == NULL) { printf("Unknown message
digest %s\n", argv[1]); exit(1); } mdctx = EVP_MD_CTX_new();
EVP_DigestInit_ex(mdctx, md, NULL); EVP_DigestUpdate(mdctx, mess1,
strlen(mess1)); EVP_DigestUpdate(mdctx, mess2, strlen(mess2));
EVP_DigestFinal_ex(mdctx, md_value, &md_len); EVP_MD_CTX_free(mdctx);
printf("Digest is: "); for (i = 0; i < md_len; i++) printf("%02x",
md_value[i]); printf("\n"); exit(0); }

* SEE ALSO
*EVP_MD_meth_new* (3), *dgst* (1), *evp* (7)

The full list of digest algorithms are provided below.

*EVP_blake2b512* (3), *EVP_md2* (3), *EVP_md4* (3), *EVP_md5* (3),
*EVP_mdc2* (3), *EVP_ripemd160* (3), *EVP_sha1* (3), *EVP_sha224* (3),
*EVP_sha3_224* (3), *EVP_sm3* (3), *EVP_whirlpool* (3)

* HISTORY
The *EVP_MD_CTX_create()* and *EVP_MD_CTX_destroy()* functions were
renamed to *EVP_MD_CTX_new()* and *EVP_MD_CTX_free()* in OpenSSL 1.1.0,
respectively.

The link between digests and signing algorithms was fixed in OpenSSL 1.0
and later, so now *EVP_sha1()* can be used with RSA and DSA.

The *EVP_dss1()* function was removed in OpenSSL 1.1.0.

The *EVP_MD_CTX_set_pkey_ctx()* function was added in 1.1.1.

* COPYRIGHT
Copyright 2000-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
