#+TITLE: Manpages - CURLOPT_GSSAPI_DELEGATION.3
#+DESCRIPTION: Linux manpage for CURLOPT_GSSAPI_DELEGATION.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_GSSAPI_DELEGATION - allowed GSS-API delegation

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_GSSAPI_DELEGATION, long
level);

* DESCRIPTION
Set the long parameter /level/ to *CURLGSSAPI_DELEGATION_FLAG* to allow
unconditional GSSAPI credential delegation. The delegation is disabled
by default since 7.21.7. Set the parameter to
*CURLGSSAPI_DELEGATION_POLICY_FLAG* to delegate only if the
OK-AS-DELEGATE flag is set in the service ticket in case this feature is
supported by the GSS-API implementation and the definition of
GSS_C_DELEG_POLICY_FLAG was available at compile-time.

* DEFAULT
CURLGSSAPI_DELEGATION_NONE

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode ret;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    /* delegate if okayed by policy */
    curl_easy_setopt(curl, CURLOPT_GSSAPI_DELEGATION,
                           CURLGSSAPI_DELEGATION_POLICY_FLAG);
    ret = curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.22.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_HTTPAUTH*(3), *CURLOPT_PROXYAUTH*(3),
