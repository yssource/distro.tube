#+TITLE: Manpages - pcre2_match_data_free.3
#+DESCRIPTION: Linux manpage for pcre2_match_data_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  void pcre2_match_data_free(pcre2_match_data *match_data);
#+end_example

* DESCRIPTION
If /match_data/ is NULL, this function does nothing. Otherwise,
/match_data/ must point to a match data block, which this function
frees, using the memory freeing function from the general context or
compiled pattern with which it was created, or *free()* if that was not
set.

If the PCRE2_COPY_MATCHED_SUBJECT was used for a successful match using
this match data block, the copy of the subject that was remembered with
the block is also freed.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
