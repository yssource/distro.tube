#+TITLE: Manpages - XtIsSensitive.3
#+DESCRIPTION: Linux manpage for XtIsSensitive.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtIsSensitive.3 is found in manpage for: [[../man3/XtSetSensitive.3][man3/XtSetSensitive.3]]