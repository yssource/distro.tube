#+TITLE: Manpages - SDL_SemPost.3
#+DESCRIPTION: Linux manpage for SDL_SemPost.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_SemPost - Unlock a semaphore.

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*int SDL_SemPost*(*SDL_sem *sem*);

* DESCRIPTION
*SDL_SemPost* unlocks the semaphore pointed to by *sem* and atomically
increments the semaphores value. Threads that were blocking on the
semaphore may be scheduled after this call succeeds.

*SDL_SemPost* should be called after a semaphore is locked by a
successful call to /SDL_SemWait/, /SDL_SemTryWait/ or
/SDL_SemWaitTimeout/.

* RETURN VALUE
Returns *0* if successful or *-1* if there was an error (leaving the
semaphore unchanged).

* EXAMPLES
#+begin_example
  SDL_SemPost(my_sem);
#+end_example

* SEE ALSO
*SDL_CreateSemaphore*, *SDL_DestroySemaphore*, *SDL_SemWait*,
*SDL_SemTryWait*, *SDL_SemWaitTimeout*, *SDL_SemValue*
