#+TITLE: Manpages - rpc_gss_max_data_length.3t
#+DESCRIPTION: Linux manpage for rpc_gss_max_data_length.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Calculate the maximum message size that will fit into a packet of size

given the current service and QoP setting.

A handle to a RPCSEC_GSS security context

Maximum packet size of the underlying transport protocol

The maximum message size that can be encoded

The

function is part of libtirpc.

This manual page was written by
