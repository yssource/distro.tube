#+TITLE: Manpages - xcb_input_device_property_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_input_device_property_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_input_device_property_notify_event_t -

* SYNOPSIS
*#include <xcb/xinput.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_input_device_property_notify_event_t {
      uint8_t         response_type;
      uint8_t         state;
      uint16_t        sequence;
      xcb_timestamp_t time;
      xcb_atom_t      property;
      uint8_t         pad0[19];
      uint8_t         device_id;
  } xcb_input_device_property_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_INPUT_DEVICE_PROPERTY_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- state :: NOT YET DOCUMENTED.

- time :: NOT YET DOCUMENTED.

- property :: NOT YET DOCUMENTED.

- device_id :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from xinput.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
