#+TITLE: Manpages - XGetEventData.3
#+DESCRIPTION: Linux manpage for XGetEventData.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XGetEventData, XFreeEventData, XGenericEventCookie - retrieve and free
additional event data through cookies.

* SYNTAX
Bool XGetEventData ( Display */display/ , XGenericEventCookie
*/cookie/ );

void XFreeEventData ( Display */display/ , XGenericEventCookie
*/cookie/ );

* ARGUMENTS
- display :: Specifies the connection to the X server.

- cookie :: Specifies the cookie to free or retrieve the data for.

* STRUCTURES
#+begin_example
  typedef struct {
          int type;
          unsigned long serial;
          Bool send_event;
          Display *display;
          int extension;
          int evtype;
          unsigned int cookie;
          void *data;
  } XGenericEventCookie;
#+end_example

* DESCRIPTION
Some extension *XGenericEvents* require additional memory to store
information. For these events, the library returns a
*XGenericEventCookie* with a token ('cookie') unique to this event. The
*XGenericEventCookie*'s data pointer is undefined until *XGetEventData*
is called.

The *XGetEventData* function retrieves this extra data for the given
cookie. No round-trip to the server is required. If the cookie is
invalid or the event is not an event handled by cookie handlers, *False*
is returned. If *XGetEventData* returns *True*, the cookie's data
pointer points to the memory containing the event information. A client
must call *XFreeEventData* to free this memory. *XGetEventData* returns
*False* for multiple calls for the same event cookie.

The *XFreeEventData* function frees the data associated with a cookie. A
client must call *XFreeEventData* for each cookie claimed with
*XGetEventData*.

* EXAMPLE CODE
#+begin_example
  XEvent event;
  XGenericEventCookie *cookie = &ev;

  XNextEvent(display, &event);
  if (XGetEventData(display, cookie)) {
      handle_cookie_event(cookie->data);
  } else
      handle_event(&event);
  }
  XFreeEventData(display, cookie);
#+end_example

* NOTES
A cookie is defined as unclaimed if it has been returned to the client
through *XNextEvent* but its data has not been retrieved via
*XGetEventData*. Subsequent calls to *XNextEvent* may free memory
associated with unclaimed cookies. Multi-threaded X clients must ensure
that *XGetEventData* is called before the next call to *XNextEvent*.

* SEE ALSO
XNextEvent(3),\\
/Xlib - C Language X Interface/
