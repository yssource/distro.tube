#+TITLE: Manpages - std_chi_squared_distribution_param_type.3
#+DESCRIPTION: Linux manpage for std_chi_squared_distribution_param_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::chi_squared_distribution< _RealType >::param_type

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef *chi_squared_distribution*< _RealType > *distribution_type*\\

** Public Member Functions
*param_type* (_RealType __n)\\

_RealType *n* () const\\

** Friends
bool *operator!=* (const *param_type* &__p1, const *param_type* &__p2)\\

bool *operator==* (const *param_type* &__p1, const *param_type* &__p2)\\

* Detailed Description
** "template<typename _RealType = double>
\\
struct std::chi_squared_distribution< _RealType >::param_type"Parameter
type.

Definition at line *2642* of file *random.h*.

* Member Typedef Documentation
** template<typename _RealType = double> typedef
*chi_squared_distribution*<_RealType> *std::chi_squared_distribution*<
_RealType >::*param_type::distribution_type*
Definition at line *2644* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RealType = double>
*std::chi_squared_distribution*< _RealType >::param_type::param_type
()= [inline]=
Definition at line *2646* of file *random.h*.

** template<typename _RealType = double>
*std::chi_squared_distribution*< _RealType >::param_type::param_type
(_RealType __n)= [inline]=, = [explicit]=
Definition at line *2649* of file *random.h*.

* Member Function Documentation
** template<typename _RealType = double> _RealType
*std::chi_squared_distribution*< _RealType >::param_type::n ()
const= [inline]=
Definition at line *2654* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RealType = double> bool operator!= (const
*param_type* & __p1, const *param_type* & __p2)= [friend]=
Definition at line *2662* of file *random.h*.

** template<typename _RealType = double> bool operator== (const
*param_type* & __p1, const *param_type* & __p2)= [friend]=
Definition at line *2658* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
