#+TITLE: Manpages - rtcRetainBVH.3embree3
#+DESCRIPTION: Linux manpage for rtcRetainBVH.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcRetainBVH - increments the BVH reference count
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcRetainBVH(RTCBVH bvh);
#+end_example

** DESCRIPTION
BVH objects are reference counted. The =rtcRetainBVH= function
increments the reference count of the passed BVH object (=bvh=
argument). This function together with =rtcReleaseBVH= allows to use the
internal reference counting in a C++ wrapper class to handle the
ownership of the object.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewBVH], [rtcReleaseBVH]
