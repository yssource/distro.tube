#+TITLE: Manpages - libssh2_sftp_unlink.3
#+DESCRIPTION: Linux manpage for libssh2_sftp_unlink.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_sftp_unlink - convenience macro for /libssh2_sftp_unlink_ex(3)/
calls

* SYNOPSIS
#include <libssh2.h>

int libssh2_sftp_unlink(LIBSSH2_SFTP *sftp, const char *filename);

* DESCRIPTION
This is a macro defined in a public libssh2 header file that is using
the underlying function /libssh2_sftp_unlink_ex(3)/.

* RETURN VALUE
See /libssh2_sftp_unlink_ex(3)/

* ERRORS
See /libssh2_sftp_unlink_ex(3)/

* SEE ALSO
*libssh2_sftp_unlink_ex(3)*
