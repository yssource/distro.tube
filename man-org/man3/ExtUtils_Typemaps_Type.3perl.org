#+TITLE: Manpages - ExtUtils_Typemaps_Type.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_Typemaps_Type.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::Typemaps::Type - Entry in the TYPEMAP section of a typemap

* SYNOPSIS
use ExtUtils::Typemaps; ... my $type = $typemap->get_type_map(char*); my
$input = $typemap->get_input_map($type->xstype);

* DESCRIPTION
Refer to ExtUtils::Typemaps for details. Object associates =ctype= with
=xstype=, which is the index into the in- and output mapping tables.

* METHODS
** new
Requires =xstype= and =ctype= parameters.

Optionally takes =prototype= parameter.

** proto
Returns or sets the prototype.

** xstype
Returns the name of the XS type that this C type is associated to.

** ctype
Returns the name of the C type as it was set on construction.

** tidy_ctype
Returns the canonicalized name of the C type.

* SEE ALSO
ExtUtils::Typemaps

* AUTHOR
Steffen Mueller =<smueller@cpan.org=>

* COPYRIGHT & LICENSE
Copyright 2009, 2010, 2011, 2012 Steffen Mueller

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
