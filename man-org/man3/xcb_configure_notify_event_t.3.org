#+TITLE: Manpages - xcb_configure_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_configure_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_configure_notify_event_t - NOT YET DOCUMENTED

* SYNOPSIS
*#include <xcb/xproto.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_configure_notify_event_t {
      uint8_t      response_type;
      uint8_t      pad0;
      uint16_t     sequence;
      xcb_window_t event;
      xcb_window_t window;
      xcb_window_t above_sibling;
      int16_t      x;
      int16_t      y;
      uint16_t     width;
      uint16_t     height;
      uint16_t     border_width;
      uint8_t      override_redirect;
      uint8_t      pad1;
  } xcb_configure_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_CONFIGURE_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- event :: The reconfigured window or its parent, depending on whether
  /StructureNotify/ or /SubstructureNotify/ was selected.

- window :: The window whose size, position, border, and/or stacking
  order was changed.

- above_sibling :: If /XCB_NONE/, the /window/ is on the bottom of the
  stack with respect to sibling windows. However, if set to a sibling
  window, the /window/ is placed on top of this sibling window.

24. The X coordinate of the upper-left outside corner of /window/,
    relative to the parent window's origin.

25. The Y coordinate of the upper-left outside corner of /window/,
    relative to the parent window's origin.

- width :: The inside width of /window/, not including the border.

- height :: The inside height of /window/, not including the border.

- border_width :: The border width of /window/.

- override_redirect :: Window managers should ignore this window if
  /override_redirect/ is 1.

* DESCRIPTION
* SEE ALSO
*xcb_generic_event_t*(3), *xcb_free_colormap*(3)

* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
