#+TITLE: Manpages - fgetc_unlocked.3
#+DESCRIPTION: Linux manpage for fgetc_unlocked.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fgetc_unlocked.3 is found in manpage for: [[../man3/unlocked_stdio.3][man3/unlocked_stdio.3]]