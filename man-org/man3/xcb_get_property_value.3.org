#+TITLE: Manpages - xcb_get_property_value.3
#+DESCRIPTION: Linux manpage for xcb_get_property_value.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_get_property_value.3 is found in manpage for: [[../man3/xcb_get_property.3][man3/xcb_get_property.3]]