#+TITLE: Manpages - xcb_render_create_solid_fill.3
#+DESCRIPTION: Linux manpage for xcb_render_create_solid_fill.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_render_create_solid_fill -

* SYNOPSIS
*#include <xcb/render.h>*

** Request function
xcb_void_cookie_t *xcb_render_create_solid_fill*(xcb_connection_t
*/conn/, xcb_render_picture_t /picture/, xcb_render_color_t /color/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- picture :: TODO: NOT YET DOCUMENTED.

- color :: TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_render_create_solid_fill_checked/. See *xcb-requests(3)* for
details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from render.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
