#+TITLE: Manpages - std_invalid_argument.3
#+DESCRIPTION: Linux manpage for std_invalid_argument.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::invalid_argument

* SYNOPSIS
\\

Inherits *std::logic_error*.

** Public Member Functions
*invalid_argument* (const char *) _GLIBCXX_TXN_SAFE\\

*invalid_argument* (const *invalid_argument* &)=default\\

*invalid_argument* (const *string* &__arg) _GLIBCXX_TXN_SAFE\\

*invalid_argument* (*invalid_argument* &&)=default\\

*invalid_argument* & *operator=* (const *invalid_argument* &)=default\\

*invalid_argument* & *operator=* (*invalid_argument* &&)=default\\

virtual const char * *what* () const noexcept\\

* Detailed Description
Thrown to report invalid arguments to functions.\\

Definition at line *168* of file *stdexcept*.

* Member Function Documentation
** virtual const char * std::logic_error::what () const= [virtual]=,
= [noexcept]=, = [inherited]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::exception*.

Reimplemented in *std::future_error*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
