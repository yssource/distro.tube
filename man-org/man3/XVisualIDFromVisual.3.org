#+TITLE: Manpages - XVisualIDFromVisual.3
#+DESCRIPTION: Linux manpage for XVisualIDFromVisual.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XVisualIDFromVisual.3 is found in manpage for: [[../man3/XGetVisualInfo.3][man3/XGetVisualInfo.3]]