#+TITLE: Manpages - SDL_GetKeyState.3
#+DESCRIPTION: Linux manpage for SDL_GetKeyState.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GetKeyState - Get a snapshot of the current keyboard state

* SYNOPSIS
*#include "SDL.h"*

*Uint8 *SDL_GetKeyState*(*int *numkeys*);

* DESCRIPTION
Gets a snapshot of the current keyboard state. The current state is
return as a pointer to an array, the size of this array is stored in
*numkeys*. The array is indexed by the *SDLK_** symbols. A value of 1
means the key is pressed and a value of 0 means its not. The pointer
returned is a pointer to an internal SDL array and should not be freed
by the caller.

#+begin_quote
  *Note: *

  Use *SDL_PumpEvents* to update the state array.
#+end_quote

* EXAMPLE
#+begin_example
  Uint8 *keystate = SDL_GetKeyState(NULL);
  if ( keystate[SDLK_RETURN] ) printf("Return Key Pressed.
  ");
#+end_example

* SEE ALSO
*SDL Key Symbols*, *SDL_PumpEvents*
