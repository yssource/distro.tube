#+TITLE: Manpages - XkbAllocGeomOutlines.3
#+DESCRIPTION: Linux manpage for XkbAllocGeomOutlines.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbAllocGeomOutlines - Allocate space for an arbitrary number of
outlines to a shape

* SYNOPSIS
*Status XkbAllocGeomOutlines* *( XkbShapePtr */shape/* ,* *int
*/num_needed/* );*

* ARGUMENTS
- /- shape/ :: shape for which outlines should be allocated

- /- num_needed/ :: number of new outlines required

* DESCRIPTION
Xkb provides a number of functions to allocate and free subcomponents of
a keyboard geometry. Use these functions to create or modify keyboard
geometries. Note that these functions merely allocate space for the new
element(s), and it is up to you to fill in the values explicitly in your
code. These allocation functions increase /sz_*/ but never touch /num_*/
(unless there is an allocation failure, in which case they reset both
/sz_*/ and /num_*/ to zero). These functions return Success if they
succeed, BadAlloc if they are not able to allocate space, or BadValue if
a parameter is not as expected.

/XkbAllocGeomOutlines/ allocates space for /num_needed/ outlines in the
specified /shape./ The outlines are not initialized.

To free geometry outlines, use /XkbFreeGeomOutlines./

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadValue* :: An argument is out of range

* SEE ALSO
*XkbFreeGeomOutlines*(3)
