#+TITLE: Manpages - std_binomial_distribution_param_type.3
#+DESCRIPTION: Linux manpage for std_binomial_distribution_param_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::binomial_distribution< _IntType >::param_type

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef *binomial_distribution*< _IntType > *distribution_type*\\

** Public Member Functions
*param_type* (_IntType __t, double __p=0.5)\\

double *p* () const\\

_IntType *t* () const\\

** Friends
class *binomial_distribution< _IntType >*\\

bool *operator!=* (const *param_type* &__p1, const *param_type* &__p2)\\

bool *operator==* (const *param_type* &__p1, const *param_type* &__p2)\\

* Detailed Description
** "template<typename _IntType = int>
\\
struct std::binomial_distribution< _IntType >::param_type"Parameter
type.

Definition at line *3750* of file *random.h*.

* Member Typedef Documentation
** template<typename _IntType = int> typedef
*binomial_distribution*<_IntType> *std::binomial_distribution*< _IntType
>::*param_type::distribution_type*
Definition at line *3752* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _IntType = int> *std::binomial_distribution*<
_IntType >::param_type::param_type ()= [inline]=
Definition at line *3755* of file *random.h*.

** template<typename _IntType = int> *std::binomial_distribution*<
_IntType >::param_type::param_type (_IntType __t, double __p =
=0.5=)= [inline]=, = [explicit]=
Definition at line *3758* of file *random.h*.

* Member Function Documentation
** template<typename _IntType = int> double
*std::binomial_distribution*< _IntType >::param_type::p ()
const= [inline]=
Definition at line *3772* of file *random.h*.

** template<typename _IntType = int> _IntType
*std::binomial_distribution*< _IntType >::param_type::t ()
const= [inline]=
Definition at line *3768* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _IntType = int> friend class
*binomial_distribution*< _IntType >= [friend]=
Definition at line *3752* of file *random.h*.

** template<typename _IntType = int> bool operator!= (const *param_type*
& __p1, const *param_type* & __p2)= [friend]=
Definition at line *3780* of file *random.h*.

** template<typename _IntType = int> bool operator== (const *param_type*
& __p1, const *param_type* & __p2)= [friend]=
Definition at line *3776* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
