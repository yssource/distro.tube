#+TITLE: Manpages - SDL_ResizeEvent.3
#+DESCRIPTION: Linux manpage for SDL_ResizeEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_ResizeEvent - Window resize event structure

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    Uint8 type;
    int w, h;
  } SDL_ResizeEvent;
#+end_example

* STRUCTURE DATA
- *type* :: *SDL_VIDEORESIZE*

- *w*, *h* :: New width and height of the window

* DESCRIPTION
*SDL_ResizeEvent* is a member of the *SDL_Event* union and is used when
an event of type *SDL_VIDEORESIZE* is reported.

When *SDL_RESIZABLE* is passed as a *flag* to *SDL_SetVideoMode* the
user is allowed to resize the applications window. When the window is
resized an *SDL_VIDEORESIZE* is report, with the new window width and
height values stored in *w* and *h*, respectively. When an
*SDL_VIDEORESIZE* is received the window should be resized to the new
dimensions using *SDL_SetVideoMode*.

* SEE ALSO
*SDL_Event*, *SDL_SetVideoMode*
