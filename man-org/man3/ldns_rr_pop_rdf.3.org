#+TITLE: Manpages - ldns_rr_pop_rdf.3
#+DESCRIPTION: Linux manpage for ldns_rr_pop_rdf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr_push_rdf, ldns_rr_pop_rdf - push and pop rdata fields

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

bool ldns_rr_push_rdf(ldns_rr *rr, const ldns_rdf *f);

ldns_rdf* ldns_rr_pop_rdf(ldns_rr *rr);

* DESCRIPTION
/ldns_rr_push_rdf/() sets rd_field member, it will be placed in the next
available spot. .br **rr*: rr to operate on .br **f*: the data field
member to set .br Returns bool

/ldns_rr_pop_rdf/() removes a rd_field member, it will be popped from
the last position. .br **rr*: rr to operate on .br Returns rdf which was
popped (null if nothing)

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rr/, /ldns_rr_list/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
