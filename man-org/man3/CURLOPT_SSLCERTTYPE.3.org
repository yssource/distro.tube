#+TITLE: Manpages - CURLOPT_SSLCERTTYPE.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSLCERTTYPE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSLCERTTYPE - type of client SSL certificate

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLCERTTYPE, char
*type);

* DESCRIPTION
Pass a pointer to a null-terminated string as parameter. The string
should be the format of your certificate. Supported formats are "PEM"
and "DER", except with Secure Transport. OpenSSL (versions 0.9.3 and
later) and Secure Transport (on iOS 5 or later, or OS X 10.7 or later)
also support "P12" for PKCS#12-encoded files.

The application does not have to keep the string around after setting
this option.

* DEFAULT
"PEM"

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_SSLCERT, "client.pem");
    curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
    curl_easy_setopt(curl, CURLOPT_SSLKEY, "key.pem");
    curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "s3cret");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
If built TLS enabled. Added in 7.9.3

* RETURN VALUE
Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or
CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_SSLCERT*(3), *CURLOPT_SSLKEY*(3),
