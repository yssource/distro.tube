#+TITLE: Manpages - des_crypt.3t
#+DESCRIPTION: Linux manpage for des_crypt.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

and

functions implement the

(Data Encryption Standard). These routines are faster and more general
purpose than

They also are able to utilize

hardware if it is available. The

function encrypts in

(Electronic Code Book) mode, which encrypts blocks of data
independently. The

function encrypts in

(Cipher Block Chaining) mode, which chains together successive blocks.

mode protects against insertions, deletions and substitutions of blocks.
Also, regularities in the clear text will not appear in the cipher text.

Here is how to use these routines. The first argument,

is the 8-byte encryption key with parity. To set the key's parity, which
for

is in the low bit of each byte, use

The second argument,

contains the data to be encrypted or decrypted. The third argument,

is the length in bytes of

which must be a multiple of 8. The fourth argument,

is formed by

together some things. For the encryption direction

in either

or

For software versus hardware encryption,

in either

or

If

is specified, and there is no hardware, then the encryption is performed
in software and the routine returns

For

the

argument is the 8-byte initialization vector for the chaining. It is
updated to the next initialization vector upon return.

No error.

Encryption succeeded, but done in software instead of the requested
hardware.

An error occurred in the hardware or driver.

Bad argument to routine.

Given a result status

the macro

is false only for the first two statuses.

The

and

functions are part of libtirpc.

These routines are not available in RPCSRC 4.0. This information is
provided to describe the

interface expected by Secure RPC.
