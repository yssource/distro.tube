#+TITLE: Manpages - XineramaQueryVersion.3
#+DESCRIPTION: Linux manpage for XineramaQueryVersion.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XineramaQueryVersion.3 is found in manpage for: [[../man3/Xinerama.3][man3/Xinerama.3]]