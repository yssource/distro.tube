#+TITLE: Manpages - ts_conf_get.3
#+DESCRIPTION: Linux manpage for ts_conf_get.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_conf_get - get a pointer to struct ts_module_conf holding data from
the TSLIB_CONFFILE file.

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  struct ts_module_conf *ts_conf_get(struct tsdev *ts);
#+end_example

* DESCRIPTION
*ts_conf_get*() This function returns a pointer to a struct
ts_module_conf that is part of a list of modules listed in the
TSLIB_CONFFILE configuration file. ts_conf_get() actually reads
TSLIB_CONFFILE. One struct represents one line in the file. They get
allocated for the user here:

#+begin_example
  struct ts_module_conf {
  	char *name;
  	char *params;
  	int raw;
  	int nr;

  	struct ts_module_conf *next;
  	struct ts_module_conf *prev;
  };
#+end_example

* RETURN VALUE
This function returns a pointer to a struct ts_module_conf.

* SEE ALSO
*ts_conf_set*(3), *ts.conf*(5)
