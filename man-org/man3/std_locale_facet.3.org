#+TITLE: Manpages - std_locale_facet.3
#+DESCRIPTION: Linux manpage for std_locale_facet.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::locale::facet - Localization functionality base class.

* SYNOPSIS
\\

=#include <locale_classes.h>=

Inherited by *std::__codecvt_abstract_base< _InternT, _ExternT,
encoding_state >*, *std::__codecvt_abstract_base< char, char, mbstate_t
>*, *std::__codecvt_abstract_base< char16_t, char, mbstate_t >*,
*std::__codecvt_abstract_base< char32_t, char, mbstate_t >*,
*std::__codecvt_abstract_base< wchar_t, char, mbstate_t >*,
*std::__ctype_abstract_base< wchar_t >*, *std::__codecvt_abstract_base<
_InternT, _ExternT, _StateT >*, *std::__ctype_abstract_base< _CharT >*,
std::__moneypunct_cache< _CharT, _Intl >, std::__numpunct_cache< _CharT
>, std::__timepunct< _CharT >, std::__timepunct_cache< _CharT >,
*std::collate< _CharT >*, *std::ctype< char >*, *std::messages< _CharT
>*, *std::money_get< _CharT, _InIter >*, *std::money_put< _CharT,
_OutIter >*, *std::moneypunct< _CharT, _Intl >*, *std::num_get< _CharT,
_InIter >*, *std::num_put< _CharT, _OutIter >*, *std::numpunct< _CharT
>*, *std::time_get< _CharT, _InIter >*, and *std::time_put< _CharT,
_OutIter >*.

** Protected Member Functions
*facet* (const *facet* &)=delete\\

*facet* (size_t __refs=0) throw ()\\
Facet constructor.

virtual *~facet* ()\\
Facet destructor.

*facet* & *operator=* (const *facet* &)=delete\\

** Static Protected Member Functions
static __c_locale *_S_clone_c_locale* (__c_locale &__cloc) throw ()\\

static void *_S_create_c_locale* (__c_locale &__cloc, const char *__s,
__c_locale __old=0)\\

static void *_S_destroy_c_locale* (__c_locale &__cloc)\\

static __c_locale *_S_get_c_locale* ()\\

static const char * *_S_get_c_name* () throw ()\\

static __c_locale *_S_lc_ctype_c_locale* (__c_locale __cloc, const char
*__s)\\

** Friends
class *locale*\\

class *locale::_Impl*\\

* Detailed Description
Localization functionality base class.

The facet class is the base class for a localization feature, such as
money, time, and number printing. It provides common support for facets
and reference management.

Facets may not be copied or assigned.

Definition at line *373* of file *locale_classes.h*.

* Constructor & Destructor Documentation
** std::locale::facet::facet (size_t __refs = =0=)= [inline]=,
= [explicit]=, = [protected]=
Facet constructor. This is the constructor provided by the standard. If
refs is 0, the facet is destroyed when the last referencing locale is
destroyed. Otherwise the facet will never be destroyed.

*Parameters*

#+begin_quote
  /__refs/ The initial value for reference count.
#+end_quote

Definition at line *405* of file *locale_classes.h*.

** virtual std::locale::facet::~facet ()= [protected]=, = [virtual]=
Facet destructor.

* Friends And Related Function Documentation
** friend class *locale*= [friend]=
Definition at line *376* of file *locale_classes.h*.

** friend class locale::_Impl= [friend]=
Definition at line *377* of file *locale_classes.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
