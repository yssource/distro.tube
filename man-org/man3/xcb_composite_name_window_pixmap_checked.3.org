#+TITLE: Manpages - xcb_composite_name_window_pixmap_checked.3
#+DESCRIPTION: Linux manpage for xcb_composite_name_window_pixmap_checked.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_composite_name_window_pixmap_checked.3 is found in manpage for: [[../man3/xcb_composite_name_window_pixmap.3][man3/xcb_composite_name_window_pixmap.3]]