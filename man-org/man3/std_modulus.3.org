#+TITLE: Manpages - std_modulus.3
#+DESCRIPTION: Linux manpage for std_modulus.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::modulus< _Tp > - One of the *math functors*.

* SYNOPSIS
\\

=#include <stl_function.h>=

Inherits *std::binary_function< _Tp, _Tp, _Tp >*.

** Public Types
typedef _Tp *first_argument_type*\\
=first_argument_type= is the type of the first argument

typedef _Tp *result_type*\\
=result_type= is the return type

typedef _Tp *second_argument_type*\\
=second_argument_type= is the type of the second argument

** Public Member Functions
constexpr _Tp *operator()* (const _Tp &__x, const _Tp &__y) const\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::modulus< _Tp >"One of the *math functors*.

Definition at line *207* of file *stl_function.h*.

* Member Typedef Documentation
** typedef _Tp *std::binary_function*< _Tp , _Tp , _Tp
>::*first_argument_type*= [inherited]=
=first_argument_type= is the type of the first argument

Definition at line *121* of file *stl_function.h*.

** typedef _Tp *std::binary_function*< _Tp , _Tp , _Tp
>::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *127* of file *stl_function.h*.

** typedef _Tp *std::binary_function*< _Tp , _Tp , _Tp
>::*second_argument_type*= [inherited]=
=second_argument_type= is the type of the second argument

Definition at line *124* of file *stl_function.h*.

* Member Function Documentation
** template<typename _Tp > constexpr _Tp *std::modulus*< _Tp
>::operator() (const _Tp & __x, const _Tp & __y) const= [inline]=,
= [constexpr]=
Definition at line *211* of file *stl_function.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
