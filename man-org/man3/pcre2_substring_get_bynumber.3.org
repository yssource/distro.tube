#+TITLE: Manpages - pcre2_substring_get_bynumber.3
#+DESCRIPTION: Linux manpage for pcre2_substring_get_bynumber.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  int pcre2_substring_get_bynumber(pcre2_match_data *match_data,
   uint32_t number, PCRE2_UCHAR **bufferptr, PCRE2_SIZE *bufflen);
#+end_example

* DESCRIPTION
This is a convenience function for extracting a captured substring by
number into newly acquired memory. The arguments are:

/match_data/ The match data for the match /number/ Number of the
required substring /bufferptr/ Where to put the string pointer /bufflen/
Where to put the string length

The memory in which the substring is placed is obtained by calling the
same memory allocation function that was used for the match data block.
The convenience function *pcre2_substring_free()* can be used to free it
when it is no longer needed. The yield of the function is zero for
success or one of the following error numbers:

PCRE2_ERROR_NOSUBSTRING there are no groups of that number
PCRE2_ERROR_UNAVAILBLE the ovector was too small for that group
PCRE2_ERROR_UNSET the group did not participate in the match
PCRE2_ERROR_NOMEMORY memory could not be obtained

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
