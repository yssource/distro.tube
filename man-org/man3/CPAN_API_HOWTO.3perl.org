#+TITLE: Manpages - CPAN_API_HOWTO.3perl
#+DESCRIPTION: Linux manpage for CPAN_API_HOWTO.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
CPAN::API::HOWTO - a recipe book for programming with CPAN.pm

* RECIPES
All of these recipes assume that you have put use CPAN at the top of
your program.

** What distribution contains a particular module?
my $distribution = CPAN::Shell->expand( "Module", "Data::UUID"
)->distribution()->pretty_id();

This returns a string of the form AUTHORID/TARBALL. If you want the full
path and filename to this distribution on a CPAN mirror, then it is
=.../authors/id/A/AU/AUTHORID/TARBALL=.

** What modules does a particular distribution contain?
CPAN::Index->reload(); my @modules = CPAN::Shell->expand(
"Distribution", "JHI/Graph-0.83.tar.gz" )->containsmods();

You may also refer to a distribution in the form A/AU/AUTHORID/TARBALL.

* SEE ALSO
the main CPAN.pm documentation

* LICENSE
This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See <http://www.perl.com/perl/misc/Artistic.html>

* AUTHOR
David Cantrell
