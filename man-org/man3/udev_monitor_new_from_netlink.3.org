#+TITLE: Manpages - udev_monitor_new_from_netlink.3
#+DESCRIPTION: Linux manpage for udev_monitor_new_from_netlink.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
udev_monitor_new_from_netlink, udev_monitor_ref, udev_monitor_unref -
Create, acquire and release a udev monitor object

* SYNOPSIS
#+begin_example
  #include <libudev.h>
#+end_example

*struct udev_monitor *udev_monitor_new_from_netlink(struct udev
**/udev/*, const char **/name/*);*

*struct udev_monitor *udev_monitor_ref(struct udev_monitor
**/udev_monitor/*);*

*struct udev_monitor *udev_monitor_unref(struct udev_monitor
**/udev_monitor/*);*

* RETURN VALUE
On success, *udev_monitor_new_from_netlink()* returns a pointer to the
allocated udev monitor. On failure, *NULL* is returned.
*udev_monitor_ref()* returns the argument that it was passed,
unmodified. *udev_monitor_unref()* always returns *NULL*.

* SEE ALSO
*udev_new*(3), *udev_device_new_from_syspath*(3),
*udev_enumerate_new*(3), *udev_monitor_filter_update*(3),
*udev_monitor_receive_device*(3), *udev_list_entry*(3), *systemd*(1),
