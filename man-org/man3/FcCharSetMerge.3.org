#+TITLE: Manpages - FcCharSetMerge.3
#+DESCRIPTION: Linux manpage for FcCharSetMerge.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetMerge - Merge charsets

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcCharSetMerge (FcCharSet */a/*, const FcCharSet **/b/*, FcBool
**/changed/*);*

* DESCRIPTION
Adds all chars in /b/ to /a/. In other words, this is an in-place
version of FcCharSetUnion. If /changed/ is not NULL, then it returns
whether any new chars from /b/ were added to /a/. Returns FcFalse on
failure, either when /a/ is a constant set or from running out of
memory.
