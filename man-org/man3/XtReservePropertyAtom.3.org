#+TITLE: Manpages - XtReservePropertyAtom.3
#+DESCRIPTION: Linux manpage for XtReservePropertyAtom.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtReservePropertyAtom, XtReleasePropertyAtom - maintain a cache of
property atoms

* SYNTAX
#include <X11/Intrinsic.h>

Atom XtReservePropertyAtom(Widget /w/);

void XtReleasePropertyAtom(Widget /w/, Atom /atom/);

* ARGUMENTS

23. Specifies the widget used to reserve the atom.

- atom :: Specifies the atom whose reservation is to be released.

* DESCRIPTION
*XtReservePropertyAtom* returns an atom that may be used for properties
in conjunction with conversion requests from widget /w/. The atom
returned will be unique for the display of the widget specified.

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
