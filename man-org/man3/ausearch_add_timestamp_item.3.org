#+TITLE: Manpages - ausearch_add_timestamp_item.3
#+DESCRIPTION: Linux manpage for ausearch_add_timestamp_item.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ausearch_add_timestamp_item - build up search rule

* SYNOPSIS
*#include <auparse.h>*

int ausearch_add_timestamp_item(auparse_state_t *au, const char *op,
time_t sec, unsigned milli, ausearch_rule_t how)

* DESCRIPTION
ausearch_add_timestamp_item adds an event time condition to the current
audit search expression. The search conditions can then be used to scan
logs, files, or buffers for something of interest. The op parameter
specifies the desired comparison. Legal op values are /</, /<=/, />=/,
/>/ and /=/. The left operand of the comparison operator is the
timestamp of the examined event, the right operand is specified by the
sec and milli parameters.

The how value determines how this search condition will affect the
existing search expression if one is already defined. The possible
values are:

#+begin_quote
  - /AUSEARCH_RULE_CLEAR/ :: Clear the current search expression, if
    any, and use only this search condition.

  - /AUSEARCH_RULE_OR/ :: If a search expression /E/ is already
    configured, replace it by *(*/E/* || */this_search_condition/*)*.

  - /AUSEARCH_RULE_AND/ :: If a search expression /E/ is already
    configured, replace it by *(*/E/* && */this_search_condition/*)*.
#+end_quote

* RETURN VALUE
Returns -1 if an error occurs; otherwise, 0 for success.

* APPLICATION USAGE
Use *ausearch_add_item*(3) and *ausearch_add_interpreted_item*(3) to add
conditions that check audit record fields. Use
*ausearch_add_expression*(3) to add complex search expressions using a
single function call.

* SEE ALSO
*ausearch_add_expression*(3), *ausearch_add_item*(3),
*ausearch_add_interpreted_item*(3), *ausearch_add_regex*(3),
*ausearch_set_stop*(3), *ausearch_clear*(3), *ausearch_next_event*(3),
*ausearch-expression*(5).

* AUTHOR
Miloslav Trmac
