#+TITLE: Manpages - auparse_first_field.3
#+DESCRIPTION: Linux manpage for auparse_first_field.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_first_field - reposition field cursor

* SYNOPSIS
*#include <auparse.h>*

int auparse_first_field(auparse_state_t *au);

* DESCRIPTION
auparse_first_field repositions the library's internal cursor to point
to the first field of the current record in the current event.

* RETURN VALUE
Returns 0 if there is no event data; otherwise, 1 for success.

* SEE ALSO
*auparse_next_field*(3).

* AUTHOR
Steve Grubb
