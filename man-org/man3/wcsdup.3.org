#+TITLE: Manpages - wcsdup.3
#+DESCRIPTION: Linux manpage for wcsdup.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wcsdup - duplicate a wide-character string

* SYNOPSIS
#+begin_example
  #include <wchar.h>

  wchar_t *wcsdup(const wchar_t *s);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*wcsdup*():

#+begin_example
      Since glibc 2.10:
          _POSIX_C_SOURCE >= 200809L
      Before glibc 2.10:
          _GNU_SOURCE
#+end_example

* DESCRIPTION
The *wcsdup*() function is the wide-character equivalent of the
*strdup*(3) function. It allocates and returns a new wide-character
string whose initial contents is a duplicate of the wide-character
string pointed to by /s/.

Memory for the new wide-character string is obtained with *malloc*(3),
and should be freed with *free*(3).

* RETURN VALUE
On success, *wcsdup*() returns a pointer to the new wide-character
string. On error, it returns NULL, with /errno/ set to indicate the
error.

* ERRORS
- *ENOMEM* :: Insufficient memory available to allocate duplicate
  string.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface  | Attribute     | Value   |
| *wcsdup*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2008. This function is not specified in POSIX.1-2001, and is not
widely available on other systems.

* SEE ALSO
*strdup*(3), *wcscpy*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
