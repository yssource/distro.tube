#+TITLE: Manpages - XMapRaised.3
#+DESCRIPTION: Linux manpage for XMapRaised.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XMapRaised.3 is found in manpage for: [[../man3/XMapWindow.3][man3/XMapWindow.3]]