#+TITLE: Manpages - MPI_Barrier.3
#+DESCRIPTION: Linux manpage for MPI_Barrier.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Barrier, MPI_Ibarrier* - Synchronization between MPI processes in a
group

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Barrier(MPI_Comm comm)

  int MPI_Ibarrier(MPI_Comm comm, MPI_Request *request)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_BARRIER(COMM, IERROR)
  	INTEGER	COMM, IERROR

  MPI_IBARRIER(COMM, REQUEST, IERROR)
  	INTEGER	COMM, REQUEST, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Barrier(comm, ierror)
  	TYPE(MPI_Comm), INTENT(IN) :: comm
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror

  MPI_Ibarrier(comm, request, ierror)
  	TYPE(MPI_Comm), INTENT(IN) :: comm
  	TYPE(MPI_Request), INTENT(OUT) :: request
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void MPI::Comm::Barrier() const = 0
#+end_example

* INPUT PARAMETER
- comm :: Communicator (handle).

* OUTPUT PARAMETERS
- request :: Request (handle, non-blocking only).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
An MPI barrier completes after all group members have entered the
barrier.

* WHEN COMMUNICATOR IS AN INTER-COMMUNICATOR
When the communicator is an inter-communicator, the barrier operation is
performed across all processes in both groups. All processes in the
first group may exit the barrier when all processes in the second group
have entered the barrier.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

* SEE ALSO
MPI_Bcast
