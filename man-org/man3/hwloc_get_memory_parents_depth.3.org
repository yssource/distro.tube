#+TITLE: Manpages - hwloc_get_memory_parents_depth.3
#+DESCRIPTION: Linux manpage for hwloc_get_memory_parents_depth.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_get_memory_parents_depth.3 is found in manpage for: [[../man3/hwlocality_levels.3][man3/hwlocality_levels.3]]