#+TITLE: Manpages - ldns_dname_compare.3
#+DESCRIPTION: Linux manpage for ldns_dname_compare.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_dname_compare, ldns_dname_interval - compare two dnames

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

int ldns_dname_compare(const ldns_rdf *dname1, const ldns_rdf *dname2);

int ldns_dname_interval(const ldns_rdf *prev, const ldns_rdf *middle,
const ldns_rdf *next);

* DESCRIPTION
/ldns_dname_compare/() Compares the two dname rdf's according to the
algorithm for ordering in RFC4034 Section 6. .br *dname1*: First dname
rdf to compare .br *dname2*: Second dname rdf to compare .br Returns -1
if dname1 comes before dname2, 1 if dname1 comes after dname2, and 0 if
they are equal.

/ldns_dname_interval/() check if middle lays in the interval defined by
prev and next prev <= middle < next. This is useful for nsec checking
.br *prev*: the previous dname .br *middle*: the dname to check .br
*next*: the next dname return 0 on error or unknown, -1 when middle is
in the interval, +1 when not

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_dname_is_subdomain/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
