#+TITLE: Manpages - XQueryBestCursor.3
#+DESCRIPTION: Linux manpage for XQueryBestCursor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XQueryBestCursor.3 is found in manpage for: [[../man3/XRecolorCursor.3][man3/XRecolorCursor.3]]