#+TITLE: Manpages - Alien_Build_Plugin_Probe_CommandLine.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Probe_CommandLine.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Probe::CommandLine - Probe for tools or commands
already available

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin Probe::CommandLine => ( command => gzip, args => [
--version ], match => qr/gzip/, version => qr/gzip ([0-9\.]+)/, );

* DESCRIPTION
This plugin probes for the existence of the given command line program.

* PROPERTIES
** command
The name of the command.

** args
The arguments to pass to the command.

** secondary
If you are using another probe plugin (such as
Alien::Build::Plugin::Probe::CBuilder or
Alien::Build::Plugin::PkgConfig::Negotiate) to detect the existence of a
library, but also need a program to exist, then you should set secondary
to a true value. For example when you need both:

use alienfile; # requires both liblzma library and xz program plugin
PkgConfig => liblzma; plugin Probe::CommandLine => ( command => xz,
secondary => 1, );

When you don't:

use alienfile; plugin Probe::CommandLine => ( command => gzip, secondary
=> 0, # default );

** match
Regular expression for which the program output should match.

** match_stderr
Regular expression for which the program standard error should match.

** version
Regular expression to parse out the version from the program output. The
regular expression should store the version number in =$1=.

** version_stderr
Regular expression to parse out the version from the program standard
error. The regular expression should store the version number in =$1=.

* SEE ALSO
Alien::Build, alienfile, Alien::Build::MM, Alien

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
