#+TITLE: Manpages - rtcCommitGeometry.3embree3
#+DESCRIPTION: Linux manpage for rtcCommitGeometry.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcCommitGeometry - commits geometry changes
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcCommitGeometry(RTCGeometry geometry);
#+end_example

** DESCRIPTION
The =rtcCommitGeometry= function is used to commit all geometry changes
performed to a geometry (=geometry= parameter). After a geometry gets
modified, this function must be called to properly update the internal
state of the geometry to perform interpolations using =rtcInterpolate=
or to commit a scene containing the geometry using =rtcCommitScene=.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcInterpolate], [rtcCommitScene]
