#+TITLE: Manpages - WildMidi_GetInfo.3
#+DESCRIPTION: Linux manpage for WildMidi_GetInfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
WildMidi_GetInfo - get information on a midi

* LIBRARY
*libWildMidi*

* SYNOPSIS
*#include <wildmidi_lib.h>*

*struct _WM_Info *WildMidi_GetInfo (midi */handle/);*

* DESCRIPTION
Obtains information allowing you to determine song position, current
options, and any copyright information.

- handle :: The identifier obtained from opening a midi file with
  *WildMidi_Open*(3)* or WildMidi_OpenBuffer*(3)

* RETURN VALUE
On error returns NULL with an error message displayed to stderr.

Otherwise returns a pointer to a struct containing the information.

#+begin_example
  struct _WM_Info {
     char *copyright;
     uint32_t current_sample;
     uint32_t approx_total_samples;
     uint32_t total_midi_time;
     uint16_t mixer_options;
     uint32_t total_midi_time;
  };
#+end_example

- copyright :: A pointer to a \0 terminated string containing any
  copyright MIDI events found while processing the MIDI file /handle/*
  refers to. If more than one copyright event was found then each one is
  separated by \n*

If /copyright/* is NULL then no copyright MIDI events were found.*

- current_sample :: This is the number of stereo samples libWildMidi has
  processed for the MIDI file referred to by /handle/*. You can use this
  value to determine the current playing time by dividing this value by
  the */rate/* given when libWildMidi was initialized by
  WildMidi_Init*(3)*.*

- approx_total_samples :: This is the total number of stereo samples
  libWildMidi expects to process. This can be used to obtain the total
  playing time by dividing this value by the /rate/* given when
  libWildMidi was initialized by WildMidi_Init*(3).* Also when you
  divide */current_sample/* by this value and multiplying by 100, you
  have the percentage currently processed.*

- total_midi_time :: This is the total time of MIDI events in 1/1000's
  of a second. It differs from /approx_total_samples/* in that it only
  states the total time within the MIDI file and does not take into
  account the extra bit of time to finish playing sampling smoothly.*

- mixer_options :: 

  - WM_MO_LOG_VOLUME :: Using curved volume settings instead of linear
    ones.

  - WM_MO_ENHANCED_RESAMPLING :: The enhanced resampler is active

  - WM_MO_REVERB :: Reverb is being added to the final output.

* SEE ALSO
*WildMidi_GetVersion*(3)*,* *WildMidi_Init*(3)*,*
*WildMidi_MasterVolume*(3)*,* *WildMidi_Open*(3)*,*
*WildMidi_OpenBuffer*(3)*,* *WildMidi_SetOption*(3)*,*
*WildMidi_GetOutput*(3)*,* *WildMidi_GetMidiOutput*(3)*,*
*WildMidi_FastSeek*(3)*,* *WildMidi_Close*(3)*,*
*WildMidi_Shutdown*(3)*,* *wildmidi.cfg*(5)

* AUTHOR
Chris Ison <chrisisonwildcode@gmail.com> Bret Curtis <psi29a@gmail.com>

* COPYRIGHT
Copyright (C) WildMidi Developers 2001-2016

This file is part of WildMIDI.

WildMIDI is free software: you can redistribute and/or modify the player
under the terms of the GNU General Public License and you can
redistribute and/or modify the library under the terms of the GNU Lesser
General Public License as published by the Free Software Foundation,
either version 3 of the licenses, or(at your option) any later version.

WildMIDI is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License and
the GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License and
the GNU Lesser General Public License along with WildMIDI. If not, see
<http://www.gnu.org/licenses/>.

This manpage is licensed under the Creative Commons Attribution-Share
Alike 3.0 Unported License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to
Creative Commons, 171 Second Street, Suite 300, San Francisco,
California, 94105, USA.
