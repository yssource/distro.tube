#+TITLE: Manpages - MPI_Cartdim_get.3
#+DESCRIPTION: Linux manpage for MPI_Cartdim_get.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Cartdim_get * - Retrieves Cartesian topology information associated
with a communicator.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Cartdim_get(MPI_Comm comm, int *ndims)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_CARTDIM_GET(COMM, NDIMS, IERROR)
  	INTEGER	COMM, NDIMS, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Cartdim_get(comm, ndims, ierror)
  	TYPE(MPI_Comm), INTENT(IN) :: comm
  	INTEGER, INTENT(OUT) :: ndims
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  int Cartcomm::Get_dim() const
#+end_example

* INPUT PARAMETER
- comm :: Communicator with Cartesian structure (handle).

* OUTPUT PARAMETERS
- ndims :: Number of dimensions of the Cartesian structure (integer).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_Cartdim_get returns the number of dimensions of the Cartesian
structure.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

* SEE ALSO
#+begin_example
  MPI_Cart_get
  MPI_Cart_create
#+end_example
