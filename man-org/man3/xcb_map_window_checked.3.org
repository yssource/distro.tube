#+TITLE: Manpages - xcb_map_window_checked.3
#+DESCRIPTION: Linux manpage for xcb_map_window_checked.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_map_window_checked.3 is found in manpage for: [[../man3/xcb_map_window.3][man3/xcb_map_window.3]]