#+TITLE: Manpages - CURLOPT_COOKIEFILE.3
#+DESCRIPTION: Linux manpage for CURLOPT_COOKIEFILE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_COOKIEFILE - file name to read cookies from

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_COOKIEFILE, char
*filename);

* DESCRIPTION
Pass a pointer to a null-terminated string as parameter. It should point
to the file name of your file holding cookie data to read. The cookie
data can be in either the old Netscape / Mozilla cookie data format or
just regular HTTP headers (Set-Cookie style) dumped to a file.

It also enables the cookie engine, making libcurl parse and send cookies
on subsequent requests with this handle.

Given an empty or non-existing file or by passing the empty string ("")
to this option, you can enable the cookie engine without reading any
initial cookies. If you tell libcurl the file name is "-" (just a single
minus sign), libcurl will instead read from stdin.

This option only *reads* cookies. To make libcurl write cookies to file,
see /CURLOPT_COOKIEJAR(3)/.

If you use the Set-Cookie file format and do not specify a domain then
the cookie is not sent since the domain will never match. To address
this, set a domain in Set-Cookie line (doing that will include
sub-domains) or preferably: use the Netscape format.

If you use this option multiple times, you just add more files to read.
Subsequent files will add more cookies.

The application does not have to keep the string around after setting
this option.

Setting this option to NULL will (since 7.77.0) explicitly disable the
cookie engine and clear the list of files to read cookies from.

* DEFAULT
NULL

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

    /* get cookies from an existing file */
    curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "/tmp/cookies.txt");

    ret = curl_easy_perform(curl);

    curl_easy_cleanup(curl);
  }
#+end_example

* Cookie file format
The cookie file format and general cookie concepts in curl are described
in the HTTP-COOKIES.md file, also hosted online here:
https://curl.se/docs/http-cookies.html

* AVAILABILITY
As long as HTTP is supported

* RETURN VALUE
Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

* SEE ALSO
*CURLOPT_COOKIE*(3), *CURLOPT_COOKIEJAR*(3),
