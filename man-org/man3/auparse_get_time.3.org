#+TITLE: Manpages - auparse_get_time.3
#+DESCRIPTION: Linux manpage for auparse_get_time.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_get_time - get event's time

* SYNOPSIS
*#include <auparse.h>*

time_t auparse_get_time(auparse_state_t *au);

* DESCRIPTION
auparse_get_time will access just the time portion of the timestamp data
structure for the current event.

* RETURN VALUE
Returns 0 if an error occurs; otherwise, the valid time value in time_t
format.

* SEE ALSO
*time*(3), *auparse_get_timestamp*(3), *auparse_get_milli*(3).
*auparse_get_serial*(3). *auparse_get_node*(3).

* AUTHOR
Steve Grubb
