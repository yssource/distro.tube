#+TITLE: Manpages - capng_restore_state.3
#+DESCRIPTION: Linux manpage for capng_restore_state.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
capng_restore_state - set the internal library state

* SYNOPSIS
*#include <cap-ng.h>*

void capng_restore_state(void **state);

* DESCRIPTION
capng_restore_state is a function that takes the state information
previously saved by capng_save_state and restores the libraries internal
state. This function is not available in the python bindings.

* RETURN VALUE
None.

* NOTES
capng_restore_state free's the previously malloc'd state, thus the state
can't be restored multiple times.

The working pid is part of the restored state, if restoring the state to
a different thread, capng_setpid can be used to update it.

* SEE ALSO
*capng_save_state*(3), *capabilities*(7)

* AUTHOR
Steve Grubb
