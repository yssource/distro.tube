#+TITLE: Manpages - iconv_close.3
#+DESCRIPTION: Linux manpage for iconv_close.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
iconv_close - deallocate descriptor for character set conversion

* SYNOPSIS
#+begin_example
  #include <iconv.h>

  int iconv_close(iconv_t cd);
#+end_example

* DESCRIPTION
The *iconv_close*() function deallocates a conversion descriptor /cd/
previously allocated using *iconv_open*(3).

* RETURN VALUE
On success, *iconv_close*() returns 0; otherwise, it returns -1 and sets
/errno/ to indicate the error.

* VERSIONS
This function is available in glibc since version 2.1.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface       | Attribute     | Value   |
| *iconv_close*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, SUSv2.

* SEE ALSO
*iconv*(3), *iconv_open*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
