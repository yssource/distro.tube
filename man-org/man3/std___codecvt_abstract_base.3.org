#+TITLE: Manpages - std___codecvt_abstract_base.3
#+DESCRIPTION: Linux manpage for std___codecvt_abstract_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__codecvt_abstract_base< _InternT, _ExternT, _StateT > - Common
base for codecvt functions.

* SYNOPSIS
\\

=#include <codecvt.h>=

Inherits *std::locale::facet*, and *std::codecvt_base*.

Inherited by *std::codecvt< _Elem, char, mbstate_t >*, and
*std::codecvt< _InternT, _ExternT, _StateT >*.

** Public Types
typedef _ExternT *extern_type*\\

typedef _InternT *intern_type*\\

typedef codecvt_base::result *result*\\

typedef _StateT *state_type*\\

** Public Member Functions
bool *always_noconv* () const throw ()\\

int *encoding* () const throw ()\\

result *in* (state_type &__state, const extern_type *__from, const
extern_type *__from_end, const extern_type *&__from_next, intern_type
*__to, intern_type *__to_end, intern_type *&__to_next) const\\
Convert from external to internal character set.

int *length* (state_type &__state, const extern_type *__from, const
extern_type *__end, size_t __max) const\\

int *max_length* () const throw ()\\

result *out* (state_type &__state, const intern_type *__from, const
intern_type *__from_end, const intern_type *&__from_next, extern_type
*__to, extern_type *__to_end, extern_type *&__to_next) const\\
Convert from internal to external character set.

result *unshift* (state_type &__state, extern_type *__to, extern_type
*__to_end, extern_type *&__to_next) const\\
Reset conversion state.

** Protected Member Functions
*__codecvt_abstract_base* (size_t __refs=0)\\

virtual bool *do_always_noconv* () const =0 throw ()\\

virtual int *do_encoding* () const =0 throw ()\\

virtual result *do_in* (state_type &__state, const extern_type *__from,
const extern_type *__from_end, const extern_type *&__from_next,
intern_type *__to, intern_type *__to_end, intern_type *&__to_next) const
=0\\

virtual int *do_length* (state_type &, const extern_type *__from, const
extern_type *__end, size_t __max) const =0\\

virtual int *do_max_length* () const =0 throw ()\\

virtual result *do_out* (state_type &__state, const intern_type *__from,
const intern_type *__from_end, const intern_type *&__from_next,
extern_type *__to, extern_type *__to_end, extern_type *&__to_next) const
=0\\
Convert from internal to external character set.

virtual result *do_unshift* (state_type &__state, extern_type *__to,
extern_type *__to_end, extern_type *&__to_next) const =0\\

** Static Protected Member Functions
static __c_locale *_S_clone_c_locale* (__c_locale &__cloc) throw ()\\

static void *_S_create_c_locale* (__c_locale &__cloc, const char *__s,
__c_locale __old=0)\\

static void *_S_destroy_c_locale* (__c_locale &__cloc)\\

static __c_locale *_S_get_c_locale* ()\\

static const char * *_S_get_c_name* () throw ()\\

static __c_locale *_S_lc_ctype_c_locale* (__c_locale __cloc, const char
*__s)\\

* Detailed Description
** "template<typename _InternT, typename _ExternT, typename _StateT>
\\
class std::__codecvt_abstract_base< _InternT, _ExternT, _StateT >"Common
base for codecvt functions.

This template class provides implementations of the public functions
that forward to the protected virtual functions.

This template also provides abstract stubs for the protected virtual
functions.

Definition at line *71* of file *codecvt.h*.

* Member Typedef Documentation
** template<typename _InternT , typename _ExternT , typename _StateT >
typedef _ExternT *std::__codecvt_abstract_base*< _InternT, _ExternT,
_StateT >::extern_type
Definition at line *78* of file *codecvt.h*.

** template<typename _InternT , typename _ExternT , typename _StateT >
typedef _InternT *std::__codecvt_abstract_base*< _InternT, _ExternT,
_StateT >::intern_type
Definition at line *77* of file *codecvt.h*.

** template<typename _InternT , typename _ExternT , typename _StateT >
typedef codecvt_base::result *std::__codecvt_abstract_base*< _InternT,
_ExternT, _StateT >::result
Definition at line *76* of file *codecvt.h*.

** template<typename _InternT , typename _ExternT , typename _StateT >
typedef _StateT *std::__codecvt_abstract_base*< _InternT, _ExternT,
_StateT >::state_type
Definition at line *79* of file *codecvt.h*.

* Constructor & Destructor Documentation
** template<typename _InternT , typename _ExternT , typename _StateT >
*std::__codecvt_abstract_base*< _InternT, _ExternT, _StateT
>::*__codecvt_abstract_base* (size_t __refs = =0=)= [inline]=,
= [explicit]=, = [protected]=
Definition at line *227* of file *codecvt.h*.

** template<typename _InternT , typename _ExternT , typename _StateT >
virtual *std::__codecvt_abstract_base*< _InternT, _ExternT, _StateT
>::~*__codecvt_abstract_base* ()= [inline]=, = [protected]=,
= [virtual]=
Definition at line *230* of file *codecvt.h*.

* Member Function Documentation
** template<typename _InternT , typename _ExternT , typename _StateT >
bool *std::__codecvt_abstract_base*< _InternT, _ExternT, _StateT
>::always_noconv () const= [inline]=
Definition at line *213* of file *codecvt.h*.

** template<typename _InternT , typename _ExternT , typename _StateT >
virtual result *std::__codecvt_abstract_base*< _InternT, _ExternT,
_StateT >::do_out (state_type & __state, const intern_type * __from,
const intern_type * __from_end, const intern_type *& __from_next,
extern_type * __to, extern_type * __to_end, extern_type *& __to_next)
const= [protected]=, = [pure virtual]=
Convert from internal to external character set. Converts input string
of intern_type to output string of extern_type. This function is a hook
for derived classes to change the value returned.

*See also*

#+begin_quote
  out for more information.
#+end_quote

Implemented in *std::codecvt< _InternT, _ExternT, _StateT >*,
*std::codecvt< _Elem, char, mbstate_t >*, *std::codecvt< char, char,
mbstate_t >*, *std::codecvt< wchar_t, char, mbstate_t >*, *std::codecvt<
char16_t, char, mbstate_t >*, *std::codecvt< char32_t, char, mbstate_t
>*, and *std::codecvt< _InternT, _ExternT, encoding_state >*.

Referenced by *std::__codecvt_abstract_base< _InternT, _ExternT, _StateT
>::out()*.

** template<typename _InternT , typename _ExternT , typename _StateT >
int *std::__codecvt_abstract_base*< _InternT, _ExternT, _StateT
>::encoding () const= [inline]=
Definition at line *209* of file *codecvt.h*.

** template<typename _InternT , typename _ExternT , typename _StateT >
result *std::__codecvt_abstract_base*< _InternT, _ExternT, _StateT >::in
(state_type & __state, const extern_type * __from, const extern_type *
__from_end, const extern_type *& __from_next, intern_type * __to,
intern_type * __to_end, intern_type *& __to_next) const= [inline]=
Convert from external to internal character set. Converts input string
of extern_type to output string of intern_type. This is analogous to
mbsrtowcs. It does this by calling codecvt::do_in.

The source and destination character sets are determined by the facet's
locale, internal and external types.

The characters in [from,from_end) are converted and written to
[to,to_end). from_next and to_next are set to point to the character
following the last successfully converted character, respectively. If
the result needed no conversion, from_next and to_next are not affected.

The /state/ argument should be initialized if the input is at the
beginning and carried from a previous call if continuing conversion.
There are no guarantees about how /state/ is used.

The result returned is a member of codecvt_base::result. If all the
input is converted, returns codecvt_base::ok. If no conversion is
necessary, returns codecvt_base::noconv. If the input ends early or
there is insufficient space in the output, returns
codecvt_base::partial. Otherwise the conversion failed and
codecvt_base::error is returned.

*Parameters*

#+begin_quote
  /__state/ Persistent conversion state data.\\
  /__from/ Start of input.\\
  /__from_end/ End of input.\\
  /__from_next/ Returns start of unconverted data.\\
  /__to/ Start of output buffer.\\
  /__to_end/ End of output buffer.\\
  /__to_next/ Returns start of unused output area.
#+end_quote

*Returns*

#+begin_quote
  codecvt_base::result.
#+end_quote

Definition at line *199* of file *codecvt.h*.

** template<typename _InternT , typename _ExternT , typename _StateT >
int *std::__codecvt_abstract_base*< _InternT, _ExternT, _StateT
>::length (state_type & __state, const extern_type * __from, const
extern_type * __end, size_t __max) const= [inline]=
Definition at line *217* of file *codecvt.h*.

** template<typename _InternT , typename _ExternT , typename _StateT >
int *std::__codecvt_abstract_base*< _InternT, _ExternT, _StateT
>::max_length () const= [inline]=
Definition at line *222* of file *codecvt.h*.

** template<typename _InternT , typename _ExternT , typename _StateT >
result *std::__codecvt_abstract_base*< _InternT, _ExternT, _StateT
>::out (state_type & __state, const intern_type * __from, const
intern_type * __from_end, const intern_type *& __from_next, extern_type
* __to, extern_type * __to_end, extern_type *& __to_next)
const= [inline]=
Convert from internal to external character set. Converts input string
of intern_type to output string of extern_type. This is analogous to
wcsrtombs. It does this by calling codecvt::do_out.

The source and destination character sets are determined by the facet's
locale, internal and external types.

The characters in [from,from_end) are converted and written to
[to,to_end). from_next and to_next are set to point to the character
following the last successfully converted character, respectively. If
the result needed no conversion, from_next and to_next are not affected.

The /state/ argument should be initialized if the input is at the
beginning and carried from a previous call if continuing conversion.
There are no guarantees about how /state/ is used.

The result returned is a member of codecvt_base::result. If all the
input is converted, returns codecvt_base::ok. If no conversion is
necessary, returns codecvt_base::noconv. If the input ends early or
there is insufficient space in the output, returns
codecvt_base::partial. Otherwise the conversion failed and
codecvt_base::error is returned.

*Parameters*

#+begin_quote
  /__state/ Persistent conversion state data.\\
  /__from/ Start of input.\\
  /__from_end/ End of input.\\
  /__from_next/ Returns start of unconverted data.\\
  /__to/ Start of output buffer.\\
  /__to_end/ End of output buffer.\\
  /__to_next/ Returns start of unused output area.
#+end_quote

*Returns*

#+begin_quote
  codecvt_base::result.
#+end_quote

Definition at line *119* of file *codecvt.h*.

References *std::__codecvt_abstract_base< _InternT, _ExternT, _StateT
>::do_out()*.

** template<typename _InternT , typename _ExternT , typename _StateT >
result *std::__codecvt_abstract_base*< _InternT, _ExternT, _StateT
>::unshift (state_type & __state, extern_type * __to, extern_type *
__to_end, extern_type *& __to_next) const= [inline]=
Reset conversion state. Writes characters to output that would restore
/state/ to initial conditions. The idea is that if a partial conversion
occurs, then the converting the characters written by this function
would leave the state in initial conditions, rather than partial
conversion state. It does this by calling codecvt::do_unshift().

For example, if 4 external characters always converted to 1 internal
character, and input to in() had 6 external characters with state saved,
this function would write two characters to the output and set the state
to initialized conditions.

The source and destination character sets are determined by the facet's
locale, internal and external types.

The result returned is a member of codecvt_base::result. If the state
could be reset and data written, returns codecvt_base::ok. If no
conversion is necessary, returns codecvt_base::noconv. If the output has
insufficient space, returns codecvt_base::partial. Otherwise the reset
failed and codecvt_base::error is returned.

*Parameters*

#+begin_quote
  /__state/ Persistent conversion state data.\\
  /__to/ Start of output buffer.\\
  /__to_end/ End of output buffer.\\
  /__to_next/ Returns start of unused output area.
#+end_quote

*Returns*

#+begin_quote
  codecvt_base::result.
#+end_quote

Definition at line *158* of file *codecvt.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
