#+TITLE: Manpages - MPI_Win_sync.3
#+DESCRIPTION: Linux manpage for MPI_Win_sync.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Win_sync*, - Synchronize the private and public copies of the
window

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Win_sync (MPI_Win win)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_WIN_SYNC(WIN, IERROR)
  	INTEGER WIN, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Win_sync(win, ierror)
  	TYPE(MPI_Win), INTENT(IN) :: win
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* INPUT PARAMETERS
- win :: Window object (handle).

* OUTPUT PARAMETER
- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
*MPI_Win_sync* synchronizes the private and public window copies of
/win/. For the purposes of synchronizing the private and public window,
*MPI_Win_sync* has the effect of ending and reopening an access and
exposure epoch on the window (note that it does not actually end an
epoch or complete any pending MPI RMA operations).

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
*MPI_Comm_set_errhandler*; the predefined error handler
MPI_ERRORS_RETURN may be used to cause error values to be returned. Note
that MPI does not guarantee that an MPI program can continue past an
error.
