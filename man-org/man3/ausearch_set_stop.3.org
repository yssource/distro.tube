#+TITLE: Manpages - ausearch_set_stop.3
#+DESCRIPTION: Linux manpage for ausearch_set_stop.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ausearch_set_stop - set the cursor position

* SYNOPSIS
*#include <auparse.h>*

int ausearch_set_stop(auparse_state_t *au, austop_t where);

* DESCRIPTION
ausearch_set_stop determines where the internal cursor will stop when a
search condition is met. The possible values are:

#+begin_quote
  - /AUSEARCH_STOP_EVENT/ :: This one repositions the cursors to the
    first field of the first record of the event containing the items
    searched for.

  - /AUSEARCH_STOP_RECORD/ :: This one repositions the cursors to the
    first field of the record containing the items searched for.

  - /AUSEARCH_STOP_FIELD/ :: This one simply stops on the current field
    when the evaluation of the rules becomes true.
#+end_quote

* RETURN VALUE
Returns -1 if an error occurs; otherwise, 0 for success.

* SEE ALSO
*ausearch_add_item*(3), *ausearch_add_regex*(3), *ausearch_clear*(3),
*ausearch_next_event*(3).

* AUTHOR
Steve Grubb
