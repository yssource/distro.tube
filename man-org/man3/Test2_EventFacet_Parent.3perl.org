#+TITLE: Manpages - Test2_EventFacet_Parent.3perl
#+DESCRIPTION: Linux manpage for Test2_EventFacet_Parent.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::EventFacet::Parent - Facet for events contains other events

* DESCRIPTION
This facet is used when an event contains other events, such as a
subtest.

* FIELDS
- $string = $parent->{details} :: 

- $string = $parent->details() :: 

Human readable description of the event.

- $hid = $parent->{hid} :: 

- $hid = $parent->hid() :: 

Hub ID of the hub that is represented in the parent-child relationship.

- $arrayref = $parent->{children} :: 

- $arrayref = $parent->children() :: 

Arrayref containing the facet-data hashes of events nested under this
one. /To get the actual events you need to get them from the parent
event directly/

- $bool = $parent->{buffered} :: 

- $bool = $parent->buffered() :: 

True if the subtest is buffered (meaning the formatter has probably not
seen them yet).

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
