#+TITLE: Manpages - xcb_expose_event_t.3
#+DESCRIPTION: Linux manpage for xcb_expose_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_expose_event_t - NOT YET DOCUMENTED

* SYNOPSIS
*#include <xcb/xproto.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_expose_event_t {
      uint8_t      response_type;
      uint8_t      pad0;
      uint16_t     sequence;
      xcb_window_t window;
      uint16_t     x;
      uint16_t     y;
      uint16_t     width;
      uint16_t     height;
      uint16_t     count;
      uint8_t      pad1[2];
  } xcb_expose_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case /XCB_EXPOSE/.
  This field is also present in the /xcb_generic_event_t/ and can be
  used to tell events apart from each other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- window :: The exposed (damaged) window.

24. The X coordinate of the left-upper corner of the exposed rectangle,
    relative to the /window/'s origin.

25. The Y coordinate of the left-upper corner of the exposed rectangle,
    relative to the /window/'s origin.

- width :: The width of the exposed rectangle.

- height :: The height of the exposed rectangle.

- count :: The amount of /Expose/ events following this one. Simple
  applications that do not want to optimize redisplay by distinguishing
  between subareas of its window can just ignore all Expose events with
  nonzero counts and perform full redisplays on events with zero counts.

* DESCRIPTION
* SEE ALSO
*xcb_generic_event_t*(3)

* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
