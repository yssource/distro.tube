#+TITLE: Manpages - XDisplayMotionBufferSize.3
#+DESCRIPTION: Linux manpage for XDisplayMotionBufferSize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XDisplayMotionBufferSize.3 is found in manpage for: [[../man3/XSendEvent.3][man3/XSendEvent.3]]