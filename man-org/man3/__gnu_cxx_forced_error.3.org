#+TITLE: Manpages - __gnu_cxx_forced_error.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_forced_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::forced_error - Thown by exception safety machinery.

* SYNOPSIS
\\

=#include <throw_allocator.h>=

Inherits *std::exception*.

** Public Member Functions
virtual const char * *what* () const noexcept\\

* Detailed Description
Thown by exception safety machinery.

Definition at line *79* of file *throw_allocator.h*.

* Member Function Documentation
** virtual const char * std::exception::what () const= [virtual]=,
= [noexcept]=, = [inherited]=
Returns a C-style character string describing the general cause of the
current error.\\

Reimplemented in *std::ios_base::failure*, *std::bad_alloc*,
*std::filesystem::filesystem_error*, *std::bad_weak_ptr*,
*std::bad_function_call*,
*std::experimental::fundamentals_v1::bad_any_cast*,
*std::experimental::filesystem::v1::filesystem_error*,
*std::bad_any_cast*, *std::future_error*, *std::logic_error*,
*std::runtime_error*, *std::bad_exception*, *std::bad_cast*,
*std::bad_typeid*, and *std::bad_optional_access*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
