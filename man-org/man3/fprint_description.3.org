#+TITLE: Manpages - fprint_description.3
#+DESCRIPTION: Linux manpage for fprint_description.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fprint_description.3 is found in manpage for: [[../man3/netsnmp_mib_api.3][man3/netsnmp_mib_api.3]]