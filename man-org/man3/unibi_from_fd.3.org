#+TITLE: Manpages - unibi_from_fd.3
#+DESCRIPTION: Linux manpage for unibi_from_fd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_from_fd - read a terminfo entry from a file descriptor

* SYNOPSIS
#include <unibilium.h> unibi_term *unibi_from_fd(int fd);

* DESCRIPTION
This function reads up to 4096 bytes from /fd/, then calls
=unibi_from_mem=.

* RETURN VALUE
See *unibi_from_mem* (3).

* SEE ALSO
*unibilium.h* (3), *unibi_from_mem* (3), *unibi_destroy* (3)
