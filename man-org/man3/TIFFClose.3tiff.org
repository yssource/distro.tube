#+TITLE: Manpages - TIFFClose.3tiff
#+DESCRIPTION: Linux manpage for TIFFClose.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFClose - close a previously opened

file

* SYNOPSIS
*#include <tiffio.h>*

*void TIFFClose(TIFF **/tif/*)*

* DESCRIPTION
/TIFFClose/ closes a file that was previously opened with
*TIFFOpen*(3TIFF). Any buffered data are flushed to the file, including
the contents of the current directory (if modified); and all resources
are reclaimed.

* DIAGNOSTICS
All error messages are directed to the *TIFFError*(3TIFF) routine.
Likewise, warning messages are directed to the *TIFFWarning*(3TIFF)
routine.

* SEE ALSO
*libtiff*(3TIFF), *TIFFOpen*(3TIFF)

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
