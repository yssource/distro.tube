#+TITLE: Manpages - EVP_PKEY_CTX_set1_pbe_pass.3ssl
#+DESCRIPTION: Linux manpage for EVP_PKEY_CTX_set1_pbe_pass.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_PKEY_CTX_set1_pbe_pass - generic KDF support functions

* SYNOPSIS
#include <openssl/kdf.h> int EVP_PKEY_CTX_set1_pbe_pass(EVP_PKEY_CTX
*pctx, unsigned char *pass, int passlen);

* DESCRIPTION
These functions are generic support functions for all KDF algorithms.

*EVP_PKEY_CTX_set1_pbe_pass()* sets the password to the *passlen* first
bytes from *pass*.

* STRING CTRLS
There is also support for string based control operations via
*EVP_PKEY_CTX_ctrl_str* (3). The *password* can be directly specified
using the *type* parameter pass or given in hex encoding using the
hexpass parameter.

* NOTES
All these functions are implemented as macros.

* RETURN VALUES
All these functions return 1 for success and 0 or a negative value for
failure. In particular a return value of -2 indicates the operation is
not supported by the public key algorithm.

* SEE ALSO
*EVP_PKEY_CTX_new* (3), *EVP_PKEY_CTX_ctrl_str* (3),
*EVP_PKEY_derive* (3)

* COPYRIGHT
Copyright 2018 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
