#+TITLE: Manpages - SSL_SESSION_get_ex_data.3ssl
#+DESCRIPTION: Linux manpage for SSL_SESSION_get_ex_data.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_SESSION_set_ex_data, SSL_SESSION_get_ex_data - get and set
application specific data on a session

* SYNOPSIS
#include <openssl/ssl.h> int SSL_SESSION_set_ex_data(SSL_SESSION *ss,
int idx, void *data); void *SSL_SESSION_get_ex_data(const SSL_SESSION
*s, int idx);

* DESCRIPTION
*SSL_SESSION_set_ex_data()* enables an application to store arbitrary
application specific data *data* in an SSL_SESSION structure *ss*. The
index *idx* should be a value previously returned from a call to
*CRYPTO_get_ex_new_index* (3).

*SSL_SESSION_get_ex_data()* retrieves application specific data
previously stored in an SSL_SESSION structure *s*. The *idx* value
should be the same as that used when originally storing the data.

* RETURN VALUES
*SSL_SESSION_set_ex_data()* returns 1 for success or 0 for failure.

*SSL_SESSION_get_ex_data()* returns the previously stored value or NULL
on failure. NULL may also be a valid value.

* SEE ALSO
*ssl* (7), *CRYPTO_get_ex_new_index* (3)

* COPYRIGHT
Copyright 2017 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
