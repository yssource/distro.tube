#+TITLE: Manpages - Config_Extensions.3perl
#+DESCRIPTION: Linux manpage for Config_Extensions.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Config::Extensions - hash lookup of which core extensions were built.

* SYNOPSIS
use Config::Extensions %Extensions; if ($Extensions{PerlIO::via}) { #
This perl has PerlIO::via built }

* DESCRIPTION
The Config::Extensions module provides a hash =%Extensions= containing
all the core extensions that were enabled for this perl. The hash is
keyed by extension name, with each entry having one of 3 possible
values:

- dynamic :: The extension is dynamically linked

- nonxs :: The extension is pure perl, so doesn't need linking to the
  perl executable

- static :: The extension is statically linked to the perl binary

As all values evaluate to true, a simple =if= test is good enough to
determine whether an extension is present.

All the data uses to generate the =%Extensions= hash is already present
in the =Config= module, but not in such a convenient format to quickly
reference.

* AUTHOR
Nicholas Clark <nick@ccl4.org>
