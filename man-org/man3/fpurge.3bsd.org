#+TITLE: Manpages - fpurge.3bsd
#+DESCRIPTION: Linux manpage for fpurge.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The function

erases any input or output buffered in the given

For output streams this discards any unwritten output. For input streams
this discards any input read from the underlying object but not yet
obtained via

this includes any text pushed back via

Upon successful completion 0 is returned. Otherwise,

is returned and the global variable

is set to indicate the error.

is not an open stream.

The

function first appeared in
