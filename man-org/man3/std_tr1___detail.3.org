#+TITLE: Manpages - std_tr1___detail.3
#+DESCRIPTION: Linux manpage for std_tr1___detail.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::tr1::__detail - Implementation details not part of the namespace
std::tr1 interface.

* SYNOPSIS
\\

* Detailed Description
Implementation details not part of the namespace std::tr1 interface.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
