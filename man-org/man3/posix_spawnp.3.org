#+TITLE: Manpages - posix_spawnp.3
#+DESCRIPTION: Linux manpage for posix_spawnp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about posix_spawnp.3 is found in manpage for: [[../man3/posix_spawn.3][man3/posix_spawn.3]]