#+TITLE: Manpages - std_default_delete.3
#+DESCRIPTION: Linux manpage for std_default_delete.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::default_delete< _Tp > - Primary template of default_delete, used by
unique_ptr for single objects.

* SYNOPSIS
\\

=#include <unique_ptr.h>=

** Public Member Functions
constexpr *default_delete* () noexcept=default\\
Default constructor.

template<typename _Up , typename = _Require<is_convertible<_Up*, _Tp*>>>
*default_delete* (const *default_delete*< _Up > &) noexcept\\
Converting constructor.

void *operator()* (_Tp *__ptr) const\\
Calls =delete __ptr=

* Detailed Description
** "template<typename _Tp>
\\
struct std::default_delete< _Tp >"Primary template of default_delete,
used by unique_ptr for single objects.

Definition at line *63* of file *unique_ptr.h*.

* Constructor & Destructor Documentation
** template<typename _Tp > constexpr *std::default_delete*< _Tp
>::*default_delete* ()= [constexpr]=, = [default]=, = [noexcept]=
Default constructor.

** template<typename _Tp > template<typename _Up , typename =
_Require<is_convertible<_Up*, _Tp*>>> *std::default_delete*< _Tp
>::*default_delete* (const *default_delete*< _Up > &)= [inline]=,
= [noexcept]=
Converting constructor. Allows conversion from a deleter for objects of
another type, =_Up=, only if =_Up*= is convertible to =_Tp*=.

Definition at line *75* of file *unique_ptr.h*.

* Member Function Documentation
** template<typename _Tp > void *std::default_delete*< _Tp >::operator()
(_Tp * __ptr) const= [inline]=
Calls =delete __ptr=

Definition at line *79* of file *unique_ptr.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
