#+TITLE: Manpages - getspent.3
#+DESCRIPTION: Linux manpage for getspent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getspent.3 is found in manpage for: [[../man3/getspnam.3][man3/getspnam.3]]