#+TITLE: Manpages - SDL_JoystickGetBall.3
#+DESCRIPTION: Linux manpage for SDL_JoystickGetBall.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_JoystickGetBall - Get relative trackball motion

* SYNOPSIS
*#include "SDL.h"*

*int SDL_JoystickGetBall*(*SDL_Joystick *joystick, int ball, int *dx,
int *dy*);

* DESCRIPTION
Get the *ball* axis change.

Trackballs can only return relative motion since the last call to
*SDL_JoystickGetBall*, these motion deltas a placed into *dx* and *dy*.

* RETURN VALUE
Returns *0* on success or *-1* on failure

* EXAMPLES
#+begin_example
  int delta_x, delta_y;
  SDL_Joystick *joy;
  .
  .
  .
  SDL_JoystickUpdate();
  if(SDL_JoystickGetBall(joy, 0, &delta_x, &delta_y)==-1)
    printf("TrackBall Read Error!
  ");
  printf("Trackball Delta- X:%d, Y:%d
  ", delta_x, delta_y);
#+end_example

* SEE ALSO
*SDL_JoystickNumBalls*
