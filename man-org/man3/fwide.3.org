#+TITLE: Manpages - fwide.3
#+DESCRIPTION: Linux manpage for fwide.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fwide - set and determine the orientation of a FILE stream

* SYNOPSIS
#+begin_example
  #include <wchar.h>

  int fwide(FILE *stream, int mode);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*fwide*():

#+begin_example
      _XOPEN_SOURCE >= 500 || _ISOC99_SOURCE
          || _POSIX_C_SOURCE >= 200112L
#+end_example

* DESCRIPTION
When /mode/ is zero, the *fwide*() function determines the current
orientation of /stream/. It returns a positive value if /stream/ is
wide-character oriented, that is, if wide-character I/O is permitted but
char I/O is disallowed. It returns a negative value if /stream/ is byte
oriented---that is, if char I/O is permitted but wide-character I/O is
disallowed. It returns zero if /stream/ has no orientation yet; in this
case the next I/O operation might change the orientation (to byte
oriented if it is a char I/O operation, or to wide-character oriented if
it is a wide-character I/O operation).

Once a stream has an orientation, it cannot be changed and persists
until the stream is closed.

When /mode/ is nonzero, the *fwide*() function first attempts to set
/stream/'s orientation (to wide-character oriented if /mode/ is greater
than 0, or to byte oriented if /mode/ is less than 0). It then returns a
value denoting the current orientation, as above.

* RETURN VALUE
The *fwide*() function returns the stream's orientation, after possibly
changing it. A positive return value means wide-character oriented. A
negative return value means byte oriented. A return value of zero means
undecided.

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C99.

* NOTES
Wide-character output to a byte oriented stream can be performed through
the *fprintf*(3) function with the *%lc* and *%ls* directives.

Char oriented output to a wide-character oriented stream can be
performed through the *fwprintf*(3) function with the *%c* and *%s*
directives.

* SEE ALSO
*fprintf*(3), *fwprintf*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
