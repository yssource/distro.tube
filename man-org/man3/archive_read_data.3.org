#+TITLE: Manpages - archive_read_data.3
#+DESCRIPTION: Linux manpage for archive_read_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

Read data associated with the header just read. Internally, this is a
convenience function that calls

and fills any gaps with nulls so that callers see a single continuous
stream of data.

Return the next available block of data for this entry. Unlike

the

function avoids copying data and allows you to correctly handle sparse
files, as supported by some archive formats. The library guarantees that
offsets will increase and that blocks will not overlap. Note that the
blocks returned from this function can be much larger than the block
size read from disk, due to compression and internal buffer
optimizations.

A convenience function that repeatedly calls

to skip all of the data for this archive entry. Note that this function
is invoked automatically by

if the previous entry was not completely consumed.

A convenience function that repeatedly calls

to copy the entire entry to the provided file descriptor.

Most functions return zero on success, non-zero on error. The possible
return codes include:

(the operation succeeded),

(the operation succeeded but a non-critical error was encountered),

(end-of-archive was encountered),

(the operation failed but can be retried), and

(there was a fatal error; the archive should be closed immediately).

returns a count of bytes actually read or zero at the end of the entry.
On error, a value of

or

is returned.

Detailed error codes and textual descriptions are available from the

and

functions.
