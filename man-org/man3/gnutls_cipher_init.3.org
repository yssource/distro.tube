#+TITLE: Manpages - gnutls_cipher_init.3
#+DESCRIPTION: Linux manpage for gnutls_cipher_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_cipher_init - API function

* SYNOPSIS
*#include <gnutls/crypto.h>*

*int gnutls_cipher_init(gnutls_cipher_hd_t * */handle/*,
gnutls_cipher_algorithm_t */cipher/*, const gnutls_datum_t * */key/*,
const gnutls_datum_t * */iv/*);*

* ARGUMENTS
- gnutls_cipher_hd_t * handle :: is a *gnutls_cipher_hd_t* type

- gnutls_cipher_algorithm_t cipher :: the encryption algorithm to use

- const gnutls_datum_t * key :: the key to be used for
  encryption/decryption

- const gnutls_datum_t * iv :: the IV to use (if not applicable set
  NULL)

* DESCRIPTION
This function will initialize the /handle/ context to be usable for
encryption/decryption of data. This will effectively use the current
crypto backend in use by gnutls or the cryptographic accelerator in use.

* RETURNS
Zero or a negative error code on error.

* SINCE
2.10.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
