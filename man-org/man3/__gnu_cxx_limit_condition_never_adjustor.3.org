#+TITLE: Manpages - __gnu_cxx_limit_condition_never_adjustor.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_limit_condition_never_adjustor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::limit_condition::never_adjustor - Never enter the condition.

* SYNOPSIS
\\

=#include <throw_allocator.h>=

Inherits __gnu_cxx::limit_condition::adjustor_base.

* Detailed Description
Never enter the condition.

Definition at line *446* of file *throw_allocator.h*.

* Constructor & Destructor Documentation
** __gnu_cxx::limit_condition::never_adjustor::never_adjustor
()= [inline]=
Definition at line *448* of file *throw_allocator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
