#+TITLE: Manpages - MPI_Get_elements_x.3
#+DESCRIPTION: Linux manpage for MPI_Get_elements_x.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about MPI_Get_elements_x.3 is found in manpage for: [[../man3/MPI_Get_elements.3][man3/MPI_Get_elements.3]]