#+TITLE: Manpages - std_filesystem_path.3
#+DESCRIPTION: Linux manpage for std_filesystem_path.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::filesystem::path - A filesystem path.

* SYNOPSIS
\\

=#include <fs_path.h>=

** Classes
class *iterator*\\
An iterator for the components of a path.

** Public Types
using *const_iterator* = *iterator*\\

enum *format* : unsigned char { *native_format*, *generic_format*,
*auto_format* }\\
path::format is ignored in this implementation

using *string_type* = *std::basic_string*< value_type >\\

using *value_type* = char\\

** Public Member Functions
template<typename _InputIterator , typename _Require =
__detail::_Path2<_InputIterator>, typename _Req2 =
__detail::__value_type_is_char<_InputIterator>> *path* (_InputIterator
__first, _InputIterator __last, const *locale* &__loc,
*format*=auto_format)\\

template<typename _InputIterator , typename _Require =
__detail::_Path2<_InputIterator>> *path* (_InputIterator __first,
_InputIterator __last, *format*=auto_format)\\

template<typename _Source , typename _Require =
__detail::_Path<_Source>> *path* (_Source const &__source,
*format*=auto_format)\\

template<typename _Source , typename _Require =
__detail::_Path<_Source>, typename _Require2 =
__detail::__value_type_is_char<_Source>> *path* (_Source const &__src,
const *locale* &__loc, *format*=auto_format)\\

*path* (const *path* &__p)=default\\

*path* (*path* &&__p) noexcept\\

*path* (*string_type* &&__source, *format*=auto_format)\\

template<typename _InputIterator > __detail::_Path2< _InputIterator > &
*append* (_InputIterator __first, _InputIterator __last)\\

template<typename _Source > __detail::_Path< _Source > & *append*
(_Source const &__source)\\

template<typename _InputIterator > __detail::_Path2< _InputIterator > &
*assign* (_InputIterator __first, _InputIterator __last)\\

template<typename _Source > __detail::_Path< _Source > & *assign*
(_Source const &__source)\\

*path* & *assign* (*string_type* &&__source)\\

*iterator* *begin* () const\\

const value_type * *c_str* () const noexcept\\

void *clear* () noexcept\\

int *compare* (*basic_string_view*< value_type > __s) const noexcept\\

int *compare* (const *path* &__p) const noexcept\\

int *compare* (const *string_type* &__s) const noexcept\\

int *compare* (const value_type *__s) const noexcept\\

template<typename _InputIterator > __detail::_Path2< _InputIterator > &
*concat* (_InputIterator __first, _InputIterator __last)\\

template<typename _Source > __detail::_Path< _Source > & *concat*
(_Source const &__x)\\

bool *empty* () const noexcept\\

*iterator* *end* () const\\

*path* *extension* () const\\

*path* *filename* () const\\

*std::string* *generic_string* () const\\

template<typename _CharT , typename _Traits = std::char_traits<_CharT>,
typename _Allocator = std::allocator<_CharT>> *std::basic_string*<
_CharT, _Traits, _Allocator > *generic_string* (const _Allocator
&__a=_Allocator()) const\\

*std::u16string* *generic_u16string* () const\\

*std::u32string* *generic_u32string* () const\\

*std::string* *generic_u8string* () const\\

*std::wstring* *generic_wstring* () const\\

bool *has_extension* () const noexcept\\

bool *has_filename* () const noexcept\\

bool *has_parent_path* () const noexcept\\

bool *has_relative_path* () const noexcept\\

bool *has_root_directory* () const noexcept\\

bool *has_root_name* () const noexcept\\

bool *has_root_path* () const noexcept\\

bool *has_stem* () const noexcept\\

bool *is_absolute* () const noexcept\\

bool *is_relative* () const noexcept\\

*path* *lexically_normal* () const\\

*path* *lexically_proximate* (const *path* &base) const\\

*path* *lexically_relative* (const *path* &base) const\\

*path* & *make_preferred* ()\\

const *string_type* & *native* () const noexcept\\

*operator string_type* () const\\

template<typename _CharT > __detail::_Path2< _CharT * > & *operator+=*
(_CharT __x)\\

template<typename _Source > __detail::_Path< _Source > & *operator+=*
(_Source const &__x)\\

*path* & *operator+=* (*basic_string_view*< value_type > __x)\\

*path* & *operator+=* (const *path* &__x)\\

*path* & *operator+=* (const *string_type* &__x)\\

*path* & *operator+=* (const value_type *__x)\\

*path* & *operator+=* (value_type __x)\\

template<typename _Source > __detail::_Path< _Source > & *operator/=*
(_Source const &__source)\\

*path* & *operator/=* (const *path* &__p)\\

template<typename _Source > __detail::_Path< _Source > & *operator=*
(_Source const &__source)\\

*path* & *operator=* (const *path* &)\\

*path* & *operator=* (*path* &&) noexcept\\

*path* & *operator=* (*string_type* &&__source)\\

*path* *parent_path* () const\\

*path* *relative_path* () const\\

*path* & *remove_filename* ()\\

*path* & *replace_extension* (const *path* &__replacement=*path*())\\

*path* & *replace_filename* (const *path* &__replacement)\\

*path* *root_directory* () const\\

*path* *root_name* () const\\

*path* *root_path* () const\\

*path* *stem* () const\\

*std::string* *string* () const\\

template<typename _CharT , typename _Traits , typename _Allocator >
*basic_string*< _CharT, _Traits, _Allocator > *string* (const _Allocator
&__a) const\\

template<typename _CharT , typename _Traits = std::char_traits<_CharT>,
typename _Allocator = std::allocator<_CharT>> *std::basic_string*<
_CharT, _Traits, _Allocator > *string* (const _Allocator
&__a=_Allocator()) const\\

void *swap* (*path* &__rhs) noexcept\\

*std::u16string* *u16string* () const\\

*std::u32string* *u32string* () const\\

*std::string* *u8string* () const\\

*std::wstring* *wstring* () const\\

** Static Public Attributes
static constexpr value_type *preferred_separator*\\

** Friends
bool *operator!=* (const *path* &__lhs, const *path* &__rhs) noexcept\\
Compare paths.

*path* *operator/* (const *path* &__lhs, const *path* &__rhs)\\
Append one path to another.

bool *operator<* (const *path* &__lhs, const *path* &__rhs) noexcept\\
Compare paths.

template<typename _CharT , typename _Traits > *std::basic_ostream*<
_CharT, _Traits > & *operator<<* (*std::basic_ostream*< _CharT, _Traits
> &__os, const *path* &__p)\\
Write a path to a stream.

bool *operator<=* (const *path* &__lhs, const *path* &__rhs) noexcept\\
Compare paths.

bool *operator==* (const *path* &__lhs, const *path* &__rhs) noexcept\\
Compare paths.

bool *operator>* (const *path* &__lhs, const *path* &__rhs) noexcept\\
Compare paths.

bool *operator>=* (const *path* &__lhs, const *path* &__rhs) noexcept\\
Compare paths.

template<typename _CharT , typename _Traits > *std::basic_istream*<
_CharT, _Traits > & *operator>>* (*std::basic_istream*< _CharT, _Traits
> &__is, *path* &__p)\\
Read a path from a stream.

** Related Functions
(Note that these are not member functions.)

template<typename _InputIterator , typename _Require =
__detail::_Path2<_InputIterator>, typename _CharT =
__detail::__value_type_is_char_or_char8_t<_InputIterator>> *path*
*u8path* (_InputIterator __first, _InputIterator __last)\\

template<typename _Source , typename _Require =
__detail::_Path<_Source>, typename _CharT =
__detail::__value_type_is_char_or_char8_t<_Source>> *path* *u8path*
(const _Source &__source)\\

\\

void *swap* (*path* &__lhs, *path* &__rhs) noexcept\\

size_t *hash_value* (const *path* &__p) noexcept\\

* Detailed Description
A filesystem path.

Definition at line *245* of file *bits/fs_path.h*.

* Member Typedef Documentation
** using *std::filesystem::path::const_iterator* = *iterator*
Definition at line *487* of file *bits/fs_path.h*.

** using *std::filesystem::path::string_type* =
*std::basic_string*<value_type>
Definition at line *260* of file *bits/fs_path.h*.

** using std::filesystem::path::value_type = char
Definition at line *256* of file *bits/fs_path.h*.

* Member Enumeration Documentation
** enum *std::filesystem::path::format* : unsigned char
path::format is ignored in this implementation

Definition at line *263* of file *bits/fs_path.h*.

* Constructor & Destructor Documentation
** std::filesystem::path::path ()= [inline]=, = [noexcept]=
Definition at line *267* of file *bits/fs_path.h*.

** std::filesystem::path::path (*path* && __p)= [inline]=, = [noexcept]=
Definition at line *271* of file *bits/fs_path.h*.

** std::filesystem::path::path (*string_type* && __source, *format* =
=auto_format=)= [inline]=
Definition at line *279* of file *bits/fs_path.h*.

** template<typename _Source , typename _Require =
__detail::_Path<_Source>> std::filesystem::path::path (_Source const &
__source, *format* = =auto_format=)= [inline]=
Definition at line *285* of file *bits/fs_path.h*.

** template<typename _InputIterator , typename _Require =
__detail::_Path2<_InputIterator>> std::filesystem::path::path
(_InputIterator __first, _InputIterator __last, *format* =
=auto_format=)= [inline]=
Definition at line *291* of file *bits/fs_path.h*.

** template<typename _Source , typename _Require =
__detail::_Path<_Source>, typename _Require2 =
__detail::__value_type_is_char<_Source>> std::filesystem::path::path
(_Source const & __src, const *locale* & __loc, *format* =
=auto_format=)= [inline]=
Definition at line *298* of file *bits/fs_path.h*.

** template<typename _InputIterator , typename _Require =
__detail::_Path2<_InputIterator>, typename _Req2 =
__detail::__value_type_is_char<_InputIterator>>
std::filesystem::path::path (_InputIterator __first, _InputIterator
__last, const *locale* & __loc, *format* = =auto_format=)= [inline]=
Definition at line *305* of file *bits/fs_path.h*.

* Member Function Documentation
** template<typename _InputIterator > __detail::_Path2< _InputIterator >
& std::filesystem::path::append (_InputIterator __first, _InputIterator
__last)= [inline]=
Definition at line *356* of file *bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::filesystem::path::append (_Source const & __source)= [inline]=
Definition at line *348* of file *bits/fs_path.h*.

** template<typename _InputIterator > __detail::_Path2< _InputIterator >
& std::filesystem::path::assign (_InputIterator __first, _InputIterator
__last)= [inline]=
Definition at line *331* of file *bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::filesystem::path::assign (_Source const & __source)= [inline]=
Definition at line *326* of file *bits/fs_path.h*.

** const value_type * std::filesystem::path::c_str () const= [inline]=,
= [noexcept]=
Definition at line *408* of file *bits/fs_path.h*.

** void std::filesystem::path::clear ()= [inline]=, = [noexcept]=
Definition at line *396* of file *bits/fs_path.h*.

** template<typename _InputIterator > __detail::_Path2< _InputIterator >
& std::filesystem::path::concat (_InputIterator __first, _InputIterator
__last)= [inline]=
Definition at line *388* of file *bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::filesystem::path::concat (_Source const & __x)= [inline]=
Definition at line *380* of file *bits/fs_path.h*.

** bool std::filesystem::path::empty () const= [inline]=, = [noexcept]=
Definition at line *468* of file *bits/fs_path.h*.

** bool std::filesystem::path::is_relative () const= [inline]=,
= [noexcept]=
Definition at line *478* of file *bits/fs_path.h*.

** const *string_type* & std::filesystem::path::native ()
const= [inline]=, = [noexcept]=
Definition at line *407* of file *bits/fs_path.h*.

** std::filesystem::path::operator *string_type* () const= [inline]=
Definition at line *409* of file *bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::filesystem::path::operator+= (_Source const & __x)= [inline]=
Definition at line *372* of file *bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::filesystem::path::operator/= (_Source const & __source)= [inline]=
Definition at line *340* of file *bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::filesystem::path::operator= (_Source const & __source)= [inline]=
Definition at line *321* of file *bits/fs_path.h*.

* Friends And Related Function Documentation
** bool operator!= (const *path* & __lhs, const *path* &
__rhs)= [friend]=
Compare paths.

Definition at line *525* of file *bits/fs_path.h*.

** *path* operator/ (const *path* & __lhs, const *path* &
__rhs)= [friend]=
Append one path to another.

Definition at line *546* of file *bits/fs_path.h*.

** bool operator< (const *path* & __lhs, const *path* &
__rhs)= [friend]=
Compare paths.

Definition at line *529* of file *bits/fs_path.h*.

** template<typename _CharT , typename _Traits > *std::basic_ostream*<
_CharT, _Traits > & operator<< (*std::basic_ostream*< _CharT, _Traits >
& __os, const *path* & __p)= [friend]=
Write a path to a stream.

Definition at line *494* of file *bits/fs_path.h*.

** bool operator<= (const *path* & __lhs, const *path* &
__rhs)= [friend]=
Compare paths.

Definition at line *533* of file *bits/fs_path.h*.

** bool operator== (const *path* & __lhs, const *path* &
__rhs)= [friend]=
Compare paths.

Definition at line *515* of file *bits/fs_path.h*.

** bool operator> (const *path* & __lhs, const *path* &
__rhs)= [friend]=
Compare paths.

Definition at line *537* of file *bits/fs_path.h*.

** bool operator>= (const *path* & __lhs, const *path* &
__rhs)= [friend]=
Compare paths.

Definition at line *541* of file *bits/fs_path.h*.

** template<typename _CharT , typename _Traits > *std::basic_istream*<
_CharT, _Traits > & operator>> (*std::basic_istream*< _CharT, _Traits >
& __is, *path* & __p)= [friend]=
Read a path from a stream.

Definition at line *504* of file *bits/fs_path.h*.

* Member Data Documentation
** constexpr value_type
std::filesystem::path::preferred_separator= [static]=, = [constexpr]=
Definition at line *258* of file *bits/fs_path.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
