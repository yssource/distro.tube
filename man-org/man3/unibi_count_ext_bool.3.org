#+TITLE: Manpages - unibi_count_ext_bool.3
#+DESCRIPTION: Linux manpage for unibi_count_ext_bool.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_count_ext_bool, unibi_count_ext_num, unibi_count_ext_str - count
extended terminal capabilities

* SYNOPSIS
#include <unibilium.h> size_t unibi_count_ext_bool(const unibi_term
*ut); size_t unibi_count_ext_num(const unibi_term *ut); size_t
unibi_count_ext_str(const unibi_term *ut);

* DESCRIPTION
These functions count the number of extended (i.e. user specified)
capabilities of the respective type in /ut/.

* RETURN VALUE
The number of extended capabilities.

* SEE ALSO
*unibilium.h* (3)
