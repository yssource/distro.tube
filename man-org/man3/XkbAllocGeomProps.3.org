#+TITLE: Manpages - XkbAllocGeomProps.3
#+DESCRIPTION: Linux manpage for XkbAllocGeomProps.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbAllocGeomProps - Allocate geometry properties

* SYNOPSIS
*Status XkbAllocGeomProps* *( XkbGeometryPtr */geom/* ,* *int
*/num_needed/* );*

* ARGUMENTS
- /- geom/ :: _summary_

- /- num_needed/ :: _summary_

* DESCRIPTION
Xkb provides a number of functions to allocate and free subcomponents of
a keyboard geometry. Use these functions to create or modify keyboard
geometries. Note that these functions merely allocate space for the new
element(s), and it is up to you to fill in the values explicitly in your
code. These allocation functions increase /sz_*/ but never touch /num_*/
(unless there is an allocation failure, in which case they reset both
/sz_*/ and /num_*/ to zero). These functions return Success if they
succeed, BadAlloc if they are not able to allocate space, or BadValue if
a parameter is not as expected.

/XkbAllocGeomProps/ allocates space for /num_needed/ properties and adds
them to the specified geometry /geom./ No initialization of the
properties is done. A geometry property associates an arbitrary string
with an equally arbitrary name. Geometry properties can be used to
provide hints to programs that display images of keyboards, but they are
not interpreted by Xkb. No other geometry structures refer to geometry
properties.

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadValue* :: An argument is out of range
