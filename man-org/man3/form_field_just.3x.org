#+TITLE: Manpages - form_field_just.3x
#+DESCRIPTION: Linux manpage for form_field_just.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*set_field_just*, *field_just* - retrieve field characteristics

* SYNOPSIS
*#include <form.h>*

*int set_field_just(FIELD **/field/*, int */justification/*);*\\
*int field_just(const FIELD **/field/*);*\\

* DESCRIPTION
The function *set_field_just* sets the justification attribute of a
field; *field_just* returns a field's justification attribute. The
attribute may be one of NO_JUSTIFICATION, JUSTIFY_RIGHT, JUSTIFY_LEFT,
or JUSTIFY_CENTER.

* RETURN VALUE
The function *field_just* returns one of: NO_JUSTIFICATION,
JUSTIFY_RIGHT, JUSTIFY_LEFT, or JUSTIFY_CENTER.

The function *set_field_just* returns one of the following:

- *E_OK* :: The routine succeeded.

- *E_SYSTEM_ERROR* :: System error occurred (see *errno*(3)).

- *E_BAD_ARGUMENT* :: Routine detected an incorrect or out-of-range
  argument.

* SEE ALSO
*curses*(3X) and related pages whose names begin form_ for detailed
descriptions of the entry points.

* NOTES
The header file *<form.h>* automatically includes the header file
*<curses.h>*.

* PORTABILITY
These routines emulate the System V forms library. They were not
supported on Version 7 or BSD versions.

* AUTHORS
Juergen Pfeifer. Manual pages and adaptation for new curses by Eric S.
Raymond.
