#+TITLE: Manpages - RootWindowOfScreen.3
#+DESCRIPTION: Linux manpage for RootWindowOfScreen.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about RootWindowOfScreen.3 is found in manpage for: [[../man3/BlackPixelOfScreen.3][man3/BlackPixelOfScreen.3]]