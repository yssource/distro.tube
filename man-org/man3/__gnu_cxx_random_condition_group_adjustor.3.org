#+TITLE: Manpages - __gnu_cxx_random_condition_group_adjustor.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_random_condition_group_adjustor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::random_condition::group_adjustor - Group condition.

* SYNOPSIS
\\

=#include <throw_allocator.h>=

Inherits __gnu_cxx::random_condition::adjustor_base.

** Public Member Functions
*group_adjustor* (size_t size)\\

* Detailed Description
Group condition.

Definition at line *518* of file *throw_allocator.h*.

* Constructor & Destructor Documentation
** __gnu_cxx::random_condition::group_adjustor::group_adjustor (size_t
size)= [inline]=
Definition at line *520* of file *throw_allocator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
