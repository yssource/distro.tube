#+TITLE: Manpages - SDL_ListModes.3
#+DESCRIPTION: Linux manpage for SDL_ListModes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_ListModes - Returns a pointer to an array of available screen
dimensions for the given format and video flags

* SYNOPSIS
*#include "SDL.h"*

*SDL_Rect **SDL_ListModes*(*SDL_PixelFormat *format, Uint32 flags*);

* DESCRIPTION
Return a pointer to an array of available screen dimensions for the
given format and video flags, sorted largest to smallest. Returns *NULL*
if there are no dimensions available for a particular format, or *-1* if
any dimension is okay for the given format.

If *format* is *NULL*, the mode list will be for the format returned by
/SDL_GetVideoInfo()/->*vfmt*. The *flag* parameter is an OR'd
combination of /surface/ flags. The flags are the same as those used
*SDL_SetVideoMode* and they play a strong role in deciding what modes
are valid. For instance, if you pass *SDL_HWSURFACE* as a flag only
modes that support hardware video surfaces will be returned.

* EXAMPLE
#+begin_example
  SDL_Rect **modes;
  int i;
  .
  .
  .

  /* Get available fullscreen/hardware modes */
  modes=SDL_ListModes(NULL, SDL_FULLSCREEN|SDL_HWSURFACE);

  /* Check is there are any modes available */
  if(modes == (SDL_Rect **)0){
    printf("No modes available!
  ");
    exit(-1);
  }

  /* Check if or resolution is restricted */
  if(modes == (SDL_Rect **)-1){
    printf("All resolutions available.
  ");
  }
  else{
    /* Print valid modes */
    printf("Available Modes
  ");
    for(i=0;modes[i];++i)
      printf("  %d x %d
  ", modes[i]->w, modes[i]->h);
  }
  .
  .
#+end_example

* SEE ALSO
*SDL_SetVideoMode*, *SDL_GetVideoInfo*, *SDL_Rect*, *SDL_PixelFormat*
