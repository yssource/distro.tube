#+TITLE: Manpages - termkey_set_flags.3
#+DESCRIPTION: Linux manpage for termkey_set_flags.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_set_flags, termkey_get_flags - control the operational flags

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  void termkey_set_flags(TermKey *tk, int newflags);
  int termkey_get_flags(TermKey *tk);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_set_flags*() changes the set of operation flags in the
*termkey*(7) instance to those given by /newflags/.

*termkey_get_flags*() returns the value set by the last call to
*termkey_set_flags*(), or the value given to the constructor.

* RETURN VALUE
*termkey_set_flags*() returns no value. *termkey_get_flags*() returns
the current operational flags.

* SEE ALSO
*termkey_new*(3), *termkey*(7)
