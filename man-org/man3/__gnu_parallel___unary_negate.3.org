#+TITLE: Manpages - __gnu_parallel___unary_negate.3
#+DESCRIPTION: Linux manpage for __gnu_parallel___unary_negate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::__unary_negate< _Predicate, argument_type > - Similar to
std::unary_negate, but giving the argument types explicitly.

* SYNOPSIS
\\

=#include <base.h>=

Inherits *std::unary_function< argument_type, bool >*.

** Public Types
typedef *argument_type* *argument_type*\\
=argument_type= is the type of the argument

typedef bool *result_type*\\
=result_type= is the return type

** Public Member Functions
*__unary_negate* (const _Predicate &__x)\\

bool *operator()* (const *argument_type* &__x)\\

** Protected Attributes
_Predicate *_M_pred*\\

* Detailed Description
** "template<typename _Predicate, typename *argument_type*>
\\
class __gnu_parallel::__unary_negate< _Predicate, argument_type
>"Similar to std::unary_negate, but giving the argument types
explicitly.

Definition at line *173* of file *base.h*.

* Member Typedef Documentation
** typedef *argument_type* *std::unary_function*< *argument_type* , bool
>::*argument_type*= [inherited]=
=argument_type= is the type of the argument

Definition at line *108* of file *stl_function.h*.

** typedef bool *std::unary_function*< *argument_type* , bool
>::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *111* of file *stl_function.h*.

* Constructor & Destructor Documentation
** template<typename _Predicate , typename *argument_type* >
*__gnu_parallel::__unary_negate*< _Predicate, *argument_type*
>::*__unary_negate* (const _Predicate & __x)= [inline]=, = [explicit]=
Definition at line *181* of file *base.h*.

* Member Function Documentation
** template<typename _Predicate , typename *argument_type* > bool
*__gnu_parallel::__unary_negate*< _Predicate, *argument_type*
>::operator() (const *argument_type* & __x)= [inline]=
Definition at line *184* of file *base.h*.

* Member Data Documentation
** template<typename _Predicate , typename *argument_type* > _Predicate
*__gnu_parallel::__unary_negate*< _Predicate, *argument_type*
>::_M_pred= [protected]=
Definition at line *177* of file *base.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
