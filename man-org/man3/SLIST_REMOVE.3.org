#+TITLE: Manpages - SLIST_REMOVE.3
#+DESCRIPTION: Linux manpage for SLIST_REMOVE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about SLIST_REMOVE.3 is found in manpage for: [[../man3/slist.3][man3/slist.3]]