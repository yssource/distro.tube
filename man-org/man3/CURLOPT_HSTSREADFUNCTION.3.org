#+TITLE: Manpages - CURLOPT_HSTSREADFUNCTION.3
#+DESCRIPTION: Linux manpage for CURLOPT_HSTSREADFUNCTION.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_HSTSREADFUNCTION - read callback for HSTS hosts

* SYNOPSIS
#include <curl/curl.h>

CURLSTScode hstsread(CURL *easy, struct curl_hstsentry *sts, void
*userp);

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HSTSREADFUNCTION,
hstsread);

* DESCRIPTION
Pass a pointer to your callback function, as the prototype shows above.

This callback function gets called by libcurl repeatedly when it
populates the in-memory HSTS cache.

Set the /userp/ argument with the /CURLOPT_HSTSREADDATA(3)/ option or it
will be NULL.

When this callback is invoked, the /sts/ pointer points to a populated
struct: Copy the host name to 'name' (no longer than 'namelen' bytes).
Make it null-terminated. Set 'includeSubDomains' to TRUE or FALSE. Set
'expire' to a date stamp or a zero length string for *forever* (wrong
date stamp format might cause the name to not get accepted)

The callback should return /CURLSTS_OK/ if it returns a name and is
prepared to be called again (for another host) or /CURLSTS_DONE/ if it
has no entry to return. It can also return /CURLSTS_FAIL/ to signal
error. Returning /CURLSTS_FAIL/ will stop the transfer from being
performed and make /CURLE_ABORTED_BY_CALLBACK/ get returned.

This option does not enable HSTS, you need to use /CURLOPT_HSTS_CTRL(3)/
to do that.

* DEFAULT
NULL - no callback.

* PROTOCOLS
This feature is only used for HTTP(S) transfer.

* EXAMPLE
#+begin_example
  {
    /* set HSTS read callback */
    curl_easy_setopt(curl, CURLOPT_HSTSREADFUNCTION, hstsread);

    /* pass in suitable argument to the callback */
    curl_easy_setopt(curl, CURLOPT_HSTSREADDATA, &hstspreload[0]);

    result = curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.74.0

* RETURN VALUE
This will return CURLE_OK.

* SEE ALSO
*CURLOPT_HSTSREADDATA*(3), *CURLOPT_HSTSWRITEFUNCTION*(3),
*CURLOPT_HSTS*(3), *CURLOPT_HSTS_CTRL*(3),
