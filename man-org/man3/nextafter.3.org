#+TITLE: Manpages - nextafter.3
#+DESCRIPTION: Linux manpage for nextafter.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
nextafter, nextafterf, nextafterl, nexttoward, nexttowardf,
nexttowardl - floating-point number manipulation

* SYNOPSIS
#+begin_example
  #include <math.h>

  double nextafter(double x, double y);
  float nextafterf(float x, float y);
  long double nextafterl(long double x, long double y);

  double nexttoward(double x, long double y);
  float nexttowardf(float x, long double y);
  long double nexttowardl(long double x, long double y);
#+end_example

Link with /-lm/.

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*nextafter*():

#+begin_example
      _ISOC99_SOURCE || _POSIX_C_SOURCE >= 200112L
          || _XOPEN_SOURCE >= 500
          || /* Since glibc 2.19: */ _DEFAULT_SOURCE
          || /* Glibc <= 2.19: */ _BSD_SOURCE || _SVID_SOURCE
#+end_example

*nextafterf*(), *nextafterl*():

#+begin_example
      _ISOC99_SOURCE || _POSIX_C_SOURCE >= 200112L
          || /* Since glibc 2.19: */ _DEFAULT_SOURCE
          || /* Glibc <= 2.19: */ _BSD_SOURCE || _SVID_SOURCE
#+end_example

*nexttoward*(), *nexttowardf*(), *nexttowardl*():

#+begin_example
      _XOPEN_SOURCE >= 600 || _ISOC99_SOURCE
          || _POSIX_C_SOURCE >= 200112L
#+end_example

* DESCRIPTION
The *nextafter*(), *nextafterf*(), and *nextafterl*() functions return
the next representable floating-point value following /x/ in the
direction of /y/. If /y/ is less than /x/, these functions will return
the largest representable number less than /x/.

If /x/ equals /y/, the functions return /y/.

The *nexttoward*(), *nexttowardf*(), and *nexttowardl*() functions do
the same as the corresponding *nextafter*() functions, except that they
have a /long double/ second argument.

* RETURN VALUE
On success, these functions return the next representable floating-point
value after /x/ in the direction of /y/.

If /x/ equals /y/, then /y/ (cast to the same type as /x/) is returned.

If /x/ or /y/ is a NaN, a NaN is returned.

If /x/ is finite, and the result would overflow, a range error occurs,
and the functions return *HUGE_VAL*, *HUGE_VALF*, or *HUGE_VALL*,
respectively, with the correct mathematical sign.

If /x/ is not equal to /y/, and the correct function result would be
subnormal, zero, or underflow, a range error occurs, and either the
correct value (if it can be represented), or 0.0, is returned.

* ERRORS
See *math_error*(7) for information on how to determine whether an error
has occurred when calling these functions.

The following errors can occur:

- Range error: result overflow :: /errno/ is set to *ERANGE*. An
  overflow floating-point exception (*FE_OVERFLOW*) is raised.

- Range error: result is subnormal or underflows :: /errno/ is set to
  *ERANGE*. An underflow floating-point exception (*FE_UNDERFLOW*) is
  raised.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                                                                                       | Attribute     | Value   |
| *nextafter*(), *nextafterf*(), *nextafterl*(), *nexttoward*(), *nexttowardf*(), *nexttowardl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008. This function is defined in IEC 559
(and the appendix with recommended functions in IEEE 754/IEEE 854).

* BUGS
In glibc version 2.5 and earlier, these functions do not raise an
underflow floating-point (*FE_UNDERFLOW*) exception when an underflow
occurs.

Before glibc version 2.23 these functions did not set /errno/.

* SEE ALSO
*nearbyint*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
