#+TITLE: Manpages - __gnu_pbds_basic_invalidation_guarantee.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_basic_invalidation_guarantee.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::basic_invalidation_guarantee

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherited by *__gnu_pbds::point_invalidation_guarantee*.

* Detailed Description
Signifies a basic invalidation guarantee that any iterator, pointer, or
reference to a container object's mapped value type is valid as long as
the container is not modified.

Definition at line *93* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
