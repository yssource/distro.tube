#+TITLE: Manpages - get_nprocs_conf.3
#+DESCRIPTION: Linux manpage for get_nprocs_conf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
get_nprocs, get_nprocs_conf - get number of processors

* SYNOPSIS
#+begin_example
  #include <sys/sysinfo.h>

  int get_nprocs(void);
  int get_nprocs_conf(void);
#+end_example

* DESCRIPTION
The function *get_nprocs_conf*() returns the number of processors
configured by the operating system.

The function *get_nprocs*() returns the number of processors currently
available in the system. This may be less than the number returned by
*get_nprocs_conf*() because processors may be offline (e.g., on
hotpluggable systems).

* RETURN VALUE
As given in DESCRIPTION.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                           | Attribute     | Value   |
| *get_nprocs*(), *get_nprocs_conf*() | Thread safety | MT-Safe |

* CONFORMING TO
These functions are GNU extensions.

* NOTES
The current implementation of these functions is rather expensive, since
they open and parse files in the //sys/ filesystem each time they are
called.

The following *sysconf*(3) calls make use of the functions documented on
this page to return the same information.

#+begin_example
  np = sysconf(_SC_NPROCESSORS_CONF);     /* processors configured */
  np = sysconf(_SC_NPROCESSORS_ONLN);     /* processors available */
#+end_example

* EXAMPLES
The following example shows how *get_nprocs*() and *get_nprocs_conf*()
can be used.

#+begin_example
  #include <stdlib.h>
  #include <stdio.h>
  #include <sys/sysinfo.h>

  int
  main(int argc, char *argv[])
  {
      printf("This system has %d processors configured and "
              "%d processors available.\n",
              get_nprocs_conf(), get_nprocs());
      exit(EXIT_SUCCESS);
  }
#+end_example

* SEE ALSO
*nproc*(1)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
