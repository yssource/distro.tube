#+TITLE: Manpages - ldap_parse_sort_control.3
#+DESCRIPTION: Linux manpage for ldap_parse_sort_control.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldap_parse_sort_control - Decode the information returned from a search
operation that used a server-side sort control

* LIBRARY
OpenLDAP LDAP (libldap, -lldap)

* SYNOPSIS
#+begin_example
  #include <ldap.h>
  int ldap_parse_sort_control(ld, ctrls, returnCode, attribute)
  LDAP *ld;
  LDAPControl **ctrls;
  unsigned long *returnCode;
  char **attribute;
#+end_example

* DESCRIPTION
This function is used to parse the results returned in a search
operation that uses a server-side sort control.

It takes a null terminated array of LDAPControl structures usually
obtained by a call to the *ldap_parse_result* function. A returncode
which points to the sort control result code,and an array of LDAPControl
structures that list the client controls to use with the search. The
function also takes an out parameter /attribute/ and if the sort
operation fails, the server may return a string that indicates the first
attribute in the sortKey list that caused the failure. If this parameter
is NULL, no string is returned. If a string is returned, the memory
should be freed by calling the ldap_memfree function.

* NOTES
* SEE ALSO
*ldap_result*(3), *ldap_controls_free*(3)

* ACKNOWLEDGEMENTS
*OpenLDAP Software* is developed and maintained by The OpenLDAP Project
<http://www.openldap.org/>. *OpenLDAP Software* is derived from the
University of Michigan LDAP 3.3 Release.
