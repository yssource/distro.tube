#+TITLE: Manpages - XSetLocaleModifiers.3
#+DESCRIPTION: Linux manpage for XSetLocaleModifiers.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSetLocaleModifiers.3 is found in manpage for: [[../man3/XSupportsLocale.3][man3/XSupportsLocale.3]]