#+TITLE: Manpages - stringprep_ucs4_to_utf8.3
#+DESCRIPTION: Linux manpage for stringprep_ucs4_to_utf8.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
stringprep_ucs4_to_utf8 - API function

* SYNOPSIS
*#include <stringprep.h>*

*char * stringprep_ucs4_to_utf8(const uint32_t * */str/*, ssize_t
*/len/*, size_t * */items_read/*, size_t * */items_written/*);*

* ARGUMENTS
- const uint32_t * str :: a UCS-4 encoded string

- ssize_t len :: the maximum length of /str/ to use. If /len/ < 0, then
  the string is terminated with a 0 character.

- size_t * items_read :: location to store number of characters read
  read, or *NULL*.

- size_t * items_written :: location to store number of bytes written or
  *NULL*. The value here stored does not include the trailing 0 byte.

* DESCRIPTION
Convert a string from a 32-bit fixed width representation as UCS-4. to
UTF-8. The result will be terminated with a 0 byte.

Return value: a pointer to a newly allocated UTF-8 string. This value
must be deallocated by the caller. If an error occurs, *NULL* will be
returned.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
