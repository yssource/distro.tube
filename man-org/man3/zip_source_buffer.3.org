#+TITLE: Manpages - zip_source_buffer.3
#+DESCRIPTION: Linux manpage for zip_source_buffer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The functions

and

create a zip source from the buffer

of size

If

is non-zero, the buffer will be freed when it is no longer needed.

must remain valid for the lifetime of the created source.

The source can be used to open a zip archive from.

Upon successful completion, the created source is returned. Otherwise,

is returned and the error code in

or

is set to indicate the error.

and

fail if:

is greater than zero and

is

Required memory could not be allocated.

and

were added in libzip 1.0.

and
