#+TITLE: Manpages - XListDeviceProperties.3
#+DESCRIPTION: Linux manpage for XListDeviceProperties.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XListDeviceProperties - List a devices properties.

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  Atom* XListDeviceProperties( Display *display,
                               XDevice *device,
                               int *nprops_return);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  device
         The device to list the properties for.
#+end_example

#+begin_example
  nprops_return
         Specifies the number of Atoms returned.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    The XListDeviceProperties function returns a list of the
    properties associated with the input device specified in
    device. Each device may have an arbitrary number of properties
    attached, some of which were created by the driver and/or
    server, others created by clients at runtime.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The client is expected to free the list of properties using
    XFree.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XListDeviceProperties can generate a BadDevice error.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The device does not
           exist.
  #+end_example
#+end_quote

* SEE ALSO

#+begin_quote
  #+begin_example
    XChangeDeviceProperty(3),
    XGetDeviceProperty(3),
    XDeleteDeviceProperty(3)
  #+end_example
#+end_quote
