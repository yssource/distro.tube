#+TITLE: Manpages - rpc_clnt_auth.3t
#+DESCRIPTION: Linux manpage for rpc_clnt_auth.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
These routines are part of the RPC library that allows C language
programs to make procedure calls on other machines across the network,
with desired authentication.

These routines are normally called after creating the

handle. The

field of the

structure should be initialized by the

structure returned by some of the following routines. The client's
authentication information is passed to the server when the RPC call is
made.

Only the

and the

style of authentication is discussed here.

A function macro that destroys the authentication information associated
with

Destruction usually involves deallocation of private data structures.
The use of

is undefined after calling

Create and return an RPC authentication handle that passes nonusable
authentication information with each remote procedure call. This is the
default authentication used by RPC.

Create and return an RPC authentication handle that contains

authentication information. The

argument is the name of the machine on which the information was
created;

is the user's user ID;

is the user's current group ID;

and

refer to a counted array of groups to which the user belongs.

Call

with the appropriate arguments.

These functions are part of libtirpc.
