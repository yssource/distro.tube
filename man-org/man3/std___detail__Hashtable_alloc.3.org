#+TITLE: Manpages - std___detail__Hashtable_alloc.3
#+DESCRIPTION: Linux manpage for std___detail__Hashtable_alloc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Hashtable_alloc< _NodeAlloc >

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

Inherits *std::__detail::_Hashtable_ebo_helper< 0, _NodeAlloc >*.

** Public Types
using *__buckets_alloc_traits* = *std::allocator_traits*<
__buckets_alloc_type >\\

using *__buckets_alloc_type* = __alloc_rebind< __node_alloc_type,
*__node_base_ptr* >\\

using *__buckets_ptr* = *__node_base_ptr* *\\

using *__node_alloc_traits* = *__gnu_cxx::__alloc_traits*<
__node_alloc_type >\\

using *__node_alloc_type* = _NodeAlloc\\

using *__node_base* = *_Hash_node_base*\\

using *__node_base_ptr* = *__node_base* *\\

using *__node_ptr* = __node_type *\\

using *__node_type* = typename _NodeAlloc::value_type\\

using *__value_alloc_traits* = typename __node_alloc_traits::template
rebind_traits< typename __node_type::value_type >\\

** Public Member Functions
template<typename _Alloc > *_Hashtable_alloc* (_Alloc &&__a)\\

*_Hashtable_alloc* (*_Hashtable_alloc* &&)=default\\

*_Hashtable_alloc* (const *_Hashtable_alloc* &)=default\\

*__buckets_ptr* *_M_allocate_buckets* (std::size_t __bkt_count)\\

template<typename... _Args> __node_ptr *_M_allocate_node* (_Args &&...
__args)\\

template<typename... _Args> auto *_M_allocate_node* (_Args &&... __args)
-> __node_ptr\\

void *_M_deallocate_buckets* (*__buckets_ptr*, std::size_t
__bkt_count)\\

void *_M_deallocate_node* (__node_ptr __n)\\

void *_M_deallocate_node_ptr* (__node_ptr __n)\\

void *_M_deallocate_nodes* (__node_ptr __n)\\

__node_alloc_type & *_M_node_allocator* ()\\

const __node_alloc_type & *_M_node_allocator* () const\\

* Detailed Description
** "template<typename _NodeAlloc>
\\
struct std::__detail::_Hashtable_alloc< _NodeAlloc >"This type deals
with all allocation and keeps an allocator instance through inheritance
to benefit from EBO when possible.

Definition at line *1799* of file *hashtable_policy.h*.

* Member Typedef Documentation
** template<typename _NodeAlloc > using
*std::__detail::_Hashtable_alloc*< _NodeAlloc
>::*__buckets_alloc_traits* =
*std::allocator_traits*<__buckets_alloc_type>
Definition at line *1817* of file *hashtable_policy.h*.

** template<typename _NodeAlloc > using
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::__buckets_alloc_type =
__alloc_rebind<__node_alloc_type, *__node_base_ptr*>
Definition at line *1815* of file *hashtable_policy.h*.

** template<typename _NodeAlloc > using
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::*__buckets_ptr* =
*__node_base_ptr**
Definition at line *1818* of file *hashtable_policy.h*.

** template<typename _NodeAlloc > using
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::*__node_alloc_traits* =
*__gnu_cxx::__alloc_traits*<__node_alloc_type>
Definition at line *1807* of file *hashtable_policy.h*.

** template<typename _NodeAlloc > using
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::__node_alloc_type =
_NodeAlloc
Definition at line *1805* of file *hashtable_policy.h*.

** template<typename _NodeAlloc > using
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::*__node_base* =
*_Hash_node_base*
Definition at line *1813* of file *hashtable_policy.h*.

** template<typename _NodeAlloc > using
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::*__node_base_ptr* =
*__node_base**
Definition at line *1814* of file *hashtable_policy.h*.

** template<typename _NodeAlloc > using
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::__node_ptr =
__node_type*
Definition at line *1812* of file *hashtable_policy.h*.

** template<typename _NodeAlloc > using
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::__node_type = typename
_NodeAlloc::value_type
Definition at line *1804* of file *hashtable_policy.h*.

** template<typename _NodeAlloc > using
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::__value_alloc_traits =
typename __node_alloc_traits::template rebind_traits<typename
__node_type::value_type>
Definition at line *1809* of file *hashtable_policy.h*.

* Constructor & Destructor Documentation
** template<typename _NodeAlloc > template<typename _Alloc >
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::*_Hashtable_alloc*
(_Alloc && __a)= [inline]=
Definition at line *1825* of file *hashtable_policy.h*.

* Member Function Documentation
** template<typename _NodeAlloc > __node_alloc_type &
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::_M_node_allocator
()= [inline]=
Definition at line *1830* of file *hashtable_policy.h*.

** template<typename _NodeAlloc > const __node_alloc_type &
*std::__detail::_Hashtable_alloc*< _NodeAlloc >::_M_node_allocator ()
const= [inline]=
Definition at line *1834* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
