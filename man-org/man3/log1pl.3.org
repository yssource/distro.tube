#+TITLE: Manpages - log1pl.3
#+DESCRIPTION: Linux manpage for log1pl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about log1pl.3 is found in manpage for: [[../man3/log1p.3][man3/log1p.3]]