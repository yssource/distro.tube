#+TITLE: Manpages - gnutls_x509_privkey_export_dsa_raw.3
#+DESCRIPTION: Linux manpage for gnutls_x509_privkey_export_dsa_raw.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_x509_privkey_export_dsa_raw - API function

* SYNOPSIS
*#include <gnutls/x509.h>*

*int gnutls_x509_privkey_export_dsa_raw(gnutls_x509_privkey_t */key/*,
gnutls_datum_t * */p/*, gnutls_datum_t * */q/*, gnutls_datum_t * */g/*,
gnutls_datum_t * */y/*, gnutls_datum_t * */x/*);*

* ARGUMENTS
- gnutls_x509_privkey_t key :: a key

- gnutls_datum_t * p :: will hold the p

- gnutls_datum_t * q :: will hold the q

- gnutls_datum_t * g :: will hold the g

- gnutls_datum_t * y :: will hold the y

- gnutls_datum_t * x :: will hold the x

* DESCRIPTION
This function will export the DSA private key's parameters found in the
given structure. The new parameters will be allocated using
*gnutls_malloc()* and will be stored in the appropriate datum.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
