#+TITLE: Manpages - SDL_Init.3
#+DESCRIPTION: Linux manpage for SDL_Init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_Init - Initializes SDL

* SYNOPSIS
*#include "SDL.h"*

*int SDL_Init*(*Uint32 flags*);

* DESCRIPTION
Initializes SDL. This should be called before all other SDL functions.
The *flags* parameter specifies what part(s) of SDL to initialize.

- *SDL_INIT_TIMER* :: Initializes the /timer/ subsystem.

- *SDL_INIT_AUDIO* :: Initializes the /audio/ subsystem.

- *SDL_INIT_VIDEO* :: Initializes the /video/ subsystem.

- *SDL_INIT_CDROM* :: Initializes the /cdrom/ subsystem.

- *SDL_INIT_JOYSTICK* :: Initializes the /joystick/ subsystem.

- *SDL_INIT_EVERYTHING* :: Initialize all of the above.

- *SDL_INIT_NOPARACHUTE* :: Prevents SDL from catching fatal signals.

- *SDL_INIT_EVENTTHREAD* :: 

* RETURN VALUE
Returns *-1* on an error or *0* on success.

* SEE ALSO
*SDL_Quit*, *SDL_InitSubSystem*
