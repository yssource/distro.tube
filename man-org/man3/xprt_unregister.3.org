#+TITLE: Manpages - xprt_unregister.3
#+DESCRIPTION: Linux manpage for xprt_unregister.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xprt_unregister.3 is found in manpage for: [[../man3/rpc.3][man3/rpc.3]]