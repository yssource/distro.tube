#+TITLE: Manpages - Encode_CJKConstants.3perl
#+DESCRIPTION: Linux manpage for Encode_CJKConstants.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Encode::CJKConstants.pm -- Internally used by Encode::??::ISO_2022_*
