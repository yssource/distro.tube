#+TITLE: Manpages - unibi_add_ext_num.3
#+DESCRIPTION: Linux manpage for unibi_add_ext_num.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_add_ext_bool, unibi_add_ext_num, unibi_add_ext_str - add extended
capabilities to a terminal object

* SYNOPSIS
#include <unibilium.h> size_t unibi_add_ext_bool(unibi_term *ut, const
char *t, int b); size_t unibi_add_ext_num(unibi_term *ut, const char *t,
int v); size_t unibi_add_ext_str(unibi_term *ut, const char *t, const
char *s);

* DESCRIPTION
Add an extended boolean, numeric, or string capability with the
specified name and value.

Note that these functions simply store any pointers they are given. They
will not free /t/ or /s/ or copy any strings.

* RETURN VALUE
The return value is the index of the new capability, which can be used
in *unibi_get_ext_bool* (3), *unibi_set_ext_bool* (3), etc. If an error
occurs, =SIZE_MAX= is returned.

* SEE ALSO
*unibilium.h* (3), *unibi_del_ext_bool* (3), *unibi_del_ext_num* (3),
*unibi_del_ext_str* (3)
