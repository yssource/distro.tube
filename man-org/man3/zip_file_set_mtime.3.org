#+TITLE: Manpages - zip_file_set_mtime.3
#+DESCRIPTION: Linux manpage for zip_file_set_mtime.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function sets the last modification time (mtime) for the file at
position

in the zip archive to

Currently, no support for any

is implemented.

In the zip archive, the time and date are saved as two 16-bit integers.
To set the values directly, call the

function. The values of the time bytes are defined as follows:

seconds divided by two (1-2 = 1, 3-4 = 2, ...)

minute (0-59)

hour (0-23)

The values of the date bytes are defined as follows:

day of the month (1-31)

month (January = 1, February = 2, ...)

year offset from 1980 (1980 = 0, 1981 = 1, ...)

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error information in

is set to indicate the error.

and

fail if:

is not a valid file index in

Required memory could not be allocated.

The

was opened in read-only mode.

was added in libzip 1.0.

was added in libzip 1.6.

and

Following historical practice, the

function translates the time from the zip archive into the local time
zone. If you want to avoid this, use the

function instead.
