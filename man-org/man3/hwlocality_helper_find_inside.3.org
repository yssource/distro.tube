#+TITLE: Manpages - hwlocality_helper_find_inside.3
#+DESCRIPTION: Linux manpage for hwlocality_helper_find_inside.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwlocality_helper_find_inside - Finding Objects inside a CPU set

* SYNOPSIS
\\

** Functions
static *hwloc_obj_t* *hwloc_get_first_largest_obj_inside_cpuset*
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set)\\

int *hwloc_get_largest_objs_inside_cpuset* (*hwloc_topology_t* topology,
*hwloc_const_cpuset_t* set, *hwloc_obj_t* *restrict objs, int max)\\

static *hwloc_obj_t* *hwloc_get_next_obj_inside_cpuset_by_depth*
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set, int depth,
*hwloc_obj_t* prev)\\

static *hwloc_obj_t* *hwloc_get_next_obj_inside_cpuset_by_type*
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set,
*hwloc_obj_type_t* type, *hwloc_obj_t* prev)\\

static *hwloc_obj_t* *hwloc_get_obj_inside_cpuset_by_depth*
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set, int depth,
unsigned idx)\\

static *hwloc_obj_t* *hwloc_get_obj_inside_cpuset_by_type*
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set,
*hwloc_obj_type_t* type, unsigned idx)\\

static unsigned *hwloc_get_nbobjs_inside_cpuset_by_depth*
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set, int depth)\\

static int *hwloc_get_nbobjs_inside_cpuset_by_type* (*hwloc_topology_t*
topology, *hwloc_const_cpuset_t* set, *hwloc_obj_type_t* type)\\

static int *hwloc_get_obj_index_inside_cpuset* (*hwloc_topology_t*
topology, *hwloc_const_cpuset_t* set, *hwloc_obj_t* obj)\\

* Detailed Description
* Function Documentation
** static *hwloc_obj_t* hwloc_get_first_largest_obj_inside_cpuset
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set)= [inline]=,
= [static]=
Get the first largest object included in the given cpuset =set=.

*Returns*

#+begin_quote
  the first object that is included in =set= and whose parent is not.
#+end_quote

This is convenient for iterating over all largest objects within a CPU
set by doing a loop getting the first largest object and clearing its
CPU set from the remaining CPU set.

** int hwloc_get_largest_objs_inside_cpuset (*hwloc_topology_t*
topology, *hwloc_const_cpuset_t* set, *hwloc_obj_t* *restrict objs, int
max)
Get the set of largest objects covering exactly a given cpuset =set=.

*Returns*

#+begin_quote
  the number of objects returned in =objs=.
#+end_quote

** static unsigned hwloc_get_nbobjs_inside_cpuset_by_depth
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set, int
depth)= [inline]=, = [static]=
Return the number of objects at depth =depth= included in CPU set =set=.

*Note*

#+begin_quote
  Objects with empty CPU sets are ignored (otherwise they would be
  considered included in any given set).

  This function cannot work if objects at the given depth do not have
  CPU sets (I/O or Misc objects).
#+end_quote

** static int hwloc_get_nbobjs_inside_cpuset_by_type (*hwloc_topology_t*
topology, *hwloc_const_cpuset_t* set, *hwloc_obj_type_t*
type)= [inline]=, = [static]=
Return the number of objects of type =type= included in CPU set =set=.
If no object for that type exists inside CPU set =set=, 0 is returned.
If there are several levels with objects of that type inside CPU set
=set=, -1 is returned.

*Note*

#+begin_quote
  Objects with empty CPU sets are ignored (otherwise they would be
  considered included in any given set).

  This function cannot work if objects of the given type do not have CPU
  sets (I/O objects).
#+end_quote

** static *hwloc_obj_t* hwloc_get_next_obj_inside_cpuset_by_depth
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set, int depth,
*hwloc_obj_t* prev)= [inline]=, = [static]=
Return the next object at depth =depth= included in CPU set =set=. If
=prev= is =NULL=, return the first object at depth =depth= included in
=set=. The next invokation should pass the previous return value in
=prev= so as to obtain the next object in =set=.

*Note*

#+begin_quote
  Objects with empty CPU sets are ignored (otherwise they would be
  considered included in any given set).

  This function cannot work if objects at the given depth do not have
  CPU sets (I/O or Misc objects).
#+end_quote

** static *hwloc_obj_t* hwloc_get_next_obj_inside_cpuset_by_type
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set,
*hwloc_obj_type_t* type, *hwloc_obj_t* prev)= [inline]=, = [static]=
Return the next object of type =type= included in CPU set =set=. If
there are multiple or no depth for given type, return =NULL= and let the
caller fallback to *hwloc_get_next_obj_inside_cpuset_by_depth()*.

*Note*

#+begin_quote
  Objects with empty CPU sets are ignored (otherwise they would be
  considered included in any given set).

  This function cannot work if objects of the given type do not have CPU
  sets (I/O or Misc objects).
#+end_quote

** static int hwloc_get_obj_index_inside_cpuset (*hwloc_topology_t*
topology, *hwloc_const_cpuset_t* set, *hwloc_obj_t* obj)= [inline]=,
= [static]=
Return the logical index among the objects included in CPU set =set=.
Consult all objects in the same level as =obj= and inside CPU set =set=
in the logical order, and return the index of =obj= within them. If
=set= covers the entire topology, this is the logical index of =obj=.
Otherwise, this is similar to a logical index within the part of the
topology defined by CPU set =set=.

*Note*

#+begin_quote
  Objects with empty CPU sets are ignored (otherwise they would be
  considered included in any given set).

  This function cannot work if obj does not have CPU sets (I/O objects).
#+end_quote

** static *hwloc_obj_t* hwloc_get_obj_inside_cpuset_by_depth
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set, int depth,
unsigned idx)= [inline]=, = [static]=
Return the (logically) =idx= -th object at depth =depth= included in CPU
set =set=.

*Note*

#+begin_quote
  Objects with empty CPU sets are ignored (otherwise they would be
  considered included in any given set).

  This function cannot work if objects at the given depth do not have
  CPU sets (I/O or Misc objects).
#+end_quote

** static *hwloc_obj_t* hwloc_get_obj_inside_cpuset_by_type
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set,
*hwloc_obj_type_t* type, unsigned idx)= [inline]=, = [static]=
Return the =idx= -th object of type =type= included in CPU set =set=. If
there are multiple or no depth for given type, return =NULL= and let the
caller fallback to *hwloc_get_obj_inside_cpuset_by_depth()*.

*Note*

#+begin_quote
  Objects with empty CPU sets are ignored (otherwise they would be
  considered included in any given set).

  This function cannot work if objects of the given type do not have CPU
  sets (I/O or Misc objects).
#+end_quote

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
