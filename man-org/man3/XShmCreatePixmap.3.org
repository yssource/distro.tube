#+TITLE: Manpages - XShmCreatePixmap.3
#+DESCRIPTION: Linux manpage for XShmCreatePixmap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XShmCreatePixmap.3 is found in manpage for: [[../man3/XShm.3][man3/XShm.3]]