#+TITLE: Manpages - fmaxl.3
#+DESCRIPTION: Linux manpage for fmaxl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fmaxl.3 is found in manpage for: [[../man3/fmax.3][man3/fmax.3]]