#+TITLE: Manpages - SSL_CONF_CTX_set_flags.3ssl
#+DESCRIPTION: Linux manpage for SSL_CONF_CTX_set_flags.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_CONF_CTX_set_flags, SSL_CONF_CTX_clear_flags - Set or clear SSL
configuration context flags

* SYNOPSIS
#include <openssl/ssl.h> unsigned int
SSL_CONF_CTX_set_flags(SSL_CONF_CTX *cctx, unsigned int flags); unsigned
int SSL_CONF_CTX_clear_flags(SSL_CONF_CTX *cctx, unsigned int flags);

* DESCRIPTION
The function *SSL_CONF_CTX_set_flags()* sets *flags* in the context
*cctx*.

The function *SSL_CONF_CTX_clear_flags()* clears *flags* in the context
*cctx*.

* NOTES
The flags set affect how subsequent calls to *SSL_CONF_cmd()* or
*SSL_CONF_argv()* behave.

Currently the following *flags* values are recognised:

- SSL_CONF_FLAG_CMDLINE, SSL_CONF_FLAG_FILE :: recognise options
  intended for command line or configuration file use. At least one of
  these flags must be set.

- SSL_CONF_FLAG_CLIENT, SSL_CONF_FLAG_SERVER :: recognise options
  intended for use in SSL/TLS clients or servers. One or both of these
  flags must be set.

- SSL_CONF_FLAG_CERTIFICATE :: recognise certificate and private key
  options.

- SSL_CONF_FLAG_REQUIRE_PRIVATE :: If this option is set then if a
  private key is not specified for a certificate it will attempt to load
  a private key from the certificate file when *SSL_CONF_CTX_finish()*
  is called. If a key cannot be loaded from the certificate file an
  error occurs.

- SSL_CONF_FLAG_SHOW_ERRORS :: indicate errors relating to unrecognised
  options or missing arguments in the error queue. If this option isn't
  set such errors are only reflected in the return values of
  *SSL_CONF_set_cmd()* or *SSL_CONF_set_argv()*

* RETURN VALUES
*SSL_CONF_CTX_set_flags()* and *SSL_CONF_CTX_clear_flags()* returns the
new flags value after setting or clearing flags.

* SEE ALSO
*SSL_CONF_CTX_new* (3), *SSL_CONF_CTX_set_ssl_ctx* (3),
*SSL_CONF_CTX_set1_prefix* (3), *SSL_CONF_cmd* (3),
*SSL_CONF_cmd_argv* (3)

* HISTORY
These functions were added in OpenSSL 1.0.2.

* COPYRIGHT
Copyright 2012-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
