#+TITLE: Manpages - curl_url_get.3
#+DESCRIPTION: Linux manpage for curl_url_get.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_url_get - extract a part from a URL

* SYNOPSIS
*#include <curl/curl.h>*

#+begin_example
  CURLUcode curl_url_get(CURLU *url,
                         CURLUPart what,
                         char **part,
                         unsigned int flags)
#+end_example

* DESCRIPTION
Given the /url/ handle of an already parsed URL, this function lets the
user extract individual pieces from it.

The /what/ argument should be the particular part to extract (see list
below) and /part/ points to a 'char *' to get updated to point to a
newly allocated string with the contents.

The /flags/ argument is a bitmask with individual features.

The returned part pointer must be freed with /curl_free(3)/ after use.

* FLAGS
The flags argument is zero, one or more bits set in a bitmask.

- CURLU_DEFAULT_PORT :: If the handle has no port stored, this option
  will make /curl_url_get(3)/ return the default port for the used
  scheme.

- CURLU_DEFAULT_SCHEME :: If the handle has no scheme stored, this
  option will make /curl_url_get(3)/ return the default scheme instead
  of error.

- CURLU_NO_DEFAULT_PORT :: Instructs /curl_url_get(3)/ to not return a
  port number if it matches the default port for the scheme.

- CURLU_URLDECODE :: Asks /curl_url_get(3)/ to URL decode the contents
  before returning it. It will not attempt to decode the scheme, the
  port number or the full URL. ´ The query component will also get
  plus-to-space conversion as a bonus when this bit is set.

Note that this URL decoding is charset unaware and you will get a zero
terminated string back with data that could be intended for a particular
encoding.

If there's any byte values lower than 32 in the decoded string, the get
operation will return an error instead.

- CURLU_URLENCODE :: If set, will make /curl_url_get(3)/ URL encode the
  host name part when a full URL is retrieved. If not set (default),
  libcurl returns the URL with the host name "raw" to support IDN names
  to appear as-is. IDN host names are typically using non-ASCII bytes
  that otherwise will be percent-encoded.

Note that even when not asking for URL encoding, the '%' (byte 37) will
be URL encoded to make sure the host name remains valid.

* PARTS
- CURLUPART_URL :: When asked to return the full URL, /curl_url_get(3)/
  will return a normalized and possibly cleaned up version of what was
  previously parsed.

- CURLUPART_SCHEME :: Scheme cannot be URL decoded on get.

- CURLUPART_USER :: 

- CURLUPART_PASSWORD :: 

- CURLUPART_OPTIONS :: 

- CURLUPART_HOST :: The host name. If it is an IPv6 numeric address, the
  zoneid will not be part of it but is provided separately in
  /CURLUPART_ZONEID/. IPv6 numerical addresses are returned within
  brackets ([]).

- CURLUPART_ZONEID :: If the host name is a numeric IPv6 address, this
  field might also be set.

- CURLUPART_PORT :: Port cannot be URL decoded on get.

- CURLUPART_PATH :: /part/ will be '/' even if no path is supplied in
  the URL.

- CURLUPART_QUERY :: The initial question mark that denotes the
  beginning of the query part is a delimiter only. It is not part of the
  query contents.

A not-present query will lead /part/ to be set to NULL. A zero-length
query will lead /part/ to be set to a zero-length string.

The query part will also get pluses converted to space when asked to URL
decode on get with the CURLU_URLDECODE bit.

- CURLUPART_FRAGMENT :: 

* EXAMPLE
#+begin_example
    CURLUcode rc;
    CURLU *url = curl_url();
    rc = curl_url_set(url, CURLUPART_URL, "https://example.com", 0);
    if(!rc) {
      char *scheme;
      rc = curl_url_get(url, CURLUPART_SCHEME, &scheme, 0);
      if(!rc) {
        printf("the scheme is %s\n", scheme);
        curl_free(scheme);
      }
      curl_url_cleanup(url);
    }
#+end_example

* AVAILABILITY
Added in 7.62.0. CURLUPART_ZONEID was added in 7.65.0.

* RETURN VALUE
Returns a CURLUcode error value, which is CURLUE_OK (0) if everything
went fine. See the /libcurl-errors(3)/ man page for the full list with
descriptions.

If this function returns an error, no URL part is returned.

* SEE ALSO
*curl_url_cleanup*(3), *curl_url*(3), *curl_url_set*(3),
*curl_url_dup*(3), *curl_url_strerror*(3), *CURLOPT_CURLU*(3)
