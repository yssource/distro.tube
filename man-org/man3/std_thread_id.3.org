#+TITLE: Manpages - std_thread_id.3
#+DESCRIPTION: Linux manpage for std_thread_id.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::thread::id - thread::id

* SYNOPSIS
\\

=#include <std_thread.h>=

** Public Member Functions
*id* (native_handle_type __id)\\

** Friends
struct *hash< id >*\\

bool *operator<* (*id* __x, *id* __y) noexcept\\

template<class _CharT , class _Traits > *basic_ostream*< _CharT, _Traits
> & *operator<<* (*basic_ostream*< _CharT, _Traits > &__out, *id*
__id)\\

bool *operator==* (*id* __x, *id* __y) noexcept\\

class *thread*\\

* Detailed Description
thread::id

Definition at line *81* of file *std_thread.h*.

* Constructor & Destructor Documentation
** std::thread::id::id ()= [inline]=, = [noexcept]=
Definition at line *86* of file *std_thread.h*.

** std::thread::id::id (native_handle_type __id)= [inline]=,
= [explicit]=
Definition at line *89* of file *std_thread.h*.

* Friends And Related Function Documentation
** friend struct *hash*< *id* >= [friend]=
Definition at line *92* of file *std_thread.h*.

** bool operator< (*thread::id* __x, *thread::id* __y)= [friend]=
Definition at line *69* of file *thread*.

** template<class _CharT , class _Traits > *basic_ostream*< _CharT,
_Traits > & operator<< (*basic_ostream*< _CharT, _Traits > & __out,
*thread::id* __id)= [friend]=
Definition at line *86* of file *thread*.

** bool operator== (*thread::id* __x, *thread::id* __y)= [friend]=
Definition at line *281* of file *std_thread.h*.

** friend class *thread*= [friend]=
Definition at line *92* of file *std_thread.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
