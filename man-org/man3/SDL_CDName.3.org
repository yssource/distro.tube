#+TITLE: Manpages - SDL_CDName.3
#+DESCRIPTION: Linux manpage for SDL_CDName.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CDName - Returns a human-readable, system-dependent identifier for
the CD-ROM.

* SYNOPSIS
*#include "SDL.h"*

*const char *SDL_CDName*(*int drive*);

* DESCRIPTION
Returns a human-readable, system-dependent identifier for the CD-ROM.
*drive* is the index of the drive. Drive indices start to 0 and end at
*SDL_CDNumDrives()*-1.

* EXAMPLES
-  · :: "/dev/cdrom"

-  · :: "E:"

-  · :: "/dev/disk/ide/1/master"

* SEE ALSO
*SDL_CDNumDrives*
