#+TITLE: Manpages - MPI_Win_get_errhandler.3
#+DESCRIPTION: Linux manpage for MPI_Win_get_errhandler.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Win_get_errhandler* - Retrieves the error handler currently
associated with a window.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Win_get_errhandler(MPI_Win win, MPI_Errhandler *errhandler)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_WIN_GET_ERRHANDLER(WIN, ERRHANDLER, IERROR)
  	INTEGER WIN, ERRHANDLER, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Win_get_errhandler(win, errhandler, ierror)
  	TYPE(MPI_Win), INTENT(IN) :: win
  	TYPE(MPI_Errhandler), INTENT(OUT) :: errhandler
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  MPI::Errhandler MPI::Win::Get_errhandler() const
#+end_example

* INPUT PARAMETER
- win :: Window (handle).

* OUTPUT PARAMETERS
- errhandler :: Error handler currently associated with window (handle).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_Win_get_errhandler retrieves the error handler currently associated
with a window.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.
