#+TITLE: Manpages - ieee1284_set_timeout.3
#+DESCRIPTION: Linux manpage for ieee1284_set_timeout.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ieee1284_set_timeout - modify inactivity timeout

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

*struct timeval *ieee1284_set_timeout(struct parport **/port/*, struct
timeval **/timeout/*);*

* DESCRIPTION
This function sets a new value for the inactivity timeout (used for
block transfer functions), and returns the old value.

The /port/ must be claimed.

The /timeout/ parameter may be *NULL*, in which case the old value is
left unchanged.

* RETURN VALUE
This function returns a pointer to a struct timeval representing the old
value. This uses the same storage as the /port/ structure, and so is not
valid after closing the port.

* NOTES
Note that this is an inactivity time-out, not an absolute time-out.
During a data transfer, if the peripheral is inactive for the length of
time specified then the host gives up.

It is also advisory; no guarantee is made that the transfer will ever
complete.

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
