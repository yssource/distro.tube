#+TITLE: Manpages - Test_Alien_Run.3pm
#+DESCRIPTION: Linux manpage for Test_Alien_Run.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test::Alien::Run - Run object

* VERSION
version 2.44

* SYNOPSIS
use Test2::V0; use Test::Alien; run_ok([ $^X, -e => print "some output";
exit 22]) ->exit_is(22) ->out_like(qr{some});

* DESCRIPTION
This class stores information about a process run as performed by
Test::Alien#run_ok. That function is the /ONLY/ way to create an
instance of this class.

* ATTRIBUTES
** out
my $str = $run->out;

The standard output from the run.

** err
my $str = $run->err;

The standard error from the run.

** exit
my $int = $run->exit;

The exit value of the run.

** signal
my $int = $run->signal;

The signal that killed the run, or zero if the process was terminated
normally.

* METHODS
These methods return the run object itself, so they can be chained, as
in the synopsis above.

** success
$run->success; $run->success($message);

Passes if the process terminated normally with an exit value of 0.

** exit_is
$run->exit_is($exit); $run->exit_is($exit, $message);

Passes if the process terminated with the given exit value.

** exit_isnt
$run->exit_isnt($exit); $run->exit_isnt($exit, $message);

Passes if the process terminated with an exit value of anything but the
given value.

** out_like
$run->out_like($regex); $run->out_like($regex, $message);

Passes if the output of the run matches the given pattern.

** out_unlike
$run->out_unlike($regex); $run->out_unlike($regex, $message);

Passes if the output of the run does not match the given pattern.

** err_like
$run->err_like($regex); $run->err_like($regex, $message);

Passes if the standard error of the run matches the given pattern.

** err_unlike
$run->err_unlike($regex); $run->err_unlike($regex, $message);

Passes if the standard error of the run does not match the given
pattern.

** note
$run->note;

Send the output and standard error as test note.

** diag
$run->diag;

Send the output and standard error as test diagnostic.

* SEE ALSO
- Test::Alien :: 

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
