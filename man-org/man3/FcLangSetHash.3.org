#+TITLE: Manpages - FcLangSetHash.3
#+DESCRIPTION: Linux manpage for FcLangSetHash.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcLangSetHash - return a hash value for a langset

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar32 FcLangSetHash (const FcLangSet */ls/*);*

* DESCRIPTION
This function returns a value which depends solely on the languages
supported by /ls/. Any language which equals /ls/ will have the same
result from *FcLangSetHash*. However, two langsets with the same hash
value may not be equal.
