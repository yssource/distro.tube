#+TITLE: Manpages - pcre2_set_glob_separator.3
#+DESCRIPTION: Linux manpage for pcre2_set_glob_separator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  int pcre2_set_glob_separator(pcre2_convert_context *cvcontext,
   uint32_t separator_char);
#+end_example

* DESCRIPTION
This function is part of an experimental set of pattern conversion
functions. It sets the component separator character that is used when
converting globs. The second argument must be one of the characters
forward slash, backslash, or dot. The default is backslash when running
under Windows, otherwise forward slash. The result of the function is
zero for success or PCRE2_ERROR_BADDATA if the second argument is
invalid.

The pattern conversion functions are described in the *pcre2convert*
documentation.
