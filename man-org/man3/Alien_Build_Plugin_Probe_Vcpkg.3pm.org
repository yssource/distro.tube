#+TITLE: Manpages - Alien_Build_Plugin_Probe_Vcpkg.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Probe_Vcpkg.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Probe::Vcpkg - Probe for system libraries using
Vcpkg

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin Probe::Vcpkg => libffi;

* DESCRIPTION
This plugin probe can be used to find system packages using Microsoft's
=Vcpkg= package manager for Visual C++ builds of Perl. =Vcpkg= is a
package manager for Visual C++ that includes a number of open source
packages. Although =Vcpkg= does also support Linux and macOS, this
plugin does not support finding =Vcpkg= packages on those platforms. For
more details on =Vcpkg=, see the project github page here:

<https://github.com/microsoft/vcpkg>

Here is the quick start guide for getting Alien::Build to work with
=Vpkg=:

# install Vcpkg C:\> git clone https://github.com/Microsoft/vcpkg.git
C:\> cd vcpkg C:\vcpkg> .\bootstrap-vcpkg.bat C:\vcpkg> .\vcpkg
integrate install # update PATH to include the bin directory # so that
.DLL files can be found by Perl C:\vcpkg> path
c:\vcpkg\installed\x64-windows\bin;%PATH% # install the packages that
you want C:\vcpkg> .\vcpkg install libffi # install the alien that uses
it C:\vcpkg> cpanm Alien::FFI

If you are using 32 bit build of Perl, then substitute =x86-windows= for
=x64-windows=. If you do not want to add the =bin= directory to the
=PATH=, then you can use =x64-windows-static= instead, which will
provide static libraries. (As of this writing static libraries for 32
bit Windows are not available). The main downside to using
=x64-windows-static= is that Aliens that require dynamic libraries for
FFI will not be installable.

If you do not want to install =Vcpkg= user wide (the =integrate install=
command above), then you can use the =PERL_WIN32_VCPKG_ROOT= environment
variable instead:

# install Vcpkg C:\> git clone https://github.com/Microsoft/vcpkg.git
C:\> cd vcpkg C:\vcpkg> .\bootstrap-vcpkg.bat C:\vcpkg> set
PERL_WIN32_VCPKG_ROOT=c:\vcpkg

* PROPERTIES
** name
Specifies the name of the Vcpkg. This should not be used with the =lib=
property below, choose only one.

This is the default property, so these two are equivalent:

plugin Probe::Vcpkg => (name => foo);

and

plugin Probe::Vcpkg => foo;

** lib
Specifies the list of libraries that make up the Vcpkg. This should not
be used with the =name= property above, choose only one. Note that using
this detection method, the version number of the package will not be
automatically determined (since multiple packages could potentially make
up the list of libraries), so you need to determine the version number
another way if you need it.

This must be an array reference. Do not include the =.lib= extension.

plugin Probe::Vcpkg => (lib => [foo,bar]);

** ffi_name
Specifies an alternate ffi_name for finding dynamic libraries.

* SEE ALSO
Alien::Build, alienfile, Alien::Build::MM, Alien

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
