#+TITLE: Man3 - O
#+DESCRIPTION: Man3 - O
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* O
#+begin_src bash :exports results
readarray -t starts_with_o < <(find . -type f -iname "o*" | sort)

for x in "${starts_with_o[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./o2i_SCT_LIST.3ssl.org][o2i_SCT_LIST.3ssl]]                 |
| [[file:./O.3perl.org][O.3perl]]                           |
| [[file:./OBJ_nid2obj.3ssl.org][OBJ_nid2obj.3ssl]]                  |
| [[file:./OCSP_cert_to_id.3ssl.org][OCSP_cert_to_id.3ssl]]              |
| [[file:./OCSP_request_add1_nonce.3ssl.org][OCSP_request_add1_nonce.3ssl]]      |
| [[file:./OCSP_REQUEST_new.3ssl.org][OCSP_REQUEST_new.3ssl]]             |
| [[file:./OCSP_resp_find_status.3ssl.org][OCSP_resp_find_status.3ssl]]        |
| [[file:./OCSP_response_status.3ssl.org][OCSP_response_status.3ssl]]         |
| [[file:./OCSP_sendreq_new.3ssl.org][OCSP_sendreq_new.3ssl]]             |
| [[file:./ODBM_File.3perl.org][ODBM_File.3perl]]                   |
| [[file:./off64_t.3.org][off64_t.3]]                         |
| [[file:./offsetof.3.org][offsetof.3]]                        |
| [[file:./off_t.3.org][off_t.3]]                           |
| [[file:./ok.3perl.org][ok.3perl]]                          |
| [[file:./OMPI_Affinity_str.3.org][OMPI_Affinity_str.3]]               |
| [[file:./on_exit.3.org][on_exit.3]]                         |
| [[file:./Opcode.3perl.org][Opcode.3perl]]                      |
| [[file:./open.3perl.org][open.3perl]]                        |
| [[file:./opendir.3.org][opendir.3]]                         |
| [[file:./openlog.3.org][openlog.3]]                         |
| [[file:./open_memstream.3.org][open_memstream.3]]                  |
| [[file:./OpenMPI.3.org][OpenMPI.3]]                         |
| [[file:./openproc.3.org][openproc.3]]                        |
| [[file:./openpty.3.org][openpty.3]]                         |
| [[file:./OpenSSL_add_all_algorithms.3ssl.org][OpenSSL_add_all_algorithms.3ssl]]   |
| [[file:./OPENSSL_Applink.3ssl.org][OPENSSL_Applink.3ssl]]              |
| [[file:./OPENSSL_config.3ssl.org][OPENSSL_config.3ssl]]               |
| [[file:./OPENSSL_fork_prepare.3ssl.org][OPENSSL_fork_prepare.3ssl]]         |
| [[file:./OPENSSL_ia32cap.3ssl.org][OPENSSL_ia32cap.3ssl]]              |
| [[file:./OPENSSL_init_crypto.3ssl.org][OPENSSL_init_crypto.3ssl]]          |
| [[file:./OPENSSL_init_ssl.3ssl.org][OPENSSL_init_ssl.3ssl]]             |
| [[file:./OPENSSL_instrument_bus.3ssl.org][OPENSSL_instrument_bus.3ssl]]       |
| [[file:./OPENSSL_LH_COMPFUNC.3ssl.org][OPENSSL_LH_COMPFUNC.3ssl]]          |
| [[file:./OPENSSL_LH_stats.3ssl.org][OPENSSL_LH_stats.3ssl]]             |
| [[file:./OPENSSL_load_builtin_modules.3ssl.org][OPENSSL_load_builtin_modules.3ssl]] |
| [[file:./OPENSSL_malloc.3ssl.org][OPENSSL_malloc.3ssl]]               |
| [[file:./OPENSSL_secure_malloc.3ssl.org][OPENSSL_secure_malloc.3ssl]]        |
| [[file:./OPENSSL_VERSION_NUMBER.3ssl.org][OPENSSL_VERSION_NUMBER.3ssl]]       |
| [[file:./open_wmemstream.3.org][open_wmemstream.3]]                 |
| [[file:./ops.3perl.org][ops.3perl]]                         |
| [[file:./optarg.3.org][optarg.3]]                          |
| [[file:./opterr.3.org][opterr.3]]                          |
| [[file:./optind.3.org][optind.3]]                          |
| [[file:./optopt.3.org][optopt.3]]                          |
| [[file:./opus_ctlvalues.3.org][opus_ctlvalues.3]]                  |
| [[file:./opus_custom.3.org][opus_custom.3]]                     |
| [[file:./opus_custom.h.3.org][opus_custom.h.3]]                   |
| [[file:./opus_decoder.3.org][opus_decoder.3]]                    |
| [[file:./opus_decoderctls.3.org][opus_decoderctls.3]]                |
| [[file:./opus_defines.h.3.org][opus_defines.h.3]]                  |
| [[file:./opus_encoder.3.org][opus_encoder.3]]                    |
| [[file:./opus_encoderctls.3.org][opus_encoderctls.3]]                |
| [[file:./opus_errorcodes.3.org][opus_errorcodes.3]]                 |
| [[file:./opus_genericctls.3.org][opus_genericctls.3]]                |
| [[file:./opus_libinfo.3.org][opus_libinfo.3]]                    |
| [[file:./opus_multistream.3.org][opus_multistream.3]]                |
| [[file:./opus_multistream_ctls.3.org][opus_multistream_ctls.3]]           |
| [[file:./opus_multistream.h.3.org][opus_multistream.h.3]]              |
| [[file:./opus_repacketizer.3.org][opus_repacketizer.3]]               |
| [[file:./opus_types.h.3.org][opus_types.h.3]]                    |
| [[file:./ordchr.3am.org][ordchr.3am]]                        |
| [[file:./OS_CheckUpdates_AUR.3pm.org][OS_CheckUpdates_AUR.3pm]]           |
| [[file:./OSSL_STORE_expect.3ssl.org][OSSL_STORE_expect.3ssl]]            |
| [[file:./OSSL_STORE_INFO.3ssl.org][OSSL_STORE_INFO.3ssl]]              |
| [[file:./OSSL_STORE_LOADER.3ssl.org][OSSL_STORE_LOADER.3ssl]]            |
| [[file:./OSSL_STORE_open.3ssl.org][OSSL_STORE_open.3ssl]]              |
| [[file:./OSSL_STORE_SEARCH.3ssl.org][OSSL_STORE_SEARCH.3ssl]]            |
| [[file:./overload.3perl.org][overload.3perl]]                    |
| [[file:./overloading.3perl.org][overloading.3perl]]                 |
