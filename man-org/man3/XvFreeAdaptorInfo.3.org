#+TITLE: Manpages - XvFreeAdaptorInfo.3
#+DESCRIPTION: Linux manpage for XvFreeAdaptorInfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XvFreeAdaptorInfo - free adaptor information

* SYNOPSIS
*void XvFreeAdaptorInfo(XvAdaptorInfo **/p_adaptor_info/*);*

* ARGUMENTS
- p_adaptor_info :: Pointer to where the adaptor information is located.

* DESCRIPTION
*XvFreeAdaptorInfo*(3) frees adaptor information that was returned by
*XvQueryAdaptors*(3). The data structure used for adaptor information is
defined in the reference page for *XvQueryAdaptors*(3).

* SEE ALSO
*XvQueryAdaptors*(3), *XvFreeEncodingInfo*(3)
