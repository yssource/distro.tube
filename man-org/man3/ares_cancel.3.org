#+TITLE: Manpages - ares_cancel.3
#+DESCRIPTION: Linux manpage for ares_cancel.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_cancel - Cancel a resolve

* SYNOPSIS
#+begin_example
  #include <ares.h>

  void ares_cancel(ares_channel channel)
#+end_example

* DESCRIPTION
The *ares_cancel(3)* function cancels all lookups/requests made on the
the name service channel identified by /channel/. *ares_cancel(3)*
invokes the callbacks for each pending query on the channel, passing a
status of *ARES_ECANCELLED*. These calls give the callbacks a chance to
clean up any state which might have been stored in their arguments. If
such a callback invocation adds a new request to the channel, that
request will /not/ be cancelled by the current invocation of
*ares_cancel(3)*.

* SEE ALSO
*ares_init*(3) *ares_destroy*(3)

* NOTES
This function was added in c-ares 1.2.0

c-ares 1.6.0 and earlier pass a status of *ARES_ETIMEOUT* instead of
*ARES_ECANCELLED*.

* AUTHOR
Dirk Manske
