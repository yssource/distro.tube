#+TITLE: Manpages - svc_getargs.3
#+DESCRIPTION: Linux manpage for svc_getargs.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about svc_getargs.3 is found in manpage for: [[../man3/rpc.3][man3/rpc.3]]