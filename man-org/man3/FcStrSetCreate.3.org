#+TITLE: Manpages - FcStrSetCreate.3
#+DESCRIPTION: Linux manpage for FcStrSetCreate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrSetCreate - create a string set

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcStrSet * FcStrSetCreate (void*);*

* DESCRIPTION
Create an empty set.
