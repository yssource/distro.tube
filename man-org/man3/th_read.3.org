#+TITLE: Manpages - th_read.3
#+DESCRIPTION: Linux manpage for th_read.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
th_read, th_write - read and write a file header block from a tar
archive

* SYNOPSIS
*#include <libtar.h>*

*int th_read(TAR **/t/*);*

*int th_write(TAR **/t/*);*

* VERSION
This man page documents version 1.2 of *libtar*.

* DESCRIPTION
The *th_read*() function reads the next block from the tar archive
associated with the /TAR/ handle /t/. It then sets the current tar
header associated with /t/ to the contents of the block read.

The *th_write*() function writes the contents of the current tar header
associated with /t/ to the tar archive associated with /t/.

* RETURN VALUE
On successful completion, *th_read*() and *th_write*() will return 0. On
failure, they will return -1 and set /errno/ to an appropriate value.

On /EOF/, *th_read*() will return 1.

* ERRORS
*th_read*() and *th_write*() will fail if:

- EINVAL :: Less than *T_BLOCKSIZE* blocks were read or written.

* SEE ALSO
*tar_block_read*(3), *tar_block_write*(3)
