#+TITLE: Manpages - Alien_Build_Plugin_Decode_Mojo.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Decode_Mojo.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Decode::Mojo - Plugin to extract links from HTML
using Mojo::DOM or Mojo::DOM58

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin Decode::Mojo;

Force using =Decode::Mojo= via the download negotiator:

use alienfile 1.68; configure { requires
Alien::Build::Plugin::Decode::Mojo; }; plugin Download => ( ... decoder
=> Decode::Mojo, );

* DESCRIPTION
Note: in most cases you will want to use
Alien::Build::Plugin::Download::Negotiate instead. It picks the
appropriate decode plugin based on your platform and environment. In
some cases you may need to use this plugin directly instead.

This plugin decodes an HTML file listing into a list of candidates for
your Prefer plugin. It works just like
Alien::Build::Plugin::Decode::HTML except it uses either Mojo::DOM or
Mojo::DOM58 to do its job.

This plugin is much lighter than The =Decode::HTML= plugin, and doesn't
require XS. It is the default decode plugin used by
Alien::Build::Plugin::Download::Negotiate if it detects that you need to
parse an HTML index.

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
