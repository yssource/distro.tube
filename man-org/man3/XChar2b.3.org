#+TITLE: Manpages - XChar2b.3
#+DESCRIPTION: Linux manpage for XChar2b.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XChar2b.3 is found in manpage for: [[../man3/XLoadFont.3][man3/XLoadFont.3]]