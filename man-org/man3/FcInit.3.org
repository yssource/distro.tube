#+TITLE: Manpages - FcInit.3
#+DESCRIPTION: Linux manpage for FcInit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcInit - initialize fontconfig library

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcInit (void*);*

* DESCRIPTION
Loads the default configuration file and the fonts referenced therein
and sets the default configuration to that result. Returns whether this
process succeeded or not. If the default configuration has already been
loaded, this routine does nothing and returns FcTrue.
