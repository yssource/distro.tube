#+TITLE: Manpages - std_ratio_greater_equal.3
#+DESCRIPTION: Linux manpage for std_ratio_greater_equal.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::ratio_greater_equal< _R1, _R2 > - ratio_greater_equal

* SYNOPSIS
\\

Inherits *std::integral_constant< bool, !ratio_less< _R1, _R2 >::value
>*.

** Public Types
typedef *integral_constant*< bool, __v > *type*\\

typedef bool *value_type*\\

** Public Member Functions
constexpr *operator value_type* () const noexcept\\

constexpr value_type *operator()* () const noexcept\\

** Static Public Attributes
static constexpr bool *value*\\

* Detailed Description
** "template<typename _R1, typename _R2>
\\
struct std::ratio_greater_equal< _R1, _R2 >"ratio_greater_equal

Definition at line *415* of file *std/ratio*.

* Member Typedef Documentation
** typedef *integral_constant*<bool , __v> *std::integral_constant*<
bool , __v >::*type*= [inherited]=
Definition at line *61* of file *std/type_traits*.

** typedef bool *std::integral_constant*< bool , __v
>::value_type= [inherited]=
Definition at line *60* of file *std/type_traits*.

* Member Function Documentation
** constexpr *std::integral_constant*< bool , __v >::operator value_type
() const= [inline]=, = [constexpr]=, = [noexcept]=, = [inherited]=
Definition at line *62* of file *std/type_traits*.

** constexpr value_type *std::integral_constant*< bool , __v
>::operator() () const= [inline]=, = [constexpr]=, = [noexcept]=,
= [inherited]=
Definition at line *67* of file *std/type_traits*.

* Member Data Documentation
** constexpr bool *std::integral_constant*< bool , __v
>::value= [static]=, = [constexpr]=, = [inherited]=
Definition at line *59* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
