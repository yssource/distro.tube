#+TITLE: Manpages - asctime.3
#+DESCRIPTION: Linux manpage for asctime.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about asctime.3 is found in manpage for: [[../man3/ctime.3][man3/ctime.3]]