#+TITLE: Manpages - ceilf.3
#+DESCRIPTION: Linux manpage for ceilf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ceilf.3 is found in manpage for: [[../man3/ceil.3][man3/ceil.3]]