#+TITLE: Manpages - form_data.3x
#+DESCRIPTION: Linux manpage for form_data.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*data_ahead*, *data_behind* - test for off-screen data in given forms

* SYNOPSIS
*#include <form.h>*

*bool data_ahead(const FORM **/form/*);*\\
*bool data_behind(const FORM **/form/*);*\\

* DESCRIPTION
The function *data_ahead* tests whether there is off-screen data ahead
in the given form. It returns TRUE (1) or FALSE (0).

The function *data_behind* tests whether there is off-screen data behind
in the given form. It returns TRUE (1) or FALSE (0).

* SEE ALSO
*curses*(3X), *form*(3X).

* NOTES
The header file *<form.h>* automatically includes the header file
*<curses.h>*.

* PORTABILITY
These routines emulate the System V forms library. They were not
supported on Version 7 or BSD versions.

* AUTHORS
Juergen Pfeifer. Manual pages and adaptation for new curses by Eric S.
Raymond.
