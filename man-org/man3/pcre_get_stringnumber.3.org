#+TITLE: Manpages - pcre_get_stringnumber.3
#+DESCRIPTION: Linux manpage for pcre_get_stringnumber.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE - Perl-compatible regular expressions

* SYNOPSIS
*#include <pcre.h>*

#+begin_example
  int pcre_get_stringnumber(const pcre *code,
   const char *name);

  int pcre16_get_stringnumber(const pcre16 *code,
   PCRE_SPTR16 name);

  int pcre32_get_stringnumber(const pcre32 *code,
   PCRE_SPTR32 name);
#+end_example

* DESCRIPTION
This convenience function finds the number of a named substring
capturing parenthesis in a compiled pattern. Its arguments are:

/code/ Compiled regular expression /name/ Name whose number is required

The yield of the function is the number of the parenthesis if the name
is found, or PCRE_ERROR_NOSUBSTRING otherwise. When duplicate names are
allowed (PCRE_DUPNAMES is set), it is not defined which of the numbers
is returned by *pcre[16|32]_get_stringnumber()*. You can obtain the
complete list by calling *pcre[16|32]_get_stringtable_entries()*.

There is a complete description of the PCRE native API in the *pcreapi*
page and a description of the POSIX API in the *pcreposix* page.
