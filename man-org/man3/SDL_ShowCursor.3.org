#+TITLE: Manpages - SDL_ShowCursor.3
#+DESCRIPTION: Linux manpage for SDL_ShowCursor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_ShowCursor - Toggle whether or not the cursor is shown on the
screen.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_ShowCursor*(*int toggle*);

* DESCRIPTION
Toggle whether or not the cursor is shown on the screen. Passing
*SDL_ENABLE* displays the cursor and passing *SDL_DISABLE* hides it. The
current state of the mouse cursor can be queried by passing *SDL_QUERY*,
either *SDL_DISABLE* or *SDL_ENABLE* will be returned.

The cursor starts off displayed, but can be turned off.

* RETURN VALUE
Returns the current state of the cursor.

* SEE ALSO
*SDL_CreateCursor*, *SDL_SetCursor*
