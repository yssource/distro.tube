#+TITLE: Manpages - XActivateScreenSaver.3
#+DESCRIPTION: Linux manpage for XActivateScreenSaver.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XActivateScreenSaver.3 is found in manpage for: [[../man3/XSetScreenSaver.3][man3/XSetScreenSaver.3]]