#+TITLE: Manpages - XML_Parser_Style_Debug.3pm
#+DESCRIPTION: Linux manpage for XML_Parser_Style_Debug.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
XML::Parser::Style::Debug - Debug style for XML::Parser

* SYNOPSIS
use XML::Parser; my $p = XML::Parser->new(Style => Debug);
$p->parsefile(foo.xml);

* DESCRIPTION
This just prints out the document in outline form to STDERR. Nothing
special is returned by parse.
