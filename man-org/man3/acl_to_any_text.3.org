#+TITLE: Manpages - acl_to_any_text.3
#+DESCRIPTION: Linux manpage for acl_to_any_text.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function translates the ACL pointed to by the argument

into a

terminated character string. This character string is composed of the
ACL entries contained in

in the entry text format described on

Entries are separated from each other by the

character. If the argument

is not

each entry is prefixed by this character string.

If the argument

is

ACL entries are converted using the entry tag type keywords

and

User IDs and group IDs of ACL entries that contain such qualifiers are
converted to their corresponding names; if an identifier has no
corresponding name, a decimal number string is produced. The ACL text
representation contains no additional comments. A bitwise combinations
of the following

can be used to modify the result:

Instead of the full tag type keywords, single letter abbreviations are
used. The abbreviation for

is

the abbreviation for

is

the abbreviation for

is

and the abbreviation for

is

User IDs and group IDs are included as decimal numbers instead of names.

A comment containing the effective permissions of the ACL entry is
included after ACL entries that contain permissions which are
ineffective because they are masked by an ACL_MASK entry. The ACL entry
and the comment are separated by a tab character.

A comment containing the effective permissions of the ACL entry is
included after all ACL entries that are affected by an ACL_MASK entry.
The comment is included even if the permissions contained in the ACL
entry equal the effective permissions. The ACL entry and the comment are
separated by a tab character.

This option is used in combination with the TEXT_SOME_EFFECTIVE or
TEXT_ALL_EFFECTIVE option. The number of tab characters inserted between
the ACL entry and the comment is increased so that the comment is
aligned to the fourth tab stop position. A tab width of 8 characters is
assumed.

The ACL referred to by

is not changed.

This function allocates any memory necessary to contain the string and
returns a pointer to the string. The caller should free any releasable
memory, when the new string is no longer required, by calling

with the

returned by

as an argument.

On success, this function returns a pointer to the text representation
of the ACL. On error, a value of

is returned, and

is set appropriately.

If any of the following conditions occur, the

function returns a value of

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

The ACL referenced by

contains one or more improperly formed ACL entries, or for some other
reason cannot be translated into the text form of an ACL.

The character string to be returned requires more memory than is allowed
by the hardware or system-imposed memory management constraints.

This is a non-portable, Linux specific extension to the ACL manipulation
functions defined in IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned).

Written by
