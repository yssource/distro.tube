#+TITLE: Manpages - FcConfigSetRescanInterval.3
#+DESCRIPTION: Linux manpage for FcConfigSetRescanInterval.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigSetRescanInterval - Set config rescan interval

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigSetRescanInterval (FcConfig */config/*, int
*/rescanInterval/*);*

* DESCRIPTION
Sets the rescan interval. Returns FcFalse if the interval cannot be set
(due to allocation failure). Otherwise returns FcTrue. An interval
setting of zero disables automatic checks. If /config/ is NULL, the
current configuration is used.
