#+TITLE: Manpages - XChangeProperty.3
#+DESCRIPTION: Linux manpage for XChangeProperty.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XChangeProperty.3 is found in manpage for: [[../man3/XGetWindowProperty.3][man3/XGetWindowProperty.3]]