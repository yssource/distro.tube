#+TITLE: Manpages - afWriteFrames.3
#+DESCRIPTION: Linux manpage for afWriteFrames.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
afWriteFrames - write sample frames to a track in an audio file

* SYNOPSIS
#+begin_example
  #include <audiofile.h>
#+end_example

#+begin_example
  AFframecount afWriteFrames(AFfilehandle file, int track, const void *data,
      AFframecount count);
#+end_example

* DESCRIPTION
afWriteFrames attempts to write up to /count/ frames of audio data from
the buffer /data/ to the audio file handle /file/.

* PARAMETERS
/file/ is a valid file handle returned by *afOpenFile*(3).

/track/ is always AF_DEFAULT_TRACK for all currently supported file
formats.

/data/ is a buffer of sample frames to be written to the file.

/count/ is the number of sample frames to be written.

* RETURN VALUE
afWriteFrames returns the number of sample frames successfully written
to /file/.

* ERRORS
afWriteFrames can produce these errors:

AF_BAD_FILEHANDLE

#+begin_quote
  the file handle was invalid
#+end_quote

AF_BAD_TRACKID

#+begin_quote
  the track is not AF_DEFAULT_TRACK
#+end_quote

AF_BAD_WRITE

#+begin_quote
  writing audio data to the file failed
#+end_quote

AF_BAD_LSEEK

#+begin_quote
  seeking within the file failed
#+end_quote

* SEE ALSO
*afReadFrames*(3)

* AUTHOR
Michael Pruett <michael@68k.org>
