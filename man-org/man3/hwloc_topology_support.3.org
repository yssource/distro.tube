#+TITLE: Manpages - hwloc_topology_support.3
#+DESCRIPTION: Linux manpage for hwloc_topology_support.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwloc_topology_support

* SYNOPSIS
\\

=#include <hwloc.h>=

** Data Fields
struct *hwloc_topology_discovery_support* * *discovery*\\

struct *hwloc_topology_cpubind_support* * *cpubind*\\

struct *hwloc_topology_membind_support* * *membind*\\

struct *hwloc_topology_misc_support* * *misc*\\

* Detailed Description
Set of flags describing actual support for this topology.

This is retrieved with *hwloc_topology_get_support()* and will be valid
until the topology object is destroyed. Note: the values are correct
only after discovery.

* Field Documentation
** struct *hwloc_topology_cpubind_support**
hwloc_topology_support::cpubind
** struct *hwloc_topology_discovery_support**
hwloc_topology_support::discovery
** struct *hwloc_topology_membind_support**
hwloc_topology_support::membind
** struct *hwloc_topology_misc_support** hwloc_topology_support::misc
* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
