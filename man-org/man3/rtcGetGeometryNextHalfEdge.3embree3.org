#+TITLE: Manpages - rtcGetGeometryNextHalfEdge.3embree3
#+DESCRIPTION: Linux manpage for rtcGetGeometryNextHalfEdge.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcGetGeometryNextHalfEdge - returns the next half edge
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  unsigned int rtcGetGeometryNextHalfEdge(
    RTCGeometry geometry,
    unsigned int edgeID
  );
#+end_example

** DESCRIPTION
The =rtcGetGeometryNextHalfEdge= function returns the ID of the next
half edge of the specified half edge (=edgeID= argument). For instance
in the following example the next half edge of =e10= is =e11=.

#+begin_example
#+end_example

This function can only be used for subdivision geometries. As all
topologies of a subdivision geometry share the same face buffer the
function does not depend on the topology ID.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcGetGeometryFirstHalfEdge], [rtcGetGeometryFace],
[rtcGetGeometryOppositeHalfEdge], [rtcGetGeometryNextHalfEdge],
[rtcGetGeometryPreviousHalfEdge]
