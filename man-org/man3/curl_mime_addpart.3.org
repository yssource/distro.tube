#+TITLE: Manpages - curl_mime_addpart.3
#+DESCRIPTION: Linux manpage for curl_mime_addpart.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_mime_addpart - append a new empty part to a mime structure

* SYNOPSIS
*#include <curl/curl.h>*

*curl_mimepart * curl_mime_addpart(curl_mime * */mime/*);*

* DESCRIPTION
/curl_mime_addpart(3)/ creates and appends a new empty part to the given
mime structure and returns a handle to it. The returned part handle can
subsequently be populated using functions from the mime API.

/mime/ is the handle of the mime structure in which the new part must be
appended.

* EXAMPLE
#+begin_example
   curl_mime *mime;
   curl_mimepart *part;

   /* create a mime handle */
   mime = curl_mime_init(easy);

   /* add a part */
   part = curl_mime_addpart(mime);

   /* continue and set name + data to the part */
   curl_mime_data(part, "This is the field data", CURL_ZERO_TERMINATED);
   curl_mime_name(part, "data");
#+end_example

* AVAILABILITY
As long as at least one of HTTP, SMTP or IMAP is enabled. Added in
7.56.0.

* RETURN VALUE
A mime part structure handle, or NULL upon failure.

* SEE ALSO
*curl_mime_init*(3), *curl_mime_name*(3), *curl_mime_data*(3),
*curl_mime_data_cb*(3), *curl_mime_filedata*(3),
*curl_mime_filename*(3), *curl_mime_subparts*(3), *curl_mime_type*(3),
*curl_mime_headers*(3), *curl_mime_encoder*(3)
