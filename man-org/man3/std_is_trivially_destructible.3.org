#+TITLE: Manpages - std_is_trivially_destructible.3
#+DESCRIPTION: Linux manpage for std_is_trivially_destructible.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_trivially_destructible< _Tp > - is_trivially_destructible

* SYNOPSIS
\\

Inherits std::__and_<... >.

* Detailed Description
** "template<typename _Tp>
\\
struct std::is_trivially_destructible< _Tp >"is_trivially_destructible

Definition at line *1287* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
