#+TITLE: Manpages - libmicrohttpd.3
#+DESCRIPTION: Linux manpage for libmicrohttpd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
GNU libmicrohttpd (short MHD) allows applications to easily integrate
the functionality of a simple HTTP server. MHD is a GNU package.

The details of the API are described in comments in the header file, a
detailed reference documentation in Texinfo, a tutorial, and in brief on
the MHD webpage.

libmicrohttpd is released under both the LGPL Version 2.1 or higher and
the GNU GPL with eCos extension. For details on both licenses please
read the respective appendix in the Texinfo manual.

libmicrohttpd include file

libmicrohttpd library

info libmicrohttpd

GNU

was originally designed by

and

The original implementation was done by

and Christian Grothoff. SSL/TLS support was added by Sagie Amir using
code from GnuTLS. See the AUTHORS file in the distribution for a more
detailed list of contributors.

You can obtain the latest version from

Report bugs by using
