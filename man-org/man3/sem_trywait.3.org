#+TITLE: Manpages - sem_trywait.3
#+DESCRIPTION: Linux manpage for sem_trywait.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sem_trywait.3 is found in manpage for: [[../man3/sem_wait.3][man3/sem_wait.3]]