#+TITLE: Manpages - keyctl_revoke.3
#+DESCRIPTION: Linux manpage for keyctl_revoke.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
keyctl_revoke - revoke a key

* SYNOPSIS
#+begin_example
  #include <keyutils.h>

  long keyctl_revoke(key_serial_t key);
#+end_example

* DESCRIPTION
*keyctl_revoke*() marks a key as being revoked.

After this operation has been performed on a key, attempts to access it
will meet with error *EKEYREVOKED*.

The caller must have *write* permission on a key to be able to revoke
it.

* RETURN VALUE
On success *keyctl_revoke*() returns *0*. On error, the value *-1* will
be returned and /errno/ will have been set to an appropriate error.

* ERRORS
- *ENOKEY* :: The specified key does not exist.

- *EKEYREVOKED* :: The key has already been revoked.

- *EACCES* :: The named key exists, but is not *writable* by the calling
  process.

* LINKING
This is a library function that can be found in /libkeyutils/. When
linking, *-lkeyutils* should be specified to the linker.

* SEE ALSO
*keyctl*(1), *add_key*(2), *keyctl*(2), *request_key*(2), *keyctl*(3),
*keyrings*(7), *keyutils*(7)
