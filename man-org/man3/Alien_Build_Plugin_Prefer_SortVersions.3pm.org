#+TITLE: Manpages - Alien_Build_Plugin_Prefer_SortVersions.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Prefer_SortVersions.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Prefer::SortVersions - Plugin to sort candidates
by most recent first

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin Prefer::SortVersions;

* DESCRIPTION
Note: in most case you will want to use
Alien::Build::Plugin::Download::Negotiate instead. It picks the
appropriate fetch plugin based on your platform and environment. In some
cases you may need to use this plugin directly instead.

This Prefer plugin sorts the packages that were retrieved from a dir
listing, either directly from a Fetch plugin, or from a Decode plugin.
It Returns a listing with the items sorted from post preferable to
least, and filters out any undesirable candidates.

This plugin updates the file list to include the versions that are
extracted, so they can be used by other plugins, such as
Alien::Build::Plugin::Prefer::BadVersion.

* PROPERTIES
** filter
This is a regular expression that lets you filter out files that you do
not want to consider downloading. For example, if the directory listing
contained tarballs and readme files like this:

foo-1.0.0.tar.gz foo-1.0.0.readme

You could specify a filter of =qr/\.tar\.gz$/= to make sure only
tarballs are considered for download.

** version
Regular expression to parse out the version from a filename. The regular
expression should store the result in =$1=. The default =qr/([0-9\.]+)/=
is frequently reasonable.

* SEE ALSO
Alien::Build::Plugin::Download::Negotiate, Alien::Build, alienfile,
Alien::Build::MM, Alien

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
