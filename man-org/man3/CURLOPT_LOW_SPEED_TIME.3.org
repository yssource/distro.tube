#+TITLE: Manpages - CURLOPT_LOW_SPEED_TIME.3
#+DESCRIPTION: Linux manpage for CURLOPT_LOW_SPEED_TIME.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_LOW_SPEED_TIME - low speed limit time period

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_LOW_SPEED_TIME, long
speedtime);

* DESCRIPTION
Pass a long as parameter. It contains the time in number seconds that
the transfer speed should be below the /CURLOPT_LOW_SPEED_LIMIT(3)/ for
the library to consider it too slow and abort.

* DEFAULT
0, disabled

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, url);
    /* abort if slower than 30 bytes/sec during 60 seconds */
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 60L);
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 30L);
    res = curl_easy_perform(curl);
    if(CURLE_OPERATION_TIMEDOUT == res) {
      printf("Timeout!\n");
    }
    /* always cleanup */
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_LOW_SPEED_LIMIT*(3), *CURLOPT_TIMEOUT*(3),
