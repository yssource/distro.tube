#+TITLE: Manpages - DH_size.3ssl
#+DESCRIPTION: Linux manpage for DH_size.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DH_size, DH_bits, DH_security_bits - get Diffie-Hellman prime size and
security bits

* SYNOPSIS
#include <openssl/dh.h> int DH_size(const DH *dh); int DH_bits(const DH
*dh); int DH_security_bits(const DH *dh);

* DESCRIPTION
*DH_size()* returns the Diffie-Hellman prime size in bytes. It can be
used to determine how much memory must be allocated for the shared
secret computed by *DH_compute_key* (3).

*DH_bits()* returns the number of significant bits.

*dh* and *dh->p* must not be *NULL*.

*DH_security_bits()* returns the number of security bits of the given
*dh* key. See *BN_security_bits* (3).

* RETURN VALUES
*DH_size()* returns the prime size of Diffie-Hellman in bytes.

*DH_bits()* returns the number of bits in the key.

*DH_security_bits()* returns the number of security bits.

* SEE ALSO
*DH_new* (3), *DH_generate_key* (3), *BN_num_bits* (3)

* HISTORY
The *DH_bits()* function was added in OpenSSL 1.1.0.

* COPYRIGHT
Copyright 2000-2018 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
