#+TITLE: Manpages - iruserok.3
#+DESCRIPTION: Linux manpage for iruserok.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about iruserok.3 is found in manpage for: [[../man3/rcmd.3][man3/rcmd.3]]