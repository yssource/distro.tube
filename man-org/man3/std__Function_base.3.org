#+TITLE: Manpages - std__Function_base.3
#+DESCRIPTION: Linux manpage for std__Function_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Function_base - Base class of all polymorphic function object
wrappers.

* SYNOPSIS
\\

=#include <std_function.h>=

Inherited by *std::function< _Res(_ArgTypes...)>*= [private]=.

** Public Types
typedef bool(* *_Manager_type*) (_Any_data &, const _Any_data &,
_Manager_operation)\\

** Public Member Functions
bool *_M_empty* () const\\

** Public Attributes
_Any_data *_M_functor*\\

_Manager_type *_M_manager*\\

** Static Public Attributes
static const size_t *_M_max_align*\\

static const size_t *_M_max_size*\\

* Detailed Description
Base class of all polymorphic function object wrappers.

Definition at line *114* of file *std_function.h*.

* Member Typedef Documentation
** typedef bool(* std::_Function_base::_Manager_type) (_Any_data &,
const _Any_data &, _Manager_operation)
Definition at line *250* of file *std_function.h*.

* Constructor & Destructor Documentation
** std::_Function_base::_Function_base ()= [inline]=
Definition at line *240* of file *std_function.h*.

** std::_Function_base::~_Function_base ()= [inline]=
Definition at line *242* of file *std_function.h*.

* Member Function Documentation
** bool std::_Function_base::_M_empty () const= [inline]=
Definition at line *248* of file *std_function.h*.

* Member Data Documentation
** _Any_data std::_Function_base::_M_functor
Definition at line *253* of file *std_function.h*.

** _Manager_type std::_Function_base::_M_manager
Definition at line *254* of file *std_function.h*.

** const size_t std::_Function_base::_M_max_align= [static]=
Definition at line *118* of file *std_function.h*.

** const size_t std::_Function_base::_M_max_size= [static]=
Definition at line *117* of file *std_function.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
