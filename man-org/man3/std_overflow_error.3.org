#+TITLE: Manpages - std_overflow_error.3
#+DESCRIPTION: Linux manpage for std_overflow_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::overflow_error

* SYNOPSIS
\\

Inherits *std::runtime_error*.

** Public Member Functions
*overflow_error* (const char *) _GLIBCXX_TXN_SAFE\\

*overflow_error* (const *overflow_error* &)=default\\

*overflow_error* (const *string* &__arg) _GLIBCXX_TXN_SAFE\\

*overflow_error* (*overflow_error* &&)=default\\

*overflow_error* & *operator=* (const *overflow_error* &)=default\\

*overflow_error* & *operator=* (*overflow_error* &&)=default\\

virtual const char * *what* () const noexcept\\

* Detailed Description
Thrown to indicate arithmetic overflow.\\

Definition at line *273* of file *stdexcept*.

* Member Function Documentation
** virtual const char * std::runtime_error::what () const= [virtual]=,
= [noexcept]=, = [inherited]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::exception*.

Reimplemented in *std::filesystem::filesystem_error*, and
*std::experimental::filesystem::v1::filesystem_error*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
