#+TITLE: Manpages - rpcbind.3t
#+DESCRIPTION: Linux manpage for rpcbind.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
These routines allow client C programs to make procedure calls to the
RPC binder service. (see

maintains a list of mappings between programs and their universal
addresses.

An interface to the rpcbind service, which returns a list of the current
RPC program-to-address mappings on

It uses the transport specified through

to contact the remote rpcbind service on

This routine will return

if the remote rpcbind could not be contacted.

An interface to the rpcbind service, which finds the address of the
service on

that is registered with program number

version

and speaks the transport protocol associated with

The address found is returned in

The

argument should be preallocated. This routine returns

if it succeeds. A return value of

means that the mapping does not exist or that the RPC system failed to
contact the remote rpcbind service. In the latter case, the global
variable

(see

contains the RPC status.

This routine returns the time on

in

If

is

returns the time on its own machine. This routine returns

if it succeeds,

if it fails. The

function can be used to synchronize the time between the client and the
remote server.

An interface to the rpcbind service, which instructs rpcbind on

to make an RPC call on your behalf to a procedure on that host. The

structure should correspond to a connectionless transport. The

argument will be modified to the server's address if the procedure
succeeds (see

and

in

for the definitions of other arguments).

This procedure should normally be used for a

and nothing else. This routine allows programs to do lookup and call,
all in one step.

Note: Even if the server is not running

does not return any error messages to the caller. In such a case, the
caller times out.

Note:

is only available for connectionless transports.

An interface to the rpcbind service, which establishes a mapping between
the triple

and

on the machine's rpcbind service. The value of

must correspond to a network identifier that is defined by the netconfig
database. This routine returns

if it succeeds,

otherwise. (See also

in

If there already exists such an entry with rpcbind,

will fail.

An interface to the rpcbind service, which destroys the mapping between
the triple

and the address on the machine's rpcbind service. If

is

destroys all mapping between the triple

and the addresses on the machine's rpcbind service. This routine returns

if it succeeds,

otherwise. Only the owner of the service or the super-user can destroy
the mapping. (See also

in

These functions are part of libtirpc.
