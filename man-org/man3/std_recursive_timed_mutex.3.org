#+TITLE: Manpages - std_recursive_timed_mutex.3
#+DESCRIPTION: Linux manpage for std_recursive_timed_mutex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::recursive_timed_mutex - recursive_timed_mutex

* SYNOPSIS
\\

** Public Member Functions
*recursive_timed_mutex* (const *recursive_timed_mutex* &)=delete\\

void *lock* ()\\

*recursive_timed_mutex* & *operator=* (const *recursive_timed_mutex*
&)=delete\\

bool *try_lock* ()\\

template<typename _Rep , typename _Period > bool *try_lock_for* (const
*chrono::duration*< _Rep, _Period > &__rtime)\\

template<typename _Clock , typename _Duration > bool *try_lock_until*
(const *chrono::time_point*< _Clock, _Duration > &__atime)\\

void *unlock* ()\\

* Detailed Description
recursive_timed_mutex

Definition at line *410* of file *mutex*.

* Constructor & Destructor Documentation
** std::recursive_timed_mutex::~recursive_timed_mutex ()= [inline]=
Definition at line *432* of file *mutex*.

* Member Function Documentation
** void std::recursive_timed_mutex::lock ()= [inline]=
Definition at line *438* of file *mutex*.

** bool std::recursive_timed_mutex::try_lock ()= [inline]=
Definition at line *451* of file *mutex*.

** template<typename _Rep , typename _Period > bool
std::recursive_timed_mutex::try_lock_for (const *chrono::duration*<
_Rep, _Period > & __rtime)= [inline]=
Definition at line *467* of file *mutex*.

** template<typename _Clock , typename _Duration > bool
std::recursive_timed_mutex::try_lock_until (const *chrono::time_point*<
_Clock, _Duration > & __atime)= [inline]=
Definition at line *483* of file *mutex*.

** void std::recursive_timed_mutex::unlock ()= [inline]=
Definition at line *498* of file *mutex*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
