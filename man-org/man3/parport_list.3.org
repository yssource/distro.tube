#+TITLE: Manpages - parport_list.3
#+DESCRIPTION: Linux manpage for parport_list.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
parport_list - a collection of parallel ports

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

* DESCRIPTION
A parport_list structure is just a vector of parport structures.

* STRUCTURE MEMBERS
The structure has the following members:

#+begin_quote
  #+begin_example
    struct parport_list {
      /* Number of elements in the vector. */
      int portc;

      /* The ports. */
      struct parport **portv;
    };
  #+end_example
#+end_quote

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
