#+TITLE: Manpages - std_bad_function_call.3
#+DESCRIPTION: Linux manpage for std_bad_function_call.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::bad_function_call - Exception class thrown when class template
function's operator() is called with an empty target.

* SYNOPSIS
\\

=#include <std_function.h>=

Inherits *std::exception*.

** Public Member Functions
const char * *what* () const noexcept\\

* Detailed Description
Exception class thrown when class template function's operator() is
called with an empty target.

Definition at line *54* of file *std_function.h*.

* Member Function Documentation
** const char * std::bad_function_call::what () const= [virtual]=,
= [noexcept]=
Returns a C-style character string describing the general cause of the
current error.\\

Reimplemented from *std::exception*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
