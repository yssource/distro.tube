#+TITLE: Manpages - zip_fdopen.3
#+DESCRIPTION: Linux manpage for zip_fdopen.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The zip archive specified by the open file descriptor

is opened and a pointer to a

used to manipulate the archive, is returned. In contrast to

using

the archive can only be opened in read-only mode. The

argument may not be used any longer after calling

The

are specified by

the following values, or 0 for none of them.

Perform additional stricter consistency checks on the archive, and error
if they fail.

If an error occurs and

is

it will be set to the corresponding error code.

Upon successful completion

returns a

pointer, and

should not be used any longer, nor passed to

Otherwise,

is returned and

is set to indicate the error. In the error case,

remains unchanged.

The file specified by

is prepared for use by

unless:

Inconsistencies were found in the file specified by

This error is often caused by specifying

but can also happen without it.

The

argument is invalid. Not all

flags are allowed for

see

Required memory could not be allocated.

The file specified by

is not a zip archive.

The file specified by

could not be prepared for use by

A read error occurred; see

for details.

The file specified by

does not allow seeks.

was added in libzip 1.0.

and
