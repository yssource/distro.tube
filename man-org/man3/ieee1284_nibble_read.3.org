#+TITLE: Manpages - ieee1284_nibble_read.3
#+DESCRIPTION: Linux manpage for ieee1284_nibble_read.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ieee1284_nibble_read, ieee1284_compat_write, ieee1284_byte_read,
ieee1284_epp_read_data, ieee1284_epp_write_data, ieee1284_epp_read_addr,
ieee1284_epp_write_addr, ieee1284_ecp_read_data,
ieee1284_ecp_write_data, ieee1284_ecp_read_addr,
ieee1284_ecp_write_addr - data transfer functions

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

*ssize_t ieee1284_nibble_read(struct parport **/port/*, int */flags/*,
char **/buffer/*, size_t */len/*);*

*ssize_t ieee1284_compat_write(struct parport **/port/*, int */flags/*,
const char **/buffer/*, size_t */len/*);*

*ssize_t ieee1284_byte_read(struct parport **/port/*, int */flags/*,
char **/buffer/*, size_t */len/*);*

*ssize_t ieee1284_epp_read_data(struct parport **/port/*, int */flags/*,
char **/buffer/*, size_t */len/*);*

*ssize_t ieee1284_epp_write_data(struct parport **/port/*, int
*/flags/*, const char **/buffer/*, size_t */len/*);*

*ssize_t ieee1284_epp_read_addr(struct parport **/port/*, int */flags/*,
char **/buffer/*, size_t */len/*);*

*ssize_t ieee1284_epp_write_addr(struct parport **/port/*, int
*/flags/*, const char **/buffer/*, size_t */len/*);*

*ssize_t ieee1284_ecp_read_data(struct parport **/port/*, int */flags/*,
char **/buffer/*, size_t */len/*);*

*ssize_t ieee1284_ecp_write_data(struct parport **/port/*, int
*/flags/*, const char **/buffer/*, size_t */len/*);*

*ssize_t ieee1284_ecp_read_addr(struct parport **/port/*, int */flags/*,
char **/buffer/*, size_t */len/*);*

*ssize_t ieee1284_ecp_write_addr(struct parport **/port/*, int
*/flags/*, const char **/buffer/*, size_t */len/*);*

* DESCRIPTION
This set of functions is for tranferring bytes in the relevant transfer
mode. For ECP and EPP modes two types of transfer are possible: data and
address (usually referred to as channel in ECP).

The supplied /port/ must be a claimed port.

The supplied /buffer/ must be at least /len/ bytes long. When reading,
the transferred data is stored in the buffer; when writing the data to
be transferred is taken from the buffer.

For reads (peripheral to host): if no data is available and
*F1284_NONBLOCK* is not in effect, the inactivity timer is started. If
data becomes available before the inactivity time-out elapses it is
read; otherwise the return value will be *E1284_TIMEDOUT*.

For writes (host to peripheral): if the peripheral is not willing to
accept data and *F1284_NONBLOCK* is not in effect, the inactivity timer
is started. If the peripheral indicates that it is willing to accept
data before the inactivity time-out elapses it is sent; otherwise the
return value will be *E1284_TIMEDOUT*

The /flags/ may alter the behaviour slightly:

*F1284_NONBLOCK*

#+begin_quote
  For reads (peripheral to host): if no data is available, return
  immediately (with *E1284_TIMEDOUT*).

  For writes (host to peripheral): if the peripheral is not willing to
  accept data, return immediately (with *E1284_TIMEDOUT*).
#+end_quote

*F1284_SWE*

#+begin_quote
  Dont use hardware assistance for the transfer, but instead set the
  parallel port pins according to the wire protocol.
#+end_quote

*F1284_RLE* (for ECP only)

#+begin_quote
  Use run length encoding. If the peripheral is in ECP mode with RLE,
  calls to *ieee1284_ecp_read_data* /must/ set this flag in order for
  the RLE from the peripheral to be interpreted correctly, and calls to
  *ieee1284_ecp_write_data* /may/ set this flag in order to take
  advantage of RLE.
#+end_quote

*F1284_FASTEPP* (for EPP only)

#+begin_quote
  Use multi-byte transfers. Several bytes at a time are transferred
  using hardware assistance, if supporting hardware is present. The
  price of this increased speed is that the return value will be less
  reliable when this flag is used.
#+end_quote

For ECP mode, a given direction is in force at any particular time, and
it is up to the application to ensure that it is only writing when in
forward mode, and reading when in reverse mode.

* RETURN VALUE
The return value is the number of bytes successfully transferred or, if
negative, one of:

*E1284_NOTIMPL*

#+begin_quote
  This transfer mode and flags combination is not yet implemented in
  libieee1284.
#+end_quote

*E1284_TIMEDOUT*

#+begin_quote
  Timed out waiting for peripheral to handshake.
#+end_quote

*E1284_NOMEM*

#+begin_quote
  Not enough memory is available.
#+end_quote

*E1284_SYS*

#+begin_quote
  There was a problem at the operating system level. The global variable
  /errno/ has been set appropriately.
#+end_quote

*E1284_INVALIDPORT*

#+begin_quote
  The /port/ parameter is invalid (for instance, perhaps the /port/ is
  not claimed).
#+end_quote

If any bytes are successfully transferred, that number is returned. An
error is returned only if no bytes are transferred.

For host-to-peripheral transfers, all data is at the peripheral by the
time the call returns.

* SEE ALSO
*ieee1284_ecp_fwd_to_rev*(3)

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
