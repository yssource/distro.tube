#+TITLE: Manpages - std_filesystem_path_iterator.3
#+DESCRIPTION: Linux manpage for std_filesystem_path_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::filesystem::path::iterator - An iterator for the components of a
path.

* SYNOPSIS
\\

=#include <fs_path.h>=

** Public Types
using *difference_type* = std::ptrdiff_t\\

using *iterator_category* = *std::bidirectional_iterator_tag*\\

using *pointer* = const *path* *\\

using *reference* = const *path* &\\

using *value_type* = *path*\\

** Public Member Functions
*iterator* (const *iterator* &)=default\\

*reference* *operator** () const\\

*iterator* & *operator++* ()\\

*iterator* *operator++* (int)\\

*iterator* & *operator--* ()\\

*iterator* *operator--* (int)\\

*pointer* *operator->* () const\\

*iterator* & *operator=* (const *iterator* &)=default\\

** Friends
void *__path_iter_advance* (*iterator* &__i, difference_type __n)\\

difference_type *__path_iter_distance* (const *iterator* &__first, const
*iterator* &__last)\\

bool *operator!=* (const *iterator* &__lhs, const *iterator* &__rhs)\\

bool *operator==* (const *iterator* &__lhs, const *iterator* &__rhs)\\

class *path*\\

* Detailed Description
An iterator for the components of a path.

Definition at line *869* of file *bits/fs_path.h*.

* Member Typedef Documentation
** using std::filesystem::path::iterator::difference_type =
std::ptrdiff_t
Definition at line *872* of file *bits/fs_path.h*.

** using *std::filesystem::path::iterator::iterator_category* =
*std::bidirectional_iterator_tag*
Definition at line *876* of file *bits/fs_path.h*.

** using *std::filesystem::path::iterator::pointer* = const *path**
Definition at line *875* of file *bits/fs_path.h*.

** using *std::filesystem::path::iterator::reference* = const *path*&
Definition at line *874* of file *bits/fs_path.h*.

** using *std::filesystem::path::iterator::value_type* = *path*
Definition at line *873* of file *bits/fs_path.h*.

* Constructor & Destructor Documentation
** std::filesystem::path::iterator::iterator ()= [inline]=
Definition at line *878* of file *bits/fs_path.h*.

* Member Function Documentation
** *iterator* std::filesystem::path::iterator::operator++
(int)= [inline]=
Definition at line *887* of file *bits/fs_path.h*.

** *iterator* std::filesystem::path::iterator::operator--
(int)= [inline]=
Definition at line *890* of file *bits/fs_path.h*.

** *pointer* std::filesystem::path::iterator::operator-> ()
const= [inline]=
Definition at line *884* of file *bits/fs_path.h*.

* Friends And Related Function Documentation
** void __path_iter_advance (*iterator* & __i, difference_type
__n)= [friend]=
Definition at line *917* of file *bits/fs_path.h*.

** difference_type __path_iter_distance (const *iterator* & __first,
const *iterator* & __last)= [friend]=
Definition at line *904* of file *bits/fs_path.h*.

** bool operator!= (const *iterator* & __lhs, const *iterator* &
__rhs)= [friend]=
Definition at line *895* of file *bits/fs_path.h*.

** bool operator== (const *iterator* & __lhs, const *iterator* &
__rhs)= [friend]=
Definition at line *892* of file *bits/fs_path.h*.

** friend class *path*= [friend]=
Definition at line *899* of file *bits/fs_path.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
