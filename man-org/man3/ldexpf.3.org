#+TITLE: Manpages - ldexpf.3
#+DESCRIPTION: Linux manpage for ldexpf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ldexpf.3 is found in manpage for: [[../man3/ldexp.3][man3/ldexp.3]]