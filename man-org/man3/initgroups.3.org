#+TITLE: Manpages - initgroups.3
#+DESCRIPTION: Linux manpage for initgroups.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
initgroups - initialize the supplementary group access list

* SYNOPSIS
#+begin_example
  #include <sys/types.h>
  #include <grp.h>

  int initgroups(const char *user, gid_t group);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*initgroups*():

#+begin_example
      Since glibc 2.19:
          _DEFAULT_SOURCE
      Glibc 2.19 and earlier:
          _BSD_SOURCE
#+end_example

* DESCRIPTION
The *initgroups*() function initializes the group access list by reading
the group database //etc/group/ and using all groups of which /user/ is
a member. The additional group /group/ is also added to the list.

The /user/ argument must be non-NULL.

* RETURN VALUE
The *initgroups*() function returns 0 on success. On error, -1 is
returned, and /errno/ is set to indicate the error.

* ERRORS
- *ENOMEM* :: Insufficient memory to allocate group information
  structure.

- *EPERM* :: The calling process has insufficient privilege. See the
  underlying system call *setgroups*(2).

* FILES
- //etc/group/ :: group database file

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface      | Attribute     | Value          |
| *initgroups*() | Thread safety | MT-Safe locale |

* CONFORMING TO
SVr4, 4.3BSD.

* SEE ALSO
*getgroups*(2), *setgroups*(2), *credentials*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
