#+TITLE: Manpages - Xutf8TextPerCharExtents.3
#+DESCRIPTION: Linux manpage for Xutf8TextPerCharExtents.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about Xutf8TextPerCharExtents.3 is found in manpage for: [[../man3/XmbTextPerCharExtents.3][man3/XmbTextPerCharExtents.3]]