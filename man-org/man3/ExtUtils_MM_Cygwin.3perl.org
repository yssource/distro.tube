#+TITLE: Manpages - ExtUtils_MM_Cygwin.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MM_Cygwin.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MM_Cygwin - methods to override UN*X behaviour in
ExtUtils::MakeMaker

* SYNOPSIS
use ExtUtils::MM_Cygwin; # Done internally by ExtUtils::MakeMaker if
needed

* DESCRIPTION
See ExtUtils::MM_Unix for a documentation of the methods provided there.

- os_flavor :: We're Unix and Cygwin.

- cflags :: if configured for dynamic loading, triggers #define EXT in
  EXTERN.h

- replace_manpage_separator :: replaces strings '::' with '.' in MAN*POD
  man page names

- init_linker :: points to libperl.a

- maybe_command :: Determine whether a file is native to Cygwin by
  checking whether it resides inside the Cygwin installation (using
  Windows paths). If so, use ExtUtils::MM_Unix to determine if it may be
  a command. Otherwise use the tests from ExtUtils::MM_Win32.

- dynamic_lib :: Use the default to produce the *.dll's. But for new
  archdir dll's use the same rebase address if the old exists.

- install :: Rebase dll's with the global rebase database after
  installation.
