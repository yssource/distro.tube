#+TITLE: Manpages - XPoint.3
#+DESCRIPTION: Linux manpage for XPoint.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XPoint.3 is found in manpage for: [[../man3/XDrawPoint.3][man3/XDrawPoint.3]]