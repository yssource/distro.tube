#+TITLE: Manpages - strncpy.3
#+DESCRIPTION: Linux manpage for strncpy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about strncpy.3 is found in manpage for: [[../man3/strcpy.3][man3/strcpy.3]]