#+TITLE: Manpages - CURLOPT_SSL_ENABLE_NPN.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSL_ENABLE_NPN.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSL_ENABLE_NPN - use NPN

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_ENABLE_NPN, long
npn);

* DESCRIPTION
Pass a long as parameter, 0 or 1 where 1 is for enable and 0 for
disable. This option enables/disables NPN in the SSL handshake (if the
SSL backend libcurl is built to use supports it), which can be used to
negotiate http2.

* DEFAULT
1, enabled

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_SSL_ENABLE_NPN, 1L);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.36.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_SSL_ENABLE_ALPN*(3), *CURLOPT_SSL_OPTIONS*(3),
