#+TITLE: Manpages - ne_sock_exit.3
#+DESCRIPTION: Linux manpage for ne_sock_exit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ne_sock_exit.3 is found in manpage for: [[../ne_sock_init.3][ne_sock_init.3]]