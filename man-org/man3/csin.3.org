#+TITLE: Manpages - csin.3
#+DESCRIPTION: Linux manpage for csin.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
csin, csinf, csinl - complex sine function

* SYNOPSIS
#+begin_example
  #include <complex.h>

  double complex csin(double complex z);
  float complex csinf(float complex z);
  long double complex csinl(long double complex z);

  Link with -lm.
#+end_example

* DESCRIPTION
These functions calculate the complex sine of /z/.

The complex sine function is defined as:

#+begin_example
      csin(z) = (exp(i * z) - exp(-i * z)) / (2 * i)
#+end_example

* VERSIONS
These functions first appeared in glibc in version 2.1.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                      | Attribute     | Value   |
| *csin*(), *csinf*(), *csinl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

* SEE ALSO
*cabs*(3), *casin*(3), *ccos*(3), *ctan*(3), *complex*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
