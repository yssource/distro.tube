#+TITLE: Manpages - rtcReleaseDevice.3embree3
#+DESCRIPTION: Linux manpage for rtcReleaseDevice.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcReleaseDevice - decrements the device reference count
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcReleaseDevice(RTCDevice device);
#+end_example

** DESCRIPTION
Device objects are reference counted. The =rtcReleaseDevice= function
decrements the reference count of the passed device object (=device=
argument). When the reference count falls to 0, the device gets
destroyed.

All objects created from the device (like scenes, geometries, etc.) hold
a reference to the device, thus the device will not get destroyed unless
these objects are destroyed first.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewDevice], [rtcRetainDevice]
