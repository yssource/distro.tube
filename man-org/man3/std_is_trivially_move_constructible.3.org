#+TITLE: Manpages - std_is_trivially_move_constructible.3
#+DESCRIPTION: Linux manpage for std_is_trivially_move_constructible.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_trivially_move_constructible< _Tp > -
is_trivially_move_constructible

* SYNOPSIS
\\

Inherits std::__is_trivially_move_constructible_impl< _Tp, bool >.

* Detailed Description
** "template<typename _Tp>
\\
struct std::is_trivially_move_constructible< _Tp
>"is_trivially_move_constructible

Definition at line *1227* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
