#+TITLE: Manpages - XF86VidModeSwitchMode.3
#+DESCRIPTION: Linux manpage for XF86VidModeSwitchMode.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XF86VidModeSwitchMode.3 is found in manpage for: [[../man3/XF86VM.3][man3/XF86VM.3]]