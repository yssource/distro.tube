#+TITLE: Manpages - archive_read_new.3
#+DESCRIPTION: Linux manpage for archive_read_new.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

Allocates and initializes a

object suitable for reading from an archive.

is returned on error.

A complete description of the

object can be found in the overview manual page for
