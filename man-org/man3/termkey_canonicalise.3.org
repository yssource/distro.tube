#+TITLE: Manpages - termkey_canonicalise.3
#+DESCRIPTION: Linux manpage for termkey_canonicalise.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_canonicalise - canonicalise a key event

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  void termkey_canonicalise(TermKey *tk, TermKeyKey *key);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_canonicalise*(3) modifies the key event structure given by
/key/ according to the canonicalisation flags set on the given *termkey*
instance. This operation is performed implicitly by *termkey_getkey*(3),
*termkey_waitkey*(3) and *termkey_strpkey*(3), and is also provided
explicitly by this function.

See *termkey*(7) for a list of canonicalisation flags.

* RETURN VALUE
*termkey_canonicalise*() returns no value.

* SEE ALSO
*termkey_set_canonflags*(3), *termkey_waitkey*(3), *termkey_strpkey*(3),
*termkey*(7)
