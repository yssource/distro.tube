#+TITLE: Manpages - XSync.3
#+DESCRIPTION: Linux manpage for XSync.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSync.3 is found in manpage for: [[../man3/XFlush.3][man3/XFlush.3]]