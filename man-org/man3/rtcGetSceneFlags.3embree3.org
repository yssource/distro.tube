#+TITLE: Manpages - rtcGetSceneFlags.3embree3
#+DESCRIPTION: Linux manpage for rtcGetSceneFlags.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcGetSceneFlags - returns the flags of the scene
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  enum RTCSceneFlags rtcGetSceneFlags(RTCScene scene);
#+end_example

** DESCRIPTION
Queries the flags of a scene. This function can be useful when setting
individual flags, e.g. to just set the robust mode without changing
other flags the following way:

#+begin_example
  RTCSceneFlags flags = rtcGetSceneFlags(scene);
  rtcSetSceneFlags(scene, RTC_SCENE_FLAG_ROBUST | flags);
#+end_example

** EXIT STATUS
On failure =RTC_SCENE_FLAG_NONE= is returned and an error code is set
that can be queried using =rtcGetDeviceError=.

** SEE ALSO
[rtcSetSceneFlags]
