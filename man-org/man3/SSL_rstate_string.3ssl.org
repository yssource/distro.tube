#+TITLE: Manpages - SSL_rstate_string.3ssl
#+DESCRIPTION: Linux manpage for SSL_rstate_string.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_rstate_string, SSL_rstate_string_long - get textual description of
state of an SSL object during read operation

* SYNOPSIS
#include <openssl/ssl.h> const char *SSL_rstate_string(SSL *ssl); const
char *SSL_rstate_string_long(SSL *ssl);

* DESCRIPTION
*SSL_rstate_string()* returns a 2 letter string indicating the current
read state of the SSL object *ssl*.

*SSL_rstate_string_long()* returns a string indicating the current read
state of the SSL object *ssl*.

* NOTES
When performing a read operation, the SSL/TLS engine must parse the
record, consisting of header and body. When working in a blocking
environment, SSL_rstate_string[_long]() should always return RD/read
done.

This function should only seldom be needed in applications.

* RETURN VALUES
*SSL_rstate_string()* and *SSL_rstate_string_long()* can return the
following values:

- "RH"/"read header" :: The header of the record is being evaluated.

- "RB"/"read body" :: The body of the record is being evaluated.

- "RD"/"read done" :: The record has been completely processed.

- "unknown"/"unknown" :: The read state is unknown. This should never
  happen.

* SEE ALSO
*ssl* (7)

* COPYRIGHT
Copyright 2001-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
