#+TITLE: Manpages - sd_event_source_set_ratelimit.3
#+DESCRIPTION: Linux manpage for sd_event_source_set_ratelimit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_event_source_set_ratelimit, sd_event_source_get_ratelimit,
sd_event_source_is_ratelimited - Configure rate limiting on event
sources

* SYNOPSIS
#+begin_example
  #include <systemd/sd-event.h>
#+end_example

*int sd_event_source_set_ratelimit(sd_event_source **/source/*, uint64_t
*/interval_usec/*, unsigned */burst/*);*

*int sd_event_source_get_ratelimit(sd_event_source **/source/*,
uint64_t* */ret_interval_usec/*, unsigned* */ret_burst/*);*

*int sd_event_source_is_ratelimited(sd_event_source **/source/*);*

* DESCRIPTION
*sd_event_source_set_ratelimit()* may be used to enforce rate limiting
on an event source. When used an event source will be temporarily turned
off when it fires more often then a specified burst number within a
specified time interval. This is useful as simple mechanism to avoid
event source starvation if high priority event sources fire very
frequently.

Pass the event source to operate on as first argument, a time interval
in microseconds as second argument and a maximum dispatch limit
("burst") as third parameter. Whenever the event source is dispatched
more often than the specified burst within the specified interval it is
placed in a mode similar to being disabled with
*sd_event_source_set_enabled*(3) and the *SD_EVENT_OFF* parameter.
However it is disabled only temporarily -- once the specified interval
is over regular operation resumes. It is again disabled temporarily once
the specified rate limiting is hit the next time. If either the interval
or the burst value are specified as zero, rate limiting is turned off.
By default event sources do not have rate limiting enabled. Note that
rate limiting and disabling via *sd_event_source_set_enabled()* are
independent of each other, and an event source will only effect event
loop wake-ups and is dispatched while it both is enabled and rate
limiting is not in effect.

*sd_event_source_get_ratelimit()* may be used to query the current rate
limiting parameters set on the event source object /source/. The
previously set interval and burst vales are returned in the second and
third argument.

*sd_event_source_is_ratelimited()* may be used to query whether the
event source is currently affected by rate limiting, i.e. it has
recently hit the rate limit and is currently temporarily disabled due to
that.

Rate limiting is currently implemented for I/O, timer, signal, defer and
inotify event sources.

* RETURN VALUE
On success, *sd_event_source_set_ratelimit()* and
*sd_event_source_get_ratelimit()* return a non-negative integer. On
failure, they return a negative errno-style error code.
*sd_event_source_is_ratelimited* returns zero if rate limiting is
currently not in effect and greater than zero if it is in effect; it
returns a negative errno-style error code on failure.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  /source/ is not a valid pointer to an sd_event_source object.
#+end_quote

*-ECHILD*

#+begin_quote
  The event loop has been created in a different process.
#+end_quote

*-EDOM*

#+begin_quote
  It was attempted to use the rate limiting feature on an event source
  type that does not support rate limiting.
#+end_quote

*-ENOEXEC*

#+begin_quote
  *sd_event_source_get_ratelimit()* was called on an event source that
  doesnt have rate limiting configured.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*sd-event*(3), *sd_event_add_io*(3), *sd_event_add_time*(3),
*sd_event_add_signal*(3), *sd_event_add_inotify*(3),
*sd_event_add_defer*(3), *sd_event_source_set_enabled*(3)
