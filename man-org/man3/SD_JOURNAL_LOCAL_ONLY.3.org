#+TITLE: Manpages - SD_JOURNAL_LOCAL_ONLY.3
#+DESCRIPTION: Linux manpage for SD_JOURNAL_LOCAL_ONLY.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about SD_JOURNAL_LOCAL_ONLY.3 is found in manpage for: [[../sd_journal_open.3][sd_journal_open.3]]