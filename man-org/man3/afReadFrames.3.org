#+TITLE: Manpages - afReadFrames.3
#+DESCRIPTION: Linux manpage for afReadFrames.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
afReadFrames - read sample frames from a track in an audio file

* SYNOPSIS
#+begin_example
  #include <audiofile.h>
#+end_example

#+begin_example
  AFframecount afReadFrames(AFfilehandle file, int track, void *data,
      int count);
#+end_example

* DESCRIPTION
afReadFrames attempts to read up to /count/ frames of audio data from
the audio file handle /file/ into the buffer at /data/.

* PARAMETERS
/file/ is a valid file handle returned by *afOpenFile*(3).

/track/ is always AF_DEFAULT_TRACK for all currently supported file
formats.

/data/ is a buffer of storing /count/ frames of audio sample data.

/count/ is the number of sample frames to be read.

* RETURN VALUE
afReadFrames returns the number of frames successfully read from /file/.

* ERRORS
afReadFrames can produce these errors:

AF_BAD_FILEHANDLE

#+begin_quote
  the file handle was invalid
#+end_quote

AF_BAD_TRACKID

#+begin_quote
  the track parameter is not AF_DEFAULT_TRACK
#+end_quote

AF_BAD_READ

#+begin_quote
  reading audio data from the file failed
#+end_quote

AF_BAD_LSEEK

#+begin_quote
  seeking within the file failed
#+end_quote

* SEE ALSO
*afWriteFrames*(3)

* AUTHOR
Michael Pruett <michael@68k.org>
