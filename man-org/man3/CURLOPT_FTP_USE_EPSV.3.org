#+TITLE: Manpages - CURLOPT_FTP_USE_EPSV.3
#+DESCRIPTION: Linux manpage for CURLOPT_FTP_USE_EPSV.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_FTP_USE_EPSV - use EPSV for FTP

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_USE_EPSV, long
epsv);

* DESCRIPTION
Pass /epsv/ as a long. If the value is 1, it tells curl to use the EPSV
command when doing passive FTP downloads (which it does by default).
Using EPSV means that it will first attempt to use EPSV before using
PASV, but if you pass zero to this option, it will not try using EPSV,
only plain PASV.

If the server is an IPv6 host, this option will have no effect as of
7.12.3.

* DEFAULT
1

* PROTOCOLS
FTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/old-server/file.txt");

    /* let's shut off this modern feature */
    curl_easy_setopt(curl, CURLOPT_FTP_USE_EPSV, 0L);

    ret = curl_easy_perform(curl);

    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Along with FTP

* RETURN VALUE
Returns CURLE_OK if FTP is supported, and CURLE_UNKNOWN_OPTION if not.

* SEE ALSO
*CURLOPT_FTP_USE_EPRT*(3), *CURLOPT_FTPPORT*(3),
