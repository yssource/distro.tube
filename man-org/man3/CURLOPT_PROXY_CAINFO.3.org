#+TITLE: Manpages - CURLOPT_PROXY_CAINFO.3
#+DESCRIPTION: Linux manpage for CURLOPT_PROXY_CAINFO.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_PROXY_CAINFO - path to proxy Certificate Authority (CA) bundle

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_CAINFO, char
*path);

* DESCRIPTION
This option is for connecting to an HTTPS proxy, not an HTTPS server.

Pass a char * to a null-terminated string naming a file holding one or
more certificates to verify the HTTPS proxy with.

If /CURLOPT_PROXY_SSL_VERIFYPEER(3)/ is zero and you avoid verifying the
server's certificate, /CURLOPT_PROXY_CAINFO(3)/ need not even indicate
an accessible file.

This option is by default set to the system path where libcurl's cacert
bundle is assumed to be stored, as established at build time.

If curl is built against the NSS SSL library, the NSS PEM PKCS#11 module
(libnsspem.so) needs to be available for this option to work properly.

(iOS and macOS only) If curl is built against Secure Transport, then
this option is supported for backward compatibility with other SSL
engines, but it should not be set. If the option is not set, then curl
will use the certificates in the system and user Keychain to verify the
peer, which is the preferred method of verifying the peer's certificate
chain.

The application does not have to keep the string around after setting
this option.

* DEFAULT
Built-in system specific

* PROTOCOLS
Used with HTTPS proxy

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    /* using an HTTPS proxy */
    curl_easy_setopt(curl, CURLOPT_PROXY, "https://localhost:443");
    curl_easy_setopt(curl, CURLOPT_PROXY_CAINFO, "/etc/certs/cabundle.pem");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.52.0

For TLS backends that do not support certificate files, the
/CURLOPT_PROXY_CAINFO(3)/ option is ignored. Refer to
https://curl.se/docs/ssl-compared.html

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_PROXY_CAINFO_BLOB*(3), *CURLOPT_PROXY_CAPATH*(3),
*CURLOPT_PROXY_SSL_VERIFYPEER*(3), *CURLOPT_PROXY_SSL_VERIFYHOST*(3),
*CURLOPT_CAINFO*(3), *CURLOPT_CAINFO_BLOB*(3), *CURLOPT_CAPATH*(3),
*CURLOPT_SSL_VERIFYPEER*(3), *CURLOPT_SSL_VERIFYHOST*(3),
