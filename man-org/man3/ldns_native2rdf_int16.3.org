#+TITLE: Manpages - ldns_native2rdf_int16.3
#+DESCRIPTION: Linux manpage for ldns_native2rdf_int16.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_native2rdf_int8, ldns_native2rdf_int16, ldns_native2rdf_int32,
ldns_native2rdf_int16_data, ldns_rdf2native_int8, ldns_rdf2native_int16,
ldns_rdf2native_int32, ldns_rdf2native_sockaddr_storage,
ldns_rdf2native_time_t - rdf numeric conversion functions

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_rdf* ldns_native2rdf_int8(ldns_rdf_type type, uint8_t value);

ldns_rdf* ldns_native2rdf_int16(ldns_rdf_type type, uint16_t value);

ldns_rdf* ldns_native2rdf_int32(ldns_rdf_type type, uint32_t value);

ldns_rdf* ldns_native2rdf_int16_data(size_t size, uint8_t *data);

uint8_t ldns_rdf2native_int8(const ldns_rdf *rd);

uint16_t ldns_rdf2native_int16(const ldns_rdf *rd);

uint32_t ldns_rdf2native_int32(const ldns_rdf *rd);

ldns_rdf2native_sockaddr_storage();

time_t ldns_rdf2native_time_t(const ldns_rdf *rd);

* DESCRIPTION
/ldns_native2rdf_int8/() returns the rdf containing the native uint8_t
repr. .br *type*: the ldns_rdf type to use .br *value*: the uint8_t to
use .br Returns ldns_rdf* with the converted value

/ldns_native2rdf_int16/() returns the rdf containing the native uint16_t
representation. .br *type*: the ldns_rdf type to use .br *value*: the
uint16_t to use .br Returns ldns_rdf* with the converted value

/ldns_native2rdf_int32/() returns an rdf that contains the given int32
value.

Because multiple rdf types can contain an int32, the type must be
specified .br *type*: the ldns_rdf type to use .br *value*: the uint32_t
to use .br Returns ldns_rdf* with the converted value

/ldns_native2rdf_int16_data/() returns an int16_data rdf that contains
the data in the given array, preceded by an int16 specifying the length.

The memory is copied, and an LDNS_RDF_TYPE_INT16DATA is returned .br
*size*: the size of the data .br **data*: pointer to the actual data

.br Returns ldns_rd* the rdf with the data

/ldns_rdf2native_int8/() returns the native uint8_t representation from
the rdf. .br *rd*: the ldns_rdf to operate on .br Returns uint8_t the
value extracted

/ldns_rdf2native_int16/() returns the native uint16_t representation
from the rdf. .br *rd*: the ldns_rdf to operate on .br Returns uint16_t
the value extracted

/ldns_rdf2native_int32/() returns the native uint32_t representation
from the rdf. .br *rd*: the ldns_rdf to operate on .br Returns uint32_t
the value extracted

/ldns_rdf2native_sockaddr_storage/()

/ldns_rdf2native_time_t/() returns the native time_t representation from
the rdf. .br *rd*: the ldns_rdf to operate on .br Returns time_t the
value extracted (32 bits currently)

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rdf/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
