#+TITLE: Manpages - acl_get_qualifier.3
#+DESCRIPTION: Linux manpage for acl_get_qualifier.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function retrieves the qualifier from the ACL entry indicated by the
argument

into working storage and returns a pointer to that storage.

If the value of the tag type in the ACL entry referred to by

is ACL_USER, then the value returned by

is a pointer to type

If the value of the tag type in the ACL entry referred to by

is ACL_GROUP, then the value returned by

is a pointer to type

If the tag type in the ACL entry referred to by

is a tag type for which a qualifier is not supported,

returns a value of

and the function fails. Subsequent operations using the returned pointer
operate on an independent copy of the qualifier in working storage, and
will not change the qualifier of the ACL entry.

This function may cause memory to be allocated. The caller should free
any releasable memory, when the new qualifier is no longer required, by
calling

with the

value returned by

as an argument.

The argument

and any other ACL entry descriptors that refer to entries within the ACL
containing the entry referred to by

continue to refer to those entries. The order of all existing entries in
the ACL containing the entry referred to by

remains unchanged.

On success, the function returns a pointer to the tag qualifier that was
retrieved into ACL working storage. On error, a value of

is returned and

is set appropriately.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid descriptor for an ACL entry.

The value of the tag type in the ACL entry referenced by the argument

is neither ACL_USER nor ACL_GROUP.

The value to be returned requires more memory than is allowed by the
hardware or system-imposed memory management constraints.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
