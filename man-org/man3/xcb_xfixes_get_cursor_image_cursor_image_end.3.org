#+TITLE: Manpages - xcb_xfixes_get_cursor_image_cursor_image_end.3
#+DESCRIPTION: Linux manpage for xcb_xfixes_get_cursor_image_cursor_image_end.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_xfixes_get_cursor_image_cursor_image_end.3 is found in manpage for: [[../man3/xcb_xfixes_get_cursor_image.3][man3/xcb_xfixes_get_cursor_image.3]]