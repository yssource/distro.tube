#+TITLE: Manpages - XkbGetNamedGeometry.3
#+DESCRIPTION: Linux manpage for XkbGetNamedGeometry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbGetNamedGeometry - Loads a keyboard geometry description from this
database by name

* SYNOPSIS
*Status XkbGetNamedGeometry* *( Display **/dpy/* ,* *XkbDescPtr
*/xkb/* ,* *Atom */name/* );*

* ARGUMENTS
- /- dpy/ :: connection to the X server

- /- xkb/ :: keyboard description into which the geometry should be
  loaded

- /- name/ :: name of the geometry to be loaded

* DESCRIPTION
It is also possible to load a keyboard geometry by name. The X server
maintains a database of keyboard components (see below).

/XkbGetNamedGeometry/ can return BadName if the name cannot be found.

The X server maintains a database of keyboard components, identified by
component type. The database contains all the information necessary to
build a complete keyboard description for a particular device, as well
as to assemble partial descriptions. Table 1 identifies the component
types and the type of information they contain.

TABLE

While a keymap is a database entry for a complete keyboard description,
and therefore logically different from the individual component database
entries, the rules for processing keymap entries are identical to those
for the individual components. In the discussion that follows, the term
component is used to refer to either individual components or a keymap.

There may be multiple entries for each of the component types. An entry
may be either /complete/ or /partial./ Partial entries describe only a
piece of the corresponding keyboard component and are designed to be
combined with other entries of the same type to form a complete entry.

For example, a partial symbols map might describe the differences
between a common ASCII keyboard and some national layout. Such a partial
map is not useful on its own because it does not include those symbols
that are the same on both the ASCII and national layouts (such as
function keys). On the other hand, this partial map can be used to
configure /any/ ASCII keyboard to use a national layout.

When a keyboard description is built, the components are processed in
the order in which they appear in Table 1; later definitions override
earlier ones.

* DIAGNOSTICS
- *BadName* :: A font or color of the specified name does not exist.
