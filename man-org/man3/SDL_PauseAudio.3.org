#+TITLE: Manpages - SDL_PauseAudio.3
#+DESCRIPTION: Linux manpage for SDL_PauseAudio.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_PauseAudio - Pauses and unpauses the audio callback processing

* SYNOPSIS
*#include "SDL.h"*

*void SDL_PauseAudio*(*int pause_on*);

* DESCRIPTION
This function pauses and unpauses the audio callback processing. It
should be called with *pause_on*=0 after opening the audio device to
start playing sound. This is so you can safely initialize data for your
callback function after opening the audio device. Silence will be
written to the audio device during the pause.

* SEE ALSO
*SDL_GetAudioStatus*, *SDL_OpenAudio*
