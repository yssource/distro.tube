#+TITLE: Manpages - XShape.3
#+DESCRIPTION: Linux manpage for XShape.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XShapeQueryExtension, XShapeQueryVersion, XShapeCombineRegion,
XShapeCombineRectangles, XShapeCombineMask, XShapeCombineShape,
XShapeOffsetShape, XShapeQueryExtents, XShapeSelectInput,
XShapeInputSelected, XShapeGetRectangles - X nonrectangular shape
functions

* SYNTAX
#+begin_example
  #include <X11/extensions/shape.h>
  Bool XShapeQueryExtension (
  	Display *dpy,
  	int *event_basep,
  	int *error_basep);
  Status XShapeQueryVersion (
  	Display *dpy,
  	int *major_versionp,
  	int *minor_versionp);
  void XShapeCombineRegion (
  	Display *dpy,
  	Window dest,
  	int destKind,
  	int xOff,
  	int yOff,
  	struct _XRegion *r,
  	int op);
  void XShapeCombineRectangles (
  	Display *dpy,
  	XID dest,
  	int destKind,
  	int xOff,
  	int yOff,
  	XRectangle *rects,
  	int n_rects,
  	int op,
  	int ordering);
  void XShapeCombineMask (
  	Display *dpy,
  	XID dest,
  	int destKind,
  	int xOff,
  	int yOff,
  	Pixmap src,
  	int op);
  void XShapeCombineShape (
  	Display *dpy,
  	XID dest,
  	int destKind,
  	int xOff,
  	int yOff,
  	Pixmap src,
  	int srcKind,
  	int op);
  void XShapeOffsetShape (
  	Display *dpy,
  	XID dest,
  	int destKind,
  	int xOff,
  	int yOff);
  Status XShapeQueryExtents (
  	Display *dpy,
  	Window window,
  	int *bShaped,
  	int *xbs,
  	int *ybs,
  	unsigned int *wbs,
  	unsigned int *hbs,
  	int *cShaped,
  	int *xcs,
  	int *ycs,
  	unsigned int *wcs,
  	unsigned int *hcs);
  void XShapeSelectInput (
  	Display *dpy,
  	Window window,
  	unsigned longmask);
  unsigned long XShapeInputSelected (
  	Display *dpy,
  	Window window);
  XRectangle *XShapeGetRectangles (
  	Display *dpy,
  	Window window,
  	int kind,
  	int *count,
  	int *ordering);
#+end_example

* STRUCTURES
#+begin_example
  typedef struct {
      int type;	/* of event */
      unsigned long serial;	/* # of last request processed by server */
      Bool send_event;	/* true if this came from a SendEvent request */
      Display *display;	/* Display the event was read from */
      Window window;	/* window of event */
      int kind;	/* ShapeBounding or ShapeClip */
      int x, y;	/* extents of new region */
      unsigned width, height;
      Time time;	/* server timestamp when region changed */
      Bool shaped;	/* true if the region exists */
  } XShapeEvent;
#+end_example

* DESCRIPTION
The /X11 Nonrectangular Window Shape Extension/ adds nonrectangular
windows to the X Window System.

* PREDEFINED VALUES
Operations:

#+begin_quote
  #+begin_example
     ShapeSet 
     ShapeUnion 
     ShapeIntersect 
     ShapeSubtract 
     ShapeInvert 
  #+end_example
#+end_quote

Shape Kinds:

#+begin_quote
  #+begin_example
     ShapeBounding 
     ShapeClip 
  #+end_example
#+end_quote

Event defines:

#+begin_quote
  #+begin_example
     ShapeNotifyMask 
     ShapeNotify 
  #+end_example
#+end_quote

* BUGS
This manual page needs a lot more work.

* SEE ALSO
/X11 Nonrectangular Window Shape Extension/
