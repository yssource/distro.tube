#+TITLE: Manpages - MPI_Win_get_info.3
#+DESCRIPTION: Linux manpage for MPI_Win_get_info.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Win_get_info* - Retrieves active window info hints

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Win_get_info(MPI_Win win, MPI_Info *info_used)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_WIN_GET_INFO(WIN, INFO_USED, IERROR)
  	INTEGER	COMM, INFO_USED, IERROR 
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Win_get_info(win, info_used, ierror)
  	TYPE(MPI_Win), INTENT(IN) :: win
  	TYPE(MPI_Info), INTENT(OUT) :: info_used
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* INPUT PARAMETERS
- win :: Window from which to receive active info hints

* OUTPUT PARAMETERS
- info_used :: New info object returned with all active hints on this
  window.

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_Win_get_info returns a new info object containing the hints of the
window associated with /win/. The current setting of all hints actually
used by the system related to this window is returned in /info_used/. If
no such hints exist, a handle to a newly created info object is returned
that contains no key/value pair. The user is responsible for freeing
info_used via MPI_Info_free.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

* SEE ALSO
MPI_Win_set_info, MPI_Win_free
