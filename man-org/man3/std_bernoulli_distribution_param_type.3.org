#+TITLE: Manpages - std_bernoulli_distribution_param_type.3
#+DESCRIPTION: Linux manpage for std_bernoulli_distribution_param_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::bernoulli_distribution::param_type

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef *bernoulli_distribution* *distribution_type*\\

** Public Member Functions
*param_type* (double __p)\\

double *p* () const\\

** Friends
bool *operator!=* (const *param_type* &__p1, const *param_type* &__p2)\\

bool *operator==* (const *param_type* &__p1, const *param_type* &__p2)\\

* Detailed Description
Parameter type.

Definition at line *3530* of file *random.h*.

* Member Typedef Documentation
** typedef *bernoulli_distribution*
*std::bernoulli_distribution::param_type::distribution_type*
Definition at line *3532* of file *random.h*.

* Constructor & Destructor Documentation
** std::bernoulli_distribution::param_type::param_type ()= [inline]=
Definition at line *3534* of file *random.h*.

** std::bernoulli_distribution::param_type::param_type (double
__p)= [inline]=, = [explicit]=
Definition at line *3537* of file *random.h*.

* Member Function Documentation
** double std::bernoulli_distribution::param_type::p () const= [inline]=
Definition at line *3544* of file *random.h*.

* Friends And Related Function Documentation
** bool operator!= (const *param_type* & __p1, const *param_type* &
__p2)= [friend]=
Definition at line *3552* of file *random.h*.

** bool operator== (const *param_type* & __p1, const *param_type* &
__p2)= [friend]=
Definition at line *3548* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
