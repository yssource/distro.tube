#+TITLE: Manpages - FcCharSetIntersect.3
#+DESCRIPTION: Linux manpage for FcCharSetIntersect.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetIntersect - Intersect charsets

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcCharSet * FcCharSetIntersect (const FcCharSet */a/*, const FcCharSet
**/b/*);*

* DESCRIPTION
Returns a set including only those chars found in both /a/ and /b/.
