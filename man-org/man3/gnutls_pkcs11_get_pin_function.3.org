#+TITLE: Manpages - gnutls_pkcs11_get_pin_function.3
#+DESCRIPTION: Linux manpage for gnutls_pkcs11_get_pin_function.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_pkcs11_get_pin_function - API function

* SYNOPSIS
*#include <gnutls/pkcs11.h>*

*gnutls_pin_callback_t gnutls_pkcs11_get_pin_function(void **
*/userdata/*);*

* ARGUMENTS
- void ** userdata :: data to be supplied to callback

* DESCRIPTION
This function will return the callback function set using
*gnutls_pkcs11_set_pin_function()*.

* RETURNS
The function set or NULL otherwise.

* SINCE
3.1.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
