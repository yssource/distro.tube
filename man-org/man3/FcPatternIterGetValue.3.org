#+TITLE: Manpages - FcPatternIterGetValue.3
#+DESCRIPTION: Linux manpage for FcPatternIterGetValue.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternIterGetValue - Returns a value which the iterator point to

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcResult FcPatternIterGetValue (const FcPattern */p/*, FcPatternIter
**/iter/*, int*/id/*, FcValue **/v/*, FcValueBinding **/b/*);*

* DESCRIPTION
Returns in /v/ the /id/'th value which /iter/ point to. also binding to
/b/ if given. The value returned is not a copy, but rather refers to the
data stored within the pattern directly. Applications must not free this
value.

* SINCE
version 2.13.1
