#+TITLE: Manpages - sd-hwdb.3
#+DESCRIPTION: Linux manpage for sd-hwdb.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd-hwdb - Read-only access to the hardware description database

* SYNOPSIS
#+begin_example
  #include <systemd/sd-hwdb.h>
#+end_example

*pkg-config --cflags --libs libsystemd*

* DESCRIPTION
sd-hwdb.h allows read-only access the systemd database of hardware
properties. See *hwdb*(7) and *systemd-hwdb*(8) for more information
about the database.

See *sd_hwdb_new*(3) and *sd_hwdb_get*(3) for information about the
functions available.

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *systemd-udevd.service*(8)
