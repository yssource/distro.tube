#+TITLE: Manpages - std_allocator.3
#+DESCRIPTION: Linux manpage for std_allocator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::allocator< _Tp > - The /standard/ allocator, as per [20.4].

* SYNOPSIS
\\

=#include <allocator.h>=

Inherits __allocator_base< _Tp >.

** Public Types
typedef const _Tp * *const_pointer*\\

typedef const _Tp & *const_reference*\\

typedef ptrdiff_t *difference_type*\\

typedef *true_type* *is_always_equal*\\

typedef _Tp * *pointer*\\

typedef *true_type* *propagate_on_container_move_assignment*\\

typedef _Tp & *reference*\\

typedef size_t *size_type*\\

typedef _Tp *value_type*\\

** Public Member Functions
constexpr *allocator* (const *allocator* &__a) noexcept\\

template<typename _Tp1 > constexpr *allocator* (const *allocator*< _Tp1
> &) noexcept\\

*allocator* & *operator=* (const *allocator* &)=default\\

** Friends
constexpr friend bool *operator!=* (const *allocator* &, const
*allocator* &) noexcept\\

constexpr friend bool *operator==* (const *allocator* &, const
*allocator* &) noexcept\\

* Detailed Description
** "template<typename _Tp>
\\
class std::allocator< _Tp >"The /standard/ allocator, as per [20.4].

See
https://gcc.gnu.org/onlinedocs/libstdc++/manual/memory.html#std.util.memory.allocator
for further details.

*Template Parameters*

#+begin_quote
  /_Tp/ Type of allocated object.
#+end_quote

Definition at line *116* of file *allocator.h*.

* Member Typedef Documentation
** template<typename _Tp > typedef const _Tp* *std::allocator*< _Tp
>::const_pointer
Definition at line *124* of file *allocator.h*.

** template<typename _Tp > typedef const _Tp& *std::allocator*< _Tp
>::const_reference
Definition at line *126* of file *allocator.h*.

** template<typename _Tp > typedef ptrdiff_t *std::allocator*< _Tp
>::difference_type
Definition at line *121* of file *allocator.h*.

** template<typename _Tp > typedef *true_type* *std::allocator*< _Tp
>::*is_always_equal*
Definition at line *138* of file *allocator.h*.

** template<typename _Tp > typedef _Tp* *std::allocator*< _Tp >::pointer
Definition at line *123* of file *allocator.h*.

** template<typename _Tp > typedef *true_type* *std::allocator*< _Tp
>::*propagate_on_container_move_assignment*
Definition at line *136* of file *allocator.h*.

** template<typename _Tp > typedef _Tp& *std::allocator*< _Tp
>::reference
Definition at line *125* of file *allocator.h*.

** template<typename _Tp > typedef size_t *std::allocator*< _Tp
>::size_type
Definition at line *120* of file *allocator.h*.

** template<typename _Tp > typedef _Tp *std::allocator*< _Tp
>::value_type
Definition at line *119* of file *allocator.h*.

* Constructor & Destructor Documentation
** template<typename _Tp > constexpr *std::allocator*< _Tp
>::*allocator* ()= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *144* of file *allocator.h*.

** template<typename _Tp > constexpr *std::allocator*< _Tp
>::*allocator* (const *allocator*< _Tp > & __a)= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *147* of file *allocator.h*.

** template<typename _Tp > template<typename _Tp1 > constexpr
*std::allocator*< _Tp >::*allocator* (const *allocator*< _Tp1 >
&)= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *157* of file *allocator.h*.

** template<typename _Tp > *std::allocator*< _Tp >::~*allocator*
()= [inline]=, = [noexcept]=
Definition at line *162* of file *allocator.h*.

* Friends And Related Function Documentation
** template<typename _Tp > constexpr friend bool operator!= (const
*allocator*< _Tp > &, const *allocator*< _Tp > &)= [friend]=
Definition at line *197* of file *allocator.h*.

** template<typename _Tp > constexpr friend bool operator== (const
*allocator*< _Tp > &, const *allocator*< _Tp > &)= [friend]=
Definition at line *192* of file *allocator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
