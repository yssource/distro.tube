#+TITLE: Manpages - rpc_gss_set_defaults.3t
#+DESCRIPTION: Linux manpage for rpc_gss_set_defaults.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Set the service and quality of protection to be used for RPC requests.
The new values apply for the rest of the lifetime of the context (unless
changed again with this function).

The service type to use for subsequent RPC requests

The quality of protection to use or NULL for the default

Returns

if the values were set

The

function is part of libtirpc.

This manual page was written by
