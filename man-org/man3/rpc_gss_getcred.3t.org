#+TITLE: Manpages - rpc_gss_getcred.3t
#+DESCRIPTION: Linux manpage for rpc_gss_getcred.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
This function returns the RPCSEC_GSS authenticated credentials
associated with an RPC request.

The RPC request to query

If non-null,

is set to point at the raw credentials for this request

If non-null,

is set to point at the corresponding unix credentials

If non-null,

is set to the cookie value returned by a callback function registered
with

Returns

if successful,

otherwise.

The

function is part of libtirpc.

This manual page was written by
