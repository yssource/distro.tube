#+TITLE: Manpages - pcre2_general_context_create.3
#+DESCRIPTION: Linux manpage for pcre2_general_context_create.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  pcre2_general_context *pcre2_general_context_create(
   void *(*private_malloc)(PCRE2_SIZE, void *),
   void (*private_free)(void *, void *), void *memory_data);
#+end_example

* DESCRIPTION
This function creates and initializes a general context. The arguments
define custom memory management functions and a data value that is
passed to them when they are called. The *private_malloc()* function is
used to get memory for the context. If either of the first two arguments
is NULL, the system memory management function is used. The result is
NULL if no memory could be obtained.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
