#+TITLE: Manpages - std_front_insert_iterator.3
#+DESCRIPTION: Linux manpage for std_front_insert_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::front_insert_iterator< _Container > - Turns assignment into
insertion.

* SYNOPSIS
\\

=#include <stl_iterator.h>=

Inherits *std::iterator< output_iterator_tag, void, void, void, void >*.

** Public Types
typedef _Container *container_type*\\
A nested typedef for the type of whatever container you used.

typedef void *difference_type*\\
Distance between iterators is represented as this type.

typedef *output_iterator_tag* *iterator_category*\\
One of the *tag types*.

typedef void *pointer*\\
This type represents a pointer-to-value_type.

typedef void *reference*\\
This type represents a reference-to-value_type.

typedef void *value_type*\\
The type 'pointed to' by the iterator.

** Public Member Functions
constexpr *front_insert_iterator* (_Container &__x)\\
The only way to create this iterator is with a container.

constexpr *front_insert_iterator* & *operator** ()\\
Simply returns *this.

constexpr *front_insert_iterator* & *operator++* ()\\
Simply returns *this. (This iterator does not /move/.)

constexpr *front_insert_iterator* *operator++* (int)\\
Simply returns *this. (This iterator does not /move/.)

constexpr *front_insert_iterator* & *operator=* (const typename
_Container::value_type &__value)\\

constexpr *front_insert_iterator* & *operator=* (typename
_Container::value_type &&__value)\\

** Protected Attributes
_Container * *container*\\

* Detailed Description
** "template<typename _Container>
\\
class std::front_insert_iterator< _Container >"Turns assignment into
insertion.

These are output iterators, constructed from a container-of-T. Assigning
a T to the iterator prepends it to the container using push_front.

Tip: Using the front_inserter function to create these iterators can
save typing.

Definition at line *731* of file *bits/stl_iterator.h*.

* Member Typedef Documentation
** template<typename _Container > typedef _Container
*std::front_insert_iterator*< _Container >::*container_type*
A nested typedef for the type of whatever container you used.

Definition at line *739* of file *bits/stl_iterator.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*difference_type*= [inherited]=
Distance between iterators is represented as this type.

Definition at line *134* of file *stl_iterator_base_types.h*.

** typedef *output_iterator_tag* *std::iterator*< *output_iterator_tag*
, void , void , void , void >::*iterator_category*= [inherited]=
One of the *tag types*.

Definition at line *130* of file *stl_iterator_base_types.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*pointer*= [inherited]=
This type represents a pointer-to-value_type.

Definition at line *136* of file *stl_iterator_base_types.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*reference*= [inherited]=
This type represents a reference-to-value_type.

Definition at line *138* of file *stl_iterator_base_types.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*value_type*= [inherited]=
The type 'pointed to' by the iterator.

Definition at line *132* of file *stl_iterator_base_types.h*.

* Constructor & Destructor Documentation
** template<typename _Container > constexpr
*std::front_insert_iterator*< _Container >::*front_insert_iterator*
(_Container & __x)= [inline]=, = [explicit]=, = [constexpr]=
The only way to create this iterator is with a container.

Definition at line *748* of file *bits/stl_iterator.h*.

* Member Function Documentation
** template<typename _Container > constexpr *front_insert_iterator* &
*std::front_insert_iterator*< _Container >::operator* ()= [inline]=,
= [constexpr]=
Simply returns *this.

Definition at line *790* of file *bits/stl_iterator.h*.

** template<typename _Container > constexpr *front_insert_iterator* &
*std::front_insert_iterator*< _Container >::operator++ ()= [inline]=,
= [constexpr]=
Simply returns *this. (This iterator does not /move/.)

Definition at line *796* of file *bits/stl_iterator.h*.

** template<typename _Container > constexpr *front_insert_iterator*
*std::front_insert_iterator*< _Container >::operator++ (int)= [inline]=,
= [constexpr]=
Simply returns *this. (This iterator does not /move/.)

Definition at line *802* of file *bits/stl_iterator.h*.

** template<typename _Container > constexpr *front_insert_iterator* &
*std::front_insert_iterator*< _Container >::operator= (const typename
_Container::value_type & __value)= [inline]=, = [constexpr]=
*Parameters*

#+begin_quote
  /__value/ An instance of whatever type container_type::const_reference
  is; presumably a reference-to-const T for container<T>.
#+end_quote

*Returns*

#+begin_quote
  This iterator, for chained operations.
#+end_quote

This kind of iterator doesn't really have a /position/ in the container
(you can think of the position as being permanently at the front, if you
like). Assigning a value to the iterator will always prepend the value
to the front of the container.

Definition at line *772* of file *bits/stl_iterator.h*.

** template<typename _Container > constexpr *front_insert_iterator* &
*std::front_insert_iterator*< _Container >::operator= (typename
_Container::value_type && __value)= [inline]=, = [constexpr]=
Definition at line *780* of file *bits/stl_iterator.h*.

* Member Data Documentation
** template<typename _Container > _Container*
*std::front_insert_iterator*< _Container >::container= [protected]=
Definition at line *735* of file *bits/stl_iterator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
