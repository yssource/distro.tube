#+TITLE: Manpages - gnutls_priority_certificate_type_list2.3
#+DESCRIPTION: Linux manpage for gnutls_priority_certificate_type_list2.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_priority_certificate_type_list2 - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int gnutls_priority_certificate_type_list2(gnutls_priority_t
*/pcache/*, const unsigned int ** */list/*, gnutls_ctype_target_t
*/target/*);*

* ARGUMENTS
- gnutls_priority_t pcache :: is a *gnutls_priority_t* type.

- const unsigned int ** list :: will point to an integer list.

- gnutls_ctype_target_t target :: is a *gnutls_ctype_target_t* type.
  Valid arguments are GNUTLS_CTYPE_CLIENT and GNUTLS_CTYPE_SERVER

* DESCRIPTION
Get a list of available certificate types for the given target in the
priority structure.

* RETURNS
the number of certificate types, or an error code.

* SINCE
3.6.4

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
