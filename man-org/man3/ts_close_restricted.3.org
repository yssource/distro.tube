#+TITLE: Manpages - ts_close_restricted.3
#+DESCRIPTION: Linux manpage for ts_close_restricted.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_close_restricted - use a custom function for closing the
touchscreen's input device file

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  "void(*ts_close_restricted)(intfd,void*user_data);
#+end_example

* DESCRIPTION
*ts_close_restricted*() is useful if libts should not be run as root. If
such a function is implemented by the user and assigned to the
ts_close_restricted pointer, it will be called by ts_close() instead of
the close() system call directly.

It should obviously close the device with file descriptor *fd* while
*user_data* is currently unused.

* SEE ALSO
*ts_open_restricted*(3), *ts_open*(3), *ts_setup*(3), *ts_close*(3),
*ts.conf*(5)
