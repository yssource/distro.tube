#+TITLE: Manpages - sd_bus_message_append_strv.3
#+DESCRIPTION: Linux manpage for sd_bus_message_append_strv.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_message_append_strv - Attach an array of strings to a message

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_message_append_strv(sd_bus_message **/m/*, char ***/l/*);*

* DESCRIPTION
The *sd_bus_message_append()* function can be used to append an array of
strings to message /m/. The parameter /l/ shall point to a
*NULL*-terminated array of pointers to *NUL*-terminated strings. Each
string must satisfy the same constraints as described for the "s" type
in *sd_bus_message_append_basic*(3).

The memory pointed at by /p/ and the contents of the strings themselves
are copied into the memory area containing the message and may be
changed after this call. Note that the signature of /l/ parameter is to
be treated as *const char *const **, and the contents will not be
modified.

* RETURN VALUE
On success, this call returns 0 or a positive integer. On failure, a
negative errno-style error code is returned.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  Specified parameter is invalid.
#+end_quote

*-EPERM*

#+begin_quote
  Message has been sealed.
#+end_quote

*-ESTALE*

#+begin_quote
  Message is in invalid state.
#+end_quote

*-ENXIO*

#+begin_quote
  Message cannot be appended to.
#+end_quote

*-ENOMEM*

#+begin_quote
  Memory allocation failed.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_message_append*(3),
*sd_bus_message_append_array*(3), *The D-Bus specification*[1]

* NOTES
-  1. :: The D-Bus specification

  http://dbus.freedesktop.org/doc/dbus-specification.html
