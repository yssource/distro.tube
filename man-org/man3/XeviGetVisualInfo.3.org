#+TITLE: Manpages - XeviGetVisualInfo.3
#+DESCRIPTION: Linux manpage for XeviGetVisualInfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XeviGetVisualInfo.3 is found in manpage for: [[../man3/Xevi.3][man3/Xevi.3]]