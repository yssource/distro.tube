#+TITLE: Manpages - afSetVirtualPCMMapping.3
#+DESCRIPTION: Linux manpage for afSetVirtualPCMMapping.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about afSetVirtualPCMMapping.3 is found in manpage for: [[../afSetVirtualByteOrder.3][afSetVirtualByteOrder.3]]