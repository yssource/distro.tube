#+TITLE: Manpages - XSetWindowAttributes.3
#+DESCRIPTION: Linux manpage for XSetWindowAttributes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSetWindowAttributes.3 is found in manpage for: [[../man3/XCreateWindow.3][man3/XCreateWindow.3]]