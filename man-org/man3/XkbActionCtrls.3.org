#+TITLE: Manpages - XkbActionCtrls.3
#+DESCRIPTION: Linux manpage for XkbActionCtrls.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbActionCtrls - Returns the ctrls fields of act converted to an
unsigned int

* SYNOPSIS
*unsigned int XkbActionCtrls* *( XkbCtrlsAction */act/* );*

* ARGUMENTS
- /- act/ :: action from which to extract controls

* DESCRIPTION
Actions associated with the XkbCtrlsAction structure change the state of
the boolean controls.

The /type/ field can have any one of the values shown in Table 1.

TABLE

The /flags/ field is composed of the bitwise inclusive OR of the masks
shown in Table 2.

TABLE

The XkbSA_SetControls action implements a key that enables a boolean
control when pressed and disables it when released. The
XkbSA_LockControls action is used to implement a key that toggles the
state of a boolean control each time it is pressed and released. The
XkbSA_LockNoLock and XkbSA_LockNoUnlock flags allow modifying the
toggling behavior to only unlock or only lock the boolean control.

The /ctrls0, ctrls1, ctrls2,/ and /ctrls3/ fields represent the boolean
controls in the /enabled_ctrls/ field of the controls structure. Xkb
macros, to convert between the two formats.

/XkbActionCtrls/ returns the /ctrls/ fields of /act/ converted to an
unsigned int.

* STRUCTURES
#+begin_example

      typedef struct _XkbCtrlsAction {
          unsigned char    type;    /* XkbSA_SetControls, XkbSA_LockControls */
          unsigned char    flags;   /* with type, controls enabling and disabling 
  of controls */
          unsigned char    ctrls3;  /* ctrls0 through ctrls3 represent the boolean 
  controls */
          unsigned char    ctrls2;  /* ctrls0 through ctrls3 represent the boolean 
  controls */
          unsigned char    ctrls1;  /* ctrls0 through ctrls3 represent the boolean 
  controls */
          unsigned char    ctrls0;  /* ctrls0 through ctrls3 represent the boolean 
  controls */
      } XkbCtrlsAction;
#+end_example
