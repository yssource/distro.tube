#+TITLE: Manpages - RSA_new.3ssl
#+DESCRIPTION: Linux manpage for RSA_new.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
RSA_new, RSA_free - allocate and free RSA objects

* SYNOPSIS
#include <openssl/rsa.h> RSA *RSA_new(void); void RSA_free(RSA *rsa);

* DESCRIPTION
*RSA_new()* allocates and initializes an *RSA* structure. It is
equivalent to calling RSA_new_method(NULL).

*RSA_free()* frees the *RSA* structure and its components. The key is
erased before the memory is returned to the system. If *rsa* is NULL
nothing is done.

* RETURN VALUES
If the allocation fails, *RSA_new()* returns *NULL* and sets an error
code that can be obtained by *ERR_get_error* (3). Otherwise it returns a
pointer to the newly allocated structure.

*RSA_free()* returns no value.

* SEE ALSO
*ERR_get_error* (3), *RSA_generate_key* (3), *RSA_new_method* (3)

* COPYRIGHT
Copyright 2000-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
