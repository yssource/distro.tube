#+TITLE: Manpages - libssh2_channel_wait_closed.3
#+DESCRIPTION: Linux manpage for libssh2_channel_wait_closed.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_channel_wait_closed - wait for the remote to close the channel

* SYNOPSIS
#include <libssh2.h>

int libssh2_channel_wait_closed(LIBSSH2_CHANNEL *channel);

* DESCRIPTION
Enter a temporary blocking state until the remote host closes the named
channel. Typically sent after /libssh2_channel_close(3)/ in order to
examine the exit status.

* RETURN VALUE
Return 0 on success or negative on failure. It returns
LIBSSH2_ERROR_EAGAIN when it would otherwise block. While
LIBSSH2_ERROR_EAGAIN is a negative number, it isn't really a failure per
se.

* SEE ALSO
*libssh2_channel_send_eof(3)* *libssh2_channel_eof(3)*
*libssh2_channel_wait_eof(3)*
