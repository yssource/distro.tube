#+TITLE: Manpages - DBI_Gofer_Serializer_Base.3pm
#+DESCRIPTION: Linux manpage for DBI_Gofer_Serializer_Base.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DBI::Gofer::Serializer::Base - base class for Gofer serialization

* SYNOPSIS
$serializer = $serializer_class->new(); $string =
$serializer->serialize( $data ); ($string, $deserializer_class) =
$serializer->serialize( $data ); $data = $serializer->deserialize(
$string );

* DESCRIPTION
DBI::Gofer::Serializer::* classes implement a very minimal subset of the
Data::Serializer API.

Gofer serializers are expected to be very fast and are not required to
deal with anything other than non-blessed references to arrays and
hashes, and plain scalars.
