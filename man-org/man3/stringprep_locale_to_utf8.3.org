#+TITLE: Manpages - stringprep_locale_to_utf8.3
#+DESCRIPTION: Linux manpage for stringprep_locale_to_utf8.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
stringprep_locale_to_utf8 - API function

* SYNOPSIS
*#include <stringprep.h>*

*char * stringprep_locale_to_utf8(const char * */str/*);*

* ARGUMENTS
- const char * str :: input zero terminated string.

* DESCRIPTION
Convert string encoded in the locale's character set into UTF-8 by using
*stringprep_convert()*.

Return value: Returns newly allocated zero-terminated string which is
/str/ transcoded into UTF-8.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
