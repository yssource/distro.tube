#+TITLE: Manpages - CURLOPT_KRBLEVEL.3
#+DESCRIPTION: Linux manpage for CURLOPT_KRBLEVEL.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_KRBLEVEL - FTP kerberos security level

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_KRBLEVEL, char *level);

* DESCRIPTION
Pass a char * as parameter. Set the kerberos security level for FTP;
this also enables kerberos awareness. This is a string that should match
one of the following: 'clear', 'safe', 'confidential' or 'private'. If
the string is set but does not match one of these, 'private' will be
used. Set the string to NULL to disable kerberos support for FTP.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
FTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/foo.bin");
    curl_easy_setopt(curl, CURLOPT_KRBLEVEL, "private");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
This option was known as CURLOPT_KRB4LEVEL up to 7.16.3

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_KRBLEVEL*(3), *CURLOPT_USE_SSL*(3),
