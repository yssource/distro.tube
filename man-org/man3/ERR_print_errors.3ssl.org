#+TITLE: Manpages - ERR_print_errors.3ssl
#+DESCRIPTION: Linux manpage for ERR_print_errors.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ERR_print_errors, ERR_print_errors_fp, ERR_print_errors_cb - print error
messages

* SYNOPSIS
#include <openssl/err.h> void ERR_print_errors(BIO *bp); void
ERR_print_errors_fp(FILE *fp); void ERR_print_errors_cb(int (*cb)(const
char *str, size_t len, void *u), void *u)

* DESCRIPTION
*ERR_print_errors()* is a convenience function that prints the error
strings for all errors that OpenSSL has recorded to *bp*, thus emptying
the error queue.

*ERR_print_errors_fp()* is the same, except that the output goes to a
*FILE*.

*ERR_print_errors_cb()* is the same, except that the callback function,
*cb*, is called for each error line with the string, length, and
userdata *u* as the callback parameters.

The error strings will have the following format:

[pid]:error:[error code]:[library name]:[function name]:[reason
string]:[filename]:[line]:[optional text message]

/error code/ is an 8 digit hexadecimal number. /library name/, /function
name/ and /reason string/ are ASCII text, as is /optional/ text message
if one was set for the respective error code.

If there is no text string registered for the given error code, the
error string will contain the numeric code.

* RETURN VALUES
*ERR_print_errors()* and *ERR_print_errors_fp()* return no values.

* SEE ALSO
*ERR_error_string* (3), *ERR_get_error* (3)

* COPYRIGHT
Copyright 2000-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
