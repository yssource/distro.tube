#+TITLE: Manpages - XFreeColormap.3
#+DESCRIPTION: Linux manpage for XFreeColormap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XFreeColormap.3 is found in manpage for: [[../man3/XCreateColormap.3][man3/XCreateColormap.3]]