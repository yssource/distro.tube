#+TITLE: Manpages - XtGetApplicationNameAndClass.3
#+DESCRIPTION: Linux manpage for XtGetApplicationNameAndClass.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtGetApplicationNameAndClass - retrieve application name and class

* SYNTAX
#include <X11/Intrinsic.h>

void XtGetApplicationNameAndClass(Display* /display/, String*
/name_return/, String* /class_return/);

* ARGUMENTS
- display :: Specifies an open display connection that has been
  initialized with *XtDisplayInitialize*.

- name_return :: Returns the application name.

- class_return :: Returns the application class.

* DESCRIPTION
*XtGetApplicationNameAndClass* returns the application name and class
passed to *XtDisplayInitialize* for the specified display. If the
display was never initialized or has been closed, the result is
undefined. The returned strings are owned by the Intrinsics and must not
be modified or freed by the caller.

* SEE ALSO
*XtDisplayInitialize*(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
