#+TITLE: Manpages - elf_getdata.3
#+DESCRIPTION: Linux manpage for elf_getdata.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
elf_getdata - Get washed data of section

#+begin_example
#+end_example

* SYNOPSIS
*#include <libelf.h>*

*Elf_Data * elf_getdata (Elf_Scn **/scn/*, Elf_Data **/data/*);*

* DESCRIPTION
The *elf_getdata*() function allows the user to retrieve the data
buffers of the section /scn/ . There can be more than one buffer if the
user explicitly added them. When a file is read the libelf library
creates exactly one data buffer.

The first buffer in the list can be obtained by passing a null pointer
in the parameter data. To get the next data buffer the previously
returned value must be passed in the data parameter. If there are no
more buffer left in the list a null pointer is returned.

If the data parameter is not a null pointer it must be a descriptor for
a buffer associated with the section scn . If this is not the case a
null pointer is returned. To facilitate error handling elf_getdata also
returns a null pointer if the scn parameter is a null pointer.
