#+TITLE: Manpages - gnutls_pkcs7_set_crl_raw.3
#+DESCRIPTION: Linux manpage for gnutls_pkcs7_set_crl_raw.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_pkcs7_set_crl_raw - API function

* SYNOPSIS
*#include <gnutls/pkcs7.h>*

*int gnutls_pkcs7_set_crl_raw(gnutls_pkcs7_t */pkcs7/*, const
gnutls_datum_t * */crl/*);*

* ARGUMENTS
- gnutls_pkcs7_t pkcs7 :: The pkcs7 type

- const gnutls_datum_t * crl :: the DER encoded crl to be added

* DESCRIPTION
This function will add a crl to the PKCS7 or RFC2630 crl set.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
