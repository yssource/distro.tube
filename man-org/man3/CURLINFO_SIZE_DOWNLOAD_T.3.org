#+TITLE: Manpages - CURLINFO_SIZE_DOWNLOAD_T.3
#+DESCRIPTION: Linux manpage for CURLINFO_SIZE_DOWNLOAD_T.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_SIZE_DOWNLOAD_T - get the number of downloaded bytes

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SIZE_DOWNLOAD_T,
curl_off_t *dlp);

* DESCRIPTION
Pass a pointer to a /curl_off_t/ to receive the total amount of bytes
that were downloaded. The amount is only for the latest transfer and
will be reset again for each new transfer. This counts actual payload
data, what's also commonly called body. All meta and header data are
excluded and will not be counted in this number.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* Perform the request */
    res = curl_easy_perform(curl);

    if(!res) {
      /* check the size */
      curl_off_t dl;
      res = curl_easy_getinfo(curl, CURLINFO_SIZE_DOWNLOAD_T, &dl);
      if(!res) {
        printf("Downloaded %" CURL_FORMAT_CURL_OFF_T " bytes\n", dl);
      }
    }
  }
#+end_example

* AVAILABILITY
Added in 7.55.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
*CURLINFO_SIZE_DOWNLOAD*(3), *CURLINFO_SIZE_UPLOAD_T*(3),
