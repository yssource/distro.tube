#+TITLE: Manpages - shm_unlink.3
#+DESCRIPTION: Linux manpage for shm_unlink.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about shm_unlink.3 is found in manpage for: [[../man3/shm_open.3][man3/shm_open.3]]