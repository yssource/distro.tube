#+TITLE: Manpages - pthread_mutexattr_getpshared.3
#+DESCRIPTION: Linux manpage for pthread_mutexattr_getpshared.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pthread_mutexattr_getpshared, pthread_mutexattr_setpshared - get/set
process-shared mutex attribute

* SYNOPSIS
#+begin_example
  #include <pthread.h>

  int pthread_mutexattr_getpshared(
   const pthread_mutexattr_t *restrict attr,
   int *restrict pshared);
  int pthread_mutexattr_setpshared(pthread_mutexattr_t *attr,
   int pshared);
#+end_example

Compile and link with /-pthread/.

* DESCRIPTION
These functions get and set the process-shared attribute in a mutex
attributes object. This attribute must be appropriately set to ensure
correct, efficient operation of a mutex created using this attributes
object.

The process-shared attribute can have one of the following values:

- *PTHREAD_PROCESS_PRIVATE* :: Mutexes created with this attributes
  object are to be shared only among threads in the same process that
  initialized the mutex. This is the default value for the
  process-shared mutex attribute.

- *PTHREAD_PROCESS_SHARED* :: Mutexes created with this attributes
  object can be shared between any threads that have access to the
  memory containing the object, including threads in different
  processes.

*pthread_mutexattr_getpshared*() places the value of the process-shared
attribute of the mutex attributes object referred to by /attr/ in the
location pointed to by /pshared/.

*pthread_mutexattr_setpshared*() sets the value of the process-shared
attribute of the mutex attributes object referred to by /attr/ to the
value specified in *pshared*.

If /attr/ does not refer to an initialized mutex attributes object, the
behavior is undefined.

* RETURN VALUE
On success, these functions return 0. On error, they return a positive
error number.

* ERRORS
*pthread_mutexattr_setpshared*() can fail with the following errors:

- *EINVAL* :: The value specified in /pshared/ is invalid.

- *ENOTSUP* :: /pshared is/ *PTHREAD_PROCESS_SHARED* but the
  implementation does not support process-shared mutexes.

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008.

* SEE ALSO
*pthread_mutexattr_init*(3), *pthreads*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
