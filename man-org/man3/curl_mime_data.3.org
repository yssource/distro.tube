#+TITLE: Manpages - curl_mime_data.3
#+DESCRIPTION: Linux manpage for curl_mime_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_mime_data - set a mime part's body data from memory

* SYNOPSIS
*#include <curl/curl.h>*

*CURLcode curl_mime_data(curl_mimepart * */part/*, const char * */data/
*, size_t */datasize/*);*

* DESCRIPTION
/curl_mime_data(3)/ sets a mime part's body content from memory data.

/data/ points to the data bytes: those are copied to the part and their
storage may safely be reused after call. /datasize/ is the number of
data bytes: it can be set to /CURL_ZERO_TERMINATED/ to indicate /data/
is a null-terminated character string. /part/ is the part's to assign
contents to.

Setting a part's contents twice is valid: only the value set by the last
call is retained. It is possible to unassign part's contents by setting
/data/ to NULL.

Setting large data is memory consuming: one might consider using
/curl_mime_data_cb(3)/ in such a case.

* EXAMPLE
#+begin_example
   curl_mime *mime;
   curl_mimepart *part;

   /* create a mime handle */
   mime = curl_mime_init(easy);

   /* add a part */
   part = curl_mime_addpart(mime);

   /* add data to the part  */
   curl_mime_data(part, "raw contents to send", CURL_ZERO_TERMINATED);
#+end_example

* AVAILABILITY
As long as at least one of HTTP, SMTP or IMAP is enabled. Added in
7.56.0.

* RETURN VALUE
CURLE_OK or a CURL error code upon failure.

* SEE ALSO
*curl_mime_addpart*(3), *curl_mime_data_cb*(3), *curl_mime_name*(3),
*curl_mime_type*(3)
