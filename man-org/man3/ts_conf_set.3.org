#+TITLE: Manpages - ts_conf_set.3
#+DESCRIPTION: Linux manpage for ts_conf_set.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_conf_set - write a new TSLIB_CONFFILE config file and reload it.

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  int ts_conf_set(struct ts_module_conf *conf);
#+end_example

* DESCRIPTION
*ts_conf_set*() This function takes a pointer to a struct
ts_module_conf. It reads data from all linked structs in the list (next
and prev pointers) and writes a new TSLIB_CONFFILE. The first struct in
the list (where prev is NULL) is the first line and so on. After
writing, ts_reconfig() has to be called so the new modules and
parameters are reloaded and applied to the currently running program.

The given set of ts_module_conf structs is freed and destroyed here.
After calling ts_conf_set() one has to use ts_conf_get() again, for a
different change.

#+begin_example
  struct ts_module_conf {
  	char *name;
  	char *params;
  	int raw;
  	int nr;

  	struct ts_module_conf *next;
  	struct ts_module_conf *prev;
  };
#+end_example

* RETURN VALUE
This function returns zero on success and a negative integer on failure.

* SEE ALSO
*ts_conf_get*(3), *ts_config*(3), *ts.conf*(5)
