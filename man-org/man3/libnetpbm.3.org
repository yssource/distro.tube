#+TITLE: Manpages - libnetpbm.3
#+DESCRIPTION: Linux manpage for libnetpbm.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
libnetpbm - general introduction to the netpbm library

* DESCRIPTION
*libnetpbm* is a C programming library for reading, writing, and
manipulating Netpbm images. It also contains a few general graphics
manipulation tools, but it is not intended to be a graphics tools
library. For graphics tools, Netpbm expects you to run the Netpbm
programs. From a C program, the *libnetpbm* function *pm_system()* makes
this easy. However, since it creates a process and execs a program, this
may be too heavyweight for some applications.

To use *libnetpbm* services in your C program, #include the *pam.h*
interface header file. For historical reasons, you can also get by in
some cases with *pbm.h*, *pgm.h*, *ppm.h*, or *pnm.h*, but there's
really no point to that anymore.

The *libnetpbm* functions are divided into these categories:

- PBM functions. These have names that start with *pbm* and work only on
  PBM images.

- PGM functions. These have names that start with *pgm* and work only on
  PGM images.

- PPM functions. These have names that start with *ppm* and work only on
  PPM images.

- PNM functions. These have names that start with *pnm* and work on PBM,
  PGM, and PPM images.

- PAM functions. These also have names that start with *pnm* and work on
  all the Netpbm image types.

- PM functions. These are utility functions that aren't specific to any
  particular image format.

For new programming, you rarely need to concern yourself with the PBM,
PGM, PPM, and PNM functions, because the newer PAM functions do the same
thing and are easier to use. For certain processing of bi-level images,
the PBM functions are significantly more efficient, though.

*libnetpbm* has a backward compatibility feature that means a function
designed to read one format can read some others too, converting on the
fly. In particular, a function that reads a PGM image will also read a
PBM image, but converts it as it reads it so that for programming
purposes, it is a PGM image. Similarly, a function that reads PPM can
read PBM and PGM as well. And a function that reads PBM, PGM, or PPM can
read a PAM that has an equivalent tuple type.

For each of the five classes of *libnetpbm* image processing functions,
*libnetpbm* has in in-memory representation for a pixel, a row, and a
whole image. Do not confuse this format with the actual image format, as
you would see in a file. The *libnetpbm* in-memory format is designed to
make programming very easy. It is sometimes extremely inefficient, even
more than the actual image format. For example, a pixel that a PPM image
represents with 3 bytes, *libnetpbm*'s PAM functions represent with 16
bytes. A pixel in a PBM image is represented by a single bit, but the
PNM functions represent that pixel in memory with 96 bits.

See *Libnetpbm*User's*Manual*(3) for the basics on using *libnetpbm* in
a program.

You can look up the reference information for a particular function in
*The*libnetpbm*Directory*(1)

Before Netpbm release 10 (June 2002), this library was split into four:
libpbm, libpgm, libppm, and libpnm. That's largely the reason for the
multiple sets of functions and scattered documentation.
