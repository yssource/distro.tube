#+TITLE: Manpages - SDL_DestroySemaphore.3
#+DESCRIPTION: Linux manpage for SDL_DestroySemaphore.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_DestroySemaphore - Destroys a semaphore that was created by
/SDL_CreateSemaphore/.

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*void SDL_DestroySemaphore*(*SDL_sem *sem*);

* DESCRIPTION
*SDL_DestroySemaphore* destroys the semaphore pointed to by *sem* that
was created by *SDL_CreateSemaphore*. It is not safe to destroy a
semaphore if there are threads currently blocked waiting on it.

* EXAMPLES
#+begin_example
  if (my_sem != NULL) {
          SDL_DestroySemaphore(my_sem);
          my_sem = NULL;
  }
#+end_example

* SEE ALSO
*SDL_CreateSemaphore*, *SDL_SemWait*, *SDL_SemTryWait*,
*SDL_SemWaitTimeout*, *SDL_SemPost*, *SDL_SemValue*
