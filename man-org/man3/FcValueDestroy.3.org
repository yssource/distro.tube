#+TITLE: Manpages - FcValueDestroy.3
#+DESCRIPTION: Linux manpage for FcValueDestroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcValueDestroy - Free a value

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcValueDestroy (FcValue /v/*);*

* DESCRIPTION
Frees any memory referenced by /v/. Values of type FcTypeString,
FcTypeMatrix and FcTypeCharSet reference memory, the other types do not.
