#+TITLE: Manpages - auparse_feed_age_events.3
#+DESCRIPTION: Linux manpage for auparse_feed_age_events.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_feed_age_events - check events for complete based on time.

* SYNOPSIS
*#include <auparse.h>*

void auparse_feed_age_events(auparse_state_t *au);

* DESCRIPTION
/auparse_feed_age_events/ should be called to see if any events are
complete based on the current clock time. Any newly complete events will
be passed to the callback function.

* RETURN VALUE
None.

* SEE ALSO
*auparse_feed*(3), *auparse_flush_feed*(3), *auparse_feed_has_data*(3)

* AUTHOR
Steve Grubb
