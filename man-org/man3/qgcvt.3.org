#+TITLE: Manpages - qgcvt.3
#+DESCRIPTION: Linux manpage for qgcvt.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about qgcvt.3 is found in manpage for: [[../man3/qecvt.3][man3/qecvt.3]]