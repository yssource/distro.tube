#+TITLE: Manpages - isascii.3p
#+DESCRIPTION: Linux manpage for isascii.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
isascii --- test for a 7-bit US-ASCII character

* SYNOPSIS
#+begin_example
  #include <ctype.h>
  int isascii(int c);
#+end_example

* DESCRIPTION
The /isascii/() function shall test whether /c/ is a 7-bit US-ASCII
character code.

The /isascii/() function is defined on all integer values.

* RETURN VALUE
The /isascii/() function shall return non-zero if /c/ is a 7-bit
US-ASCII character code between 0 and octal 0177 inclusive; otherwise,
it shall return 0.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
The /isascii/() function cannot be used portably in a localized
application.

* RATIONALE
None.

* FUTURE DIRECTIONS
The /isascii/() function may be removed in a future version.

* SEE ALSO
The Base Definitions volume of POSIX.1‐2017, /*<ctype.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
