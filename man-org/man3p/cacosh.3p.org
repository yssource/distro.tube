#+TITLE: Manpages - cacosh.3p
#+DESCRIPTION: Linux manpage for cacosh.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
cacosh, cacoshf, cacoshl --- complex arc hyperbolic cosine functions

* SYNOPSIS
#+begin_example
  #include <complex.h>
  double complex cacosh(double complex z);
  float complex cacoshf(float complex z);
  long double complex cacoshl(long double complex z);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

These functions shall compute the complex arc hyperbolic cosine of /z/,
with a branch cut at values less than 1 along the real axis.

* RETURN VALUE
These functions shall return the complex arc hyperbolic cosine value, in
the range of a half-strip of non-negative values along the real axis and
in the interval [-/i/π, +/i/π] along the imaginary axis.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//ccosh/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<complex.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
