#+TITLE: Manpages - _tolower.3p
#+DESCRIPTION: Linux manpage for _tolower.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
_tolower --- transliterate uppercase characters to lowercase

* SYNOPSIS
#+begin_example
  #include <ctype.h>
  int _tolower(int c);
#+end_example

* DESCRIPTION
The /_tolower/() macro shall be equivalent to /tolower/(/c/) except that
the application shall ensure that the argument /c/ is an uppercase
letter.

* RETURN VALUE
Upon successful completion, /_tolower/() shall return the lowercase
letter corresponding to the argument passed.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
Applications should use the /tolower/() function instead of the
obsolescent /_tolower/() function.

* RATIONALE
None.

* FUTURE DIRECTIONS
The /_tolower/() function may be removed in a future version.

* SEE ALSO
//tolower/ ( )/, //isupper/ ( )/

The Base Definitions volume of POSIX.1‐2017, /Chapter 7/, /Locale/,
/*<ctype.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
