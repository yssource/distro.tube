#+TITLE: Manpages - freelocale.3p
#+DESCRIPTION: Linux manpage for freelocale.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
freelocale --- free resources allocated for a locale object

* SYNOPSIS
#+begin_example
  #include <locale.h>
  void freelocale(locale_t locobj);
#+end_example

* DESCRIPTION
The /freelocale/() function shall cause the resources allocated for a
locale object returned by a call to the /newlocale/() or /duplocale/()
functions to be released.

The behavior is undefined if the /locobj/ argument is the special locale
object LC_GLOBAL_LOCALE or is not a valid locale object handle.

Any use of a locale object that has been freed results in undefined
behavior.

* RETURN VALUE
None.

* ERRORS
None.

/The following sections are informative./

* EXAMPLES
** Freeing Up a Locale Object
The following example shows a code fragment to free a locale object
created by /newlocale/():

#+begin_quote
  #+begin_example

    #include <locale.h>
    ...
    /* Every locale object allocated with newlocale() should be
     * freed using freelocale():
     */
    locale_t loc;
    /* Get the locale. */
    loc = newlocale (LC_CTYPE_MASK | LC_TIME_MASK, "locname", NULL);
    /* ... Use the locale object ... */
    ...
    /* Free the locale object resources. */
    freelocale (loc);
  #+end_example
#+end_quote

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//duplocale/ ( )/, //newlocale/ ( )/, //uselocale/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<locale.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
