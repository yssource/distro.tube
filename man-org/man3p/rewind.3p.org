#+TITLE: Manpages - rewind.3p
#+DESCRIPTION: Linux manpage for rewind.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
rewind --- reset the file position indicator in a stream

* SYNOPSIS
#+begin_example
  #include <stdio.h>
  void rewind(FILE *stream);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

The call:

#+begin_quote
  #+begin_example

    rewind(stream)
  #+end_example
#+end_quote

shall be equivalent to:

#+begin_quote
  #+begin_example

    (void) fseek(stream, 0L, SEEK_SET)
  #+end_example
#+end_quote

except that /rewind/() shall also clear the error indicator.

Since /rewind/() does not return a value, an application wishing to
detect errors should clear /errno/, then call /rewind/(), and if /errno/
is non-zero, assume an error has occurred.

* RETURN VALUE
The /rewind/() function shall not return a value.

* ERRORS
Refer to //fseek/ ( )/ with the exception of *[EINVAL]* which does not
apply.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
/Section 2.5/, /Standard I/O Streams/, //fseek/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<stdio.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
