#+TITLE: Manpages - strsignal.3p
#+DESCRIPTION: Linux manpage for strsignal.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
strsignal --- get name of signal

* SYNOPSIS
#+begin_example
  #include <string.h>
  char *strsignal(int signum);
#+end_example

* DESCRIPTION
The /strsignal/() function shall map the signal number in /signum/ to an
implementation-defined string and shall return a pointer to it. It shall
use the same set of messages as the /psignal/() function.

The application shall not modify the string returned. The returned
pointer might be invalidated or the string content might be overwritten
by a subsequent call to /strsignal/() or /setlocale/(). The returned
pointer might also be invalidated if the calling thread is terminated.

The contents of the message strings returned by /strsignal/() should be
determined by the setting of the /LC_MESSAGES/ category in the current
locale.

The implementation shall behave as if no function defined in this
standard calls /strsignal/().

Since no return value is reserved to indicate an error, an application
wishing to check for error situations should set /errno/ to 0, then call
/strsignal/(), then check /errno/.

The /strsignal/() function need not be thread-safe.

* RETURN VALUE
Upon successful completion, /strsignal/() shall return a pointer to a
string. Otherwise, if /signum/ is not a valid signal number, the return
value is unspecified.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
If /signum/ is not a valid signal number, some implementations return
NULL, while for others the /strsignal/() function returns a pointer to a
string containing an unspecified message denoting an unknown signal.
POSIX.1‐2008 leaves this return value unspecified.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//psiginfo/ ( )/, //setlocale/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<string.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
