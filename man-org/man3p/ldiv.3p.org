#+TITLE: Manpages - ldiv.3p
#+DESCRIPTION: Linux manpage for ldiv.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
ldiv, lldiv --- compute quotient and remainder of a long division

* SYNOPSIS
#+begin_example
  #include <stdlib.h>
  ldiv_t ldiv(long numer, long denom);
  lldiv_t lldiv(long long numer, long long denom);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

These functions shall compute the quotient and remainder of the division
of the numerator /numer/ by the denominator /denom/. If the division is
inexact, the resulting quotient is the *long* integer (for the /ldiv/()
function) or *long long* integer (for the /lldiv/() function) of lesser
magnitude that is the nearest to the algebraic quotient. If the result
cannot be represented, the behavior is undefined; otherwise, /quot/ *
/denom/+/rem/ shall equal /numer/.

* RETURN VALUE
The /ldiv/() function shall return a structure of type *ldiv_t*,
comprising both the quotient and the remainder. The structure shall
include the following members, in any order:

#+begin_quote
  #+begin_example

    long   quot;    /* Quotient */
    long   rem;     /* Remainder */
  #+end_example
#+end_quote

The /lldiv/() function shall return a structure of type *lldiv_t*,
comprising both the quotient and the remainder. The structure shall
include the following members, in any order:

#+begin_quote
  #+begin_example

    long long   quot;    /* Quotient */
    long long   rem;     /* Remainder */
  #+end_example
#+end_quote

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//div/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<stdlib.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
