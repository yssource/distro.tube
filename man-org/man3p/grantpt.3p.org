#+TITLE: Manpages - grantpt.3p
#+DESCRIPTION: Linux manpage for grantpt.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
grantpt --- grant access to the slave pseudo-terminal device

* SYNOPSIS
#+begin_example
  #include <stdlib.h>
  int grantpt(int fildes);
#+end_example

* DESCRIPTION
The /grantpt/() function shall change the mode and ownership of the
slave pseudo-terminal device associated with its master pseudo-terminal
counterpart. The /fildes/ argument is a file descriptor that refers to a
master pseudo-terminal device. The user ID of the slave shall be set to
the real UID of the calling process and the group ID shall be set to an
unspecified group ID. The permission mode of the slave pseudo-terminal
shall be set to readable and writable by the owner, and writable by the
group.

The behavior of the /grantpt/() function is unspecified if the
application has installed a signal handler to catch SIGCHLD signals.

* RETURN VALUE
Upon successful completion, /grantpt/() shall return 0; otherwise, it
shall return -1 and set /errno/ to indicate the error.

* ERRORS
The /grantpt/() function may fail if:

- *EACCES* :: The corresponding slave pseudo-terminal device could not
  be accessed.

- *EBADF* :: The /fildes/ argument is not a valid open file descriptor.

- *EINVAL* :: The /fildes/ argument is not associated with a master
  pseudo-terminal device.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
See the RATIONALE section for //posix_openpt/ ( )/.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//open/ ( )/, //posix_openpt/ ( )/, //ptsname/ ( )/, //unlockpt/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<stdlib.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
