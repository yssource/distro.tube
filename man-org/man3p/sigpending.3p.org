#+TITLE: Manpages - sigpending.3p
#+DESCRIPTION: Linux manpage for sigpending.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
sigpending --- examine pending signals

* SYNOPSIS
#+begin_example
  #include <signal.h>
  int sigpending(sigset_t *set);
#+end_example

* DESCRIPTION
The /sigpending/() function shall store, in the location referenced by
the /set/ argument, the set of signals that are blocked from delivery to
the calling thread and that are pending on the process or the calling
thread.

* RETURN VALUE
Upon successful completion, /sigpending/() shall return 0; otherwise, -1
shall be returned and /errno/ set to indicate the error.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//exec/ /, //pthread_sigmask/ ( )/, //sigaddset/ ( )/,
//sigdelset/ ( )/, //sigemptyset/ ( )/, //sigfillset/ ( )/,
//sigismember/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<signal.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
