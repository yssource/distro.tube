#+TITLE: Manpages - sysconf.3p
#+DESCRIPTION: Linux manpage for sysconf.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
sysconf --- get configurable system variables

* SYNOPSIS
#+begin_example
  #include <unistd.h>
  long sysconf(int name);
#+end_example

* DESCRIPTION
The /sysconf/() function provides a method for the application to
determine the current value of a configurable system limit or option (
/variable/). The implementation shall support all of the variables
listed in the following table and may support others.

The /name/ argument represents the system variable to be queried. The
following table lists the minimal set of system variables from
/<limits.h>/ or /<unistd.h>/ that can be returned by /sysconf/(), and
the symbolic constants defined in /<unistd.h>/ that are the
corresponding values used for /name/.

TABLE

TABLE

* RETURN VALUE
If /name/ is an invalid value, /sysconf/() shall return -1 and set
/errno/ to indicate the error. If the variable corresponding to /name/
is described in /<limits.h>/ as a maximum or minimum value and the
variable has no limit, /sysconf/() shall return -1 without changing the
value of /errno/. Note that indefinite limits do not imply infinite
limits; see /<limits.h>/.

Otherwise, /sysconf/() shall return the current variable value on the
system. The value returned shall not be more restrictive than the
corresponding value described to the application when it was compiled
with the implementation's /<limits.h>/ or /<unistd.h>/. The value shall
not change during the lifetime of the calling process, except that
/sysconf/(_SC_OPEN_MAX) may return different values before and after a
call to /setrlimit/() which changes the RLIMIT_NOFILE soft limit.

If the variable corresponding to /name/ is dependent on an unsupported
option, the results are unspecified.

* ERRORS
The /sysconf/() function shall fail if:

- *EINVAL* :: The value of the /name/ argument is invalid.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
As -1 is a permissible return value in a successful situation, an
application wishing to check for error situations should set /errno/ to
0, then call /sysconf/(), and, if it returns -1, check to see if /errno/
is non-zero.

Application developers should check whether an option, such as
_POSIX_TRACE, is supported prior to obtaining and using values for
related variables, such as _POSIX_TRACE_NAME_MAX.

* RATIONALE
This functionality was added in response to requirements of application
developers and of system vendors who deal with many international system
configurations. It is closely related to /pathconf/() and /fpathconf/().

Although a conforming application can run on all systems by never
demanding more resources than the minimum values published in this
volume of POSIX.1‐2017, it is useful for that application to be able to
use the actual value for the quantity of a resource available on any
given system. To do this, the application makes use of the value of a
symbolic constant in /<limits.h>/ or /<unistd.h>/.

However, once compiled, the application must still be able to cope if
the amount of resource available is increased. To that end, an
application may need a means of determining the quantity of a resource,
or the presence of an option, at execution time.

Two examples are offered:

-  1. :: Applications may wish to act differently on systems with or
  without job control. Applications vendors who wish to distribute only
  a single binary package to all instances of a computer architecture
  would be forced to assume job control is never available if it were to
  rely solely on the /<unistd.h>/ value published in this volume of
  POSIX.1‐2017.

-  2. :: International applications vendors occasionally require
  knowledge of the number of clock ticks per second. Without these
  facilities, they would be required to either distribute their
  applications partially in source form or to have 50 Hz and 60 Hz
  versions for the various countries in which they operate.

It is the knowledge that many applications are actually distributed
widely in executable form that leads to this facility. If limited to the
most restrictive values in the headers, such applications would have to
be prepared to accept the most limited environments offered by the
smallest microcomputers. Although this is entirely portable, there was a
consensus that they should be able to take advantage of the facilities
offered by large systems, without the restrictions associated with
source and object distributions.

During the discussions of this feature, it was pointed out that it is
almost always possible for an application to discern what a value might
be at runtime by suitably testing the various functions themselves. And,
in any event, it could always be written to adequately deal with error
returns from the various functions. In the end, it was felt that this
imposed an unreasonable level of complication and sophistication on the
application developer.

This runtime facility is not meant to provide ever-changing values that
applications have to check multiple times. The values are seen as
changing no more frequently than once per system initialization, such as
by a system administrator or operator with an automatic configuration
program. This volume of POSIX.1‐2017 specifies that they shall not
change within the lifetime of the process.

Some values apply to the system overall and others vary at the file
system or directory level. The latter are described in
//fpathconf/ ( )/.

Note that all values returned must be expressible as integers. String
values were considered, but the additional flexibility of this approach
was rejected due to its added complexity of implementation and use.

Some values, such as {PATH_MAX}, are sometimes so large that they must
not be used to, say, allocate arrays. The /sysconf/() function returns a
negative value to show that this symbolic constant is not even defined
in this case.

Similar to /pathconf/(), this permits the implementation not to have a
limit. When one resource is infinite, returning an error indicating that
some other resource limit has been reached is conforming behavior.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//confstr/ ( )/, //fpathconf/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<limits.h>*/,
/*<unistd.h>*/

The Shell and Utilities volume of POSIX.1‐2017, //getconf/ /

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
