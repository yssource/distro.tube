#+TITLE: Man1 - daxctl-create-device.1
#+DESCRIPTION: Linux manpage for daxctl-create-device.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
daxctl-create-device - Create a devdax device

* SYNOPSIS
#+begin_example
  daxctl create-device [<options>]
#+end_example

\\

* EXAMPLES

#+begin_quote
  ·

  Creates dax0.1 with 4G of size
#+end_quote

#+begin_quote
  #+begin_example
    # daxctl create-device -s 4G
    [
      {
        "chardev":"dax0.1",
        "size":4294967296,
        "target_node":0,
        "mode":"devdax"
      }
    ]
  #+end_example
#+end_quote

#+begin_quote
  ·

  Creates devices with fully available size on all regions
#+end_quote

#+begin_quote
  #+begin_example
    # daxctl create-device -u
    [
      {
        "chardev":"dax0.1",
        "size":"15.63 GiB (16.78 GB)",
        "target_node":0,
        "mode":"devdax"
      },
      {
        "chardev":"dax1.1",
        "size":"15.63 GiB (16.78 GB)",
        "target_node":1,
        "mode":"devdax"
      }
    ]
  #+end_example
#+end_quote

#+begin_quote
  ·

  Creates dax0.1 with fully available size on region id 0
#+end_quote

#+begin_quote
  #+begin_example
    # daxctl create-device -r 0 -u
    {
      "chardev":"dax0.1",
      "size":"15.63 GiB (16.78 GB)",
      "target_node":0,
      "mode":"devdax"
    }
  #+end_example
#+end_quote

* DESCRIPTION
Creates dax device in /devdax/ mode in dynamic regions. The resultant
can also be convereted to the /system-ram/ mode which arranges for the
dax range to be hot-plugged into the system as regular memory.

/daxctl create-device/ expects that the BIOS or kernel defines a range
in the EFI memory map with EFI_MEMORY_SP. The resultant ranges mean that
it's 100% capacity is reserved for applications.

* OPTIONS
-r, --region=

#+begin_quote
  Restrict the operation to devices belonging to the specified
  region(s). A device-dax region is a contiguous range of memory that
  hosts one or more /dev/daxX.Y devices, where X is the region id and Y
  is the device instance id.
#+end_quote

-s, --size=

#+begin_quote
  For regions that support dax device cretion, set the device size in
  bytes. Otherwise it defaults to the maximum size specified by region.
  This option supports the suffixes "k" or "K" for KiB, "m" or "M" for
  MiB, "g" or "G" for GiB and "t" or "T" for TiB.

  #+begin_quote
    #+begin_example
      The size must be a multiple of the region alignment.
    #+end_example
  #+end_quote
#+end_quote

-a, --align

#+begin_quote
  Applications that want to establish dax memory mappings with page
  table entries greater than system base page size (4K on x86) need a
  device that is sufficiently aligned. This defaults to 2M. Note that
  "devdax" mode enforces all mappings to be aligned to this value, i.e.
  it fails unaligned mapping attempts.
#+end_quote

--input

#+begin_quote
  Applications that want to select ranges assigned to a device-dax
  instance, or wanting to establish previously created devices, can pass
  an input JSON file. The file option lets a user pass a JSON object
  similar to the one listed with "daxctl list".

  #+begin_quote
    #+begin_example
      The device name is not re-created, but if a "chardev" is passed in
      the JSON file, it will use that to get the region id.
    #+end_example
  #+end_quote

  #+begin_quote
    #+begin_example
      Note that the JSON content in the file cannot be an array of
      JSON objects but rather a single JSON object i.e. without the
      array enclosing brackets.
    #+end_example
  #+end_quote
#+end_quote

-u, --human

#+begin_quote
  By default the command will output machine-friendly raw-integer data.
  Instead, with this flag, numbers representing storage size will be
  formatted as human readable strings with units, other fields are
  converted to hexadecimal strings.
#+end_quote

-v, --verbose

#+begin_quote
  Emit more debug messages
#+end_quote

* COPYRIGHT
Copyright © 2016 - 2020, Intel Corporation. License GPLv2: GNU GPL
version 2 <http://gnu.org/licenses/gpl.html>. This is free software: you
are free to change and redistribute it. There is NO WARRANTY, to the
extent permitted by law.

* SEE ALSO
daxctl-list(1),daxctl-reconfigure-device[1],daxctl-destroy-device[1]
