#+TITLE: Man1 - ppmntsc.1
#+DESCRIPTION: Linux manpage for ppmntsc.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmntsc - Make RGB colors legal for NTSC or PAL color systems.

* SYNOPSIS
*ppmntsc*

[*--pal*] [*--legalonly*] [*--illegalonly*] [*--correctedonly*]
[*--verbose*] [*--debug*] [/infile/]

Minimum unique abbreviations of options are acceptable.

* DESCRIPTION
This program is part of *Netpbm*(1)

This program makes colors legal in the NTSC (or PAL) color systems.
Often, images generated on the computer are made for use in movies which
ultimately end up on a television screen. However, the range of colors
(as specified by their RGB values) on a computer does not match the
range of colors that can be represented using the NTSC (or PAL) systems.
If an image with 'illegal' colors is sent directly to an NTSC (or PAL)
video system for recording, the 'illegal' colors will be clipped. This
may result in an undesirable looking picture.

This utility tests each pixel in an image to see if it falls within the
legal NTSC (or PAL) range. If not, it raises or lowers the pixel's
saturation in the output so that it does fall within legal limits.
Pixels that are already OK just go unmodified into the output.

Input is from the file named /input/. If /input/ is *-*, input is from
Standard Input. If you don't specify /input/, input is from Standard
Input.

Output is always to Standard Output.

This program handles multi-image PPM input, producing multi-image PPM
output.

* OPTIONS
- *--pal* :: Use the PAL transform instead of the default NTSC.

- *--verbose* :: Print a grand total of the number of illegal pixels.

- *--debug* :: Produce a humongous listing of illegal colors and their
  legal counterparts. NOTE: This option may produce a great deal of
  output.

- *--legalonly* :: Output only pixels that are already legal. Output
  black in place of pixels that are not.

- *--illegalonly* :: Output only pixels that are illegal (and output
  them uncorrected). Output black in place of pixels that are already
  legal.

- *--correctedonly* :: Output only pixels that are corrected versions of
  illegal pixels. Output black in place of pixels that are already
  legal.

* SEE ALSO
*pamdepth*(1) , *ppmdim*(1) , *ppmbrighten*(1) , *ppm*(5)

* AUTHOR
Wes Barris, Minnesota Supercomputer Center, Inc., Bryan Henderson
