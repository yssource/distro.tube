#+TITLE: Man1 - dconf-editor.1
#+DESCRIPTION: Linux manpage for dconf-editor.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
dconf-editor - Graphical editor for gsettings and dconf

* SYNOPSIS
*dconf-editor [OPTIONS...]*

*dconf-editor [OPTIONS...] [PATH]*

*dconf-editor [OPTIONS...] [FIXED_SCHEMA | RELOCATABLE_SCHEMA:PATH]
[KEY]*

* DESCRIPTION
The *dconf-editor* program provides a graphical interface for editing
settings that are stored by other applications using the /gsettings/
functions of the /glib/ library, or in the /dconf/ database. The
*gsettings*(1) and *dconf*(1) utilities provides similar functionality
on the commandline.

* OPTIONS
- *-h*,* --help* :: Prints the command-line options and exits.

- *-v*,* --version* :: Prints the release version and exits.

- *--list-relocatable-schemas* :: Prints the list of relocatable schemas
  and exits.

- *--I-understand-that-changing-options-can-break-applications* :: Launches
  application without showing initial warning.

* SEE ALSO
*dconf*(7) *dconf*(1) *gsettings*(1)
