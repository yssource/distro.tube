#+TITLE: Man1 - json-glib-format.1
#+DESCRIPTION: Linux manpage for json-glib-format.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
json-glib-format - JSON-GLib formatting tool

* SYNOPSIS
*json-glib-format* [*-h*, *--help*] [*--output* /FILE/] [*--prettify*]
[*--indent-spaces* /SPACES/] URI [URI...]

* DESCRIPTION
*json-glib-format* offers a simple command line interface to format JSON
data. It reads a list or URIs, applies the spacified formatting rules on
the JSON data, and outputs the formatted JSON to the standard output or
to a file.

The resources to operate on are specified by the /URI/ argument.

* COMMANDS
*-h*, *--help*

#+begin_quote
  Prints help and exits.
#+end_quote

*--output* /FILE/

#+begin_quote
  Redirects the output to /FILE/ instead of using the standard output.
#+end_quote

*-p*, *--prettify*

#+begin_quote
  Prettifies the output, by adding spaces and indentation. This argument
  is useful to improve the readability of JSON data, at the expense of
  its size.
#+end_quote

*--indent-spaces* /SPACES/

#+begin_quote
  Changes the number of spaces using to indent the JSON data from the
  default of 2. This argument is only considered if *--prettify* is
  used.
#+end_quote

* AUTHOR
*Emmanuele Bassi*

#+begin_quote
  Developer
#+end_quote
