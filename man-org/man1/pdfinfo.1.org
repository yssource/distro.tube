#+TITLE: Man1 - pdfinfo.1
#+DESCRIPTION: Linux manpage for pdfinfo.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pdfinfo - Portable Document Format (PDF) document information extractor
(version 3.03)

* SYNOPSIS
*pdfinfo* [options] [/PDF-file/]

* DESCRIPTION
*Pdfinfo* prints the contents of the 'Info' dictionary (plus some other
useful information) from a Portable Document Format (PDF) file.

If /PDF-file/ is '-', it reads the PDF file from stdin.

The 'Info' dictionary contains the following values:

#+begin_quote
  title
#+end_quote

#+begin_quote
  subject
#+end_quote

#+begin_quote
  keywords
#+end_quote

#+begin_quote
  author
#+end_quote

#+begin_quote
  creator
#+end_quote

#+begin_quote
  producer
#+end_quote

#+begin_quote
  creation date
#+end_quote

#+begin_quote
  modification date
#+end_quote

In addition, the following information is printed:

#+begin_quote
  custom metadata (yes/no)
#+end_quote

#+begin_quote
  metadata stream (yes/no)
#+end_quote

#+begin_quote
  tagged (yes/no)
#+end_quote

#+begin_quote
  userproperties (yes/no)
#+end_quote

#+begin_quote
  suspects (yes/no)
#+end_quote

#+begin_quote
  form (AcroForm / XFA / none)
#+end_quote

#+begin_quote
  javascript (yes/no)
#+end_quote

#+begin_quote
  page count
#+end_quote

#+begin_quote
  encrypted flag (yes/no)
#+end_quote

#+begin_quote
  print and copy permissions (if encrypted)
#+end_quote

#+begin_quote
  page size
#+end_quote

#+begin_quote
  file size
#+end_quote

#+begin_quote
  linearized (yes/no)
#+end_quote

#+begin_quote
  PDF version
#+end_quote

#+begin_quote
  metadata (only if requested)
#+end_quote

The options -listenc, -meta, -js, -struct, and -struct-text only print
the requested information. The 'Info' dictionary and related data listed
above is not printed. At most one of these five options may be used.

* OPTIONS
- *-f*/ number/ :: Specifies the first page to examine. If multiple
  pages are requested using the "-f" and "-l" options, the size of each
  requested page (and, optionally, the bounding boxes for each requested
  page) are printed. Otherwise, only page one is examined.

- *-l*/ number/ :: Specifies the last page to examine.

- *-box* :: Prints the page box bounding boxes: MediaBox, CropBox,
  BleedBox, TrimBox, and ArtBox.

- *-meta* :: Prints document-level metadata. (This is the "Metadata"
  stream from the PDF file's Catalog object.)

- *-custom* :: Prints custom and standard metadata.

- *-js* :: Prints all JavaScript in the PDF.

- *-struct* :: Prints the logical document structure of a Tagged-PDF
  file.

- *-struct-text* :: Print the textual content along with the document
  structure of a Tagged-PDF file. Note that extracting text this way
  might be slow for big PDF files. (Implies *-struct*.)

- *-url* :: Print all URLs in the PDF. Only the URL types supported by
  Poppler are listed. Currently, this is limited to Annotations. Note:
  only URLs referenced by the PDF objects such as Link Annotations are
  listed. pdfinfo does not attempt to extract strings matching
  http://... from the text content.

- *-isodates* :: Prints dates in ISO-8601 format (including the time
  zone).

- *-rawdates* :: Prints the raw (undecoded) date strings, directly from
  the PDF file.

- *-dests* :: Print a list of all named destinations. If a page range is
  specified using "-f" and "-l", only destinations in the page range are
  listed.

- *-enc*/ encoding-name/ :: Sets the encoding to use for text output.
  This defaults to "UTF-8".

- *-listenc* :: Lits the available encodings

- *-opw*/ password/ :: Specify the owner password for the PDF file.
  Providing this will bypass all security restrictions.

- *-upw*/ password/ :: Specify the user password for the PDF file.

- *-v* :: Print copyright and version information.

- *-h* :: Print usage information. (*-help* and *--help* are
  equivalent.)

* EXIT CODES
The Xpdf tools use the following exit codes:

- 0 :: No error.

- 1 :: Error opening a PDF file.

- 2 :: Error opening an output file.

- 3 :: Error related to PDF permissions.

- 99 :: Other error.

* AUTHOR
The pdfinfo software and documentation are copyright 1996-2011 Glyph &
Cog, LLC.

* SEE ALSO
*pdfdetach*(1), *pdffonts*(1), *pdfimages*(1), *pdftocairo*(1),
*pdftohtml*(1), *pdftoppm*(1), *pdftops*(1), *pdftotext*(1)
*pdfseparate*(1), *pdfsig*(1), *pdfunite*(1)
