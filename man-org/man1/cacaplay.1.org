#+TITLE: Man1 - cacaplay.1
#+DESCRIPTION: Linux manpage for cacaplay.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cacaplay - play libcaca files

* SYNOPSIS
*cacaplay [FILE]*

* DESCRIPTION
*cacaplay* plays libcaca animation files. These files can be created by
any libcaca program by setting the *CACA_DRIVER* environment variable to
*raw* and storing the program's standard output.

If no file argument is provided or '-' is used, *cacaplay* will read its
data from the standard input.

* EXAMPLES
cacaplay file.caca

CACA_DRIVER=raw CACA_GEOMETRY=80x32 cacademo | cacaplay

* SEE ALSO
cacaserver(1)

* AUTHOR
This manual page was written by Sam Hocevar <sam@hocevar.net>.
