#+TITLE: Man1 - pip.1
#+DESCRIPTION: Linux manpage for pip.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pip - package manager for Python packages

* SYNOPSIS
pip <command> [options]

* DESCRIPTION
pip is the PyPA recommended package manager for Python packages

* OPTIONS

#+begin_quote
  - *-h, --help* :: Show help.
#+end_quote

#+begin_quote
  - *--isolated* :: Run pip in an isolated mode, ignoring environment
    variables and user configuration.
#+end_quote

#+begin_quote
  - *-v, --verbose* :: Give more output. Option is additive, and can be
    used up to 3 times.
#+end_quote

#+begin_quote
  - *-V, --version* :: Show version and exit.
#+end_quote

#+begin_quote
  - *-q, --quiet* :: Give less output. Option is additive, and can be
    used up to 3 times (corresponding to WARNING, ERROR, and CRITICAL
    logging levels).
#+end_quote

#+begin_quote
  - *--log <path>* :: Path to a verbose appending log.
#+end_quote

#+begin_quote
  - *--no-input* :: Disable prompting for input.
#+end_quote

#+begin_quote
  - *--proxy <proxy>* :: Specify a proxy in the form
    [user:passwd@]proxy.server:port.
#+end_quote

#+begin_quote
  - *--retries <retries>* :: Maximum number of retries each connection
    should attempt (default 5 times).
#+end_quote

#+begin_quote
  - *--timeout <sec>* :: Set the socket timeout (default 15 seconds).
#+end_quote

#+begin_quote
  - *--exists-action <action>* :: Default action when a path already
    exists: (s)witch, (i)gnore, (w)ipe, (b)ackup, (a)bort.
#+end_quote

#+begin_quote
  - *--trusted-host <hostname>* :: Mark this host or host:port pair as
    trusted, even though it does not have valid or any HTTPS.
#+end_quote

#+begin_quote
  - *--cert <path>* :: Path to alternate CA bundle.
#+end_quote

#+begin_quote
  - *--client-cert <path>* :: Path to SSL client certificate, a single
    file containing the private key and the certificate in PEM format.
#+end_quote

#+begin_quote
  - *--cache-dir <dir>* :: Store the cache data in <dir>.
#+end_quote

#+begin_quote
  - *--no-cache-dir* :: Disable the cache.
#+end_quote

#+begin_quote
  - *--disable-pip-version-check* :: Don't periodically check PyPI to
    determine whether a new version of pip is available for download.
    Implied with --no-index.
#+end_quote

#+begin_quote
  - *--no-color* :: Suppress colored output.
#+end_quote

#+begin_quote
  - *--no-python-version-warning* :: Silence deprecation warnings for
    upcoming unsupported Pythons.
#+end_quote

#+begin_quote
  - *--use-feature <feature>* :: Enable new functionality, that may be
    backward incompatible.
#+end_quote

#+begin_quote
  - *--use-deprecated <feature>* :: Enable deprecated functionality,
    that will be removed in the future.
#+end_quote

* COMMANDS

#+begin_quote
  - *pip-install(1)* :: Install packages.

  - *pip-download(1)* :: Download packages.

  - *pip-uninstall(1)* :: Uninstall packages.

  - *pip-freeze(1)* :: Output installed packages in requirements format.

  - *pip-list(1)* :: List installed packages.

  - *pip-show(1)* :: Show information about installed packages.

  - *pip-check(1)* :: Verify installed packages have compatible
    dependencies.

  - *pip-search(1)* :: Search PyPI for packages.

  - *pip-wheel(1)* :: Build wheels from your requirements.

  - *pip-hash(1)* :: Compute hashes of package archives.

  - *pip-help(1)* :: Show help for pip commands.
#+end_quote

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know:

    #+begin_quote

      1. What problem were you trying to solve when you came to this
         page?

      2. What content was useful?

      3. What content was not useful?
    #+end_quote
  #+end_quote
#+end_quote

* AUTHOR
pip developers

* COPYRIGHT
2008-2021, PyPA
