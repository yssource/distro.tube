#+TITLE: Man1 - ndctl-read-labels.1
#+DESCRIPTION: Linux manpage for ndctl-read-labels.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ndctl-read-labels - read out the label area on a dimm or set of dimms

* SYNOPSIS
#+begin_example
  ndctl read-labels <nmem0> [<nmem1>..<nmemN>] [<options>]
#+end_example

\\

* DESCRIPTION
The namespace label area is a small persistent partition of capacity
available on some NVDIMM devices. The label area is used to resolve
aliasing between /pmem/ and /blk/ capacity by delineating namespace
boundaries. This command dumps the raw binary data in a dimm's label
area to stdout or a file. In the multi-dimm case the data is
concatenated.

* OPTIONS
<memory device(s)>

#+begin_quote
  A /nmemX/ device name, or a dimm id number. Restrict the operation to
  the specified dimm(s). The keyword /all/ can be specified to indicate
  the lack of any restriction, however this is the same as not supplying
  a --dimm option at all.
#+end_quote

-s, --size=

#+begin_quote
  Limit the operation to the given number of bytes. A size of 0
  indicates to operate over the entire label capacity.
#+end_quote

-O, --offset=

#+begin_quote
  Begin the operation at the given offset into the label area.
#+end_quote

-b, --bus=

#+begin_quote
  A bus id number, or a provider string (e.g. "ACPI.NFIT"). Restrict the
  operation to the specified bus(es). The keyword /all/ can be specified
  to indicate the lack of any restriction, however this is the same as
  not supplying a --bus option at all.
#+end_quote

-v

#+begin_quote
  Turn on verbose debug messages in the library (if ndctl was built with
  logging and debug enabled).
#+end_quote

-I, --index

#+begin_quote
  Limit the span of the label operation to just the index-block area.
  This is useful to determine if the dimm label area is initialized.
  Note that this option and --size/--offset are mutually exclusive.
#+end_quote

-o, --output

#+begin_quote
  output file
#+end_quote

-j, --json

#+begin_quote
  parse the label data into json assuming the /NVDIMM Namespace/
  Specification format.
#+end_quote

-u, --human

#+begin_quote
  enable json output and convert number formats to human readable
  strings, for example show the size in terms of "KB", "MB", "GB", etc
  instead of a signed 64-bit numbers per the JSON interchange format
  (implies --json).
#+end_quote

* COPYRIGHT
Copyright © 2016 - 2020, Intel Corporation. License GPLv2: GNU GPL
version 2 <http://gnu.org/licenses/gpl.html>. This is free software: you
are free to change and redistribute it. There is NO WARRANTY, to the
extent permitted by law.

* SEE ALSO
/UEFI NVDIMM Label Protocol/
<http://www.uefi.org/sites/default/files/resources/UEFI_Spec_2_7.pdf>
