#+TITLE: Man1 - netctl.1
#+DESCRIPTION: Linux manpage for netctl.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
netctl - Control the netctl network profile manager

* SYNOPSIS
*netctl* {*COMMAND*} [PROFILE]

*netctl* [--help | --version]

* DESCRIPTION
*netctl* may be used to introspect and control the state of the systemd
services for the network profile manager.

* OPTIONS
The following commands are understood:

*list*

#+begin_quote
  List all available profiles. Online profiles will be marked with a
  ‘*', otherwise active profiles will be marked with a ‘+'.
#+end_quote

*store*

#+begin_quote
  Saves which profiles are currently active.
#+end_quote

*restore*

#+begin_quote
  Loads the profiles that were active during the last invocation of
  ‘*store*'.
#+end_quote

*stop-all*

#+begin_quote
  Stops all active network profiles.
#+end_quote

*start [PROFILE]*

#+begin_quote
  Start the network profile specified on the command line.
#+end_quote

*stop [PROFILE]*

#+begin_quote
  Stop the network profile specified on the command line.
#+end_quote

*restart [PROFILE]*

#+begin_quote
  Restart the network profile specified on the command line. If it is
  not started yet, it will be started.
#+end_quote

*switch-to [PROFILE]*

#+begin_quote
  Starts the network profile specified on the command line after
  stopping all profiles that refer to the same interface.
#+end_quote

*is-active [PROFILE]*

#+begin_quote
  Check whether the network profile specified on the command line is
  active. Prints the current state.
#+end_quote

*status [PROFILE]*

#+begin_quote
  Show terse runtime status information about a profile, followed by its
  most recent log data from the journal.
#+end_quote

*enable [PROFILE]*

#+begin_quote
  Enable the systemd unit for the profile specified. This will create a
  unit configuration file. If the file already exists, the command
  fails. No other profiles than the one specified will be enabled.
  Changes to the general options in a profile specification will not
  propagate to the unit configuration file automatically. After such
  changes, it is necessary to ‘*reenable*' the profile.
#+end_quote

*disable [PROFILE]*

#+begin_quote
  Disable the systemd unit for the profile specified. This will remove
  the file created by ‘*enable*' even if it was customized, so be
  careful.
#+end_quote

*reenable [PROFILE]*

#+begin_quote
  Reenable the systemd unit for the profile specified. This is
  effectively a combination of ‘*disable*' and ‘*enable*'.
#+end_quote

*is-enabled [PROFILE]*

#+begin_quote
  Check whether the systemd unit for the specified profile is enabled.
  Prints the current enable status.
#+end_quote

*edit [PROFILE]*

#+begin_quote
  Open the file of the specified profile in an editor. This does not
  reenable, restart, or verify any profiles.
#+end_quote

*verify [PROFILE]*

#+begin_quote
  Check the file of the specified profile for syntax errors. If no
  errors are found, no output is produced.
#+end_quote

*wait-online [PROFILE]*

#+begin_quote
  Wait until the interface of the profile has a routable IP address of
  some kind.
#+end_quote

* EXIT STATUS
On success 0 is returned, a non-zero failure code otherwise.

* ENVIRONMENT
/$NETCTL_DEBUG/

#+begin_quote
  If set to "yes", debugging output is generated.

  The value of this variable is independent of the value of the variable
  with the same name in profiles.
#+end_quote

/$NETCTL_STATE_FILE/

#+begin_quote
  The location of the state file. Defaults to
  //var/lib/netctl/netctl.state/.
#+end_quote

* SEE ALSO
*netctl-auto*(1), *netctl.profile*(5), *netctl.special*(7),
*systemctl*(1)
