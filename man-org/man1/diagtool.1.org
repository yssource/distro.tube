#+TITLE: Man1 - diagtool.1
#+DESCRIPTION: Linux manpage for diagtool.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
diagtool - clang diagnostics tool

* SYNOPSIS
*diagtool* /command/ [/args/]

* DESCRIPTION
*diagtool* is a combination of four tools for dealing with diagnostics
in *clang*.

* SUBCOMMANDS
*diagtool* is separated into several subcommands each tailored to a
different purpose. A brief summary of each command follows, with more
detail in the sections that follow.

#+begin_quote

  #+begin_quote

    #+begin_quote

      - /find-diagnostic-id/ - Print the id of the given diagnostic.

      - /list-warnings/ - List warnings and their corresponding flags.

      - /show-enabled/ - Show which warnings are enabled for a given
        command line.

      - /tree/ - Show warning flags in a tree view.
    #+end_quote
  #+end_quote
#+end_quote

** find-diagnostic-id
*diagtool* find-diagnostic-id /diagnostic-name/

** list-warnings
*diagtool* list-warnings

** show-enabled
*diagtool* show-enabled [/options/] /filename .../

** tree
*diagtool* tree [/diagnostic-group/]

* AUTHOR
Maintained by the Clang / LLVM Team (<http://clang.llvm.org>)

* COPYRIGHT
2007-2021, The Clang Team
