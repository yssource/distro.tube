#+TITLE: Man1 - pbmtomatrixorbital.1
#+DESCRIPTION: Linux manpage for pbmtomatrixorbital.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtomatrixorbital - convert a PBM image to a Matrix Orbital LCD image

* SYNOPSIS
*pbmtomatrixorbital*

[/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtomatrixorbital* reads a PBM image as input and produces as output
an image that can be uploaded to a Matrix Orbital LCD display.

You can upload the image to the LCD via the serial port to which it is
connected with the program *mo-upload.pl*, which you can get from the
package *pbmtomatrixorbital*. Yes, the package is the same name as this
Netpbm program, and it contains its own *pbmtomatrixorbital* program
which is slightly different from this one because it is not part of the
Netpbm package.

* HISTORY
*pbmtomatrixorbital* was added to Netpbm in Release 10.18 (September
2003).

* SEE ALSO
*pbmtox10bm*(1) , *xbmtopbm*(1) , *pbm*(5)
