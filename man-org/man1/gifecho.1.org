#+TITLE: Man1 - gifecho.1
#+DESCRIPTION: Linux manpage for gifecho.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gifecho - generate a GIF from ASCII text

* SYNOPSIS
*gifecho* [-v] [-s /colormap-size/] [-f /foreground/] [-c /R/ /G/ /B/]
[-t /text/] [-h] [/gif-file/]

* DESCRIPTION
A program to generate GIF images out of regular text. Text can be one
line or multi-line, and is converted using 8 by 8 fixed font.

This program reads stdin if no text is provided on the command line
(-t), and will dump the created GIF file to stdout.

* OPTIONS
-v

#+begin_quote
  Verbose mode (show progress). Enables printout of running scan lines.
#+end_quote

-s colormap-size

#+begin_quote
  Explicitly defines the size of the color map of the resulting gif
  image. Usually the image will be bicolor with fg as color 1, unless
  [-f] is explicitly given in case the color map size will be big enough
  to hold it. However it is sometimes convenient to set the color map
  size to certain size while the fg color is small mainly so this image
  may be merged with another (images must match color map size).
#+end_quote

-f foreground

#+begin_quote
  Select foreground index (background is always 0). By default it is one
  and therefore the image result is bicolored. if FG is set to n then
  color map will be created with 2^k entries where 2^k > n for minimum
  k, assuming k <= 8. This color map will be all zeros except this
  foreground index. This option is useful if this text image should be
  integrated into other image colormap using their colors.
#+end_quote

-c R G B

#+begin_quote
  The color to use as the foreground color. White by default.
#+end_quote

-t text

#+begin_quote
  One line of text can be provided on the command line. Note you must
  encapsulate the Text within quotes if it has spaces (The quotes
  themselves are not treated as part of the text). If no -t option is
  provided, stdin is read until end of file.
#+end_quote

-h

#+begin_quote
  Print one line command line help, similar to Usage above.
#+end_quote

* NOTES
There is a hardcoded limit of 100 the number of lines.

* AUTHOR
Gershon Elber.
