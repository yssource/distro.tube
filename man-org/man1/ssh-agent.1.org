#+TITLE: Man1 - ssh-agent.1
#+DESCRIPTION: Linux manpage for ssh-agent.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is a program to hold private keys used for public key authentication.
Through use of environment variables the agent can be located and
automatically used for authentication when logging in to other machines
using

The options are as follows:

Bind the agent to the

socket

The default is

Generate C-shell commands on

This is the default if

looks like it's a csh style of shell.

Foreground mode. When this option is specified

will not fork.

Debug mode. When this option is specified

will not fork and will write debug information to standard error.

Specifies the hash algorithm used when displaying key fingerprints.
Valid options are:

and

The default is

Kill the current agent (given by the

environment variable).

Specify a pattern-list of acceptable paths for PKCS#11 provider and FIDO
authenticator middleware shared libraries that may be used with the

or

options to

Libraries that do not match the pattern list will be refused. See
PATTERNS in

for a description of pattern-list syntax. The default list is

Generate Bourne shell commands on

This is the default if

does not look like it's a csh style of shell.

Set a default value for the maximum lifetime of identities added to the
agent. The lifetime may be specified in seconds or in a time format
specified in

A lifetime specified for an identity with

overrides this value. Without this option the default maximum lifetime
is forever.

If a command (and optional arguments) is given, this is executed as a
subprocess of the agent. The agent exits automatically when the command
given on the command line terminates.

There are two main ways to get an agent set up. The first is at the
start of an X session, where all other windows or programs are started
as children of the

program. The agent starts a command under which its environment
variables are exported, for example

When the command terminates, so does the agent.

The second method is used for a login session. When

is started, it prints the shell commands required to set its environment
variables, which in turn can be evaluated in the calling shell, for
example

In both cases,

looks at these environment variables and uses them to establish a
connection to the agent.

The agent initially does not have any private keys. Keys are added using

or by

when

is set in

Multiple identities may be stored in

concurrently and

will automatically use them if present.

is also used to remove keys from

and to query the keys that are held in one.

Connections to

may be forwarded from further remote hosts using the

option to

(but see the caveats documented therein), avoiding the need for
authentication data to be stored on other machines. Authentication
passphrases and private keys never go over the network: the connection
to the agent is forwarded over SSH remote connections and the result is
returned to the requester, allowing the user access to their identities
anywhere in the network in a secure fashion.

When

starts, it stores the name of the agent's process ID (PID) in this
variable.

When

starts, it creates a

socket and stores its pathname in this variable. It is accessible only
to the current user, but is easily abused by root or another instance of
the same user.

sockets used to contain the connection to the authentication agent.
These sockets should only be readable by the owner. The sockets should
get automatically removed when the agent exits.

OpenSSH is a derivative of the original and free ssh 1.2.12 release by

and

removed many bugs, re-added newer features and created OpenSSH.

contributed the support for SSH protocol versions 1.5 and 2.0.
