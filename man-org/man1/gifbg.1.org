#+TITLE: Man1 - gifbg.1
#+DESCRIPTION: Linux manpage for gifbg.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gifbg - generate a test-pattern GIF

* SYNOPSIS
*gifbg* [-v] [-d /dir/] [-l /lvls/] [-c /R/ /G/ /B/] [-m /min/] [-o
/max/] [-s /w/ /h/] [-h]

* DESCRIPTION
A program to generate a single-color test pattern GIF with gradually
changing intensity in any of the basic 8 directions.

The gifbg program reads no input, and will dump the created GIF file to
stdout.

* OPTIONS
-v

#+begin_quote
  Verbose mode (show progress). Enables printout of running scan lines.
#+end_quote

-d dir

#+begin_quote
  Select direction the intensity of the background should increase.
  Direction can be one of the 8 principal directions:

  "T"

  #+begin_quote
    for Top
  #+end_quote

  "TR"

  #+begin_quote
    for Top Right
  #+end_quote

  "R"

  #+begin_quote
    for Right
  #+end_quote

  "BR"

  #+begin_quote
    for Bottom Right
  #+end_quote

  "B"

  #+begin_quote
    for Bottom
  #+end_quote

  "BL"

  #+begin_quote
    for Bottom Left
  #+end_quote

  "L"

  #+begin_quote
    for left
  #+end_quote

  "TL"

  #+begin_quote
    for Top Left
  #+end_quote

  The compass directions may be use as synonyms for the above
  directions, so for example "NE" is equal to "TR".

  Direction is case insensitive. The default direction is Top (North).
#+end_quote

-l lvls

#+begin_quote
  Numeric, nmber of levels the color will be scaled to. Default is 16.
#+end_quote

-c R G B

#+begin_quote
  What to use as the primary background color to scale. This color is
  scaled between the minimum intensity (min) and maximum intensity (max)
  from one end of the screen to the other as defined by dir. See below
  (-m & -M) for min & max. Default is Blue (0, 0, 255).
#+end_quote

-m min

#+begin_quote
  Minimum intensity (in percent) to scale color. Default 10%
#+end_quote

[-M max

#+begin_quote
  Maximum intensity (in percent) to scale color. Default 100%
#+end_quote

-s W H

#+begin_quote
  Size of image to create. Default 640 by 350.
#+end_quote

-h

#+begin_quote
  Print one line of command line help, similar to Usage above.
#+end_quote

If min == max = 100 (%) and lvls == 2 then boolean mask image of
specified size will be created - all foreground.

* AUTHOR
Gershon Elber.
