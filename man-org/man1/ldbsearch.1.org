#+TITLE: Man1 - ldbsearch.1
#+DESCRIPTION: Linux manpage for ldbsearch.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldbsearch - Search for records in a LDB database

* SYNOPSIS
*ldbsearch* [-h] [-s base|one|sub] [-b basedn] [-i] [-H LDB-URL]
[expression] [attributes]

* DESCRIPTION
ldbsearch searches a LDB database for records matching the specified
expression (see the ldapsearch(1) manpage for a description of the
expression format). For each record, the specified attributes are
printed.

* OPTIONS
-h

#+begin_quote
  Show list of available options.
#+end_quote

-H <ldb-url>

#+begin_quote
  LDB URL to connect to. See ldb(3) for details.
#+end_quote

-s one|sub|base

#+begin_quote
  Search scope to use. One-level, subtree or base.
#+end_quote

-i

#+begin_quote
  Read search expressions from stdin.
#+end_quote

-b basedn

#+begin_quote
  Specify Base DN to use.
#+end_quote

* ENVIRONMENT
LDB_URL

#+begin_quote
  LDB URL to connect to (can be overridden by using the -H command-line
  option.)
#+end_quote

* VERSION
This man page is correct for version 1.1 of LDB.

* SEE ALSO
ldb(3), ldbedit(1)

* AUTHOR
ldb was written by *Andrew Tridgell*[1].

If you wish to report a problem or make a suggestion then please see the
*http://ldb.samba.org/* web site for current contact and maintainer
information.

This manpage was written by Jelmer Vernooij.

* NOTES
-  1. :: Andrew Tridgell

  https://www.samba.org/~tridge/
