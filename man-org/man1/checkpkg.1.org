#+TITLE: Manpages - checkpkg.1
#+DESCRIPTION: Linux manpage for checkpkg.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
checkpkg - Compare the current build package with the repository version

* SYNOPSIS
checkpkg

* DESCRIPTION
Searches for a locally built package corresponding to the PKGBUILD, and
downloads the last version of that package from the Pacman repositories.
It then compares the list of .so files provided by each version of the
package and outputs if there are soname differences for the new package.
A directory is also created using mktemp with files containing a file
list for both packages and a library list for both packages.

* OPTIONS
*-r, --rmdir*

#+begin_quote
  Remove the temporary directory created to contain the file and library
  list of both packages.
#+end_quote

*-w, --warn*

#+begin_quote
  Print a warning instead of a regular message in case of soname
  differences.
#+end_quote

*-h, --help*

#+begin_quote
  Show a help text
#+end_quote

* SEE ALSO
*find-libprovides*(1)

* BUGS
Bugs can be reported on the bug tracker /https://bugs.archlinux.org/ in
the Arch Linux category and title prefixed with [devtools] or via
arch-projects@archlinux.org.

* AUTHORS
Maintainers:

#+begin_quote
  ·

  Aaron Griffin <aaronmgriffin@gmail.com>
#+end_quote

#+begin_quote
  ·

  Allan McRae <allan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dan McGee <dan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dave Reisner <dreisner@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Evangelos Foutras <evangelos@foutrelis.com>
#+end_quote

#+begin_quote
  ·

  Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
#+end_quote

#+begin_quote
  ·

  Jelle van der Waa <jelle@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Levente Polyak <anthraxx@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Pierre Schmitz <pierre@archlinux.de>
#+end_quote

#+begin_quote
  ·

  Sébastien Luttringer <seblu@seblu.net>
#+end_quote

#+begin_quote
  ·

  Sven-Hendrik Haase <svenstaro@gmail.com>
#+end_quote

#+begin_quote
  ·

  Thomas Bächler <thomas@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the devtools.git
repository.

Information about checkpkg.1 is found in manpage for: [[../for a locally built package corresponding to the PKGBUILD, and downloads the last version of that package from the Pacman repositories\&. It then compares the list of \&.so files provided by each version of the package and outputs if there are soname differences for the new package\&. A directory is also created using mktemp with files containing a file list for both packages and a library list for both packages\&.][for a locally built package corresponding to the PKGBUILD, and downloads the last version of that package from the Pacman repositories\&. It then compares the list of \&.so files provided by each version of the package and outputs if there are soname differences for the new package\&. A directory is also created using mktemp with files containing a file list for both packages and a library list for both packages\&.]]