#+TITLE: Man1 - ideviceimagemounter.1
#+DESCRIPTION: Linux manpage for ideviceimagemounter.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ideviceimagemounter - Mount disk images on the device.

* SYNOPSIS
*ideviceimagemounter* [OPTIONS] IMAGE_FILE IMAGE_SIGNATURE_FILE

* DESCRIPTION
Mounts the specified disk image on the device.

* OPTIONS
- *-u, --udid UDID* :: target specific device by UDID.

- *-n, --network* :: connect to network device.

- *-d, --debug* :: enable communication debugging.

- *-l, --list* :: list mount information

- *-t, --imagetype NAME* :: the image type to use, default is
  'Developer'

- *-x, --xml* :: use XML output

- *-h, --help* :: prints usage information

- *-v, --version* :: prints version information.

- *IMAGE_FILE* :: the image filename to mount

- *IMAGE_SIGNATURE_FILE* :: corresponding signature file for image
  filename

* AUTHOR
Nikias Bassen

Man page written to conform with Debian by Julien Lavergne.

* ON THE WEB
https://libimobiledevice.org

https://github.com/libimobiledevice/libimobiledevice
