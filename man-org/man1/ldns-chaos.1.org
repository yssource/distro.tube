#+TITLE: Man1 - ldns-chaos.1
#+DESCRIPTION: Linux manpage for ldns-chaos.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns-chaos - give some information about a nameserver

* SYNOPSIS
*ldns-chaos* /NAMESERVER/

* DESCRIPTION
*ldns-chaos* retrieves all the addresses of the nameserver and then
queries each address for its /version.bind/ and /hostname.bind/.

*ldns-chaos* is a bit more complex than *ldns-mx*.

* OPTIONS
*ldns-chaos* has no options.

* AUTHOR
Written by the ldns team as an example for ldns usage.

* REPORTING BUGS
Report bugs to <ldns-team@nlnetlabs.nl>.

* COPYRIGHT
Copyright (C) 2005 NLnet Labs. This is free software. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.
