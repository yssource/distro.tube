#+TITLE: Man1 - gtk-query-immodules-3.0.1
#+DESCRIPTION: Linux manpage for gtk-query-immodules-3.0.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gtk-query-immodules-3.0 - Input method module registration utility

* SYNOPSIS
*gtk-query-immodules-3.0* [--update-cache] [MODULE...]

* DESCRIPTION
*gtk-query-immodules-3.0* collects information about loadable input
method modules for GTK+ and writes it to the default cache file
location, or to stdout.

If called without arguments, it looks for modules in the GTK+ input
method module path.

If called with arguments, it looks for the specified modules. The
arguments may be absolute or relative paths.

Normally, the output of *gtk-query-immodules-3.0* is written to
/libdir//gtk-3.0/3.0.0/immodules.cache, where GTK+ looks for it by
default. If it is written to some other location, the
*GTK_IM_MODULE_FILE* environment variable can be set to point GTK+ at
the file.

* OPTIONS
--update-cache

#+begin_quote
  Write the output to the default cache location instead of stdout
#+end_quote

* FILES
/libdir//gtk-3.0/3.0.0/immodules.cache

#+begin_quote
  The default im cache file used by GTK+ applications
#+end_quote

* ENVIRONMENT
*GTK_PATH*

#+begin_quote
  Prepends directories to the input method module path
#+end_quote

*GTK_IM_MODULE_FILE*

#+begin_quote
  Specifies an alternative im module cache for GTK+ applications
#+end_quote
