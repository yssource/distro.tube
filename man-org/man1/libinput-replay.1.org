#+TITLE: Man1 - libinput-replay.1
#+DESCRIPTION: Linux manpage for libinput-replay.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libinput-replay - replay kernel events from a recording

* SYNOPSIS
*libinput replay [options] /recording/*

* DESCRIPTION
The *libinput replay* tool replays kernel events from a device recording
made by the *libinput record(1)* tool. This tool needs to run as root to
create a device and/or replay events.

If the recording contains more than one device, all devices are replayed
simultaneously.

* OPTIONS
- *--help* :: Print help

* NOTES
This tool replays events from a recording through the the kernel and is
independent of libinput. In other words, updating or otherwise changing
libinput will not alter the output from this tool. libinput itself does
not need to be in use to replay events.

This tool does not replay kernel-emulated key repeat events (events of
type /EV_KEY/ with a value of 2).

* LIBINPUT
Part of the *libinput(1)* suite
