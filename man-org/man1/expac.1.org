#+TITLE: Man1 - expac.1
#+DESCRIPTION: Linux manpage for expac.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
expac - alpm data extraction utility

* SYNOPSIS
Usage: /expac/ [options] <format> targets...

* DESCRIPTION
expac is a data extraction tool for alpm databases. It features
printf-like flexibility and aims to be used as a simple tool for other
pacman based utilities which don't link against the library. It uses
pacman.conf as a config file for locating and loading your local and
sync databases.

Invoking expac consists of supplying a format string, which is generally
described by one to many of the formatting tokens (see the *FORMATTING*
section), any relevant options and zero to many targets. The format
string *must* be the first non-option argument. Targets can be a simple
package name, a query string (in the case of a search), or in
repo/package syntax when the -sync option is supplied.

* OPTIONS
- -Q, --query :: Search the local database for provided targets. This is
  the default behavior.

- -S, --sync :: Search the sync databases for provided targets.

- -s, --search :: Search for packages matching the strings specified by
  targets. This is a boolean AND query and regex is allowed.

- -g, --group :: Return packages matching the specified targets as
  package groups.

- --config <file> :: Read from /file/ for alpm initialization instead of
  //etc/pacman.conf/.

- -H, --humansize <size> :: Format package sizes in SI units according
  to /size/. Valid options are: B, K, M, G, T, P, E, Z, Y

- -1, --readone :: Stop searching after the first result. This only has
  an effect on -S operations without -s.

- -d, --delim <string> :: Separate each package with the specified
  /string/. The default value is a newline character.

- -l, --listdelim <string> :: Separate each list item with the specified
  /string/. Lists are any interpreted sequence specified with a capital
  letter. The default value is two spaces.

- -p, --file :: Interpret targets as paths to local files.

- -t, --timefmt <format> :: Output time described by the specified
  /format/. This string is passed directly to *strftime* (3). The
  default format is =%c=.

- -v, --verbose :: Output more. `Package not found' errors will be
  shown, and empty field values will display as 'None'.

- -V, --version :: Display version information and quit.

- -h, --help :: Display the help message and quit.

* FORMATTING
The format argument allows the following interpreted sequences:

%a architecture %B backup files %b build date %C conflicts with (no
version strings) %D depends on %d description %E depends on (no version
strings) %e package base %f filename (only with -S) %F files (only with
-Q) %g base64 encoded PGP signature (only with -S) %G groups %H
conflicts with %h sha256sum %i has install scriptlet (only with -Q) %k
download size (only with -S) %l install date (only with -Q) %L licenses
%m install size %M modified backup files (only with -Q) %n package name
%N required by %O optional deps %o optional deps (no descriptions) %p
packager name %P provides %R replaces (no version strings) %r repo %s
md5sum %S provides (no version strings) %T replaces %u project URL %V
package validation method %v version %w install reason (only with -Q) %!
result number (auto-incremented counter, starts at 0) %% literal %

Note that for any lowercase tokens aside from =%m= and =%k=, full printf
support is allowed, e.g. %-20n. This does not apply to any list based,
date, or numerical output.

Standard backslash escape sequences are supported, as per *printf* (1).

* EXAMPLES
Emulate pacman's search function: $ expac -Ss %r/%n %v\n %d <search
terms>

List the oldest 10 installed packages (by build date): $ expac
--timefmt=%s %b\t%n | sort -n | head -10

* AUTHOR
Dave Reisner <d@falconindy.com>
