#+TITLE: Man1 - fluidsynth.1
#+DESCRIPTION: Linux manpage for fluidsynth.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FluidSynth - a SoundFont synthesizer

* SYNOPSIS
*fluidsynth* [/options/] [ SoundFonts ] [ midifiles ]

* DESCRIPTION
*FluidSynth* is a real-time MIDI synthesizer based on the SoundFont(R) 2
specifications. It can be used to render MIDI input or MIDI files to
audio. The MIDI events are read from a MIDI device. The sound is
rendered in real-time to the sound output device.

The easiest way to start the synthesizer is to give it a SoundFont on
the command line: 'fluidsynth soundfont.sf2'. fluidsynth will load the
SoundFont and read MIDI events from the default MIDI device using the
default MIDI driver. Once FluidSynth is running, it reads commands from
the stdin. There are commands to send MIDI events manually, to load or
unload SoundFonts, and so forth. All the available commands are
discussed below.

FluidSynth can also be used to play a list of MIDI files. Simply run
FluidSynth with the SoundFont and the list of MIDI files to play. In
this case you might not want to open the MIDI device to read external
events. Use the -n option to deactivate MIDI input. If you also want to
deactivate the use of the shell, start FluidSynth with the -i option:
'fluidsynth -ni soundfont.sf2 midifile1.mid midifile2.mid'.

Run fluidsynth with the --help option to check for changes in the list
of options.

* OPTIONS
*fluidsynth* accepts the following options:

- *-a, --audio-driver=[label]* :: The audio driver to use. "-a help" to
  list valid options

- *-c, --audio-bufcount=[count]* :: Number of audio buffers

- *-C, --chorus* :: Turn the chorus on or off [0|1|yes|no, default = on]

- *-d, --dump* :: Dump incoming and outgoing MIDI events to stdout

- *-E, --audio-file-endian* :: Audio file endian for fast rendering or
  aufile driver ("-E help" for list)

- *-f, --load-config* :: Load command configuration file (shell
  commands)

- *-F, --fast-render=[file]* :: Render MIDI file to raw audio data and
  store in [file]

- *-g, --gain* :: Set the master gain [0 < gain < 10, default = 0.2]

- *-G, --audio-groups* :: Defines the number of LADSPA audio nodes

- *-h, --help* :: Print out this help summary

- *-i, --no-shell* :: Don't read commands from the shell [default = yes]

- *-j, --connect-jack-outputs* :: Attempt to connect the jack outputs to
  the physical ports

- *-K, --midi-channels=[num]* :: The number of midi channels [default =
  16]

- *-l, --disable-lash* :: Don't connect to LASH server

- *-L, --audio-channels=[num]* :: The number of stereo audio channels
  [default = 1]

- *-m, --midi-driver=[label]* :: The name of the midi driver to use. "-m
  help" to list valid options.

- *-n, --no-midi-in* :: Don't create a midi driver to read MIDI input
  events [default = yes]

- *-o* :: Define a setting, -o name=value ("-o help" to dump current
  values)

- *-O, --audio-file-format* :: Audio file format for fast rendering or
  aufile driver ("-O help" for list)

- *-p, --portname=[label]* :: Set MIDI port name (alsa_seq, coremidi
  drivers)

- *-q, --quiet* :: Do not print welcome message or other informational
  output

- *-r, --sample-rate* :: Set the sample rate

- *-R, --reverb* :: Turn the reverb on or off [0|1|yes|no, default = on]

- *-s, --server* :: Start FluidSynth as a server process

- *-T, --audio-file-type* :: Audio file type for fast rendering or
  aufile driver ("T help" for list)

- *-v, --verbose* :: Print out verbose messages about midi events
  (synth.verbose=1) as well as other debug messages

- *-V, --version* :: Show version of program

- *-z, --audio-bufsize=[size]* :: Size of each audio buffer

* SETTINGS
The settings to be specified with -o are documented in the
fluidsettings.xml hopefully shipped with this distribution or online at
https://www.fluidsynth.org/api/fluidsettings.xml . We recommend viewing
this file in a webbrowser, favourably Firefox.

* SHELL COMMANDS
- *GENERAL* :: 

- *help* :: Prints out list of help topics (type "help <topic>" to view
  details on available commands)

- *quit* :: Quit the synthesizer

- *SOUNDFONTS* :: 

- *load filename* :: Load a SoundFont

- *unload number* :: Unload a SoundFont. The number is the index of the
  SoundFont on the stack.

- *fonts* :: Lists the current SoundFonts on the stack

- *inst number* :: Print out the available instruments for the
  SoundFont.

- *MIDI MESSAGES* :: 

- *noteon channel key velocity* :: Send a note-on event

- *noteoff channel key* :: Send a note-off event

- *cc channel ctrl value* :: Send a control change event

- *prog chan num* :: Send program-change message

- *select chan sfont bank prog* :: Combination of bank-select and
  program-change

- *channels* :: Print out the presets of all channels.

- *AUDIO SYNTHESIS* :: 

- *gain value* :: Set the master gain (0 < gain < 5)

- *interp num* :: Choose interpolation method for all channels

- *interpc chan num* :: Choose interpolation method for one channel

- *REVERB* :: 

- *set synth.reverb.active [0|1]* :: Turn the reverb on or off

- *set synth.reverb.room-size num* :: Change reverb room size

- *set synth.reverb.damp num* :: Change reverb damping

- *set synth.reverb.width num* :: Change reverb width

- *set synth.reverb.level num* :: Change reverb level

- *CHORUS* :: 

- *set synth.chorus.active [0|1]* :: Turn the chorus on or off

- *set synth.chorus.nr n* :: Use n delay lines (default 3)

- *set synth.chorus.level num* :: Set output level of each chorus line
  to num

- *set synth.chorus.speed num* :: Set mod speed of chorus to num (Hz)

- *set synth.chorus.depth num* :: Set chorus modulation depth to num
  (ms)

- *MIDI ROUTER* :: 

- *router_default* :: Reloads the default MIDI routing rules (input
  channels are mapped 1:1 to the synth)

- *router_clear* :: Deletes all MIDI routing rules.

- *router_begin [note|cc|prog|pbend|cpress|kpress]* :: Starts a new
  routing rule for events of the given type

- *router_chan min max mul add* :: Limits the rule for events on min <=
  chan <= max. If the channel falls into the window, it is multiplied by
  'mul', then 'add' is added.

- *router_par1 min max mul add* :: Limits parameter 1 (for example note
  number in a note events). Similar to router_chan.

- *router_par2 min max mul add* :: Limits parameter 2 (for example
  velocity in a note event). Similar to router_chan

- *router_end* :: Finishes the current rule and adds it to the router.

- *Router examples* :: 

- router_clear :: 

- router_begin note :: 

- router_chan 0 7 0 15 :: 

- router_end :: 

- Will accept only note events from the lower 8 MIDI :: channels.
  Regardless of the channel, the synthesizer plays the note on ch 15
  (synthchannel=midichannel*0+15)

- router_begin cc :: 

- router_chan 0 7 0 15 :: 

- router_par1 1 1 0 64 :: 

- router_add :: Configures the modulation wheel to act as sustain pedal
  (transforms CC 1 to CC 64 on the lower 8 MIDI channels, routes to
  ch 15)

* AUTHORS
Peter Hanappe <hanappe@fluid-synth.org>\\
Markus Nentwig <nentwig@users.sourceforge.net>\\
Antoine Schmitt <as@gratin.org>\\
Josh Green <jgreen@users.sourceforge.net>\\
Stephane Letz <letz@grame.fr>\\
Tom Moebert <tom[d0t]mbrt[ÄT]gmail[d0t]com>

Please check the AUTHORS and THANKS files for all credits

* DISCLAIMER
SoundFont(R) is a registered trademark of E-mu Systems, Inc.
