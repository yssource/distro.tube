#+TITLE: Man1 - imginfo.1
#+DESCRIPTION: Linux manpage for imginfo.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
imginfo - Image information utility

* SYNOPSIS
*imginfo* [/options/]

* DESCRIPTION
The *imginfo* command displays information about an image. Please use
the --help command line switch and the JasPer Software Reference Manual
for more information.

* SEE ALSO
/jasper/(1)

* AUTHOR
Michael D. Adams <mdadams@ieee.org> This manpage was initially written
by Roland Stigge <stigge@antcom.de> for the Debian Project.
