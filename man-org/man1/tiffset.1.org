#+TITLE: Man1 - tiffset.1
#+DESCRIPTION: Linux manpage for tiffset.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tiffset - set or unset a field in a

header

* SYNOPSIS
*tiffset* [ /options/ ] /filename.tif/

* DESCRIPTION
/Tiffset/ sets the value of a

header to a specified value or removes an existing setting.

* OPTIONS
- *-d*/ dirnumber/ :: change the current directory (starting at 0).

- *-s*/ tagnumber/* [*/ count/* ]*/ value .../ :: Set the value of the
  named tag to the value or values specified.

- *-sd*/ diroffset/ :: change the current directory by offset.

- *-sf*/ tagnumber filename/ :: Set the value of the tag to the contents
  of filename. This option is supported for ASCII tags only.

- *-u*/ tagnumber/ :: Unset the tag.

* EXAMPLES
The following example sets the image description tag (270) of a.tif to
the contents of the file descrip:

#+begin_quote
  #+begin_example
    tiffset -sf 270 descrip a.tif
  #+end_example
#+end_quote

The following example sets the artist tag (315) of a.tif to the string
``Anonymous'':

#+begin_quote
  #+begin_example
    tiffset -s 315 Anonymous a.tif
  #+end_example
#+end_quote

This example sets the resolution of the file a.tif to 300 dpi:

#+begin_quote
  #+begin_example
    tiffset -s 296 2 a.tif
    tiffset -s 282 300.0 a.tif
    tiffset -s 283 300.0 a.tif
  #+end_example
#+end_quote

Set the photometric interpretation of the third page of a.tif to
min-is-black (ie. inverts it):

#+begin_quote
  #+begin_example
    tiffset -d 2 -s 262 1 a.tif
  #+end_example
#+end_quote

* SEE ALSO
*tiffdump*(1), *tiffinfo*(1), *tiffcp*(1), *libtiff*(3TIFF)

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
