#+TITLE: Man1 - bmptoppm.1
#+DESCRIPTION: Linux manpage for bmptoppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*bmptoppm* - replaced by bmptopnm

* DESCRIPTION
This program is part of *Netpbm*(1)

*bmptoppm* was replaced in Netpbm 9.25 (March 2002) by *bmptopnm*(1)

*bmptopnm* is backward compatible with *bmptoppm* except that it
generates PBM and PGM output when it is more appropriate than PPM.
