#+TITLE: Man1 - irqtop.1
#+DESCRIPTION: Linux manpage for irqtop.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
irqtop - utility to display kernel interrupt information

* SYNOPSIS
*irqtop* [options]

* DESCRIPTION
Display kernel interrupt counter information in *top*(1) style view.

The default output is subject to change. So whenever possible, you
should avoid using default outputs in your scripts. Always explicitly
define expected columns by using *--output*.

* OPTIONS
*-o*, *--output* /list/

#+begin_quote
  Specify which output columns to print. Use *--help* to get a list of
  all supported columns. The default list of columns may be extended if
  list is specified in the format /+list/.
#+end_quote

*-d*, *--delay* /seconds/

#+begin_quote
  Update interrupt output every /seconds/ intervals.
#+end_quote

*-s*, *--sort* /column/

#+begin_quote
  Specify sort criteria by column name. See *--help* output to get
  column names. The sort criteria may be changes in interactive mode.
#+end_quote

*-S*, *--softirq*

#+begin_quote
  Show softirqs information.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* INTERACTIVE MODE KEY COMMANDS
*i*

#+begin_quote
  sort by short irq name or number field
#+end_quote

*t*

#+begin_quote
  sort by total count of interrupts (the default)
#+end_quote

*d*

#+begin_quote
  sort by delta count of interrupts
#+end_quote

*n*

#+begin_quote
  sort by long descriptive name field
#+end_quote

*q Q*

#+begin_quote
  stop updates and exit program
#+end_quote

* AUTHORS
* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *irqtop* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
