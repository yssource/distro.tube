#+TITLE: Man1 - dump.erofs.1
#+DESCRIPTION: Linux manpage for dump.erofs.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
dump.erofs - retrieve directory and file entries, show specific file or
overall disk statistics information from an EROFS-formatted image.

* SYNOPSIS
*dump.erofs* [/OPTIONS/] /IMAGE/

* DESCRIPTION
*dump.erofs* is used to retrieve erofs metadata from /IMAGE/ and
demonstrate 1) overall disk statistics, 2) superblock information, 3)
file information of the given inode NID, 4) file extent information of
the given inode NID.

* OPTIONS
- *--device=*/path/ :: Specify an extra device to be used together. You
  may give multiple `--device' options in the correct order.

- *--nid=*/NID/ :: Specify an inode NID in order to print its file
  information.

- *-e* :: Show the file extent information. The option depends on option
  --nid to specify NID.

- *-V* :: Print the version number and exit.

- *-s* :: Show superblock information of the an EROFS-formatted image.

- *-S* :: Show EROFS disk statistics, including file type/size
  distribution, number of (un)compressed files, compression ratio of the
  whole image, etc.

* AUTHOR
Initial code was written by Wang Qi <mpiglet@outlook.com>, Guo Xuenan
<guoxuenan@huawei.com>.

This manual page was written by Guo Xuenan <guoxuenan@huawei.com>

* AVAILABILITY
*dump.erofs* is part of erofs-utils package and is available from
git://git.kernel.org/pub/scm/linux/kernel/git/xiang/erofs-utils.git.

* SEE ALSO
*mkfs.erofs(1),* *fsck.erofs(1)*
