#+TITLE: Man1 - ptardiff.1perl
#+DESCRIPTION: Linux manpage for ptardiff.1perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ptardiff - program that diffs an extracted archive against an
unextracted one

* DESCRIPTION
ptardiff is a small program that diffs an extracted archive against an
unextracted one, using the perl module Archive::Tar. This effectively
lets you view changes made to an archives contents. Provide the progam
with an ARCHIVE_FILE and it will look up all the files with in the
archive, scan the current working directory for a file with the name and
diff it against the contents of the archive.

* SYNOPSIS
ptardiff ARCHIVE_FILE ptardiff -h $ tar -xzf Acme-Buffy-1.3.tar.gz $ vi
Acme-Buffy-1.3/README [...] $ ptardiff Acme-Buffy-1.3.tar.gz >
README.patch

* OPTIONS
h Prints this help message

* SEE ALSO
*tar* (1), Archive::Tar.
