#+TITLE: Man1 - llvm-nm.1
#+DESCRIPTION: Linux manpage for llvm-nm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-nm - list LLVM bitcode and object file's symbol table

* SYNOPSIS
*llvm-nm* [/options/] [/filenames.../]

* DESCRIPTION
The *llvm-nm* utility lists the names of symbols from LLVM bitcode
files, object files, and archives. Each symbol is listed along with some
simple information about its provenance. If no filename is specified,
/a.out/ is used as the input. If /-/ is used as a filename, *llvm-nm*
will read a file from its standard input stream.

*llvm-nm*'s default output format is the traditional BSD *nm* output
format. Each such output record consists of an (optional) 8-digit
hexadecimal address, followed by a type code character, followed by a
name, for each symbol. One record is printed per line; fields are
separated by spaces. When the address is omitted, it is replaced by 8
spaces.

The supported type code characters are as follows. Where both lower and
upper-case characters are listed for the same meaning, a lower-case
character represents a local symbol, whilst an upper-case character
represents a global (external) symbol:

a, A

#+begin_quote

  #+begin_quote
    Absolute symbol.
  #+end_quote
#+end_quote

b, B

#+begin_quote

  #+begin_quote
    Uninitialized data (bss) object.
  #+end_quote
#+end_quote

C

#+begin_quote

  #+begin_quote
    Common symbol. Multiple definitions link together into one
    definition.
  #+end_quote
#+end_quote

d, D

#+begin_quote

  #+begin_quote
    Writable data object.
  #+end_quote
#+end_quote

i, I

#+begin_quote

  #+begin_quote
    COFF: .idata symbol or symbol in a section with IMAGE_SCN_LNK_INFO
    set.
  #+end_quote
#+end_quote

n

#+begin_quote

  #+begin_quote
    ELF: local symbol from non-alloc section.

    COFF: debug symbol.
  #+end_quote
#+end_quote

N

#+begin_quote

  #+begin_quote
    ELF: debug section symbol, or global symbol from non-alloc section.
  #+end_quote
#+end_quote

s, S

#+begin_quote

  #+begin_quote
    COFF: section symbol.

    Mach-O: absolute symbol or symbol from a section other than
    __TEXT_EXEC __text, __TEXT __text, __DATA __data, or __DATA __bss.
  #+end_quote
#+end_quote

r, R

#+begin_quote

  #+begin_quote
    Read-only data object.
  #+end_quote
#+end_quote

t, T

#+begin_quote

  #+begin_quote
    Code (text) object.
  #+end_quote
#+end_quote

u

#+begin_quote

  #+begin_quote
    ELF: GNU unique symbol.
  #+end_quote
#+end_quote

U

#+begin_quote

  #+begin_quote
    Named object is undefined in this file.
  #+end_quote
#+end_quote

v

#+begin_quote

  #+begin_quote
    ELF: Undefined weak object. It is not a link failure if the object
    is not defined.
  #+end_quote
#+end_quote

V

#+begin_quote

  #+begin_quote
    ELF: Defined weak object symbol. This definition will only be used
    if no regular definitions exist in a link. If multiple weak
    definitions and no regular definitions exist, one of the weak
    definitions will be used.
  #+end_quote
#+end_quote

w

#+begin_quote

  #+begin_quote
    Undefined weak symbol other than an ELF object symbol. It is not a
    link failure if the symbol is not defined.
  #+end_quote
#+end_quote

W

#+begin_quote

  #+begin_quote
    Defined weak symbol other than an ELF object symbol. This definition
    will only be used if no regular definitions exist in a link. If
    multiple weak definitions and no regular definitions exist, one of
    the weak definitions will be used.
  #+end_quote
#+end_quote

-

#+begin_quote

  #+begin_quote
    Mach-O: N_STAB symbol.
  #+end_quote
#+end_quote

?

#+begin_quote

  #+begin_quote
    Something unrecognizable.
  #+end_quote
#+end_quote

Because LLVM bitcode files typically contain objects that are not
considered to have addresses until they are linked into an executable
image or dynamically compiled "just-in-time", *llvm-nm* does not print
an address for any symbol in an LLVM bitcode file, even symbols which
are defined in the bitcode file.

* OPTIONS

#+begin_quote
  - *-B* :: Use BSD output format. Alias for *--format=bsd*.
#+end_quote

#+begin_quote
  - *--debug-syms, -a* :: Show all symbols, even those usually
    suppressed.
#+end_quote

#+begin_quote
  - *--defined-only* :: Print only symbols defined in this file.
#+end_quote

#+begin_quote
  - *--demangle, -C* :: Demangle symbol names.
#+end_quote

#+begin_quote
  - *--dynamic, -D* :: Display dynamic symbols instead of normal
    symbols.
#+end_quote

#+begin_quote
  - *--extern-only, -g* :: Print only symbols whose definitions are
    external; that is, accessible from other files.
#+end_quote

#+begin_quote
  - *--format=<format>, -f* :: Select an output format; /format/ may be
    /sysv/, /posix/, /darwin/, /bsd/ or /just-symbols/. The default is
    /bsd/.
#+end_quote

#+begin_quote
  - *--help, -h* :: Print a summary of command-line options and their
    meanings.
#+end_quote

#+begin_quote
  - *-j* :: Print just the symbol names. Alias for
    /--format=just-symbols`/.
#+end_quote

#+begin_quote
  - *-m* :: Use Darwin format. Alias for *--format=darwin*.
#+end_quote

#+begin_quote
  - *--no-demangle* :: Don't demangle symbol names. This is the default.
#+end_quote

#+begin_quote
  - *--no-llvm-bc* :: Disable the LLVM bitcode reader.
#+end_quote

#+begin_quote
  - *--no-sort, -p* :: Show symbols in the order encountered.
#+end_quote

#+begin_quote
  - *--no-weak* :: Don't print weak symbols.
#+end_quote

#+begin_quote
  - *--numeric-sort, -n, -v* :: Sort symbols by address.
#+end_quote

#+begin_quote
  - *--portability, -P* :: Use POSIX.2 output format. Alias for
    *--format=posix*.
#+end_quote

#+begin_quote
  - *--print-armap* :: Print the archive symbol table, in addition to
    the symbols.
#+end_quote

#+begin_quote
  - *--print-file-name, -A, -o* :: Precede each symbol with the file it
    came from.
#+end_quote

#+begin_quote
  - *--print-size, -S* :: Show symbol size as well as address (not
    applicable for Mach-O).
#+end_quote

#+begin_quote
  - *--quiet* :: Suppress 'no symbols' diagnostic.
#+end_quote

#+begin_quote
  - *--radix=<RADIX>, -t* :: Specify the radix of the symbol
    address(es). Values accepted are /d/ (decimal), /x/ (hexadecimal)
    and /o/ (octal).
#+end_quote

#+begin_quote
  - *--reverse-sort, -r* :: Sort symbols in reverse order.
#+end_quote

#+begin_quote
  - *--size-sort* :: Sort symbols by size.
#+end_quote

#+begin_quote
  - *--special-syms* :: Do not filter special symbols from the output.
#+end_quote

#+begin_quote
  - *--undefined-only, -u* :: Print only undefined symbols.
#+end_quote

#+begin_quote
  - *--version, -V* :: Display the version of the *llvm-nm* executable,
    then exit. Does not stack with other commands.
#+end_quote

#+begin_quote
  - *@<FILE>* :: Read command-line options from response file /<FILE>/.
#+end_quote

* MACH-O SPECIFIC OPTIONS

#+begin_quote
  - *--add-dyldinfo* :: Add symbols from the dyldinfo, if they are not
    already in the symbol table. This is the default.
#+end_quote

#+begin_quote
  - *--add-inlinedinfo* :: Add symbols from the inlined libraries, TBD
    file inputs only.
#+end_quote

#+begin_quote
  - *--arch=<arch1[,arch2,...]>* :: Dump the symbols from the specified
    architecture(s).
#+end_quote

#+begin_quote
  - *--dyldinfo-only* :: Dump only symbols from the dyldinfo.
#+end_quote

#+begin_quote
  - *--no-dyldinfo* :: Do not add any symbols from the dyldinfo.
#+end_quote

#+begin_quote
  - *-s <segment> <section>* :: Dump only symbols from this segment and
    section name.
#+end_quote

#+begin_quote
  - *-x* :: Print symbol entry in hex.
#+end_quote

* BUGS

#+begin_quote

  #+begin_quote

    #+begin_quote

      - *llvm-nm* does not support the full set of arguments that GNU
        *nm* does.
    #+end_quote
  #+end_quote
#+end_quote

* EXIT STATUS
*llvm-nm* exits with an exit code of zero.

* SEE ALSO
*llvm-ar(1)*, *llvm-objdump(1)*, *llvm-readelf(1)*, *llvm-readobj(1)*

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
