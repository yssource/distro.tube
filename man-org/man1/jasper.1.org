#+TITLE: Man1 - jasper.1
#+DESCRIPTION: Linux manpage for jasper.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
jasper - File format converter specialized in JPEG-2000 encoding

* SYNOPSIS
*jasper* [/options/]

* DESCRIPTION
The *jasper* command converts to and from JPEG-2000 files. Please use
the --help command line switch and the JasPer Software Reference Manual
for more information.

* AUTHOR
Michael D. Adams <mdadams@ieee.org> This manpage was initially written
by Roland Stigge <stigge@antcom.de> for the Debian Project.
