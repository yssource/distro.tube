#+TITLE: Man1 - llvm-readelf.1
#+DESCRIPTION: Linux manpage for llvm-readelf.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-readelf - GNU-style LLVM Object Reader

* SYNOPSIS
*llvm-readelf* [/options/] [/input.../]

* DESCRIPTION
The *llvm-readelf* tool displays low-level format-specific information
about one or more object files.

If *input* is "*-*", *llvm-readelf* reads from standard input.
Otherwise, it will read from the specified *filenames*.

* OPTIONS

#+begin_quote
  - *--all* :: Equivalent to specifying all the main display options
    relevant to the file format.
#+end_quote

#+begin_quote
  - *--addrsig* :: Display the address-significance table.
#+end_quote

#+begin_quote
  - *--arch-specific, -A* :: Display architecture-specific information,
    e.g. the ARM attributes section on ARM.
#+end_quote

#+begin_quote
  - *--bb-addr-map* :: Display the contents of the basic block address
    map section(s), which contain the address of each function, along
    with the relative offset of each basic block.
#+end_quote

#+begin_quote
  - *--demangle, -C* :: Display demangled symbol names in the output.
#+end_quote

#+begin_quote
  - *--dyn-relocations* :: Display the dynamic relocation entries.
#+end_quote

#+begin_quote
  - *--dyn-symbols, --dyn-syms* :: Display the dynamic symbol table.
#+end_quote

#+begin_quote
  - *--dynamic-table, --dynamic, -d* :: Display the dynamic table.
#+end_quote

#+begin_quote
  - *--cg-profile* :: Display the callgraph profile section.
#+end_quote

#+begin_quote
  - *--histogram, -I* :: Display a bucket list histogram for dynamic
    symbol hash tables.
#+end_quote

#+begin_quote
  - *--elf-linker-options* :: Display the linker options section.
#+end_quote

#+begin_quote
  - *--elf-output-style=<value>* :: Format ELF information in the
    specified style. Valid options are *LLVM* and *GNU*. *LLVM* output
    is an expanded and structured format, whilst *GNU* (the default)
    output mimics the equivalent GNU *readelf* output.
#+end_quote

#+begin_quote
  - *--section-groups, -g* :: Display section groups.
#+end_quote

#+begin_quote
  - *--expand-relocs* :: When used with /--relocations/, display each
    relocation in an expanded multi-line format.
#+end_quote

#+begin_quote
  - *--file-header, -h* :: Display file headers.
#+end_quote

#+begin_quote
  - *--gnu-hash-table* :: Display the GNU hash table for dynamic
    symbols.
#+end_quote

#+begin_quote
  - *--hash-symbols* :: Display the expanded hash table with dynamic
    symbol data.
#+end_quote

#+begin_quote
  - *--hash-table* :: Display the hash table for dynamic symbols.
#+end_quote

#+begin_quote
  - *--headers, -e* :: Equivalent to setting: /--file-header/,
    /--program-headers/, and /--sections/.
#+end_quote

#+begin_quote
  - *--help* :: Display a summary of command line options.
#+end_quote

#+begin_quote
  - *--hex-dump=<section[,section,...]>, -x* :: Display the specified
    section(s) as hexadecimal bytes. *section* may be a section index or
    section name.
#+end_quote

#+begin_quote
  - *--needed-libs* :: Display the needed libraries.
#+end_quote

#+begin_quote
  - *--notes, -n* :: Display all notes.
#+end_quote

#+begin_quote
  - *--program-headers, --segments, -l* :: Display the program headers.
#+end_quote

#+begin_quote
  - *--raw-relr* :: Do not decode relocations in RELR relocation
    sections when displaying them.
#+end_quote

#+begin_quote
  - *--relocations, --relocs, -r* :: Display the relocation entries in
    the file.
#+end_quote

#+begin_quote
  - *--sections, --section-headers, -S* :: Display all sections.
#+end_quote

#+begin_quote
  - *--section-data* :: When used with /--sections/, display section
    data for each section shown. This option has no effect for GNU style
    output.
#+end_quote

#+begin_quote
  - *--section-details, -t* :: Display all section details. Used as an
    alternative to /--sections/.
#+end_quote

#+begin_quote
  - *--section-mapping* :: Display the section to segment mapping.
#+end_quote

#+begin_quote
  - *--section-relocations* :: When used with /--sections/, display
    relocations for each section shown. This option has no effect for
    GNU style output.
#+end_quote

#+begin_quote
  - *--section-symbols* :: When used with /--sections/, display symbols
    for each section shown. This option has no effect for GNU style
    output.
#+end_quote

#+begin_quote
  - *--stackmap* :: Display contents of the stackmap section.
#+end_quote

#+begin_quote
  - *--stack-sizes* :: Display the contents of the stack sizes
    section(s), i.e. pairs of function names and the size of their stack
    frames. Currently only implemented for GNU style output.
#+end_quote

#+begin_quote
  - *--string-dump=<section[,section,...]>, -p* :: Display the specified
    section(s) as a list of strings. *section* may be a section index or
    section name.
#+end_quote

#+begin_quote
  - *--symbols, --syms, -s* :: Display the symbol table.
#+end_quote

#+begin_quote
  - *--unwind, -u* :: Display unwind information.
#+end_quote

#+begin_quote
  - *--version* :: Display the version of the *llvm-readelf* executable.
#+end_quote

#+begin_quote
  - *--version-info, -V* :: Display version sections.
#+end_quote

#+begin_quote
  - *@<FILE>* :: Read command-line options from response file /<FILE>/.
#+end_quote

* EXIT STATUS
*llvm-readelf* returns 0 under normal operation. It returns a non-zero
exit code if there were any errors.

* SEE ALSO
*llvm-nm(1)*, *llvm-objdump(1)*, *llvm-readobj(1)*

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
