#+TITLE: Man1 - xkbcli-list.1
#+DESCRIPTION: Linux manpage for xkbcli-list.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is a commandline tool to list available model, layout, variant and
option (MLVO) values from the XKB registry.

Positional arguments provided on the commandline are treated as XKB base
directory installations.

Print help and exit

Increase verbosity, use multiple times for debugging output

Load the ruleset with the given name

Do not load the default XKB include paths

Load exotic (extra) layouts
