#+TITLE: Man1 - vifm-convert-dircolors.1
#+DESCRIPTION: Linux manpage for vifm-convert-dircolors.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vifm-convert-dircolors - converts ls colorscheme to vifm colorscheme

* SYNOPSIS
*vifm-convert-dircolors* [/-h/|/--help/] [/-e/|/--environment/]
[/-r/|/--human-readable/] [/dircolors_file/]

* DESCRIPTION
Converts ls file highlighting configuration (dircolors) into set of vifm
highlight commands. Outputs results to standard output.

* OPTIONS
- *-h, --help* :: Displays brief help message.

- *-e, --environment* :: Instructs the script to use *$LS_COLORS*
  environment variable instead of data from file or standard input
  stream.

- *-r, --human-readable* :: Output patterns on separate lines.

- *dircolors_file* :: Converting of colorscheme file only.

When both *--environment* option and *dircolors_file* are absent,
standard input stream is read.

* EXAMPLES
Parse *$LS_COLORS* and append results of conversion to
/~/.vifm/colors/Default/:

#+begin_example
   vifm-convert-dircolors -e >> ~/.vifm/colors/Default
#+end_example

Parse //etc/DIR_COLORS/ and store results of conversion to
/~/.vifm/colors/ls-based/:

#+begin_example
   vifm-convert-dircolors /etc/DIR_COLORS > ~/.vifm/colors/ls-based
#+end_example

* SEE ALSO
*dircolors*(1), *ls*(1), *vifm*(1)

* AUTHOR
This manual page was written by xaizek <xaizek@posteo.net>.
