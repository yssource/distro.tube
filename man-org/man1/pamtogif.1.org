#+TITLE: Man1 - pamtogif.1
#+DESCRIPTION: Linux manpage for pamtogif.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pamtogif - convert a Netpbm image to a GIF image

* SYNOPSIS
*pamtogif*

[*-interlace*]

[*-sort*]

[*-mapfile=*/mapfile/] [*-transparent=*[*=*]/color/]

[*-alphacolor=*/color/]

[*-comment=*/text/]

[*-nolzw*]

[*-aspect=*/fraction/]

[*-verbose*] [/netpbmfile/]

All options can be abbreviated to their shortest unique prefix. You may
use two hyphens instead of one to designate an option. You may use
either white space or an equals sign between an option name and its
value.

* DESCRIPTION
This program is part of *Netpbm*(1)

*pamtogif* reads a Netpbm image as input and produces a GIF file as
output.

This program creates only individual GIF images. To combine multiple GIF
images into an animated GIF, use
[[http://www.lcdf.org/gifsicle/][*gifsicle*]] (not part of the Netpbm
package).

*pamtogif* creates either an original GIF87 format GIF file or the newer
GIF89 format. It creates GIF89 when you request features that were new
with GIF89, to wit the *-transparent* or *-comment* options. Otherwise,
it creates GIF87. Really old GIF readers conceivably could not recognize
GIF89.

The GIF format is not capable of representing an image with more than
256 colors in it (it contains a color map with a maximum size of 256).
If the image you want to convert has more colors than that (*ppmhist*
can tell you), you can use *pnmquant* to reduce it to 256.

If your input image is a PAM with transparency information, *ppmtogif*
uses one entry in the GIF colormap specifically for the transparent
pixels, so you can have at most 255 opaque colors. In contrast, if you
use the *-transparent* option, one of the colors from the input becomes
transparent, so the limit is still 256.

*pamtogif* was new in Netpbm 10.37 (December 2006). In older Netpbm, use
*ppmtogif*.

* OPTIONS
- *-interlace* :: Produce an interlaced GIF file.

- *-sort* :: Produce a GIF file with a color map sorted in a predictable
  order.

This does /not/ produce the sorted color map which is part of the GIF
format. That kind of sorted color map is one where the colors are sorted
according to how important they are, and the GIF header tells the viewer
that it is sorted that way. Its purpose is to allow the viewer to use
fewer colors than are in the color map if it is not capable of
displaying all the colors.

What this option produces is a color map sorted by red value, then
green, then blue. That can be useful in analyzing GIF images,
particularly those made with two versions of the program, because it
removes some of the variability.

- *-mapfile=*/mapfile/ :: Use the colors found in the file /mapfile/ to
  create the colormap in the GIF file, instead of the colors from
  /netpbmfile/. /mapfile/ can be any PPM file; all that matters is the
  colors in it. If the colors in /netpbmfile/ do not match those in
  /mapfile/, *pamtogif* matches them to a 'best match.' You can obtain a
  much better result by using *pnmremap* to change the colors in the
  input to those in the map file.

The /mapfile/ file is not a palette file, just an image whose colors you
want to use. The order of colors in the GIF palette have nothing to do
with where they appear in the /mapfile/ image, and duplication of colors
in the image is irrelevant.

The map file's depth must match the number of color components in the
input (which is not necessarily the same as the input's depth -- the
input might have an alpha plane in addition). If your map file does not,
or it /might/ not, run your input through *pnmremap* using the same map
file so that it does.

- *-transparent=*/color/ :: *pamtogif* marks the specified color as
  transparent in the GIF image.

If you don't specify *-transparent*, *pamtogif* does not mark any color
transparent (except as indicated by the transparency information in the
input file).

Specify the color (/color/) as described for the
[[file:libppm.html#colorname][argument of the *ppm_parsecolor()* library
routine]] .

If the color you specify is not present in the image, *pamtogif* selects
instead the color in the image that is closest to the one you specify.
Closeness is measured as a Cartesian distance between colors in RGB
space. If multiple colors are equidistant, *pamtogif* chooses one of
them arbitrarily.

However, if you prefix your color specification with '=', e.g.
*-transparent==red*, only the exact color you specify will be
transparent. If that color does not appear in the image, there will be
no transparency. *pamtogif* issues an information message when this is
the case.

When you specify *-transparent*, *pamtogif* ignores explicit
transparency information (the 'alpha channel') in the input image.

- *-alpha=*/pgmfile/ :: There is no *-alpha* option. *pamtogif*'s
  predecessor had such an option because it was not capable of taking
  PAM input that contains a transparency (alpha) plane, so one used this
  option to supply a transparency plane as a separate PGM file.

This option names a PGM file that contains an alpha mask for the image.
*pamtogif* creates fully transparent pixels wherever the alpha mask
indicates transparency greater than 50%. The color of those pixels is
that specified by the *-alphacolor* option, or black by default.

To do this, *pamtogif* creates an entry in the GIF colormap in addition
to the entries for colors that are actually in the image. It marks that
colormap entry as transparent and uses that colormap index in the output
image to create a transparent pixel.

The alpha image must be the same dimensions as the input image, but may
have any maxval. White means opaque and black means transparent.

You cannot specify both *-transparent* and *-alpha*.

- *-alphacolor=*/color/ :: This specifies the foreground color for
  transparent pixels. A viewer may use the foreground color for a
  transparent pixel if it chooses not to have another color 'show
  through.'. The default is black.

This applies only to pixels that are transparent in the GIF because they
are transparent in the Netpbm input. If a GIF pixel is transparent
because of the *-transparent* option, the foreground color is the color
indicated by that option.

Note that in GIF, all transparent pixels have the same foreground color.
(There is only one entry in the GIF colormap for transparent pixels).

Specify the color (/color/) as described for the
[[file:libppm.html#colorname][argument of the *ppm_parsecolor()* library
routine]] .

- *-comment=*/text/ :: Include a comment in the GIF output with comment
  text /text/.

Without this option, there are no comments in the output.

Note that in a command shell, you'll have to use quotation marks around
/text/ if it contains characters (e.g. space) that would make the shell
think it is multiple arguments:

#+begin_example
  $ pamtogif -comment "this is a comment" <xxx.ppm >xxx.gif
#+end_example

- *-nolzw* :: This option is mainly of historical interest -- it
  involves use of a patent that is now expired.

This option causes the GIF output, and thus *pamtogif*, not to use LZW
(Lempel-Ziv) compression. As a result, the image file is larger and,
before the patent expired, no royalties would be owed to the holder of
the patent on LZW. See the section LICENSE below.

LZW is a method for combining the information from multiple pixels into
a single GIF code. With the *-nolzw* option, *pamtogif* creates one GIF
code per pixel, so it is not doing any compression and not using LZW.
However, any GIF decoder, whether it uses an LZW decompressor or not,
will correctly decode this uncompressed format. An LZW decompressor
would see this as a particular case of LZW compression.

Note that if someone uses an LZW decompressor such as the one in
*giftopnm* or pretty much any graphics display program to process the
output of *pamtogif -nolzw *, he is then using the LZW patent. But the
patent holder expressed far less interest in enforcing the patent on
decoding than on encoding.

- *-aspect=*/fraction/ :: This is the aspect ratio of the pixels of the
  image. Its only effect is to record that information in the GIF for
  use by whatever interprets the GIF. Note that this feature of GIF is
  hardly ever used and most GIF decoders ignore this information and
  assume pixels are square.

Pixels in a Netpbm image do not have aspect ratios; there is always a
one-one correspondence between GIF pixels and Netpbm pixels.

The aspect ratio is the quotient of width divided by height. GIF allows
aspect ratios from 0.25 (1:4) to 4 (4:1) in increments of 1/64.
*pamtogif* implements a natural extension of GIF that allows an aspect
ratio up to 4 14/64. If you specify anything outside this range,
*pamtogif* fails. *pamtogif* rounds /fraction/ to the nearest 1/64.

The default is square (1.0).

This option was new in Netpbm 10.38 (March 2007). Before that, the
pixels are always square.

- *-verbose* :: This option causes *pamtogif* to display information
  about the conversion process and the image it produces.

* SEE ALSO
*giftopnm*(1) , *pnmremap*(1) , *ppmtogif*(1) ,

*gifsicle* [[http://www.lcdf.org/gifsicle]] , *pnm*(5) , *pam*(5)

* HISTORY
*pamtogif* was new in Netpbm 10.37 (December 2006). It replaced
*ppmtogif*, which created GIF images for Pbmplus/Netpbm users
since 1989.

The main outward change in the conversion from *ppmtogif* to *pamtogif*
was that *pamtogif* was able to use transparency information ('alpha
channel') in PAM input, whereas with *ppmtogif*, one had to supply the
transparency mask in a separate pseudo-PGM image (via the *-alpha*
option).

Jef Poskanzer wrote *ppmtogif* in 1989, and it has always been a
cornerstone of Pbmplus/Netpbm because GIF is such a popular image
format. Jef based the LZW encoding on GIFENCOD by David Rowley
</mgardi@watdcsu.waterloo.edu/>. Jef included GIFENCOD's GIFCOMPR.C file
pretty much whole. Rowley, in turn, adapted the LZW compression code
from classic Unix *compress*, which used techniques described in IEEE
Computer, June 1984.

Jef's *ppmtogif* notably lacked the ability to use a transparency mask
with it. You could create transparent pixels in a GIF, but only with the
*-transparent* option, which allowed one to specify that all pixels of a
certain color in the input were to be transparent. Bryan Henderson added
the *-alpha* option in July 2001 so you could supply a mask image that
indicates exactly which pixels are to be transparent, and those pixels
could have the same color as other opaque ones.

Bryan Henderson added another significant piece of code and function in
October 2001: the ability to generate a GIF without using the LZW patent
-- an uncompressed GIF. This was very important to many people at the
time because the GIF patent was still in force, and this allowed them to
make an image that any GIF viewer could display, royalty-free. Bryan
adapted code from the Independent JPEG Group's *djpeg* for that.

There is no code in *pamtogif* from Jef's original, but Jef may still
hold copyright over it because of the way in which it evolved. Virtually
all of the code in *pamtogif* was written by Bryan Henderson and
contributed to the public domain.

* LICENSE
If you use *pamtogif* without the *-nolzw* option, you are using a
patent on the LZW compression method which is owned by Unisys. The
patent has expired (in 2003 in the US and in 2004 elsewhere), so it
doesn't matter. While the patent was in force, most people who used
*pamtogif* and similar programs did so without a license from Unisys to
do so. Unisys typically asked $5000 for a license for trivial use of the
patent. Unisys never enforced the patent against trivial users.

Rumor has it that IBM also owns or owned a patent covering *pamtogif*.

A replacement for the GIF format that never required any patents to use
is the PNG format.
