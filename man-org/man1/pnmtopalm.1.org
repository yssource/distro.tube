#+TITLE: Man1 - pnmtopalm.1
#+DESCRIPTION: Linux manpage for pnmtopalm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pnmtopalm - convert a PNM image to a Palm Bitmap

* SYNOPSIS
*pnmtopalm*

[*-verbose*]

[*-depth=*/N/]

[*-maxdepth=*/N/]

[*-colormap*]

[*-transparent=*/color/]

[*-density=*/N/]

[*-offset*]

[*-withdummy*] [*-scanline-compression* | *-rle-compression* |
*-packbits-compression*]

[/pnmfile/]

Minimum unique abbreviation of option is acceptable. You may use double
hyphens instead of single hyphen to denote options. You may use white
space in place of the equals sign to separate an option name from its
value.

* DESCRIPTION
This program is part of *Netpbm*(1)

*pnmtopalm* reads a PNM image as input, from Standard Input or /pnmfile/
and produces a Palm Bitmap as output.

Palm Bitmap files are either grayscale files 1, 2, or 4 bits wide, or
color files 8 bits wide, so *pnmtopalm* automatically scales colors to
have an appropriate maxval, unless you specify a depth or max depth.
Input files must have an appropriate number and set of colors for the
selected output constraints.

This often means that you should run the PNM image through *pnmquant* or
*pnmremap* before you pass it to *pnmtopalm*. Netpbm comes with several
colormap files you can use with *pnmremap* for this purpose. They are
/palmgray2.map/ (4 shades of gray for a depth of 2), /palmgray4.map/ (16
shades of gray for a depth of 4), and /palmcolor8.map/ (232 colors in
default Palm colormap). In a standard Netpbm installation, these are in
the Netpbm data directory, and you can find the Netpbm data directory
with a *netpbm-config* --datadir shell command.

Example:

#+begin_example
    pnmremap myimage.ppm \
             -mapfile=$(netpbm-config --datadir)/palmgray2.map \
    | pnmtopalm -depth=2 >myimage.palm
#+end_example

** Palm Bitmap Version
*pnmtopalm* generates a Version 0, 1, 2, or 3 Palm Bitmap. It generates
the oldest (lowest) version it can for the given image and the options
you specify.

- If you specify a density (*-density* option) higher than 'low,' the
  version is at least 3.

- If you specify transparency (*-transparent* option) or any
  compression, the version is at least 2.

- If you specify a custom colormap (*-colormap* option), the version is
  at least 1.

- If the image has more than one bit per pixel, the version is at
  least 1. The image has more than one bit per pixel if you specify it
  with *-depth* or if you let it default and the image has more than two
  colors (or shades of gray).

All releases of Palm OS can read a Version 0 bitmap. Palm OS 3.0 and
later can read a Version 1 bitmap. Palm OS 3.5 and later can read a
Version 2 bitmap. To read a Version 3 bitmap, you need Palm OS Garnet or
a handheld running the High Density Display Feature Set.

* OPTIONS
- *-verbose* :: Display the format of the output file.

- *-depth=*/N/ :: Produce a file of depth /N/, where /N/ must be either
  1, 2, 4, 8, or 16. Because the default Palm 8-bit colormap is not
  grayscale, if the input is a grayscale or monochrome image, the output
  will never be more than 4 bits deep, regardless of the specified
  depth. Note that 8-bit color works only in PalmOS 3.5 (and higher),
  and 16-bit direct color works only in PalmOS 4.0 (and higher).
  However, the 16-bit direct color format is also compatible with the
  various PalmOS 3.x versions used in the Handspring Visor, so these
  images may also work in that device.

- *-maxdepth=*/N/ :: Produce a file of minimal depth, but in any case
  less than /N/ bits wide. If you specify 16-bit, the output will always
  be 16-bit direct color.

- *-offset* :: Set the *nextDepthOffset* field in the palm file header
  to indicate the end of the file (and pad the end of the file to 4
  bytes, since *nextDepthOffset* can point only to 4 byte boundaries).

A palm image file can contain multiple renditions of the same image,
with different color depths, so a viewer can choose one appropriate for
the display. The *nextDepthOffset* field tells where in the stream the
next rendition begins.

*pnmtopalm* creates a file that contains only one image, but you can
separately concatenate multiple one-image files to create a multi-image
file. If you do that, you'll need to use *-offset* so that the resulting
concatenation is a correct stream.

By default (if you don't specify *-offset*), *pnmtopalm* generates a
*nextDepthOffset* field that says there is no following image (and does
not add any padding after the image).

Version 3 Palm Bitmaps actually have a *nextBitmapOffset* field instead
of the *nextDepthOffset*. The foregoing applies to whichever is
relevant.

The *-offset* option was new in Netpbm 10.26 (January 2005). Before
that, *pnmtopalm* always set the *nextDepthOffset* field to 'none.'

Before Netpbm 10.27 (March 2005), you cannot use *-offset* if you create
a compressed raster (because *pnmtopalm* isn't smart enough to be able
to know the size of the image at the time it writes the header). You
also cannot use it with 16 bit color depth or with the *-colormap*
option, for much the same reason.

- *-withdummy* :: This option tells *pnmtopalm* to put in the stream,
  after the image, a dummy image header to introduce subsequent high
  density images.

This dummy image header is a special sequence specified in Palm Bitmap
specifications. It looks to an older Palm Bitmap interpreter like an
invalid image header, so such an interpreter will stop reading the
stream there. But a new Palm Bitmap interpreter recognizes it for what
it is (just something to choke an old interpreter) and skips over it.
Presumably, you will add to the stream after this high density images
which would confuse an older interpreter.

If you specify *-withdummy*, you must also specify *-offset*, since it
doesn't make any sense otherwise.

*-withdummy* was new in Netpbm 10.27 (March 2005).

- *-colormap* :: Build a custom colormap and include it in the output
  file. This is not recommended by Palm, for efficiency reasons.
  Otherwise, *pnmtopalm* uses the default Palm colormap for color
  output.

- *-transparent=*/color/ :: Marks /one/ particular color as fully
  transparent. The format to specify the color is either (when for
  example orange) '1.0,0.5,0.0', where the values are floats between
  zero and one, or with the syntax '#RGB', '#RRGGBB' or '#RRRRGGGGBBBB'
  where R, G and B are hexadecimal numbers. Transparency works only on
  Palm OS 3.5 and higher.

- *-scanline-compression* :: Specifies that the output Palm bitmap will
  use the Palm scanline compression scheme. Scanline compression works
  only in Palm OS 2.0 and higher.

- *-rle-compression* :: Specifies that the output Palm bitmap will use
  the Palm RLE compression scheme. RLE compression works only with Palm
  OS 3.5 and higher.

- *-packbits-compression* :: Specifies that the output Palm bitmap will
  use the Palm packbits compression scheme. Packbits compression works
  only with Palm OS 4.0 and higher.

This option was new in Netpbm 10.27 (March 2005).

- *-density*=/N/ :: This specifies the Palm Bitmap density. The density
  is a number that is proportional to the resolution the image should
  have when displayed. The proportionality factor is up to whatever is
  doing the displaying, but it's helpful to think of these numbers as
  being pixels per inch. The allowable values are:

- 72

- 108

- 144

- 216

- 288

This option was new in Netpbm 10.27 (March 2005). Earlier Netpbm could
not generate Version 3 Palm Bitmaps, so there was no such thing as
density.

* SEE ALSO
*palmtopnm*(1) , *pdbimgtopam*(1) , *pnmquant*(1) , *pnmremap*(1) ,
*pnm*(5)

* NOTES
Palm Bitmaps may contains multiple renditions of the same bitmap, in
different depths. To construct an N-multiple-rendition Palm Bitmap with
*pnmtopalm*, first construct renditions 1 through N-1 using the
*-offset* option, then construct the Nth image without the *-offset*
option. Then concatenate the individual renditions together in a single
file using *cat*.

If you will include both high density and low density renditions, put
the high density images last and when you create the last of the low
density images, use the *-withdummy* option.

If you specify the Palm packbits compression scheme for a 16-bit direct
color bitmap, this program generates an invalid bitmap.

* AUTHORS
This program was originally written as ppmtoTbmp.c, by Ian Goldberg and
George Caswell. It was completely re-written by Bill Janssen to add
color, compression, and transparency function. Copyright 1995-2001 by
Ian Goldberg, George Caswell, and Bill Janssen.
