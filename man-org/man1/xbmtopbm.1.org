#+TITLE: Man1 - xbmtopbm.1
#+DESCRIPTION: Linux manpage for xbmtopbm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
xbmtopbm - convert an X11 or X10 bitmap to a PBM image

* SYNOPSIS
*xbmtopbm*

[/bitmapfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*xbmtopbm* reads an X11 or X10 bitmap as input and produces a PBM image
as output.

* SEE ALSO
*pbmtoxbm*(1) , *pbmtox10bm*(1) , *pbm*(5)

* AUTHOR
Copyright (C) 1988 by Jef Poskanzer.
