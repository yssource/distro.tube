#+TITLE: Man1 - fc-scan.1
#+DESCRIPTION: Linux manpage for fc-scan.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fc-scan - scan font files or directories

* SYNOPSIS
*fc-scan* [ *-Vh* ] [ *-f */format/ | *--format */format/ ] [
*--version* ] [ *--help* ] /file .../

* DESCRIPTION
*fc-scan* scans /file/(s) recursively and prints out font pattern for
each face found.

* OPTIONS
This program follows the usual GNU command line syntax, with long
options starting with two dashes (`-'). A summary of options is included
below.

- *-f --format */format/* * :: Format output according to the format
  specifier /format/.

- *-V --version * :: Show version of the program and exit.

- *-h --help * :: Show summary of options.

- /file/* * :: Scan /file/ recursively for font faces.

* RETURN CODES
*fc-scan* returns error code 0 if at least one font was found or 1
otherwise.

* SEE ALSO
*fc-query*(1) *FcFileScan*(3) *FcDirScan*(3) *FcPatternFormat*(3)
*fc-cat*(1) *fc-cache*(1) *fc-list*(1) *fc-match*(1) *fc-pattern*(1)

The fontconfig user's guide, in HTML format:
//usr/share/doc/fontconfig/fontconfig-user.html/.

* AUTHOR
This manual page was updated by Behdad Esfahbod <behdad@behdad.org>.
