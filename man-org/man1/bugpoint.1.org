#+TITLE: Manpages - bugpoint.1
#+DESCRIPTION: Linux manpage for bugpoint.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
bugpoint - automatic test case reduction tool

* SYNOPSIS
*bugpoint* [/options/] [/input LLVM ll/bc files/] [/LLVM passes/]
*--args* /program arguments/

* DESCRIPTION
*bugpoint* narrows down the source of problems in LLVM tools and passes.
It can be used to debug three types of failures: optimizer crashes,
miscompilations by optimizers, or bad native code generation (including
problems in the static and JIT compilers). It aims to reduce large test
cases to small, useful ones. For more information on the design and
inner workings of *bugpoint*, as well as advice for using bugpoint, see
/Bugpoint in the LLVM distribution.

* OPTIONS
*--additional-so* /library/

#+begin_quote

  #+begin_quote
    Load the dynamic shared object /library/ into the test program
    whenever it is run. This is useful if you are debugging programs
    which depend on non-LLVM libraries (such as the X or curses
    libraries) to run.
  #+end_quote
#+end_quote

*--append-exit-code*=/{true,false}/

#+begin_quote

  #+begin_quote
    Append the test programs exit code to the output file so that a
    change in exit code is considered a test failure. Defaults to false.
  #+end_quote
#+end_quote

*--args* /program args/

#+begin_quote

  #+begin_quote
    Pass all arguments specified after *--args* to the test program
    whenever it runs. Note that if any of the /program args/ start with
    a "*-*", you should use:

    #+begin_quote

      #+begin_quote
        #+begin_example
          bugpoint [bugpoint args] --args -- [program args]
        #+end_example
      #+end_quote
    #+end_quote

    The "*--*" right after the *--args* option tells *bugpoint* to
    consider any options starting with "*-*" to be part of the *--args*
    option, not as options to *bugpoint* itself.
  #+end_quote
#+end_quote

*--tool-args* /tool args/

#+begin_quote

  #+begin_quote
    Pass all arguments specified after *--tool-args* to the LLVM tool
    under test (*llc*, *lli*, etc.) whenever it runs. You should use
    this option in the following way:

    #+begin_quote

      #+begin_quote
        #+begin_example
          bugpoint [bugpoint args] --tool-args -- [tool args]
        #+end_example
      #+end_quote
    #+end_quote

    The "*--*" right after the *--tool-args* option tells *bugpoint* to
    consider any options starting with "*-*" to be part of the
    *--tool-args* option, not as options to *bugpoint* itself. (See
    *--args*, above.)
  #+end_quote
#+end_quote

*--safe-tool-args* /tool args/

#+begin_quote

  #+begin_quote
    Pass all arguments specified after *--safe-tool-args* to the "safe"
    execution tool.
  #+end_quote
#+end_quote

*--gcc-tool-args* /gcc tool args/

#+begin_quote

  #+begin_quote
    Pass all arguments specified after *--gcc-tool-args* to the
    invocation of *gcc*.
  #+end_quote
#+end_quote

*--opt-args* /opt args/

#+begin_quote

  #+begin_quote
    Pass all arguments specified after *--opt-args* to the invocation of
    *opt*.
  #+end_quote
#+end_quote

*--disable-{dce,simplifycfg}*

#+begin_quote

  #+begin_quote
    Do not run the specified passes to clean up and reduce the size of
    the test program. By default, *bugpoint* uses these passes
    internally when attempting to reduce test programs. If you're trying
    to find a bug in one of these passes, *bugpoint* may crash.
  #+end_quote
#+end_quote

*--enable-valgrind*

#+begin_quote

  #+begin_quote
    Use valgrind to find faults in the optimization phase. This will
    allow bugpoint to find otherwise asymptomatic problems caused by
    memory mis-management.
  #+end_quote
#+end_quote

*-find-bugs*

#+begin_quote

  #+begin_quote
    Continually randomize the specified passes and run them on the test
    program until a bug is found or the user kills *bugpoint*.
  #+end_quote
#+end_quote

*-help*

#+begin_quote

  #+begin_quote
    Print a summary of command line options.
  #+end_quote
#+end_quote

*--input* /filename/

#+begin_quote

  #+begin_quote
    Open /filename/ and redirect the standard input of the test program,
    whenever it runs, to come from that file.
  #+end_quote
#+end_quote

*--load* /plugin/

#+begin_quote

  #+begin_quote
    Load the dynamic object /plugin/ into *bugpoint* itself. This object
    should register new optimization passes. Once loaded, the object
    will add new command line options to enable various optimizations.
    To see the new complete list of optimizations, use the *-help* and
    *--load* options together; for example:

    #+begin_quote

      #+begin_quote
        #+begin_example
          bugpoint --load myNewPass.so -help
        #+end_example
      #+end_quote
    #+end_quote
  #+end_quote
#+end_quote

*--mlimit* /megabytes/

#+begin_quote

  #+begin_quote
    Specifies an upper limit on memory usage of the optimization and
    codegen. Set to zero to disable the limit.
  #+end_quote
#+end_quote

*--output* /filename/

#+begin_quote

  #+begin_quote
    Whenever the test program produces output on its standard output
    stream, it should match the contents of /filename/ (the "reference
    output"). If you do not use this option, *bugpoint* will attempt to
    generate a reference output by compiling the program with the "safe"
    backend and running it.
  #+end_quote
#+end_quote

*--run-{int,jit,llc,custom}*

#+begin_quote

  #+begin_quote
    Whenever the test program is compiled, *bugpoint* should generate
    code for it using the specified code generator. These options allow
    you to choose the interpreter, the JIT compiler, the static native
    code compiler, or a custom command (see *--exec-command*)
    respectively.
  #+end_quote
#+end_quote

*--safe-{llc,custom}*

#+begin_quote

  #+begin_quote
    When debugging a code generator, *bugpoint* should use the specified
    code generator as the "safe" code generator. This is a known-good
    code generator used to generate the "reference output" if it has not
    been provided, and to compile portions of the program that as they
    are excluded from the testcase. These options allow you to choose
    the static native code compiler, or a custom command, (see
    *--exec-command*) respectively. The interpreter and the JIT backends
    cannot currently be used as the "safe" backends.
  #+end_quote
#+end_quote

*--exec-command* /command/

#+begin_quote

  #+begin_quote
    This option defines the command to use with the *--run-custom* and
    *--safe-custom* options to execute the bitcode testcase. This can be
    useful for cross-compilation.
  #+end_quote
#+end_quote

*--compile-command* /command/

#+begin_quote

  #+begin_quote
    This option defines the command to use with the *--compile-custom*
    option to compile the bitcode testcase. The command should exit with
    a failure exit code if the file is "interesting" and should exit
    with a success exit code (i.e. 0) otherwise (this is the same as if
    it crashed on "interesting" inputs).

    This can be useful for testing compiler output without running any
    link or execute stages. To generate a reduced unit test, you may add
    CHECK directives to the testcase and pass the name of an executable
    compile-command script in this form:

    #+begin_quote

      #+begin_quote
        #+begin_example
          #!/bin/sh
          llc "$@"
          not FileCheck [bugpoint input file].ll < bugpoint-test-program.s
        #+end_example
      #+end_quote
    #+end_quote

    This script will "fail" as long as FileCheck passes. So the result
    will be the minimum bitcode that passes FileCheck.
  #+end_quote
#+end_quote

*--safe-path* /path/

#+begin_quote

  #+begin_quote
    This option defines the path to the command to execute with the
    *--safe-{int,jit,llc,custom}* option.
  #+end_quote
#+end_quote

*--verbose-errors*=/{true,false}/

#+begin_quote

  #+begin_quote
    The default behavior of bugpoint is to print "<crash>" when it finds
    a reduced test that crashes compilation. This flag prints the output
    of the crashing program to stderr. This is useful to make sure it is
    the same error being tracked down and not a different error that
    happens to crash the compiler as well. Defaults to false.
  #+end_quote
#+end_quote

* EXIT STATUS
If *bugpoint* succeeds in finding a problem, it will exit with 0.
Otherwise, if an error occurs, it will exit with a non-zero value.

* SEE ALSO
*opt(1)*

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project

Information about bugpoint.1 is found in manpage for: [[../\-\-load myNewPass.so \-help][\-\-load myNewPass.so \-help]]