#+TITLE: Man1 - picttoppm.1
#+DESCRIPTION: Linux manpage for picttoppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
picttoppm - convert a Macintosh PICT file to a PPM

* SYNOPSIS
*picttoppm*

[*-verbose*]

[*-fullres*]

[*-noheader*]

[*-quickdraw*] [*-fontdir* /file/]

[/pictfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*picttoppm* reads a PICT file (version 1 or 2) and outputs a PPM image.

PICT is an image format that was developed by Apple Computer in 1984 as
the native format for Macintosh graphics. A PICT image is encoded in
QuickDraw commands. The PICT format is a meta-format that can be used
for both bitmap images and vector images. PICT is also known as
'Macintosh Picture' format, or the QuickDraw Picture format.

PICT files are primarily used to exchange graphics between various
Macintosh applications.

In MacOS X, PDF replaces PICT as the main graphics format.

* OPTIONS
- *-fontdir */file/ :: Make the list of BDF fonts in /file/ available
  for use by *picttoppm* when drawing text. See below for the format of
  the fontdir file. This is in addition to the built-in fonts and those
  in the file *fontdir*.

- *-fullres* :: Force any images in the PICT file to be output with at
  least their full resolution. A PICT file may indicate that a contained
  image is to be scaled down before output. This option forces images to
  retain their sizes and prevent information loss. This option disables
  all PICT operations except images.

- *-noheader* :: Do not assume the first 512 bytes of the file are a
  header. All PICT files have such a header, but this is useful when you
  have PICT data that was not stored in the data fork of a PICT file.

- *-quickdraw* :: Execute only pure quickdraw operations. In particular,
  turn off the interpretation of special PostScript printer operations.

- *-verbose* :: Print a whole bunch of information about the PICT file
  and the conversion process that only *picttoppm* hackers really care
  about.

* LIMITATIONS
The PICT file format is a general drawing format. *picttoppm* does not
recognize all the drawing commands, but it does fully implement all
image commands and mostly implement line, rectangle, polygon and text
drawing. It is useful for converting scanned images and some drawing
conversion.

With *-fullres*, *picttoppm* ignores text drawing commands. Beginning in
Netpbm 10.45 (December 2008), it issues a warning message when it omits
text for this reason.

* FONTS
Some of the information in a PICT file is text, with a number indicating
the font in which the text is supposed to rendered. *picttoppm* has one
built-in font, but you can add others by directing *picttoppm* to BDF
font files, which you do with font directory files.

*picttoppm* automatically uses the file named *fontdir* in the current
directory, if it exists. You may specify an additional font directory
file with the *-fontdir* option.

Obviously the font definitions are strongly related to the Macintosh.
You can find more font numbers and information about fonts in Macintosh
documentation.

** FONT DIR FILE FORMAT
Each line in the file is either a comment or font information. A comment
begins with *#*. The font information consists of 4 whitespace separated
fields. The first is the font number, the second is the font size in
pixels, the third is the font style and the fourth is the name of a BDF
file containing the font. The BDF format is defined by the X Window
System and is beyond the scope of this document.

The font number indicates the type face. Here is a list of known font
numbers and their faces.

- 0  :: Chicago

- 1  :: application font

- 2  :: New York

- 3  :: Geneva

- 4  :: Monaco

- 5  :: Venice

- 6  :: London

- 7  :: Athens

- 8  :: San Franciso

- 9  :: Toronto

- 11  :: Cairo

- 12  :: Los Angeles

- 20  :: Times Roman

- 21  :: Helvetica

- 22  :: Courier

- 23  :: Symbol

- 24  :: Taliesin

The font style indicates a variation on the font. Multiple variations
may apply to a font and the font style is the sum of the variation
numbers which are:

- 1  :: Boldface

- 2  :: Italic

- 4  :: Underlined

- 8  :: Outlined

- 16  :: Shadow

- 32  :: Condensed

- 64  :: Extended

* SEE ALSO
Inside Macintosh volumes 1 and 5, *ppmtopict*(1) , *ppm*(5)

* AUTHOR
Copyright 1993 George Phillips
