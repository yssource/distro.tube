#+TITLE: Man1 - libinput-debug-tablet.1
#+DESCRIPTION: Linux manpage for libinput-debug-tablet.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libinput-debug-tablet - debug and visualize tablet axes

* SYNOPSIS
*libinput debug-tablet [--help] [options] [//dev/input/event0]/*

* DESCRIPTION
The *libinput debug-tablet* tool debugs the values of the various axes
on a tablet. This is an interactive tool. When executed, the tool will
prompt the user to interact with the tablet and display the current
value on each available axis.

This is a debugging tool only, its output may change at any time. Do not
rely on the output.

This tool usually needs to be run as root to have access to the
/dev/input/eventX nodes.

* OPTIONS
If a device node is given, this tool opens that device node. Otherwise,
this tool searches for the first node that looks like a tablet and uses
that node.

- *--help* :: Print help

Events shown by this tool may not correspond to the events seen by a
different user of libinput. This tool initializes a separate context.

* LIBINPUT
Part of the *libinput(1)* suite
