#+TITLE: Man1 - alacritty.1
#+DESCRIPTION: Linux manpage for alacritty.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
Alacritty - A fast, cross-platform, OpenGL terminal emulator

* SYNOPSIS
alacritty [FLAGS] [OPTIONS]

* DESCRIPTION
Alacritty is a modern terminal emulator that comes with sensible
defaults, but allows for extensive configuration. By integrating with
other applications, rather than reimplementing their functionality, it
manages to provide a flexible set of features with high performance.

* FLAGS
- *-h*, *--help* :: Prints help information

- *--hold* :: Remain open after child process exits

- *--print-events* :: Print all events to stdout

- *-q* :: Reduces the level of verbosity (the min level is *-qq*)

- *--ref-test* :: Generates ref test

- *-v* :: Increases the level of verbosity (the max level is *-vvv*)

- *-V*, *--version* :: Prints version information

* OPTIONS
- *--class* <instance> | <instance>,<general> :: Defines the window
  class hint on Linux [default: Alacritty,Alacritty]

On Wayland the instance class sets the `app_id`, while the general class
is ignored.

- *-e*, *--command* <command>... :: Command and args to execute (must be
  last argument)

- *--config-file* <config-file> :: Specify alternative configuration
  file

Alacritty looks for the configuration file at the following paths: 1.
$XDG_CONFIG_HOME/alacritty/alacritty.yml 2.
$XDG_CONFIG_HOME/alacritty.yml 3.
$HOME/.config/alacritty/alacritty.yml 4. $HOME/.alacritty.yml

On Windows, the configuration file is located at
%APPDATA%\alacritty\alacritty.yml.

- *--embed* <parent> :: Defines the X11 window ID (as a decimal integer)
  to embed Alacritty within

- *-o*, *--option* <option>... :: Override configuration file options
  [example: cursor.style=Beam]

- *-t*, *--title* <title> :: Defines the window title [default:
  Alacritty]

- *--working-directory* <working-directory> :: Start the shell in the
  specified working directory

* SEE ALSO
See the alacritty github repository at
https://github.com/alacritty/alacritty for the full documentation.

* BUGS
Found a bug? Please report it at
https://github.com/alacritty/alacritty/issues.

* MAINTAINERS

#+begin_quote
  ·

  Christian Duerr <contact@christianduerr.com>

  ·

  Joe Wilm <joe@jwilm.com>
#+end_quote
