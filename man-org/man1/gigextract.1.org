#+TITLE: Man1 - gigextract.1
#+DESCRIPTION: Linux manpage for gigextract.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gigextract - Extract samples from Gigasampler (.gig) files.

* SYNOPSIS
*gigextract* [ -v ] GIGFILE DESTDIR [SAMPLENR] [ [SAMPLENR] ... ]

* DESCRIPTION
Extract samples from Gigasampler (.gig) files. All extracted samples
will be written in .wav format. You must at least supply name of the
.gig input file and an output path where all extracted samples should be
written to. By default *gigextract* extracts all samples contained in
the Gigasampler file, even if they are not linked by any instrument in
the .gig file, but you can also extract only particular samples by
supplying a list of samples indices at the end of the command line. You
can use the *gigdump*(1) tool to see the list of available samples and
their sample indices of a Gigasampler file.

* OPTIONS
- * GIGFILE* :: filename of the input Gigasampler file

- * DESTDIR* :: output path where all samples should be extracted to

- * SAMPLENR* :: optional index of sample(s) to be exclusively extracted

- * -v* :: print version and exit

* SEE ALSO
*gigdump(1),* *gigmerge(1),* *gig2mono(1),* *gig2stereo(1)*

* BUGS
Check and report bugs at http://bugs.linuxsampler.org

* Author
Application and manual page written by Christian Schoenebeck
<cuse@users.sf.net>
