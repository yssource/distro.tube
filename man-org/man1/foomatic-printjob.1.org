#+TITLE: Man1 - foomatic-printjob.1
#+DESCRIPTION: Linux manpage for foomatic-printjob.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
foomatic-printjob - manage printer jobs in a spooler-independent fashion

* SYNOPSIS
*foomatic-printjob [ -s spooler ] [ -P queuename ]* *[ -o option1=value1
-o option2 ... ] [ -i ]* *[ file1 file2 ... ]*

*foomatic-printjob -Q [ -s spooler ] [ -P queuename ] [ -i ] [ -a ]* *[
user1 user2 ... ]*

*foomatic-printjob -R [ -s spooler ] [ -P queuename ] [ -i ]* *[ - |
jobid1 jobid2 ... ]*

*foomatic-printjob -C [ -s spooler ] [ -i ] command [ arguments ]*

*foomatic-printjob -S [ -s spooler ] [ -i ]*

*foomatic-printjob -h [ -s spooler ] [ -P queuename ] [ -i ]*

* DESCRIPTION
*foomatic-printjob* provides a spooler-independent interface to creating
and managing printer jobs and print queues. Note that the first argument
changes the mode of the command, as it combines the functionality of
several System V-style printing commands.

** OPTIONS
- *-s*/spooler/ :: Explicit spooler type.

- *-P*/queuename/ :: Any commands specified should apply to this queue
  instead of the default.

- *-o*/option=value/ :: Set option to value

- *-o*/option/ :: Set the switch option

- *-#*/n/ :: Print n copies

- *file1 file2 ...* :: Files to be printed, when no file is given,
  standard input will be printed

- *-Q* :: Query the jobs in a queue. If a list of users is specified,
  only those users' jobs will be queried.

- *-Q -a* :: Query the jobs in all queues. If a list of users is
  specified, only those users' jobs will be queried.

- *-R [ - | jobid1 jobid2 ... ]* :: Remove a job from a queue. Using /-/
  will remove all jobs.

- *-C command [ arguments ]* :: Execute control commands for queue/job
  manipulation. The commands are the ones of the BSD "lpc" utility. Use
  the control command "help" to get a list of supported commands. Note:
  the amount of commands varies with the spooler, but the same commands
  given under different spoolers do the same thing.

- *-i* :: Interactive mode: You will be asked if foomatic-printjob is in
  doubt about something. Otherwise foomatic-printjob uses auto-detection
  or quits with an error.

- *-S* :: Save the chosen spooler as the default spooler

- *-h* :: Show this message or show a list of available options if a
  queue is specified

* SEE ALSO
foomatic-configure(1) and the documentation for your print spooler.

* AUTHOR
Manfred Wassmann </manolo@NCC-1701.B.Shuttle.de/> and Chris Lawrence
</lawrencc@debian.org/> for the foomatic project using output from the
associated binary.

* BUGS
This manpage still needs some editing.
