#+TITLE: Man1 - pnmstitch.1
#+DESCRIPTION: Linux manpage for pnmstitch.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pnmstitch - stitch together two panoramic (side-by-side) photographs

* SYNOPSIS
*pnmstitch* [ [/left_filespec/] /right_filespec/ | /left_filespec/
/right_filespec/ /output_filespec/ ] [*-width=*/width/]
[*-height=*/height/] [*-xrightpos=*/column/] [*-yrightpos=*/row/]
[*-stitcher=*{*RotateSliver*, *BiLinearSliver*,*LinearSliver*}]
[*-filter=*{*LineAtATime*,*HorizontalCrop*}]
[*-output=*/output_filespec/] [*-verbose*]

All options can be abbreviated to their shortest unique prefix. You may
use two hyphens instead of one. You may separate an option name and its
value with white space instead of an equals sign.

* DESCRIPTION
This program is part of *Netpbm*(1)

*pnmstitch* stitches together two panoramic photographs. This means if
you have photographs of the left and right side of something that is too
big for a single camera frame, *pnmstitch* can join them into one wide
picture.

*pnmstitch* works only on side-by-side images, not top and bottom
(though you could certainly use *pamflip* in combination with
*pnmstitch* to achieve this). It stitches together two images, but you
can use it repeatedly to stitch together as many as you need to.

Your photographs must overlap in order for *pnmstitch* to work, and the
overlap should be substantial. *pnmstitch* shifts and stretches the
right hand image to match it up the left hand image. You probably want
to crop the result with *pamcut* to make a nice rectangular image.

If you're just trying to join (concatenate) two images at their edges,
use *pnmcat*.

The /left_filespec/ and /right_filespec/ arguments are the
specifications (names) of the PNM files containing the left hand and
right hand images. If you specify only /right_filespec/, the left hand
image comes from Standard Input. If you specify neither, both images
come from Standard Input as a multi-image file containing first the left
and then the right image.

/output_filespec/ is the specification (name) of the output PNM file.
The *-output* option also specifies the output file. You cannot specify
both the argument and the option. If you specify neither, the output
goes to Standard Output.

* OPTIONS
- *-width=*/width/ :: 

- *-height=*/height/ :: 

- *-xrightpos=*/column/ :: 

- *-yrightpos=*/row/ :: These are constraints on where *pnmstitch*/
  stitches the images together./ For the *LinearSliver*/ method, column
  and row tell what/ location in the right hand image matches up to the
  top right corner of the left hand image.

- *-stitcher=*/{/*RotateSliver*/,/*BiLinearSliver*/,/ :: *LinearSliver*/}/
  The default is *RotateSliver*/./

- *-filter=*/{/*LineAtATime*/,/*HorizontalCrop*/}/ :: No details
  available.

- *-output=*/output_filespec/ :: Name of output file. If you don't
  specify this option, the output image goes to Standard Output.

- *-verbose* :: This option causes *pnmstitch*/ to issue messages to
  Standard Error/ about the stitching process.

* SEE ALSO
*pamcut*(1) , *pnmcat*(1) , *pamflip*(1) , *pnm*(5) ,

* HISTORY
This program was added to Netpbm in Release 10.7 (August 2002).
