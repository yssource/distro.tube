#+TITLE: Man1 - fitstopnm.1
#+DESCRIPTION: Linux manpage for fitstopnm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
fitstopnm - convert a FITS file into a PNM image

* SYNOPSIS
*fitstopnm* [*-image=*/N/] [*-scanmax*] [*-printmax*] [*-min=*/f/]
[*-max=*/f/] [*-omaxval=*/N/ [/FITSfile/]

Minimum unique abbreviation of option is acceptable. You may use double
hyphens instead of single hyphen to denote options. You may use white
space in place of the equals sign to separate an option name from its
value.

* DESCRIPTION
This program is part of *Netpbm*(1)

*fitstopnm* reads a FITS (Flexible Image Transport System) file as input
and produces a PPM image if the FITS file consists of 3 image planes
(NAXIS = 3 and NAXIS3 = 3), or a PGM image if the FITS file consists of
2 image planes (NAXIS = 2), or if you specify the *-image* option.

The program tells you what kind of PNM image it is writing.

* OPTIONS
- *-image=*/N/ :: This is for FITS files with three axes. The assumption
  is that the third axis is for multiple images, and this option lets
  you select which one you want. *-omaxval=*/N/

This is the maxval that the output PNM image is to have.

By default, the maxval is the least possible to retain all the precision
of the FITS input. That means the difference between the highest and
lowest sample value in the input. If the values range from -5 to 100,
for example, the default maxval would be 106 and each PNM sample value
would correspond to one FITS sample value.

For a FITS input with floating point sample values, the precision is
essentially unlimited, so this is not possible. In that case, the
default maxval is simply 255.

This option was new in Netpbm 10.39 (June 2007). Before that, the output
maxval is always the default.

- *-min=*/float/ :: 

- *-max=*/float/ :: You can use these options to override the min and
  max values as read from the FITS header or the image data if the
  header has no DATAMIN and DATAMAX keywords.

- *-scanmax* :: Use this option to force the program to scan the data
  even when the header has DATAMIN and DATAMAX.

- *-printmax* :: With this option, the program just prints the min and
  max values and quits without doing its normal job.

This is for use in shell programs. Example:

#+begin_example
      eval 'fitstopnm -printmax $filename | \
      awk {min = $1; max = $2} \
            END {print "min=" min; " max=" max}'
#+end_example

* NOTES
** Pixel Order
You may need to pass the output of *fitstopnm* through *pamflip*
-topbottom. See [[file:pamtofits.html#pixelorder][*pamtofits*]]

* SEE ALSO
*pamtofits*(1) , *pamflip*(1) , *pgm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer, with modifications by Daniel Briggs
(/dbriggs@nrao.edu/) and Alberto Accomazzi (/alberto@cfa.harvard.edu/).
