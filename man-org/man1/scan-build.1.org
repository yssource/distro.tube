#+TITLE: Man1 - scan-build.1
#+DESCRIPTION: Linux manpage for scan-build.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is a Perl script that invokes the Clang static analyzer. Options used by

or by the analyzer appear first, followed by the

and any

normally used to build the target system.

The static analyzer employs a long list of checking algorithms, see

Output can be written in standard

and/or HTML format.

The following options are supported:

Also analyze functions in #included files.

Enable/disable

See

Display this message.

List default checkers, see

Specify the title used on generated HTML pages. A default title is
generated if

is not specified.

Add a

option to

Currently supports make and xcodebuild. This is a convenience option;
one can specify this behavior directly using build options.

Target directory for HTML report files. Subdirectories will be created
as needed to represent separate invocations of the analyzer. If this
option is not specified, a directory is created in /tmp (TMPDIR on Mac
OS X) to store the reports.

Output the results as a set of

files. (By default the output of

is a set of HTML files.)

Output the results as a set of HTML and .plist files

Set exit status to 1 if it found potential bugs and 0 otherwise. By
default the exit status of

is that returned by

Guess the default compiler for your C++ and Objective-C++ code. Use this
option to specify an alternate compiler.

Guess the default compiler for your C and Objective-C code. Use this
option to specify an alternate compiler.

Verbose output from

and the analyzer. A second and third

increases verbosity.

View analysis results in a web browser when the build completes.

Specify the contraint engine used by the analyzer. By default the

model is used. Specifying

uses a simpler, less powerful constraint model used by checker-0.160 and
earlier.

Specify the number of times a block can be visited before giving up.
Default is 4. Increase for more comprehensive coverage at a cost of
speed.

Do not create a

subdirectory that includes analyzer crash reports and preprocessed
source files.

Generates visitation statistics for the project being analyzed.

Specify the store model used by the analyzer. By default, the

store model is used.

specifies a field- sensitive store model. Users can also specify

which is far less precise but can more quickly analyze code.

was the default store model for checker-0.221 and earlier.

returns the value returned by

unless

or

is used.

The checkers listed below may be enabled/disabled using the

and

options. A default group of checkers is run unless explicitly disabled.
Exactly which checkers constitute the default group is a function of the
operating system in use; they are listed with

Check to see if the return value of a function call is different than
the caller expects (e.g., from calls through function pointers).

Check for null pointers passed as arguments to a function whose
arguments are marked with the

attribute.

Check for logical errors for function calls and Objective-C message
expressions (e.g., uninitialized arguments, null function pointers).

Check for division by zero.

Check for dereferences of null pointers.

Check that addresses to stack memory do not escape the function.

Check for undefined results of binary operators.

Check for declarations of VLA of undefined or zero size.

Evaluate compiler builtin functions, e.g.

Evaluate

functions that are known to not return to the caller.

Check for uninitialized values used as array subscripts.

Check for assigning uninitialized values.

Check for uninitialized values used as branch conditions.

Check for blocks that capture uninitialized values.

Check for uninitialized values being returned to the caller.

Check for values stored to variables that are never read afterwards.

Display Control-Flow Graphs.

Display Call Graph.

Print the dominance tree for a given Control-Flow Graph.

Print results of live variable analysis.

Emit warnings with analyzer statistics.

Mark tainted symbols as such.

View Control-Flow Graphs using

View Call Graph using

Check code for LLVM codebase conventions.

Check for proper uses of various Mac OS X APIs.

Evaluate calls to

functions.

Check for proper uses of Secure Keychain APIs.

Check for null pointers used as mutexes for @synchronized.

Check for sending

or

directly to a Class.

Warn about Objective-C method signatures with type incompatibilities.

Warn for suboptimal uses of

in Objective-C GC mode.

Check usage of NSError** parameters.

Check for prohibited nil arguments to Objective-C method calls.

Check for leaks and improper reference count management.

Check that

is properly initialized inside an initializer method.

Warn about private ivars that are never used.

Check for passing non-Objective-C types to variadic methods that expect
only Objective-C types.

Check usage of CFErrorRef* parameters.

Check for proper uses of

Check for null arguments to

and

Checks for index out-of-bounds when using the

API.

Warns if

or

are created with non-pointer-size values.

Warn on using a floating point value as a loop counter (CERT: FLP30-C,
FLP30-CPP).

Warn on uses of functions whose return values must be always checked.

Warn on uses of

Warn on uses of

Warn when

is passed fewer than 6 X's in the format string.

Warn on uses of

Warn on uses of

and related functions.

Warn on uses of

and

Warn on uses of

Check calls to various UNIX/Posix functions.

Check for memory leaks, double free, and use-after-free.

Check the size argument passed into C string functions for common
erroneous patterns.

Check for null pointers being passed as arguments to C string functions.

The above example causes analysis reports to be deposited into a
subdirectory of

and to run

with the

option. A different subdirectory is created each time

analyzes a project. The analyzer should support most parallel builds,
but not distributed builds.

was written by

Documentation contributed by
