#+TITLE: Man1 - idevice_id.1
#+DESCRIPTION: Linux manpage for idevice_id.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idevice_id - List attached devices or print device name of given device.

* SYNOPSIS
*idevice_id* [OPTIONS] [UDID]

* DESCRIPTION
*idevice_id* prints a list of attached devices. If a UDID is given, the
name of the connected device with that UDID will be retrieved.

Without any options, *idevice_id* will list all available devices, USB
and network. The output can be further controlled with the following
OPTIONS.

* OPTIONS
- *-l, --list* :: List UDIDs of all devices attached via USB.

- *-n, --network* :: List UDIDs of all devices available via network.

- *-d, --debug* :: Enable communication debugging.

- *-h, --help* :: Prints usage information.

- *-v, --version* :: Prints version information.

* AUTHOR
Nikias Bassen

Man page written to conform with Debian by Julien Lavergne.

* ON THE WEB
https://libimobiledevice.org

https://github.com/libimobiledevice/libimobiledevice
