#+TITLE: Man1 - midicat.1
#+DESCRIPTION: Linux manpage for midicat.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

utility receives MIDI data from the given input MIDI port and/or sends
it to the given output MIDI port. The options are as follows:

Dump transferred data in hex on stderr.

Read MIDI data from this file instead of receiving it from the MIDI
port. If the option argument is

then standard input will be used.

Write MIDI data to this file instead of sending it to the MIDI port. If
the option argument is

then standard output will be used.

Use this

MIDI port for input/output. If the option is used twice, the first one
specifies the input port and the second one the output port.

If no files are specified, then

transfers data from the MIDI input port to the MIDI output port.

Send the given file to

$ midicat -i file.syx -q rmidi/0

Dump data received from

to stderr:

$ midicat -d -q rmidi/0 -o /dev/null

Send data from

to

$ midicat -q rmidi/0 -q midithru/0
