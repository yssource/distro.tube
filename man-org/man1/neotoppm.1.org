#+TITLE: Man1 - neotoppm.1
#+DESCRIPTION: Linux manpage for neotoppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
neotoppm - convert an Atari Neochrome .neo into a PPM image

* SYNOPSIS
*neotoppm* [/neofile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*neotoppm* reads an Atari Neochrome .neo file as input and produces a
PPM image as output.

* SEE ALSO
*ppmtoneo*(1) , *ppm*(5)

* AUTHOR
Copyright (C) 2001 by Teemu Hukkanen </tjhukkan@iki.fi/>, based on
pi1toppm by Steve Belczyk (/seb3@gte.com/) and Jef Poskanzer.
