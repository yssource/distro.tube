#+TITLE: Man1 - hdifftopam.1
#+DESCRIPTION: Linux manpage for hdifftopam.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
hdifftopam - convert horizontal difference image to original PAM image

* SYNOPSIS
*hdifftopam*[/pamfile/] [*-pnm*] [*-verbose*]

Minimum unique abbreviation of option is acceptable. You may use double
hyphens instead of single hyphen to denote options. You may use white
space in place of the equals sign to separate an option name from its
value.

* DESCRIPTION
This program is part of *Netpbm*(1)

*hdifftopam* undoes what *pamtohdiff* does.

* OPTIONS
- *-pnm* ::  This option tells *hdifftopam* to create a PNM image (i.e.
  PGM or PPM). Without it, *hdifftopam* creates a PAM image (with a
  tuple type of "unhdiff"). If the PAM does not have the proper depth to
  be a PGM or PPM (i.e. 1 or 3) and you specify *-pnm*, *hdifftopam*
  fails.

* SEE ALSO
*pamtohdiff*(1)

* AUTHOR
Bryan Henderson
