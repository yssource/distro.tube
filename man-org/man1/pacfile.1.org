#+TITLE: Man1 - pacfile.1
#+DESCRIPTION: Linux manpage for pacfile.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
pacfile - display information about package files

* SYNOPSIS
pacfile [options] <path>... pacfile (--help|--version)

* DESCRIPTION
Display file information from the local package database.

* OPTIONS
- --config=path :: Set an alternate configuration file path.

- --dbpath=path :: Set an alternate database path.

- --root=path :: Set an alternate installation root.

- --sysroot=path :: Set an alternate system root. See
  *pacutils-sysroot* (7).

- --package=pkgname :: Limit information to the specified package. May
  be specified multiple times.

- --check :: Compare database values to the file system.

- --help :: Display usage information and exit.

- --version :: Display version information and exit.
