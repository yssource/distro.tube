#+TITLE: Man1 - lxsettings-daemon.1
#+DESCRIPTION: Linux manpage for lxsettings-daemon.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxsettings-daemon --- lightweight XSettings daemon for LXDE

* SYNOPSIS
*lxsettings-daemon* -f /file/

This manual page documents briefly the *lxsettings-daemon* command.

* DESCRIPTION
*lxsettings-daemon* is a program that provides XSettings daemon. The
daemon handles the session settings for applications.

*lxsettings-daemon* is a part of the LXDE project.

* OPTIONS
- *-f*/ file/*, - -file=*/file/ :: path to the configuration /file/

* AUTHOR
This manual page was written by paulliu <grandpaul@gmail.com> for the
*Debian* system (but may be used by others). Permission is granted to
copy, distribute and/or modify this document under the terms of the GNU
General Public License, Version 2 any later version published by the
Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.
