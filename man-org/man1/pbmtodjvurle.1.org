#+TITLE: Man1 - pbmtodjvurle.1
#+DESCRIPTION: Linux manpage for pbmtodjvurle.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtodjvurle - convert a PBM image to DjVu Bitonal RLE format

* SYNOPSIS
*pbmtodjvurle*

[/pbmfile/ [/rlefile/]]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtodjvurle* reads a PBM image as input and produces DjVu Bitonal RLE
format as output.

* SEE ALSO
*pamtodjvurle*(1)

*pbm*(5)

* HISTORY
*pbmtodjvurle* was new in Netpbm 10.22 (April 2004).

* AUTHOR
Copyright (C) 2004 Scott Pakin <scott+pbm@pakin.org>.
