#+TITLE: Man1 - xdg-user-dir.1
#+DESCRIPTION: Linux manpage for xdg-user-dir.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xdg-user-dir - Find an XDG user dir

* SYNOPSIS
*xdg-user-dir* [NAME]

* DESCRIPTION
*xdg-user-dir* looks up the current path for one of the special XDG user
dirs.

This command expects the name of an XDG user dir as argument. The
possible names are:

#+begin_quote
  DESKTOP
#+end_quote

#+begin_quote
  DOWNLOAD
#+end_quote

#+begin_quote
  TEMPLATES
#+end_quote

#+begin_quote
  PUBLICSHARE
#+end_quote

#+begin_quote
  DOCUMENTS
#+end_quote

#+begin_quote
  MUSIC
#+end_quote

#+begin_quote
  PICTURES
#+end_quote

#+begin_quote
  VIDEOS
#+end_quote

* FILES
The values are looked up in the user-dirs.dir file. This file is created
by the xdg-user-dirs-update utility.

* ENVIRONMENT
The *XDG_CONFIG_HOME* environment variable determines where the
user-dirs.dirs file is located.

* SEE ALSO
*xdg-user-dirs-update*(1)
