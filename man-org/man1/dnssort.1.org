#+TITLE: Man1 - dnssort.1
#+DESCRIPTION: Linux manpage for dnssort.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
dnssort - sort DNS hostnames

* SYNOPSIS
*dnssort* [*-r*] [*FILES*]

* DESCRIPTON
Write sorted concatenation of all specified files to standard output. If
none are specified, the standard input is used.

* OPTIONS
- *--help* :: Display some help and exit.

- *-r* :: Reverse the sorting comparisons.

- *--version* :: Display program version and license and exit.

* SEE ALSO
sort(1)

* AUTHOR
R�mi Denis-Courmont <remi at remlab dot net>

http://www.remlab.net/ndisc6/
