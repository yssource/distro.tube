#+TITLE: Man1 - xzless.1
#+DESCRIPTION: Linux manpage for xzless.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xzless, lzless - view xz or lzma compressed (text) files

* SYNOPSIS
*xzless* [/file/...]\\
*lzless* [/file/...]

* DESCRIPTION
*xzless* is a filter that displays text from compressed files to a
terminal. It works on files compressed with *xz*(1) or *lzma*(1). If no
/files/ are given, *xzless* reads from standard input.

*xzless* uses *less*(1) to present its output. Unlike *xzmore*, its
choice of pager cannot be altered by setting an environment variable.
Commands are based on both *more*(1) and *vi*(1) and allow back and
forth movement and searching. See the *less*(1) manual for more
information.

The command named *lzless* is provided for backward compatibility with
LZMA Utils.

* ENVIRONMENT
- *LESSMETACHARS* :: A list of characters special to the shell. Set by
  *xzless* unless it is already set in the environment.

- *LESSOPEN* :: Set to a command line to invoke the *xz*(1) decompressor
  for preprocessing the input files to *less*(1).

* SEE ALSO
*less*(1), *xz*(1), *xzmore*(1), *zless*(1)
