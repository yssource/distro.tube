#+TITLE: Man1 - libinput.1
#+DESCRIPTION: Linux manpage for libinput.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libinput - tool to interface with libinput

* SYNOPSIS
*libinput [--help|--version] /<command>/ /[<args>]/*

* DESCRIPTION
libinput is a library to handle input devices and provides device
detection and input device event processing for most Wayland compositors
and the X.Org xf86-input-libinput driver.

The *libinput* tools are a set of tools to debug, interface with and
analyze data for libinput. These tools create libinput contexts separate
from that of the compositor/X server and cannot change settings in a
running session. See section *COMMANDS* for a list of available
commands.

libinput's API documentation and details on features and various
high-level concepts are available online at
/https://wayland.freedesktop.org/libinput/doc/latest//

The man page for the X.Org xf86-input-libinput driver is *libinput(4).*

* OPTIONS
- *--help* :: Print help and exit

- *--version* :: Print the version and exit

* COMMANDS
- *libinput-debug-events(1)* :: Print all events as seen by libinput

- *libinput-debug-gui(1)* :: Show a GUI to visualize libinput's events

- *libinput-debug-tablet(1)* :: A commandline tool to debug tablet axis
  values

- *libinput-list-devices(1)* :: List all devices recognized by libinput

- *libinput-measure(1)* :: Measure various properties of devices

- *libinput-record(1)* :: Record the events from a device

- *libinput-replay(1)* :: Replay the events from a device

- *libinput-analyze(1)* :: Analyze events from a device

* LIBINPUT
Part of the *libinput(1)* suite

* SEE ALSO
libinput(4)

libinput's online documentation
/https://wayland.freedesktop.org/libinput/doc/latest//
