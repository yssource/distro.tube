#+TITLE: Man1 - jack_connect.1
#+DESCRIPTION: Linux manpage for jack_connect.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*jack_connect*, *jack_disconnect* - JACK toolkit clients for connecting
& disconnecting ports

* SYNOPSIS
* jack_connect* [ /-s/ | /--server servername/ ] [/-h/ | /--help/ ]
port1 port2 * jack_disconnect* [ /-s/ | /--server servername/ ] [/-h/ |
/--help/ ] port1 port2

* DESCRIPTION
*jack_connect* connects the two named ports. *jack_disconnect*
disconnects the two named ports.

* RETURNS
The exit status is zero if successful, 1 otherwise
