#+TITLE: Man1 - ppmtolj.1
#+DESCRIPTION: Linux manpage for ppmtolj.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmtolj - convert a PPM image to an HP LaserJet PCL 5 Color file

* SYNOPSIS
*ppmtolj*

[*-gamma* /val/]

[*-resolution* {*75*|*100*|*150*|*300*|*600*}]

[*-delta*]

[*-float*]

[*-noreset*]

[/ppmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmtolj* reads a PPM image as input and converts it into a color file
suitable to be printed by an HP color PCL 5 printer.

* OPTIONS
- *-delta* :: Apply delta row compression to reduce the size of the pcl
  file.

- *-gamma* /int/ :: Gamma correct the image using the integer parameter
  as a gamma (default 0).

- *-float* :: Suppress positioning information. The default is to write
  the sequence ESC&l0E to the output.

- *-noreset* :: Don't write the reset sequence to the beginning and end
  of the output.

- *-resolution* :: Set the required output resolution 75|100|150|300|600

* SEE ALSO
HP PCL 5 & Color Reference Guide, **pnmtopclxl.html**(1) ,
**pbmtolj.html**(1) , **ppmtopj**(1) , **thinkjettopbm**(1) ,
**ppm.html**(5)

* AUTHOR
Copyright (C) 2000 by Jonathan Melvin.(/jonathan.melvin@heywood.co.uk/)
