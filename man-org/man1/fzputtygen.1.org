#+TITLE: Man1 - fzputtygen.1
#+DESCRIPTION: Linux manpage for fzputtygen.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fzputtygen - SFTP private key converter of FileZilla

* DESCRIPTION
*fzputtygen* is part of FileZilla. It is used to convert private keys
from OpenSSH or ssh.com format into a format understood by fzsftp. It
usually gets called by FileZilla and is not intended to be used
directly.

*fzputtygen* is based on the puttygen component of PuTTY.

* SUPPORT
Please visit https://filezilla-project.org/ for further information.
Report bugs only if you are using the latest version available from the
FileZilla website.

* COPYRIGHT
Copyright (C) 2004-2018 Tim Kosse\\
Copyright (C) 1997-2018 Simon Tatham and the PuTTY team

FileZilla is distributed under the terms of the GNU General Public
License version 2 or later.

* SEE ALSO
filezilla(1)
