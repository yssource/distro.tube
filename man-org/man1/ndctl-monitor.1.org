#+TITLE: Man1 - ndctl-monitor.1
#+DESCRIPTION: Linux manpage for ndctl-monitor.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ndctl-monitor - Monitor the smart events of nvdimm objects

* SYNOPSIS
#+begin_example
  ndctl monitor [<options>]
#+end_example

\\

* DESCRIPTION
Ndctl monitor is used for monitoring the smart events of nvdimm objects
and dumping the json format notifications to syslog, standard output or
a logfile.

The objects to monitor and smart events to notify can be selected by
setting options and/or the configuration file at /etc/ndctl/monitor.conf

Both, the values in configuration file and in options will work. If
there is a conflict, the values in options will override the values in
the configuration file. Any updated values in the configuration file
will take effect only after the monitor process is restarted.

* EXAMPLES
Run a monitor as a daemon to monitor DIMMs on bus "nfit_test.1"

#+begin_example
  ndctl monitor --bus=nfit_test.1 --daemon
#+end_example

\\

Run a monitor as a one-shot command and output the notifications to
/var/log/ndctl.log

#+begin_example
  ndctl monitor --log=/var/log/ndctl.log
#+end_example

\\

Run a monitor daemon as a system service

#+begin_example
  systemctl start ndctl-monitor.service
#+end_example

\\

* OPTIONS
-b, --bus=

#+begin_quote
  A bus id number, or a provider string (e.g. "ACPI.NFIT"). Restrict the
  operation to the specified bus(es). The keyword /all/ can be specified
  to indicate the lack of any restriction, however this is the same as
  not supplying a --bus option at all.
#+end_quote

-d, --dimm=

#+begin_quote
  A /nmemX/ device name, or a dimm id number. Restrict the operation to
  the specified dimm(s). The keyword /all/ can be specified to indicate
  the lack of any restriction, however this is the same as not supplying
  a --dimm option at all.
#+end_quote

-r, --region=

#+begin_quote
  A /regionX/ device name, or a region id number. Restrict the operation
  to the specified region(s). The keyword /all/ can be specified to
  indicate the lack of any restriction, however this is the same as not
  supplying a --region option at all.
#+end_quote

-n, --namespace=

#+begin_quote
  A /namespaceX.Y/ device name, or namespace region plus id tuple /X.Y/.
#+end_quote

-l, --log=

#+begin_quote
  Send log messages to the specified destination.

  #+begin_quote
    ·

    "<file>": Send log messages to specified <file>. When fopen() is not
    able to open <file>, log messages will be forwarded to syslog.
  #+end_quote

  #+begin_quote
    ·

    "syslog": Send messages to syslog.
  #+end_quote

  #+begin_quote
    ·

    "standard": Send messages to standard output.
  #+end_quote
#+end_quote

The default log destination is /syslog/ if "--daemon" is specified,
otherwise /standard/. Note that standard and relative path for <file>
will not work if "--daemon" is specified.

-c, --config-file=

#+begin_quote
  Provide the config file to use. This overrides the default config
  typically found in /etc/ndctl
#+end_quote

--daemon

#+begin_quote
  Run a monitor as a daemon.
#+end_quote

-D, --dimm-event=

#+begin_quote
  Name of an smart health event from the following:

  #+begin_quote
    ·

    "dimm-spares-remaining": Spare Blocks Remaining value has gone below
    the pre-programmed threshold.
  #+end_quote

  #+begin_quote
    ·

    "dimm-media-temperature": NVDIMM Media temperature value has gone
    above the pre-programmed threshold.
  #+end_quote

  #+begin_quote
    ·

    "dimm-controller-temperature": NVDIMM Controller temperature value
    has gone above the pre-programmed threshold.
  #+end_quote

  #+begin_quote
    ·

    "dimm-health-state": NVDIMM Normal Health Status has changed
  #+end_quote

  #+begin_quote
    ·

    "dimm-unclean-shutdown": NVDIMM Last Shutdown Status was a unclean
    shutdown.
  #+end_quote
#+end_quote

The monitor will attempt to enable the alarm control bits for all
specified events.

-p, --poll=

#+begin_quote
  Poll and report status/event every <n> seconds.
#+end_quote

-u, --human

#+begin_quote
  Output monitor notification as human friendly json format instead of
  the default machine friendly json format.
#+end_quote

-v, --verbose

#+begin_quote
  Emit extra debug messages to log.
#+end_quote

* COPYRIGHT
Copyright (c) 2018, FUJITSU LIMITED. License GPLv2: GNU GPL version 2
<http://gnu.org/licenses/gpl.html>. This is free software: you are free
to change and redistribute it. There is NO WARRANTY, to the extent
permitted by law.

* SEE ALSO
ndctl-list(1), ndctl-inject-smart(1)
