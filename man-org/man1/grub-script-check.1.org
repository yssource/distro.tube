#+TITLE: Man1 - grub-script-check.1
#+DESCRIPTION: Linux manpage for grub-script-check.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grub-script-check - check grub.cfg for syntax errors

* SYNOPSIS
*grub-script-check* [/OPTION/...] [/PATH/]

* DESCRIPTION
Checks GRUB script configuration file for syntax errors.

- *-v*, *--verbose* :: print verbose messages.

- -?, *--help* :: give this help list

- *--usage* :: give a short usage message

- *-V*, *--version* :: print program version

* REPORTING BUGS
Report bugs to <bug-grub@gnu.org>.

* SEE ALSO
*grub-mkconfig*(8)

The full documentation for *grub-script-check* is maintained as a
Texinfo manual. If the *info* and *grub-script-check* programs are
properly installed at your site, the command

#+begin_quote
  *info grub-script-check*
#+end_quote

should give you access to the complete manual.
