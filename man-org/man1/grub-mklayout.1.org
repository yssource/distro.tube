#+TITLE: Man1 - grub-mklayout.1
#+DESCRIPTION: Linux manpage for grub-mklayout.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grub-mklayout - generate a GRUB keyboard layout file

* SYNOPSIS
*grub-mklayout* [/OPTION/...] [/OPTIONS/]

* DESCRIPTION
grub-mklayout processes a keyboard layout description in *keymaps*(5)
format into a format that can be used by GRUB's *keymap* command.

Generate GRUB keyboard layout from Linux console one.

- *-i*, *--input*=/FILE/ :: set input filename. Default is STDIN

- *-o*, *--output*=/FILE/ :: set output filename. Default is STDOUT

- *-v*, *--verbose* :: print verbose messages.

- -?, *--help* :: give this help list

- *--usage* :: give a short usage message

- *-V*, *--version* :: print program version

Mandatory or optional arguments to long options are also mandatory or
optional for any corresponding short options.

* REPORTING BUGS
Report bugs to <bug-grub@gnu.org>.

* SEE ALSO
*grub-mkconfig*(8)

The full documentation for *grub-mklayout* is maintained as a Texinfo
manual. If the *info* and *grub-mklayout* programs are properly
installed at your site, the command

#+begin_quote
  *info grub-mklayout*
#+end_quote

should give you access to the complete manual.
