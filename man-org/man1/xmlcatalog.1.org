#+TITLE: Man1 - xmlcatalog.1
#+DESCRIPTION: Linux manpage for xmlcatalog.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xmlcatalog - Command line tool to parse and manipulate XML or SGML
catalog files.

* SYNOPSIS
*xmlcatalog* [*--sgml* | *--shell* | *--create* | *--del */VALUE(S)/ | [
*--add */TYPE/* */ORIG/* */REPLACE/* * | *--add */FILENAME/] | *--noout*
| *--no-super-update* | [*-v* | *--verbose*]] {/CATALOGFILE/}
{/ENTITIES/...}

* DESCRIPTION
*xmlcatalog* is a command line application allowing users to monitor and
manipulate XML and SGML catalogs. It is included in *libxml*(3).

Its functions can be invoked from a single command from the command
line, or it can perform multiple functions in interactive mode. It can
operate on both XML and SGML files.

* OPTIONS
*xmlcatalog* accepts the following options (in alphabetical order):

*--add */TYPE/* */ORIG/* */REPLACE/* *

#+begin_quote
  Add an entry to CATALOGFILE. /TYPE/ indicates the type of entry.
  Possible types are: /public/, /system/, /uri/, /rewriteSystem/,
  /rewriteURI/, /delegatePublic/, /delegateSystem/, /delegateURI/,
  /nextCatalog/. /ORIG/ is the original reference to be replaced, and
  /REPLACE/ is the URI of the replacement entity to be used. The *--add*
  option will not overwrite CATALOGFILE, outputting to stdout, unless
  *--noout* is used. The *--add* will always take three parameters even
  if some of the XML catalog constructs will have only a single
  argument.
#+end_quote

*--add */FILENAME/

#+begin_quote
  If the *--add* option is used following the *--sgml* option, only a
  single argument, a /FILENAME/, is used. This is used to add the name
  of a catalog file to an SGML supercatalog, a file that contains
  references to other included SGML catalog files.
#+end_quote

*--create*

#+begin_quote
  Create a new XML catalog. Outputs to stdout, ignoring /filename/
  unless *--noout* is used, in which case it creates a new catalog file
  /filename/.
#+end_quote

*--del */VALUE(S)/

#+begin_quote
  Remove entries from /CATALOGFILE/ matching /VALUE(S)/. The *--del*
  option will not overwrite /CATALOGFILE/, outputting to stdout, unless
  *--noout* is used.
#+end_quote

*--noout*

#+begin_quote
  Save output to the named file rather than outputting to stdout.
#+end_quote

*--no-super-update*

#+begin_quote
  Do not update the SGML super catalog.
#+end_quote

*--shell*

#+begin_quote
  Run a shell allowing interactive queries on catalog file
  /CATALOGFILE/. For the set of available commands see the section
  called “SHELL COMMANDS”.
#+end_quote

*--sgml*

#+begin_quote
  Uses SGML super catalogs for *--add* and *--del* options.
#+end_quote

*-v*, *--verbose*

#+begin_quote
  Output debugging information.
#+end_quote

Invoking *xmlcatalog* non-interactively without a designated action
(imposed with options like *--add*) will result in a lookup of the
catalog entry for /ENTITIES/ in the catalog denoted with /CATALOGFILE/.
The corresponding entries will be output to the command line. This mode
of operation, together with *--shell* mode and non-modifying (i.e.
without *--noout*) direct actions, allows for a special shortcut of the
void /CATALOGFILE/ specification (possibly expressed as "" in the shell
environment) appointing the default system catalog. That simplifies the
handling when its exact location is irrelevant but the respective
built-in still needs to be consulted.

* SHELL COMMANDS
Invoking *xmlcatalog* with the *--shell */CATALOGFILE/ option opens a
command line shell allowing interactive access to the catalog file
identified by /CATALOGFILE/. Invoking the shell provides a command line
prompt after which the following commands (described in alphabetical
order) can be entered.

*add */TYPE/* */ORIG/* */REPLACE/* *

#+begin_quote
  Add an entry to the catalog file. /TYPE/ indicates the type of entry.
  Possible types are: /public/, /system/, /uri/, /rewriteSystem/,
  /rewriteURI/, /delegatePublic/, /delegateSystem/, /delegateURI/,
  /nextCatalog/. /ORIG/ is the original reference to be replaced, and
  /REPLACE/ is the URI of the replacement entity to be used. The *--add*
  option will not overwrite CATALOGFILE, outputting to stdout, unless
  *--noout* is used. The *--add* will always take three parameters even
  if some of the XML catalog constructs will have only a single
  argument.
#+end_quote

*debug*

#+begin_quote
  Print debugging statements showing the steps *xmlcatalog* is
  executing.
#+end_quote

*del */VALUE(S)/

#+begin_quote
  Remove the catalog entry corresponding to /VALUE(S)/.
#+end_quote

*dump*

#+begin_quote
  Print the current catalog.
#+end_quote

*exit*

#+begin_quote
  Quit the shell.
#+end_quote

*public */PUBLIC-ID/

#+begin_quote
  Execute a Formal Public Identifier lookup of the catalog entry for
  /PUBLIC-ID/. The corresponding entry will be output to the command
  line.
#+end_quote

*quiet*

#+begin_quote
  Stop printing debugging statements.
#+end_quote

*system */SYSTEM-ID/

#+begin_quote
  Execute a Formal Public Identifier lookup of the catalog entry for
  /SYSTEM-ID/. The corresponding entry will be output to the command
  line.
#+end_quote

* ENVIRONMENT
*XML_CATALOG_FILES*

#+begin_quote
  XML catalog behavior can be changed by redirecting queries to the
  users own set of catalogs. This can be done by setting the
  *XML_CATALOG_FILES* environment variable to a list of catalogs. An
  empty one should deactivate loading the default /etc/xml/catalog
  catalog.
#+end_quote

* DIAGNOSTICS
*xmlcatalog* return codes provide information that can be used when
calling it from scripts.

*0*

#+begin_quote
  No error
#+end_quote

*1*

#+begin_quote
  Failed to remove an entry from the catalog
#+end_quote

*2*

#+begin_quote
  Failed to save to the catalog, check file permissions
#+end_quote

*3*

#+begin_quote
  Failed to add an entry to the catalog
#+end_quote

*4*

#+begin_quote
  Failed to look up an entry in the catalog
#+end_quote

* SEE ALSO
*libxml*(3)

More information can be found at

#+begin_quote
  ·

  *libxml*(3) web page *http://www.xmlsoft.org/*
#+end_quote

#+begin_quote
  ·

  *libxml*(3) catalog support web page at
  *http://www.xmlsoft.org/catalog.html*
#+end_quote

#+begin_quote
  ·

  James Clarks SGML catalog page *http://www.jclark.com/sp/catalog.htm*
#+end_quote

#+begin_quote
  ·

  OASIS XML catalog specification
  *http://www.oasis-open.org/committees/entity/spec.html*
#+end_quote

* AUTHOR
*John Fleck* <jfleck@inkstain.net>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001, 2004\\
