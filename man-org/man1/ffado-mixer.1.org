#+TITLE: Man1 - ffado-mixer.1
#+DESCRIPTION: Linux manpage for ffado-mixer.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ffado-mixer - a graphical front-end for the FFADO mixer DBus controls
exposed by ffado-dbus-server.

* SYNOPSIS
*ffado-mixer [options]*

Available options: *[-b]*

* DESCRIPTION
*ffado-mixer* presents a graphical application allowing a FFADO device
to be controlled. The extent of the control is determined by the level
of support for the device in FFADO and in *ffado-dbus-server.* Typical
controls offered by *ffado-mixer* include faders for the on-board mixer,
phantom power control, mode switches and so on.

* OPTIONS
- *-b* :: Do not query ffado-dbus-server for an active interface.
  Instead, create an instance of a mixer for every supported interface
  as defined in FFADO's "configuration" file with stubs providing
  placeholder values in place of settings obtained from the interface.
  This is primarily to assist with debugging.

* SEE ALSO
*ffado-dbus-server*(1)
