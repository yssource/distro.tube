#+TITLE: Manpages - loffice.1
#+DESCRIPTION: Linux manpage for loffice.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about loffice.1 is found in manpage for: [[../man1/libreoffice.1][man1/libreoffice.1]]