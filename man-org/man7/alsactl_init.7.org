#+TITLE: Manpages - alsactl_init.7
#+DESCRIPTION: Linux manpage for alsactl_init.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
alsactl_init - alsa control management - initialization

* DESCRIPTION
"alsactl init" provides soundcard specific initialization.

* CONFIGURATION
All "alsactl init" configuration files are placed in
/usr/share/alsa/init/ directory. The top level configuration file is
/usr/share/alsa/init/00main. The default top-level file can be also
specified using -i or --initfile parameter for the alsactl tool. Every
file consists of a set of lines of text. All empty lines or lines
beginning with # will be ignored.

** Rules files
The "alsactl init" rules are read from the files located in the
/usr/share/alsa/init/*. The top level configuration file is
/usr/share/alsa/init/00main. Every line in the rules file contains at
least one key value pair. There are two kind of keys, match and
assignment keys. If all match keys are matching against its value, the
rule gets applied and the assign keys get the specified value assigned.

A rule may consists of a list of one or more key value pairs separated
by a comma. Each key has a distinct operation, depending on the used
operator. Valid operators are:

*==*

#+begin_quote
  Compare for equality.
#+end_quote

*!=*

#+begin_quote
  Compare for non-equality.
#+end_quote

*=*

#+begin_quote
  Assign a value to a key. Keys that represent a list, are reset and
  only this single value is assigned.
#+end_quote

*+=*

#+begin_quote
  Add the value to a key that holds a list of entries.
#+end_quote

*:=*

#+begin_quote
  Assign a value to a key finally; disallow any later changes, which may
  be used to prevent changes by any later rules.
#+end_quote

The following key names can be used to match against device properties:

*CARDINDEX*

#+begin_quote
  Match the card index of the ALSA driver.
#+end_quote

*CTL{*/attribute/*}*

#+begin_quote
  Set or test universal control attribute. Possible attributes:

  *numid*

  #+begin_quote
    Numeric control identification.
  #+end_quote

  *iface*, *interface*

  #+begin_quote
    Control interface name (CARD, HWEDEP, MIXER, PCM, RAWMIDI, TIMER,
    SEQUENCER)
  #+end_quote

  *subdev*, *subdevice*

  #+begin_quote
    Subdevice number.
  #+end_quote

  *name*

  #+begin_quote
    Control name
  #+end_quote

  *index*

  #+begin_quote
    Control index
  #+end_quote

  *type*

  #+begin_quote
    Control type (BOOLEAN, INTEGER, INTEGER64, ENUMERATED, BYTES,
    IEC958)
  #+end_quote

  *attr*, *attribute*

  #+begin_quote
    Attributes (stored in a string - use match characters * and ?):

    *r*

    #+begin_quote
      control is readable
    #+end_quote

    *w*

    #+begin_quote
      control is writable
    #+end_quote

    *v*

    #+begin_quote
      control is volatile
    #+end_quote

    *i*

    #+begin_quote
      control is inactive
    #+end_quote

    *l*

    #+begin_quote
      control is locked
    #+end_quote

    *R*

    #+begin_quote
      control is TLV readable
    #+end_quote

    *W*

    #+begin_quote
      control is TLV writable
    #+end_quote

    *C*

    #+begin_quote
      control is TLV commandable
    #+end_quote

    *o*

    #+begin_quote
      process is owner of this control
    #+end_quote

    *u*

    #+begin_quote
      control created in user space
    #+end_quote
  #+end_quote

  *owner*

  #+begin_quote
    Control owner process PID number
  #+end_quote

  *count*

  #+begin_quote
    Control count of values
  #+end_quote

  *min*

  #+begin_quote
    Value range - minimum value
  #+end_quote

  *max*

  #+begin_quote
    Value range - maximum value
  #+end_quote

  *step*

  #+begin_quote
    Value range - step value
  #+end_quote

  *dBmin*

  #+begin_quote
    Value range - minimum dB value
  #+end_quote

  *dBmax*

  #+begin_quote
    Value range - maximum dB value
  #+end_quote

  *items*

  #+begin_quote
    Enumerated value - number of text items
  #+end_quote

  *enums*

  #+begin_quote
    Enumerated value - list of text names stored between | character
  #+end_quote

  *value*

  #+begin_quote
    Value of control stored to a string delimited by comma (,).
  #+end_quote

  *do_search*

  #+begin_quote
    Search for a control. Value "1" is returned if a control was found.
    The CTL{name} key might contain match characters * and ?. An control
    index might be specified as first argument starting from zero (e.g.
    CTL{do_search 2}="1").
  #+end_quote

  *do_count*

  #+begin_quote
    Search for a controls and return total count of matched ones. The
    CTL{name} key might contain match characters * and ?.
  #+end_quote
#+end_quote

*CONFIG{sysfs_device}*

#+begin_quote
  The relative path to sysfs subsystem specifying the root directory of
  a soundcard device. Usually, it should be set to
  "/class/sound/card$cardinfo{card}/device".
#+end_quote

*ATTR{*/filename/*}*

#+begin_quote
  Match sysfs attribute values of the soundcard device. The relative
  path to sysfs tree must be defined by CONFIG{sysfs_device} key.
  Trailing whitespace in the attribute values is ignored, if the
  specified match value does not contain trailing whitespace itself.
  Depending on the type of operator, this key is also used to set the
  value of a sysfs attribute.
#+end_quote

*ENV{*/key/*}*

#+begin_quote
  Match against the value of an environment variable. Up to five *ENV*
  keys can be specified per rule. Depending on the type of operator,
  this key is also used to export a variable to the environment.
#+end_quote

*PROGRAM*

#+begin_quote
  Execute external program. The key is true, if the program returns
  without exit code zero. The whole event environment is available to
  the executed program. The programs output printed to stdout is
  available for the RESULT key.

  Several buildin commands are available:

  *__ctl_search*

  #+begin_quote
    Search for a control. The CTL{name} key might contain match
    characters * and ?. An control index might be specified as first
    argument starting from zero (e.g. PROGRAM="__ctl_search 2").
  #+end_quote

  *__ctl_count*

  #+begin_quote
    Search for a controls and return total count of matched ones. The
    CTL{name} key might contain match characters * and ?.
  #+end_quote
#+end_quote

*RESULT*

#+begin_quote
  Match the returned string of the last PROGRAM call. This key can be
  used in the same or in any later rule after a PROGRAM call.
#+end_quote

Most of the fields support a shell style pattern matching. The following
pattern characters are supported:

***

#+begin_quote
  Matches zero, or any number of characters.
#+end_quote

*?*

#+begin_quote
  Matches any single character.
#+end_quote

*[]*

#+begin_quote
  Matches any single character specified within the brackets. For
  example, the pattern string tty[SR] would match either ttyS or ttyR.
  Ranges are also supported within this match with the - character. For
  example, to match on the range of all digits, the pattern [0-9] would
  be used. If the first character following the [ is a !, any characters
  not enclosed are matched.
#+end_quote

The following keys can get values assigned:

*CTL{numid}*, *CTL{iface}*, *CTL{device}*, *CTL{subdev}*, *CTL{name}*,
*CTL{index}*,

#+begin_quote
  Select universal control element.
#+end_quote

*CTL{value}*

#+begin_quote
  Value is set (written) also to soundcards control device and RESULT
  key is set to errno code. The result of set operation is always true
  (it means continue with next key on line).
#+end_quote

*CTL{values}*

#+begin_quote
  Value is set (written) also to soundcards control device (all control
  values are set to specified value) and RESULT key is set to errno
  code. The result of set operation is always true (it means continue
  with next key on line).
#+end_quote

*CTL{write}*

#+begin_quote
  Value is set (written) also to soundcards control device (all control
  values are set to specified value). The result of set operation is
  true when operation succeed (it means continue with next key on line).
#+end_quote

*ENV{*/key/*}*

#+begin_quote
  Export a variable to the environment. Depending on the type of
  operator, this key is also to match against an environment variable.
#+end_quote

*RESULT*

#+begin_quote
  Set RESULT variable. Note that PROGRAM also sets this variable, but
  setting this variable manually might be useful to change code
  execution order (included files).
#+end_quote

*LABEL*

#+begin_quote
  Named label where a GOTO can jump to.
#+end_quote

*GOTO*

#+begin_quote
  Jumps to the next LABEL with a matching name. The goto cannot jump
  backward.
#+end_quote

*INCLUDE*

#+begin_quote
  Include the specified filename or files in specified directory.

  When a directory is specified, only the files with the extension
  ".conf" are read. Also they are read in the alphabetical order. Thus
  its highly recommended to use some number prefix (e.g.
  "01-something.conf") to assure the order of execucions.
#+end_quote

*ACCESS*

#+begin_quote
  Check if specified file or directory exists
#+end_quote

*CONFIG{sysfs_device}*

#+begin_quote
  The relative path to sysfs subsystem specifying the root directory of
  a soundcard device. Usually, it should be set to
  "/class/sound/card$cardinfo{card}/device".
#+end_quote

*PRINT*

#+begin_quote
  PRINT value to stdout.
#+end_quote

*ERROR*

#+begin_quote
  PRINT value to stderr.
#+end_quote

*EXIT*

#+begin_quote
  Exit immediately and set program exit code to value (should be
  integer). If value is "return" string, parser leaves current included
  file and returns to parent configuration file.
#+end_quote

The *PROGRAM*, *RESULT*, *CTL{value}*, *PRINT*, *ERROR*, *EXIT*,
*CONFIG{}* fields support simple printf-like string substitutions. It
allows the use of the complete environment set by earlier matching
rules. For all other fields, substitutions are applied while the
individual rule is being processed. The available substitutions are:

*$cardinfo{*/attribute/*}*, *%i{*/attribute/*}*

#+begin_quote
  See CARDINFO{} for more details.
#+end_quote

*$ctl{*/attribute/*}*, *%C{*/attribute/*}*

#+begin_quote
  See CTL{} for more details.
#+end_quote

*$attr{*/file/*}*, *%s{*/file/*}*

#+begin_quote
  The value of a sysfs attribute found at the device, where all keys of
  the rule have matched. If the attribute is a symlink, the last element
  of the symlink target is returned as the value.
#+end_quote

*$env{*/key/*}*, *%E{*/key/*}*

#+begin_quote
  The value of an environment variable.
#+end_quote

*$result*, *%c*

#+begin_quote
  The string returned by the external program requested with PROGRAM. A
  single part of the string, separated by a space character may be
  selected by specifying the part number as an attribute: *%c{N}*. If
  the number is followed by the + char this part plus all remaining
  parts of the result string are substituted: *%c{N+}*
#+end_quote

*$sysfsroot*, *%r*

#+begin_quote
  Root directory where sysfs file-system is mounted. Ususally, this
  value is just "/sys".
#+end_quote

*$config{*/key/*}*, *%g{*/key/*}*

#+begin_quote
  The value of a configuration variable. See CONFIG{} for more details.
#+end_quote

*%%*

#+begin_quote
  The % character itself.
#+end_quote

*$$*

#+begin_quote
  The $ character itself.
#+end_quote

The count of characters to be substituted may be limited by specifying
the format length value. For example, %3s{file} will only insert the
first three characters of the sysfs attribute

* AUTHOR
Written by Jaroslav Kysela <perex@perex.cz>

Some portions are written by Greg Kroah-Hartman <greg@kroah.com> and Kay
Sievers <kay.sievers@vrfy.org>.

* SEE ALSO
*alsactl*(1)
