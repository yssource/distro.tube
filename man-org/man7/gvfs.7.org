#+TITLE: Manpages - gvfs.7
#+DESCRIPTION: Linux manpage for gvfs.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gvfs - GIO virtual file system

* DESCRIPTION
GIO provides a VFS API to GLib applications. It includes a local
implementation using POSIX. gvfs provides implementations that go beyond
that and allow to access files and storage using many protocols, such as
ftp, http, sftp, dav, nfs, etc. It also provides support for trash
folders, for cd burning and for monitoring interesting devices and
volumes on the computer.

Applications use gvfs indirectly, by means of GIO loading the gvfs
module that implements the GIO extension points. The gvfs support for
volume monitoring is included in a separate loadable module.

The actual gvfs implementation is distributed over a number of
processes. None of these are expected to be started from the
commandline, except for debugging purposes.

Main processes:

#+begin_quote
  ·

  gvfsd - the main gvfs daemon
#+end_quote

#+begin_quote
  ·

  gvfsd-fuse - mounts gvfs as a fuse filesystem
#+end_quote

#+begin_quote
  ·

  gvfsd-metadata - writes gvfs metadata
#+end_quote

Volume monitors:

#+begin_quote
  ·

  gvfs-afc-volume-monitor - a volume monitor for Apple iPhone/iPod Touch
  devices
#+end_quote

#+begin_quote
  ·

  gvfs-goa-volume-monitor - a volume monitor for GNOME Online Accounts
#+end_quote

#+begin_quote
  ·

  gvfs-gphoto2-volume-monitor - a volume monitor for PTP devices
#+end_quote

#+begin_quote
  ·

  gvfs-mtp-volume-monitor - a volume monitor for MTP devices
#+end_quote

#+begin_quote
  ·

  gvfs-udisks2-volume-monitor - a udisks2-based volume monitor
#+end_quote

Mount backends:

#+begin_quote
  ·

  gvfsd-admin - mounts local filesystem with admin privileges
#+end_quote

#+begin_quote
  ·

  gvfsd-afc - mounts iPhone/iPod touch volumes
#+end_quote

#+begin_quote
  ·

  gvfsd-afp - mounts Apple Filing Protocol volumes
#+end_quote

#+begin_quote
  ·

  gvfsd-afp-browse - browses Apple Filing Protocol volumes
#+end_quote

#+begin_quote
  ·

  gvfsd-archive - mounts archive files in various formats
#+end_quote

#+begin_quote
  ·

  gvfsd-burn - provides a location for burning CDs
#+end_quote

#+begin_quote
  ·

  gvfsd-cdda - mounts audio CDs
#+end_quote

#+begin_quote
  ·

  gvfsd-computer - provides computer://
#+end_quote

#+begin_quote
  ·

  gvfsd-dav - mounts DAV filesystems
#+end_quote

#+begin_quote
  ·

  gvfsd-dnssd - browses dnssd
#+end_quote

#+begin_quote
  ·

  gvfsd-ftp - mounts over FTP
#+end_quote

#+begin_quote
  ·

  gvfsd-google - mounts Google Drive shares
#+end_quote

#+begin_quote
  ·

  gvfsd-gphoto2 - mounts over PTP
#+end_quote

#+begin_quote
  ·

  gvfsd-http - mounts over HTTP
#+end_quote

#+begin_quote
  ·

  gvfsd-localtest - a test backend
#+end_quote

#+begin_quote
  ·

  gvfsd-mtp - mounts over MTP
#+end_quote

#+begin_quote
  ·

  gvfsd-network - provides network://
#+end_quote

#+begin_quote
  ·

  gvfsd-nfs - mounts over NFS
#+end_quote

#+begin_quote
  ·

  gvfsd-recent - provides recent://
#+end_quote

#+begin_quote
  ·

  gvfsd-sftp - mounts over sftp
#+end_quote

#+begin_quote
  ·

  gvfsd-smb - mounts Windows Shares Filesystem volumes
#+end_quote

#+begin_quote
  ·

  gvfsd-smb-browse - browses Windows Shares Filesystem volumes
#+end_quote

#+begin_quote
  ·

  gvfsd-trash - provides trash://
#+end_quote

* ENVIRONMENT
*GIO_USE_VFS*

#+begin_quote
  If set, specifies the GIO vfs implementation to use. Possible values
  include local and gvfs.
#+end_quote

*GIO_USE_VOLUME_MONITOR*

#+begin_quote
  If set, specifies the GIO volume monitor implementation to use.
  Possible values include unix, GProxyVolumeMonitorUDisks2, as well as
  other native volume monitors that are described in the key files in
  *$XDG_DATA_DIRS*/gvfs/remote-volume-monitors.
#+end_quote

* FILES
*$XDG_DATA_DIRS*/gvfs/mounts

#+begin_quote
  This directory contains key files describing mount daemons.
#+end_quote

*$XDG_DATA_DIRS*/gvfs/remote-volume-monitors

#+begin_quote
  This directory contains key files describing remote volume monitors.
#+end_quote

* SEE ALSO
*GIO documentation*[1], *gvfsd*(1), *gvfsd-fuse*(1), *gvfsd-metadata*(1)

* NOTES
-  1. :: GIO documentation

  http://developer.gnome.org/gio
