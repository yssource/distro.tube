#+TITLE: Manpages - glibc.7
#+DESCRIPTION: Linux manpage for glibc.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about glibc.7 is found in manpage for: [[../man7/libc.7][man7/libc.7]]