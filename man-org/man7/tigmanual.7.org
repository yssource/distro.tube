#+TITLE: Manpages - tigmanual.7
#+DESCRIPTION: Linux manpage for tigmanual.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tigmanual - text-mode interface for Git

* SYNOPSIS
#+begin_example
  tig        [options] [revisions] [--] [paths]
  tig show   [options] [revisions] [--] [paths]
  tig blame  [options] [rev] [--] path
  tig status
  tig <      [Git command output]
#+end_example

* DESCRIPTION
This is the manual for Tig, the ncurses-based text-mode interface for
git. Tig allows you to browse changes in a Git repository and can
additionally act as a pager for output of various Git commands. When
used as a pager, it will display input from stdin and colorize it.

When browsing repositories, Tig uses the underlying Git commands to
present the user with various views, such as summarized commit log and
showing the commit with the log message, diffstat, and the diff.

* CALLING CONVENTIONS
** Pager Mode
If stdin is a pipe, any log or diff options will be ignored and the
pager view will be opened loading data from stdin. The pager mode can be
used for colorizing output from various Git commands.

Example on how to colorize the output of git-show(1):

#+begin_quote
  #+begin_example
    $ git show | tig
  #+end_example
#+end_quote

** Git Command Options
All Git command options specified on the command line will be passed to
the given command and all will be shell quoted before they are passed to
the shell.

#+begin_quote
  \\

  *Note*

  \\

  If you specify options for the main view, you should not use the
  --pretty option as this option will be set automatically to the format
  expected by the main view.
#+end_quote

Example on how to view a commit and show both author and committer
information:

#+begin_quote
  #+begin_example
    $ tig show --pretty=fuller
  #+end_example
#+end_quote

See the section on specifying revisions for an introduction to revision
options supported by the Git commands. For details on specific Git
command options, refer to the man page of the command in question.

* THE VIEWER
The display consists of a status window on the last line of the screen
and one or more views. The default is to only show one view at a time
but it is possible to split both the main and log view to also show the
commit diff.

If you are in the log view and press /Enter/ when the current line is a
commit line, such as:

#+begin_quote
  #+begin_example
    commit 4d55caff4cc89335192f3e566004b4ceef572521
  #+end_example
#+end_quote

You will split the view so that the log view is displayed in the top
window and the diff view in the bottom window. You can switch between
the two views by pressing /Tab/. To maximize the log view again, simply
press /l/.

** Views
Various /views/ of a repository are presented. Each view is based on
output from an external command, most often /git log/, /git diff/, or
/git show/.

The main view

#+begin_quote
  Is the default view, and it shows a one line summary of each commit in
  the chosen list of revisions. The summary includes author date,
  author, and the first line of the log message. Additionally, any
  repository references, such as tags, will be shown.
#+end_quote

The log view

#+begin_quote
  Presents a more rich view of the revision log showing the whole log
  message and the diffstat.
#+end_quote

The reflog view

#+begin_quote
  Presents a view of the reflog allowing to navigate the repo history.
#+end_quote

The diff view

#+begin_quote
  Shows either the diff of the current working tree, that is, what has
  changed since the last commit, or the commit diff complete with log
  message, diffstat and diff.
#+end_quote

The tree view

#+begin_quote
  Lists directory trees associated with the current revision allowing
  subdirectories to be descended or ascended and file blobs to be
  viewed.
#+end_quote

The blob view

#+begin_quote
  Displays the file content or "blob" of data associated with a file
  name.
#+end_quote

The blame view

#+begin_quote
  Displays the file content annotated or blamed by commits.
#+end_quote

The refs view

#+begin_quote
  Displays the branches, remotes and tags in the repository.
#+end_quote

The status view

#+begin_quote
  Displays status of files in the working tree and allows changes to be
  staged/unstaged as well as adding of untracked files.
#+end_quote

The stage view

#+begin_quote
  Displays diff changes for staged or unstaged files being tracked or
  file content of untracked files.
#+end_quote

The stash view

#+begin_quote
  Displays the list of stashes in the repository.
#+end_quote

The grep view

#+begin_quote
  Displays a list of files and all the lines that matches a search
  pattern.
#+end_quote

The pager view

#+begin_quote
  Is used for displaying both input from stdin and output from Git
  commands entered in the internal prompt.
#+end_quote

The help view

#+begin_quote
  Displays a quick reference of key bindings.
#+end_quote

** Browsing State and User-defined Commands
The viewer keeps track of both what head and commit ID you are currently
viewing. The commit ID will follow the cursor line and change every time
you highlight a different commit. Whenever you reopen the diff view it
will be reloaded, if the commit ID changed. The head ID is used when
opening the main and log view to indicate from what revision to show
history.

Some of the commands used or provided by Tig can be configured. This
goes for some of the environment variables as well as the external
commands. These user-defined commands can use arguments that refer to
the current browsing state by using one of the following variables.

\\
*Table 1. Browsing state variables*

| %(head)                     | The currently viewed /head/ ID. Defaults to HEAD                                                        |
| %(commit)                   | The currently selected commit ID.                                                                       |
| %(blob)                     | The currently selected blob ID.                                                                         |
| %(branch)                   | The currently selected branch name.                                                                     |
| %(remote)                   | The currently selected remote name. For remote branches %(branch) will contain the branch name.         |
| %(tag)                      | The currently selected tag name.                                                                        |
| %(refname)                  | The currently selected reference name including the remote name for remote branches.                    |
| %(stash)                    | The currently selected stash name.                                                                      |
| %(directory)                | The current directory path in the tree view or "." if undefined.                                        |
| %(file)                     | The currently selected file.                                                                            |
| %(lineno)                   | The currently selected line number. Defaults to 0.                                                      |
| %(lineno_old)               | The currently selected line number, before the diff was applied. Defaults to 0.                         |
| %(ref)                      | The reference given to blame or HEAD if undefined.                                                      |
| %(revargs)                  | The revision arguments passed on the command line.                                                      |
| %(fileargs)                 | The file arguments passed on the command line.                                                          |
| %(cmdlineargs)              | All other options passed on the command line.                                                           |
| %(diffargs)                 | Options from /diff-options/ or /TIG_DIFF_OPTS/ used by the diff and stage view.                         |
| %(blameargs)                | Options from /blame-options/ used by the blame view.                                                    |
| %(logargs)                  | Options from /log-options/ used by the log view.                                                        |
| %(mainargs)                 | Options from /main-options/ used by the main view.                                                      |
| %(prompt)                   | Prompt for the argument value. Optionally specify a custom prompt using "%(prompt Enter branch name: )" |
| %(text)                     | The text of the currently selected line.                                                                |
| %(repo:head)                | The name of the checked out branch, e.g. master                                                         |
| %(repo:head-id)             | The commit ID of the checked out branch.                                                                |
| %(repo:remote)              | The remote associated with the checked out branch, e.g. origin/master.                                  |
| %(repo:cdup)                | The path to change directory to the repository root, e.g. ../                                           |
| %(repo:prefix)              | The path prefix of the current work directory, e.g subdir/.                                             |
| %(repo:git-dir)             | The path to the Git directory, e.g. /src/repo/.git.                                                     |
| %(repo:worktree)            | The worktree path, if defined.                                                                          |
| %(repo:is-inside-work-tree) | Whether Tig is running inside a work tree, either true or false.                                        |

Example user-defined commands:

#+begin_quote
  ·

  Allow to amend the last commit:

  #+begin_quote
    #+begin_example
      bind generic + !git commit --amend
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Copy commit ID to clipboard:

  #+begin_quote
    #+begin_example
      bind generic 9 @sh -c "echo -n %(commit) | xclip -selection c"
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Add/edit notes for the current commit used during a review:

  #+begin_quote
    #+begin_example
      bind generic T !git notes edit %(commit)
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Enter Git's interactive add for fine-grained staging of file content:

  #+begin_quote
    #+begin_example
      bind generic I !git add -i %(file)
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Rebase current branch on top of the selected branch:

  #+begin_quote
    #+begin_example
      bind refs 3 !git rebase -i %(branch)
    #+end_example
  #+end_quote
#+end_quote

** Title Windows
Each view has a title window which shows the name of the view, current
commit ID if available, and where the view is positioned:

#+begin_quote
  #+begin_example
    [main] c622eefaa485995320bc743431bae0d497b1d875 - commit 1 of 61 (1%)
  #+end_example
#+end_quote

By default, the title of the current view is highlighted using bold
font. For long loading views (taking over 3 seconds) the time since
loading started will be appended:

#+begin_quote
  #+begin_example
    [main] 77d9e40fbcea3238015aea403e06f61542df9a31 - commit 1 of 779 (0%) 5s
  #+end_example
#+end_quote

* ENVIRONMENT VARIABLES
Several options related to the interface with Git can be configured via
environment options.

** Configuration Files
Upon startup, Tig first reads the system wide configuration file
({sysconfdir}/tigrc by default) and then proceeds to read the user's
configuration file (~/.tigrc or $XDG_CONFIG_HOME/tig/config by default).
The paths to either of these files can be overridden through the
following environment variables:

TIGRC_USER

#+begin_quote
  Path of the user configuration file.
#+end_quote

TIGRC_SYSTEM

#+begin_quote
  Path of the system wide configuration file.
#+end_quote

** History Files
If compiled with readline support, Tig writes a persistent command and
search history to ~/.tig_history or $XDG_DATA_HOME/tig/history.

** Repository References
Commits that are referenced by tags and branch heads will be marked by
the reference name surrounded by /[/ and /]/:

#+begin_quote
  #+begin_example
    2006-03-26 19:42 Petr Baudis         | [cogito-0.17.1] Cogito 0.17.1
  #+end_example
#+end_quote

If you want to limit what branches are shown, say only show branches
named master or those which start with the feature/ prefix, you can do
it by setting the following variable:

#+begin_quote
  #+begin_example
    $ TIG_LS_REMOTE="git ls-remote . master feature/*" tig
  #+end_example
#+end_quote

Or set the variable permanently in your environment.

TIG_LS_REMOTE

#+begin_quote
  Command for retrieving all repository references. The command should
  output data in the same format as git-ls-remote(1). Defaults to:
#+end_quote

#+begin_quote
  #+begin_example
    git ls-remote .
  #+end_example
#+end_quote

** Diff options
It is possible to alter how diffs are shown by the diff view. If for
example you prefer to have commit and author dates shown as relative
dates, use:

#+begin_quote
  #+begin_example
    $ TIG_DIFF_OPTS="--relative-date" tig
  #+end_example
#+end_quote

Or set the variable permanently in your environment.

* DEFAULT KEYBINDINGS
Below the default key bindings are shown.

** View Switching
| Key | Action                           |
| m   | Switch to main view.             |
| d   | Switch to diff view.             |
| l   | Switch to log view.              |
| p   | Switch to pager view.            |
| t   | Switch to (directory) tree view. |
| f   | Switch to (file) blob view.      |
| g   | Switch to grep view.             |
| b   | Switch to blame view.            |
| r   | Switch to refs view.             |
| y   | Switch to stash view.            |
| h   | Switch to help view              |
| s   | Switch to status view            |
| c   | Switch to stage view             |

** View Manipulation
| Key   | Action                                                                                                                                                                                                                                                                                                                                                                                       |
| q     | Close view, if multiple views are open it will jump back to the previous view in the view stack. If it is the last open view it will quit. Use /Q/ to quit all views at once.                                                                                                                                                                                                                |
| Enter | This key is "context sensitive" depending on what view you are currently in. When in log view on a commit line or in the main view, split the view and show the commit diff. In the diff view pressing Enter will simply scroll the view one line down.                                                                                                                                      |
| Tab   | Switch to next view.                                                                                                                                                                                                                                                                                                                                                                         |
| R     | Reload and refresh the current view.                                                                                                                                                                                                                                                                                                                                                         |
| O     | Maximize the current view to fill the whole display.                                                                                                                                                                                                                                                                                                                                         |
| Up    | This key is "context sensitive" and will move the cursor one line up. However, if you opened a split view it will change the cursor to point to the previous commit in the parent view and update the child view to display it. If you prefer this key to move the cursor or scroll within the diff view instead, use bind diff <Up> move-up or bind diff <Up> scroll-line-up, respectively. |
| Down  | Similar to /Up/ but will move down.                                                                                                                                                                                                                                                                                                                                                          |
| ,     | Move to parent. In the tree view, this means switch to the parent directory. In the blame view it will load blame for the parent commit. For merges the parent is queried.                                                                                                                                                                                                                   |

** View Specific Actions
| Key | Action                                                                                                                                                                                                                                                                                                                                                 |
| u   | Update status of file. In the status view, this allows you to add an untracked file or stage changes to a file for next commit (similar to running git-add <filename>). In the stage view, when pressing this on a diff chunk line stages only that chunk for next commit, when not on a diff chunk line all changes in the displayed diff are staged. |
| M   | Resolve unmerged file by launching git-mergetool(1). Note, to work correctly this might require some initial configuration of your preferred merge tool. See the manpage of git-mergetool(1).                                                                                                                                                          |
| !   | Checkout file with unstaged changes. This will reset the file to contain the content it had at last commit.                                                                                                                                                                                                                                            |
| 1   | Stage single diff line.                                                                                                                                                                                                                                                                                                                                |
| @   | Move to next chunk in the stage view.                                                                                                                                                                                                                                                                                                                  |
| ]   | Increase the diff context.                                                                                                                                                                                                                                                                                                                             |
| [   | Decrease the diff context.                                                                                                                                                                                                                                                                                                                             |

** Cursor Navigation
| Key           | Action                     |
| k             | Move cursor one line up.   |
| j             | Move cursor one line down. |
| PgUp, -       | Move cursor one page up.   |
| PgDown, Space | Move cursor one page down. |
| Home          | Jump to first line.        |
| End           | Jump to last line.         |

** Scrolling
| Key        | Action                           |
| Insert     | Scroll view one line up.         |
| Delete     | Scroll view one line down.       |
| ScrollBack | Scroll view one page up.         |
| ScrollFwd  | Scroll view one page down.       |
| Left       | Scroll view one column left.     |
| Right      | Scroll view one column right.    |
| |          | Scroll view to the first column. |

** Searching
| Key | Action                                                             |
| /   | Search the view. Opens a prompt for entering search regexp to use. |
| ?   | Search backwards in the view. Also prompts for regexp.             |
| n   | Find next match for the current search regexp.                     |
| N   | Find previous match for the current search regexp.                 |

** Misc
| Key   | Action                                                                                                                                |
| Q     | Quit.                                                                                                                                 |
| <C-L> | Redraw screen.                                                                                                                        |
| z     | Stop all background loading. This can be useful if you use Tig in a repository with a long history without limiting the revision log. |
| v     | Show version.                                                                                                                         |
| o     | Open option menu                                                                                                                      |
| #     | Toggle line numbers on/off.                                                                                                           |
| D     | Toggle date display on/off/relative/relative-compact/custom                                                                           |
| A     | Toggle author display on/off/abbreviated/email/email user name.                                                                       |
| G     | Toggle revision graph visualization on/off.                                                                                           |
| ~     | Toggle (line) graphics mode                                                                                                           |
| F     | Toggle reference display on/off (tag and branch names).                                                                               |
| W     | Toggle ignoring whitespace on/off for diffs                                                                                           |
| X     | Toggle commit ID display on/off                                                                                                       |
| %     | Toggle file filtering in order to see the full diff instead of only the diff concerning the currently selected file.                  |
| $     | Toggle highlighting of commit title overflow.                                                                                         |
| H     | Go to the HEAD commit.                                                                                                                |
| :     | Open prompt. This allows you to specify what command to run and also to jump to a specific line, e.g. :23                             |
| e     | Open file in editor.                                                                                                                  |

** Prompt
| Key                    | Action                                                                                                                                                               |
| :<number>              | Jump to the specific line number, e.g. :80.                                                                                                                          |
| :<sha>                 | Jump to a specific commit, e.g. :2f12bcc.                                                                                                                            |
| :<x>                   | Execute the corresponding key binding, e.g. :q.                                                                                                                      |
| :!<command>            | Execute a system command in a pager, e.g. :!git log -p.                                                                                                              |
| :<action>              | Execute a Tig command, e.g. :edit.                                                                                                                                   |
| :goto <rev>            | Jump to a specific revision, e.g. :goto %(commit)^2 to goto the current commit's 2nd parent or :goto some/branch to goto the commit denoting the branch some/branch. |
| :save-display <file>   | Save current display to <file>.                                                                                                                                      |
| :save-options <file>   | Save current options to <file>.                                                                                                                                      |
| :save-view <file>      | Save view info to <file> (for testing purposes).                                                                                                                     |
| :script <file>         | Execute commands from <file>.                                                                                                                                        |
| :exec <flags><args...> | Execute command using <args> with external user-defined command option flags defined in <flags>.                                                                     |
| :echo <args...>        | Display text in the status bar.                                                                                                                                      |

** External Commands
For more custom needs, external commands provide a way to easily execute
a script or program. They are bound to keys and use information from the
current browsing state, such as the current commit ID. Tig comes with
the following built-in external commands:

| Keymap  | Key | Action                    |
| main    | C   | git cherry-pick %(commit) |
| status  | C   | git commit                |
| generic | G   | git gc                    |

* REVISION SPECIFICATION
This section describes various ways to specify what revisions to display
or otherwise limit the view to. Tig does not itself parse the described
revision options so refer to the relevant Git man pages for further
information. Relevant man pages besides git-log(1) are git-diff(1) and
git-rev-list(1).

You can tune the interaction with Git by making use of the options
explained in this section. For example, by configuring the environment
variable described in the section on diff options.

** Limit by Path Name
If you are interested only in those revisions that made changes to a
specific file (or even several files) list the files like this:

#+begin_quote
  #+begin_example
    $ tig Makefile README
  #+end_example
#+end_quote

To avoid ambiguity with Tig's subcommands or repository references such
as tag names, be sure to separate file names from other Git options
using "--". So if you have a file named /status/ it will clash with the
/status/ subcommand, and thus you will have to use:

#+begin_quote
  #+begin_example
    $ tig -- status
  #+end_example
#+end_quote

** Limit by Date or Number
To speed up interaction with Git, you can limit the amount of commits to
show both for the log and main view. Either limit by date using e.g.
--since=1.month or limit by the number of commits using -n400.

If you are only interested in changes that happened between two dates
you can use:

#+begin_quote
  #+begin_example
    $ tig --after="May 5th" --before="2006-05-16 15:44"
  #+end_example
#+end_quote

#+begin_quote
  \\

  *Note*

  \\

  If you want to avoid having to quote dates containing spaces you can
  use "." instead, e.g. --after=May.5th.
#+end_quote

** Limiting by Commit Ranges
Alternatively, commits can be limited to a specific range, such as "all
commits between /tag-1.0/ and /tag-2.0/". For example:

#+begin_quote
  #+begin_example
    $ tig tag-1.0..tag-2.0
  #+end_example
#+end_quote

This way of commit limiting makes it trivial to only browse the commits
which haven't been pushed to a remote branch. Assuming /origin/ is your
upstream remote branch, using:

#+begin_quote
  #+begin_example
    $ tig origin..HEAD
  #+end_example
#+end_quote

will list what will be pushed to the remote branch. Optionally, the
ending /HEAD/ can be left out since it is implied.

** Limiting by Reachability
Git interprets the range specifier "tag-1.0..tag-2.0" as "all commits
reachable from /tag-2.0/ but not from /tag-1.0/". Where reachability
refers to what commits are ancestors (or part of the history) of the
branch or tagged revision in question.

If you prefer to specify which commit to preview in this way use the
following:

#+begin_quote
  #+begin_example
    $ tig tag-2.0 ^tag-1.0
  #+end_example
#+end_quote

You can think of /^/ as a negation operator. Using this alternate
syntax, it is possible to further prune commits by specifying multiple
branch cut offs.

** Combining Revisions Specification
Revision options can to some degree be combined, which makes it possible
to say "show at most 20 commits from within the last month that changed
files under the Documentation/ directory."

#+begin_quote
  #+begin_example
    $ tig --since=1.month -n20 -- Documentation/
  #+end_example
#+end_quote

** Examining All Repository References
In some cases, it can be useful to query changes across all references
in a repository. An example is to ask "did any line of development in
this repository change a particular file within the last week". This can
be accomplished using:

#+begin_quote
  #+begin_example
    $ tig --all --since=1.week -- Makefile
  #+end_example
#+end_quote

* MORE INFORMATION
Please visit Tig's *home page*[1] or *main Git repository*[2] for
information about new releases and how to report bugs and feature
requests.

* COPYRIGHT
Copyright (c) 2006-2014 Jonas Fonseca <*jonas.fonseca@gmail.com*[3]>

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

* SEE ALSO
Manpages:

#+begin_quote
  ·

  *tig*(1)
#+end_quote

#+begin_quote
  ·

  *tigrc*(5)
#+end_quote

* NOTES
-  1. :: home page

  https://jonas.github.io/tig

-  2. :: main Git repository

  https://github.com/jonas/tig

-  3. :: jonas.fonseca@gmail.com

  mailto:jonas.fonseca@gmail.com
