#+TITLE: Manpages - offlineimapui.7
#+DESCRIPTION: Linux manpage for offlineimapui.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
offlineimapui - The User Interfaces

* DESCRIPTION
OfflineIMAP comes with different UIs, each aiming its own purpose.

* TTYUI
TTYUI interface is for people running in terminals. It prints out basic
status messages and is generally friendly to use on a console or xterm.

* BASIC
Basic is designed for situations in which OfflineIMAP will be run
non-attended and the status of its execution will be logged.

This user interface is not capable of reading a password from the
keyboard; account passwords must be specified using one of the
configuration file options. For example, it will not print periodic
sleep announcements and tends to be a tad less verbose, in general.

* BLINKENLIGHTS
Blinkenlights is an interface designed to be sleek, fun to watch, and
informative of the overall picture of what OfflineIMAP is doing.

Blinkenlights contains a row of "LEDs" with command buttons and a log.
The log shows more detail about what is happening and is color-coded to
match the color of the lights.

Each light in the Blinkenlights interface represents a thread of
execution --- that is, a particular task that OfflineIMAP is performing
right now. The colors indicate what task the particular thread is
performing, and are as follows:

#+begin_quote
  ·

  Black

  #+begin_quote
    #+begin_example
      indicates that this lights thread has terminated; it will light up again
      later when new threads start up.  So, black indicates no activity.
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Red (Meaning 1)

  #+begin_quote
    #+begin_example
      is the color of the main programs thread, which basically does nothing but
      monitor the others.  It might remind you of HAL 9000 in 2001.
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Gray

  #+begin_quote
    #+begin_example
      indicates that the thread is establishing a new connection to the IMAP
      server.
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Purple

  #+begin_quote
    #+begin_example
      is the color of an account synchronization thread that is monitoring the
      progress of the folders in that account (not generating any I/O).
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Cyan

  #+begin_quote
    #+begin_example
      indicates that the thread is syncing a folder.
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Green

  #+begin_quote
    #+begin_example
      means that a folders message list is being loaded.
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Blue

  #+begin_quote
    #+begin_example
      is the color of a message synchronization controller thread.
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Orange

  #+begin_quote
    #+begin_example
      indicates that an actual message is being copied.  (We use fuchsia for fake
      messages.)
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Red (meaning 2)

  #+begin_quote
    #+begin_example
      indicates that a message is being deleted.
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Yellow / bright orange

  #+begin_quote
    #+begin_example
      indicates that message flags are being added.
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Pink / bright red

  #+begin_quote
    #+begin_example
      indicates that message flags are being removed.
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Red / Black Flashing

  #+begin_quote
    #+begin_example
      corresponds to the countdown timer that runs between synchronizations.
    #+end_example
  #+end_quote
#+end_quote

The name of this interfaces derives from a bit of computer history. Eric
Raymond's Jargon File defines blinkenlights, in part, as:

#+begin_quote
  #+begin_example
    Front-panel diagnostic lights on a computer, esp. a dinosaur. Now that
    dinosaurs are rare, this term usually refers to status lights on a modem,
    network hub, or the like.
  #+end_example
#+end_quote

This term derives from the last word of the famous blackletter-Gothic
sign in mangled pseudo-German that once graced about half the computer
rooms in the English-speaking world. One version ran in its entirety as
follows:

#+begin_quote
  #+begin_example
    ACHTUNG!  ALLES LOOKENSPEEPERS!
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    Das computermachine ist nicht fuer gefingerpoken und mittengrabben.
    Ist easy schnappen der springenwerk, blowenfusen und poppencorken
    mit spitzensparken.  Ist nicht fuer gewerken bei das dumpkopfen.
    Das rubbernecken sichtseeren keepen das cotten-pickenen hans in das
    pockets muss; relaxen und watchen das blinkenlichten.
  #+end_example
#+end_quote

* QUIET
It will output nothing except errors and serious warnings. Like Basic,
this user interface is not capable of reading a password from the
keyboard; account passwords must be specified using one of the
configuration file options.

* SYSLOG
Syslog is designed for situations where OfflineIMAP is run as a daemon
(e.g., as a systemd --user service), but errors should be forwarded to
the system log. Like Basic, this user interface is not capable of
reading a password from the keyboard; account passwords must be
specified using one of the configuration file options.

* MACHINEUI
MachineUI generates output in a machine-parsable format. It is designed
for other programs that will interface to OfflineIMAP.

* SEE ALSO

#+begin_quote
  #+begin_example
    offlineimap(1)
  #+end_example
#+end_quote
