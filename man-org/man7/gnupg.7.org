#+TITLE: Manpages - gnupg.7
#+DESCRIPTION: Linux manpage for gnupg.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*GnuPG* - The GNU Privacy Guard suite of programs

* DESCRIPTION
GnuPG is a set of programs for public key encryption and digital
signatures. The program most users will want to use is the OpenPGP
command line tool, named *gpg2*. *gpgv*is a stripped down version of
*gpg2* with no encryption functionality, used only to verify signatures
against a trusted keyring. *gpgsm* is the X.509/CMS (for S/MIME)
counterpart of *gpg2*. *gpg-agent* is a passphrase and private key
daemon which may also emulate the *ssh-agent*.

* SEE ALSO
*gpg*(1), *gpg2*(1), *gpgv*(1), *gpgsm*(1), *gpg-agent*(1),
*dirmngr*(8), *scdaemon*(1)

The full documentation for this tool is maintained as a Texinfo manual.
If GnuPG and the info program are properly installed at your site, the
command

#+begin_quote
  #+begin_example
    info gnupg
  #+end_example
#+end_quote

should give you access to the complete manual including a menu structure
and an index.
