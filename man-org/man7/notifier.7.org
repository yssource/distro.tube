#+TITLE: Manpages - notifier.7
#+DESCRIPTION: Linux manpage for notifier.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
notifier - cups notification interface

* SYNOPSIS
*notifier* /recipient/ [ /user-data/ ]

* DESCRIPTION
The CUPS notifier interface provides a standard method for adding
support for new event notification methods to CUPS. Each notifier
delivers one or more IPP events from the standard input to the specified
recipient.

Notifiers *MUST* read IPP messages from the standard input using the
*ippNew*() and *ippReadFile*() functions and exit on error. Notifiers
are encouraged to exit after a suitable period of inactivity, however
they may exit after reading the first message or stay running until an
error is seen. Notifiers inherit the environment and can use the logging
mechanism documented in *filter*(7).

* SEE ALSO
*cupsd*(8), *filter*(7), CUPS Online Help (http://localhost:631/help)

* COPYRIGHT
Copyright © 2021 by OpenPrinting.
