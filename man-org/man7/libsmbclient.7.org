#+TITLE: Manpages - libsmbclient.7
#+DESCRIPTION: Linux manpage for libsmbclient.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libsmbclient - An extension library for browsers and that can be used as
a generic browsing API.

* SYNOPSIS
Browser URL:
smb://[[[domain:]user[:password@]]server[/share[/path[/file]]]]
[?options]

* DESCRIPTION
This tool is part of the *samba*(7) suite.

libsmbclient is a library toolset that permits applications to
manipulate CIFS/SMB network resources using many of the standards POSIX
functions available for manipulating local UNIX/Linux files. It permits
much more than just browsing, files can be opened and read or written,
permissions changed, file times modified, attributes and ACLs can be
manipulated, and so on. Of course, its functionality includes all the
capabilities commonly called browsing.

libsmbclient can not be used directly from the command line, instead it
provides an extension of the capabilities of tools such as file managers
and browsers. This man page describes the configuration options for this
tool so that the user may obtain greatest utility of use.

* OPTIONS
What the URLs mean:

smb://

#+begin_quote
  Shows all workgroups or domains that are visible in the network. The
  behavior matches that of the Microsoft Windows Explorer.

  The method of locating the list of workgroups (domains also) varies
  depending on the setting of the context variable
  (context->options.browse_max_lmb_count). It is the responsibility of
  the application that calls this library to set this to a sensible
  value. This is a compile-time option. This value determines the
  maximum number of local master browsers to query for the list of
  workgroups. In order to ensure that the list is complete for those
  present on the network, all master browsers must be queried. If there
  are a large number of workgroups on the network, the time spent
  querying will be significant. For small networks (just a few
  workgroups), it is suggested to set this value to 0, instructing
  libsmbclient to query all local master browsers. In an environment
  that has many workgroups a more reasonable setting may be around 3.
#+end_quote

smb://name/

#+begin_quote
  This command causes libsmbclient to perform a name look-up. If the
  NAME<1D> or NAME<1B> exists (workgroup name), libsmbclient will list
  all servers in the workgroup (or domain). Otherwise, a name look-up
  for the NAME<20> (machine name) will be performed, and the list of
  shared resources on the server will be displayed.
#+end_quote

When libsmbclient is invoked by an application it searches for a
directory called .smb in the $HOME directory that is specified in the
users shell environment. It then searches for a file called smb.conf
which, if present, will fully over-ride the system /etc/samba/smb.conf
file. If instead libsmbclient finds a file called
~/.smb/smb.conf.append, it will read the system /etc/samba/smb.conf and
then append the contents of the ~/.smb/smb.conf.append to it.

libsmbclient will check the users shell environment for the USER
parameter and will use its value when if the user parameter was not
included in the URL.

* PROGRAMMERS GUIDE
Watch this space for future updates.

* VERSION
This man page is part of version 4.15.3 of the Samba suite.

* AUTHOR
The original Samba software and related utilities were created by Andrew
Tridgell. Samba is now developed by the Samba Team as an Open Source
project similar to the way the Linux kernel is developed.

The libsmbclient manpage page was written by John H Terpstra.
