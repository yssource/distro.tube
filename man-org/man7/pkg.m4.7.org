#+TITLE: Manpages - pkg.m4.7
#+DESCRIPTION: Linux manpage for pkg.m4.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is a collection of autoconf macros which help to configure compiler and
linker flags for development libraries. This allows build systems to
detect other dependencies and use them with the system toolchain.

Checks that the version of the

autoconf macros in use is at least MIN-VERSION. This can be used to
ensure a particular

macro will be available.

Checks for an implementation of

which is at least MIN-VERSION or newer.

Checks whether a given module set exists, and if so, defines

and

variables prefixed by

with the output from

and

respectively.

The optional

and

arguments are shell fragments that should be executed if the module set
is found or not found.

If

is not defined, the

macro will be executed to locate a

implementation.

The

macro provides the same behaviour as

with static linking enabled via the

flag.

Defines the variable $pkgconfigdir as the location where a package
should install pkg-config .pc files.

By default the directory is $libdir/pkgconfig, but the default can be
changed by passing the

parameter.

This value can be overridden with the

configure parameter.

Defines the variable $noarch_pkgconfigdir as the location where a
package should install pkg-config .pc files.

By default the directory is $datadir/pkgconfig, but the default can be
changed by passing the

parameter.

This value can be overridden with the

configure parameter.

Retrieves the value of the

variable

from

and stores it in the

variable.

Note that repeated usage of

is not recommended as the check will be skipped if the variable is
already set.

Prepares a "--with-" configure option using the lowercase

name, merging the behaviour of

and

in a single macro.

Convenience macro to trigger

after a

is exported as a make variable.

Convenience macro to trigger

and

after a

is exported as a make variable.
