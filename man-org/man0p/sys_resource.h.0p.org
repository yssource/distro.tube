#+TITLE: Manpages - sys_resource.h.0p
#+DESCRIPTION: Linux manpage for sys_resource.h.0p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
sys/resource.h --- definitions for XSI resource operations

* SYNOPSIS
#+begin_example
  #include <sys/resource.h>
#+end_example

* DESCRIPTION
The /<sys/resource.h>/ header shall define the following symbolic
constants as possible values of the /which/ argument of /getpriority/()
and /setpriority/():

- PRIO_PROCESS :: Identifies the /who/ argument as a process ID.

- PRIO_PGRP :: Identifies the /who/ argument as a process group ID.

- PRIO_USER :: Identifies the /who/ argument as a user ID.

The /<sys/resource.h>/ header shall define the following type through
*typedef*:

- rlim_t :: Unsigned integer type used for limit values.

The /<sys/resource.h>/ header shall define the following symbolic
constants, which shall have values suitable for use in *#if*
preprocessing directives:

- RLIM_INFINITY :: A value of *rlim_t* indicating no limit.

- RLIM_SAVED_MAX :: A value of type *rlim_t* indicating an
  unrepresentable saved hard limit.

- RLIM_SAVED_CUR :: A value of type *rlim_t* indicating an
  unrepresentable saved soft limit.

On implementations where all resource limits are representable in an
object of type *rlim_t*, RLIM_SAVED_MAX and RLIM_SAVED_CUR need not be
distinct from RLIM_INFINITY.

The /<sys/resource.h>/ header shall define the following symbolic
constants as possible values of the /who/ parameter of /getrusage/():

- RUSAGE_SELF :: Returns information about the current process.

- RUSAGE_CHILDREN :: Returns information about children of the current
  process.

The /<sys/resource.h>/ header shall define the *rlimit* structure, which
shall include at least the following members:

#+begin_quote
  #+begin_example

    rlim_t rlim_cur  The current (soft) limit.
    rlim_t rlim_max  The hard limit.
  #+end_example
#+end_quote

The /<sys/resource.h>/ header shall define the *rusage* structure, which
shall include at least the following members:

#+begin_quote
  #+begin_example

    struct timeval ru_utime  User time used.
    struct timeval ru_stime  System time used.
  #+end_example
#+end_quote

The /<sys/resource.h>/ header shall define the *timeval* structure as
described in /<sys/time.h>/.

The /<sys/resource.h>/ header shall define the following symbolic
constants as possible values for the /resource/ argument of
/getrlimit/() and /setrlimit/():

- RLIMIT_CORE :: Limit on size of *core* file.

- RLIMIT_CPU :: Limit on CPU time per process.

- RLIMIT_DATA :: Limit on data segment size.

- RLIMIT_FSIZE :: Limit on file size.

- RLIMIT_NOFILE :: Limit on number of open files.

- RLIMIT_STACK :: Limit on stack size.

- RLIMIT_AS :: Limit on address space size.

The following shall be declared as functions and may also be defined as
macros. Function prototypes shall be provided.

#+begin_quote
  #+begin_example

    int  getpriority(int, id_t);
    int  getrlimit(int, struct rlimit *);
    int  getrusage(int, struct rusage *);
    int  setpriority(int, id_t, int);
    int  setrlimit(int, const struct rlimit *);
  #+end_example
#+end_quote

The /<sys/resource.h>/ header shall define the *id_t* type through
*typedef*, as described in /<sys/types.h>/.

Inclusion of the /<sys/resource.h>/ header may also make visible all
symbols from /<sys/time.h>/.

/The following sections are informative./

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
/*<sys_time.h>*/, /*<sys_types.h>*/

The System Interfaces volume of POSIX.1‐2017, //getpriority/ ( )/,
//getrlimit/ ( )/, //getrusage/ ( )/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
