#+TITLE: Manpages - lremovexattr.2
#+DESCRIPTION: Linux manpage for lremovexattr.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about lremovexattr.2 is found in manpage for: [[../man2/removexattr.2][man2/removexattr.2]]