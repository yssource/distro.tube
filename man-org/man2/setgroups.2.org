#+TITLE: Manpages - setgroups.2
#+DESCRIPTION: Linux manpage for setgroups.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setgroups.2 is found in manpage for: [[../man2/getgroups.2][man2/getgroups.2]]