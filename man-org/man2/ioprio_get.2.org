#+TITLE: Manpages - ioprio_get.2
#+DESCRIPTION: Linux manpage for ioprio_get.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ioprio_get.2 is found in manpage for: [[../man2/ioprio_set.2][man2/ioprio_set.2]]