#+TITLE: Manpages - preadv2.2
#+DESCRIPTION: Linux manpage for preadv2.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about preadv2.2 is found in manpage for: [[../man2/readv.2][man2/readv.2]]