#+TITLE: Manpages - outw.2
#+DESCRIPTION: Linux manpage for outw.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about outw.2 is found in manpage for: [[../man2/outb.2][man2/outb.2]]