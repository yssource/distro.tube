#+TITLE: Manpages - pwrite64.2
#+DESCRIPTION: Linux manpage for pwrite64.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about pwrite64.2 is found in manpage for: [[../man2/pwrite.2][man2/pwrite.2]]