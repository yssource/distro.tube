#+TITLE: Manpages - signalfd4.2
#+DESCRIPTION: Linux manpage for signalfd4.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about signalfd4.2 is found in manpage for: [[../man2/signalfd.2][man2/signalfd.2]]