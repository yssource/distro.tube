#+TITLE: Manpages - sched_getparam.2
#+DESCRIPTION: Linux manpage for sched_getparam.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sched_getparam.2 is found in manpage for: [[../man2/sched_setparam.2][man2/sched_setparam.2]]