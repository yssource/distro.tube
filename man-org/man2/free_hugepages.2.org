#+TITLE: Manpages - free_hugepages.2
#+DESCRIPTION: Linux manpage for free_hugepages.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about free_hugepages.2 is found in manpage for: [[../man2/alloc_hugepages.2][man2/alloc_hugepages.2]]