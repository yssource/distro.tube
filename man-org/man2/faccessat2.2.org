#+TITLE: Manpages - faccessat2.2
#+DESCRIPTION: Linux manpage for faccessat2.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about faccessat2.2 is found in manpage for: [[../man2/access.2][man2/access.2]]