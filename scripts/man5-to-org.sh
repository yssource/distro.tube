#!/usr/bin/env bash

### man5 ###
# An array of all files in the 'mandb' directory
readarray -t manfiles < <(find ../mandb/man5 -iname "*.*")

# For each file, convert to from 'man' to 'org'.
for file in "${manfiles[@]}"; do
   name=$(echo "$file" | awk -F / '{print $NF}')
   pandoc -o ../man-org/man5/"$name".org -f man -t org "$file" && \
      echo "$file.org created." || echo "Error converting $file."
done

cd ../man-org/man5/ || exit
yes | rm ./*.gz
